################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2018, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

import re
import os
import glob
import datetime

import json
import pandas
import geojson
import geopandas as gpd

import csutils.geopandas
import csutils.subprocess

import polygonSelection

SENTINEL2_INDEX_FP = os.environ.get("SENTINEL2_INDEX_FP", 'gs://gcp-public-data-sentinel-2/index.csv.gz')

SENTINEL2_DB_PATH  = os.environ.get("SENTINEL2_DB_PATH", "/mnt/disks/sentinel-db/SENTINEL2_DB")
CS_GRANULES_FP     = os.path.join(SENTINEL2_DB_PATH, 'CS_Granules.geojson')
TEMPLATE_DIR_PATH  = os.path.join(SENTINEL2_DB_PATH, 'GRANULE_TEMPLATES')

SCL_BASE_URL       = os.environ.get("SCL_BASE_URL", "/data/S2/cs-sentinel2-scl")
SCL_INDEX_FP       = os.environ.get("SCL_INDEX_FP", os.path.join(SENTINEL2_DB_PATH, 'scl_index.csv'))

def _getGranuleIndex(pn):
    global SENTINEL2_INDEX_FP
    cmd = [ "gsutil", 
            "cp %s %s" % (SENTINEL2_INDEX_FP, pn)
          ]
    csutils.subprocess.callcmd(cmd)
    return pn

def _statusSCL(granule_id, dt):
    dts = dt.strftime("%Y%m%dT%H%M%S")
    fn  = 'L2A_T%s_%s_SCL_10m.tif' % (granule_id, dts)
    url = os.path.join(SCL_BASE_URL, granule_id, fn)
    cmd = [ "du %s" % (url)
          ]
    try:
        r = csutils.subprocess.callcmd(cmd)
        r.split(' ')
        return int(r[0])>0

    except:
        None

    return None

def getGranuleTemplateFilePath(granule_id):
    global TEMPLATE_DIR_PATH
    ext = ".vrt"
    if granule_id == "TILE": ext = ".tif"
    return os.path.join(TEMPLATE_DIR_PATH, 'T' + granule_id + ext)

def getGranuleImageFilePath(granule_location, band):
    fp = os.path.join(granule_location, 'GRANULE/*L1C*/IMG_DATA/*%s.jp2' % band)
    fpl = glob.glob(fp)
    if len(fpl) == 0: return None
    return fpl[0]

def getGranule(gs, pn):
    cmd = [ "gsutil", 
            "-m", 
            "cp -r %s %s" % (gs.BASE_URL, pn)
          ]
    os.makedirs(pn)
    csutils.subprocess.callcmd(cmd)
    return os.path.join(pn, os.path.basename(gs.BASE_URL))

def getSCLIndex():
    global SCL_INDEX_FP
    # Have to tell 0-th column is index
    df = pandas.read_csv(SCL_INDEX_FP, index_col=0, parse_dates=['SENSING_TIME'])
    return df
    
def getSCLStatus(gid, dt, df=None):
    if df is not None: 
        r = df[ (df.MGRS_TILE == gid) & (df.SENSING_TIME == dt) ]
        if len(r.index)==0: return None
        return r.iloc[0].PASSED

    return _statusSCL(gid, dt)

def getSCLDates(gid):
   df = getSCLIndex()
   df = df[ df.MGRS_TILE == gid ]
   return zip(df.SENSING_TIME.tolist(), df.PASSED.tolist())

def addSCLIndex(gs, passed=True):
    global SCL_INDEX_FP
    df = getSCLIndex()
    ni = { 'BASE_URL'     : gs.BASE_URL,
           'SENSING_TIME' : gs.SENSING_TIME,
           'MGRS_TILE'    : gs.MGRS_TILE,
           'PASSED'       : passed,
         }
    r = df[ (df.MGRS_TILE == gs.MGRS_TILE) & (df.SENSING_TIME == gs.SENSING_TIME) ]
    if len(r.index)==0: df = df.append(ni, ignore_index=True)
    r = df[ (df.MGRS_TILE == gs.MGRS_TILE) & (df.SENSING_TIME == gs.SENSING_TIME) ]
    df.loc[r.index[0], 'PASSED'] = passed

    df.to_csv(SCL_INDEX_FP)

def generateSCLIndex():
    global SCL_BASE_URL
    global SCL_INDEX_FP
    print "generateSCLIndex : get granule index"
    gdf = getGranuleIndex()
    print "generateSCLIndex : get SCL files"
    cmd = [ "du %s/**.tif" % (SCL_BASE_URL) ]
    pn_list = csutils.subprocess.callcmd(cmd)
    pn_list = pn_list.splitlines()
    df = pandas.DataFrame( columns=['BASE_URL', 'SENSING_TIME', 'MGRS_TILE', 'PASSED'] )
    for pn in pn_list:
        # Format <tile>/L2a_T<tile>_<sensing_time>
        m = re.match('([0-9]+).*L2A_T([^_]*)_([^_]*).*', pn)
        size = int(m.group(1))
        tid  = m.group(2)
        dts  = m.group(3)
        dt  = datetime.datetime.strptime(dts, "%Y%m%dT%H%M%S")
        gsl = gdf[ (gdf.MGRS_TILE == tid) & (gdf.SENSING_TIME == dt) ]
        if len(gsl.index) == 0:
            print "WARNINING generateSCLIndex : index for %s %s not found" % (tid, dt)
            continue
        if len(gsl.index) > 1:
            print "WARNINING generateSCLIndex : non unique index for %s %s" % (tid, dt)
        gs  = gsl.iloc[-1]
        ni = { 'BASE_URL'     : gs.BASE_URL,
               'SENSING_TIME' : gs.SENSING_TIME,
               'MGRS_TILE'    : gs.MGRS_TILE,
               'PASSED'       : size>0,
             }
        df = df.append(ni, ignore_index=True)
   
    df.to_csv(SCL_INDEX_FP)
    n = len(df.index)
    print "generateSCLIndex : written %d entries to %s" % (n, SCL_INDEX_FP)
    return df

def getSCLGranule(gid, d, dp):
    gdf = getSCLIndex()
    gs  = gdf[ (gdf.MGRS_TILE == gid) & (gdf.SENSING_TIME == d)]
    if len(gs.index) == 0:
        raise RuntimeError("ERROR : granule %s %s not found" % (gid, d))

    url = gs.iloc[0].BASE_URL
    cmd = [ "gsutil", 
            "-m", 
            "cp -r %s %s" % (url, dp)
          ]
    os.makedirs(dp)
    csutils.subprocess.callcmd(cmd)
    return os.path.join(dp, os.path.basename(url))


def getSCL(granule_id, dt, pn):
    global SCL_BASE_URL
    fn =''
    if dt is not None:
        dts = dt.strftime("%Y%m%dT%H%M%S")
        fn  = 'L2A_T%s_%s_SCL_10m.tif' % (granule_id, dts)
    url = os.path.join(SCL_BASE_URL, granule_id, fn)
    cmd = [ "cp -r %s %s" % (url, pn)
          ]
    csutils.subprocess.callcmd(cmd)

def putSCL(gs, pn):
    gid = gs.MGRS_TILE
    dts = gs.SENSING_TIME.strftime("%Y%m%dT%H%M%S")
    fn  = 'L2A_T%s_%s_SCL_10m.tif' % (gid, dts)
    url = os.path.join(SCL_BASE_URL, gid, fn)
    cmd = [ "cp -r %s %s" % (pn, url)
          ]
    csutils.subprocess.callcmd(cmd)
    passed = os.path.exists(pn) and os.path.getsize(pn)>0
    addSCLIndex(gs, passed)

def getGranuleSensingDateTime(sensing_time):
    return datetime.datetime.strptime(sensing_time[0:19], "%Y-%m-%dT%H:%M:%S")


def getContainingGranuleId(features_df):

    granules_df = gpd.read_file(CS_GRANULES_FP)

    # get all granules containing the field
    granules = csutils.geopandas.contains(features_df, granules_df)
    if len(granules.index) == 0 : return None

    # Pick the last alphabetical sorted granule name
    granules_id_list = sorted(granules.Name.tolist())
    return str(granules_id_list[-1])

def getGranuleIndex():
    tmp_bucket_index_fp = csutils.os.makeTempFileName("bucket-index", ".csv.gz")
    _getGranuleIndex(tmp_bucket_index_fp)
    gidx_df = pandas.read_csv(tmp_bucket_index_fp, parse_dates=['SENSING_TIME'])
    os.remove(tmp_bucket_index_fp)
    granule_id_list = getGranuleIdList()
    gidx_df = gidx_df[ gidx_df['MGRS_TILE'].isin(granule_id_list) ]
    gidx_df['SENSING_TIME'] = gidx_df['SENSING_TIME'].apply(lambda x: x.replace(microsecond=0))
    return gidx_df

def getGranuleIdList():
    al = getAcquisitionList()
    return [ x['properties']['Name'] for x in al['features'] ]

def getAcquisitionList():
    return geojson.load(open(CS_GRANULES_FP))

def copyAcquisitionList():
    dts = str(format(datetime.datetime.now(), "%Y%m%dT%H%M%S"))
    shutil.copy(CS_GRANULES_FP, CS_GRANULES_FP + '.%s' % dts)

def writeAcquisitionList(acquisition_list):
    json.dump(acquisition_list, open(CS_GRANULES_FP, 'w'), indent=4, sort_keys=True)
