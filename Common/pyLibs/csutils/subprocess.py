#!/usr/bin/env python
#
# Module with utilities
#
################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2018, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################


from __future__ import absolute_import

import subprocess

def callcmd(cmd):
   scmd = ' '.join(cmd)

   try:
      result = subprocess.check_output(scmd, stderr=subprocess.STDOUT, shell=True)
      return result

   except subprocess.CalledProcessError, e:
      raise RuntimeError("ERROR : cmd failed : %s : %s" % (scmd, e.output))
