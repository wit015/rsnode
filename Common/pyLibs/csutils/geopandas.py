################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2018, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

from __future__ import absolute_import

import geopandas as gpd

# Find all shapes containing another feature shapes
#
# feature_df : GeoDataFrame containing feature shapes
# shapes_df  : GeoDataFrame containing shapes
#
def contains(feature_df, shapes_df):
    # Have feature in same CRS as shapes
    n_feature_df = feature_df.to_crs(shapes_df.crs)
    # Test whether all feature shapes fall in each shape
    r = shapes_df.geometry.apply(lambda x : n_feature_df.geometry.apply(lambda y : x.contains(y)))
    # Select shapes with all elements contained
    r = r.all(1)
    return shapes_df[r]
