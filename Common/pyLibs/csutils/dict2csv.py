################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2018, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

import StringIO
import csv

def dictWalk(d, i=0, prefix=''):
    l = {}
    empty=True
    for k, v in d.iteritems():
        if isinstance(v, dict):
            ll, le = dictWalk(v, i, prefix+k+"-")
            empty = empty and le
            l.update(ll)
        elif type(v) in (list,tuple):
            if i<len(v): empty=False; l[prefix+k]=v[i]
        elif i == 0:
            empty=False
            l[prefix+k] = v
    return l, empty

def dictIterate(d):
    i = 0
    empty=False
    while not empty:
        l, empty = dictWalk(d, i)
        if not empty: yield l
        i = i + 1

def dictHeaders(d, prefix=""):
    l=[]
    for k, v in d.iteritems():
        if isinstance(v, dict):
            l.extend(dictHeaders(v, prefix+k+"-"))
        else:
	    l.append(prefix + k)
    return l

def dict2csv(d):
    b=StringIO.StringIO()
    w=csv.DictWriter(b, dictHeaders(d))
    w.writeheader()
    for r in dictIterate(d): w.writerow(r)
    return b.getvalue()
