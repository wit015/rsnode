################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2018, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

from __future__ import absolute_import

from osgeo import ogr
from osgeo import osr

import geojson

def reproject(geometry, s_epsg, t_epsg):

    source = osr.SpatialReference()
    source.ImportFromEPSG(s_epsg)

    target = osr.SpatialReference()
    target.ImportFromEPSG(t_epsg)

    transform = osr.CoordinateTransformation(source, target)

    g = ogr.CreateGeometryFromJson(geojson.dumps(geometry))
    g.Transform(transform)

    return geojson.loads(g.ExportToJson())

def EPSGCrs(epsgcode):
    return {"type": "name", "properties": {"name": "urn:ogc:def:crs:EPSG::%s" % epsgcode}}

def EPSGCode(feature_collection):
    if feature_collection['type'] <> 'FeatureCollection':
        raise RuntimeError('ERROR : unexpected type %s' % feature_collection['type'])

    if "crs" not in feature_collection.keys():
        raise RuntimeError("ERROR : no CRS in feature collection")

    crs  = feature_collection['crs']['properties']['name']
    crsl = crs.split(':')

    if 'OGC' in crsl and crsl[-1] == 'CRS84':
        return 4326

    if 'EPSG' not in crsl:
        raise RuntimeError("ERROR : unexpected crs authority %s" % crsl[-3])

    return int(crsl[-1])
