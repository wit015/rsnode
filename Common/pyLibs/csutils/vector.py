################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2018, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

from osgeo import ogr

def getEPSGCode(fn):
    f = ogr.Open(fn)
    r = f.GetLayer().GetSpatialRef()
    authority_code = r.GetAuthorityCode(None)
    authority_name = r.GetAuthorityName(None)
    if not authority_name == 'EPSG':
        raise RuntimeError('Unexpected authority %s' % (authority_name))

    return int(authority_code)
