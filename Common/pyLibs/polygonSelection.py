#!/usr/bin/env python
#
# Module to select polygon from raster.
#
################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2018, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

import os
import os.path
import sys
import argparse
import shutil

import csutils
import csutils.os
import csutils.subprocess
import csutils.raster
import csutils.vector

from osgeo import gdal
from osgeo import ogr
from osgeo import osr

def clipPolygonFromRaster(raster_fn, polygon_fn, output_fn, dstnodata=None, dstalpha=False, fill=None, t_srs=None, outsize=None, bbox=None, oformat=None):

    #######
    # Reproject polygon to raster SRS
    #######
 
    epsg = csutils.raster.getEPSGCode(raster_fn)

    # Get cell width and height for output alignment
    rf = gdal.Open(raster_fn)
    oX, cW, _, oY, _, cH  = rf.GetGeoTransform()

    polygon_epsg = csutils.vector.getEPSGCode(polygon_fn)

    tmp_polygon_fn = csutils.os.makeTempFileName("reproject_polygon", ".geojson")
    cmd = [ 'ogr2ogr',
            '-f', 'geojson',
            '-t_srs',  'EPSG:%d' % epsg,
            tmp_polygon_fn, polygon_fn ]
    csutils.subprocess.callcmd(cmd)

    #######
    # Cut out polygon
    #######

    tmp_cropped_fn = csutils.os.makeTempFileName("cropped_raster", ".tif")

    # -tap -tr    			enforce output is aligned to pixel size in source raster
    # -wo CUTLINE_ALL_TOUCHED=YES	select all pixels touched by polygon for predictability
    # -crop_to_cutline -cutline		have output as small as possible
    cmd = [ "gdalwarp",
            "-tap",
            "-tr", str(cW), str(abs(cH)),
            "-wo", "CUTLINE_ALL_TOUCHED=YES", "-crop_to_cutline",
            "-cutline", tmp_polygon_fn, 
            raster_fn, tmp_cropped_fn
          ]
    if dstalpha  : cmd = cmd + ["-dstalpha"]
    if dstnodata is not None : cmd = cmd + [ "-dstnodata", "'" + str(dstnodata) + "'"]

    csutils.subprocess.callcmd(cmd)

    ######
    # Fill polygon if needed
    ######

    if fill is not None:
        cmd = ["gdal_rasterize",
               "-at",
               tmp_polygon_fn, tmp_cropped_fn,
              ]
        for i in range(len(fill)):
            cmd = cmd + ["-b", str(i+1) ]
        for i in range(len(fill)):
            cmd = cmd + ["-burn", str(fill[i])]

        csutils.subprocess.callcmd(cmd)

    ######
    # resample and reproject
    ######

    tmp_gdalwarp_fn = csutils.os.makeTempFileName("gdalwarp", ".tif")
    cmd = [ "gdalwarp", tmp_cropped_fn, tmp_gdalwarp_fn ]

    if t_srs is not None:
        cmd = cmd + ["-t_srs", t_srs]
    else:
        cmd = cmd + ["-t_srs", "EPSG:%0d" % polygon_epsg]

    if outsize is not None:
        xsize, ysize = outsize
        cmd = cmd + ["-ts", str(xsize), str(ysize)]

    if bbox is not None:
        te = "%f %f %f %f" % (bbox[0], bbox[1], bbox[2], bbox[3])
        cmd = cmd + ["-te", te]

    csutils.subprocess.callcmd(cmd)

    ###
    # Translate to proper format
    ###

    formats = { ".jp2"  : "JPEG2000",
                ".tif"  : "GTiff",
                ".tiff" : "GTiff",
                ".png"  : "PNG",
              }
    ext = os.path.splitext(output_fn)[1].lower()
    if oformat is None or oformat=="auto":
        if ext in formats:
            oformat = formats[ext]
        else:
            raise RuntimeError("ERROR : file extension %s not recognized" % ext)

    cmd = ["gdal_translate",
           "-of", oformat,
            tmp_gdalwarp_fn, output_fn,
          ]

    csutils.subprocess.callcmd(cmd)

    # Clean up temp files
    os.remove(tmp_polygon_fn)
    os.remove(tmp_cropped_fn)
    os.remove(tmp_gdalwarp_fn)


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("raster_fn",   help="raster filename")
    parser.add_argument("polygon_fn",  help="polygon filename")
    parser.add_argument("output_fn",   help="output filename")
    parser.add_argument("--bbox",      help="bounding box", nargs="+", type=float)
    parser.add_argument("--fill",      help="fill fill", nargs="+", type=int)
    parser.add_argument("--oformat",   help="output format", default="auto", type=str)
    parser.add_argument("--dstalpha",  help="create alpha band", action="store_true")
    parser.add_argument("--dstnodata", help="no data in target file", default=None, type=str)
    parser.add_argument("--outsize",   help="npixels-X npixels-Y", nargs=2, type=int, default=None)
    parser.add_argument("--t_srs",     help="output SRS", default=None, type=str)

    args = parser.parse_args()

    clipPolygonFromRaster(
       args.raster_fn, 
       args.polygon_fn, 
       args.output_fn,
       outsize=args.outsize,
       fill=args.fill,
       oformat=args.oformat,
       dstalpha=args.dstalpha, 
       dstnodata=args.dstnodata, 
       bbox=args.bbox,
       t_srs=args.t_srs)
