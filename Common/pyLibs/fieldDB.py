#!/usr/bin/env python
########
#
# Field Database Component
#
################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2018, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

import os
import math
import geojson
import numpy
import gdal
import osr
import datetime
import shutil
import uuid
import glob
import json

import netCDF4

import csutils.geojson

numpy.warnings.filterwarnings('ignore')

BACKEND_DB=os.environ.get('FIELD_DB_BACKEND_PATH', '/tmp/FIELD_DB')
FRONTEND_DB=os.environ.get('FIELD_DB_FRONTEND_PATH', 'localhost:/tmp/FIELD_DB')

DATA_FILE_EPOCH = datetime.datetime(2000,1,1,0,0,0)

def _getDatabasePath():
    global BACKEND_DB
    return BACKEND_DB

def _getUserPath(user_id):
    return _getDatabasePath() + '/' + 'user-' + user_id

def _getDeletedPath(user_id):
    return _getUserPath(user_id) + '/' + 'deleted'

def _getGroupPath(user_id, group_id):
    return _getUserPath(user_id) + '/' + 'group-' + group_id

def _getFieldPath(user_id, group_id, field_id):
    return _getGroupPath(user_id, group_id) + '/' + 'field-'+ field_id

def _getFieldPathNameList(user_id):
    d = _getUserPath(user_id)
    l = glob.glob(os.path.join(d, 'group-*', 'field-*'))
    return l

def _getFieldCnt(user_id):
    return len(_getFieldPathNameList(user_id))

def _getFieldList(user_id):
    l = _getFieldPathNameList(user_id)
    def s(p):
        g, f = os.path.split(p)
        p, g = os.path.split(g)
        group = g.replace('group-','',1)
        field = f.replace('field-','',1)
        return (group, field)
    return map(s, l)

def _getMaxNumberOfFields(user_id):
    return 100

def _getIndex(x, a):
    # array a must be sorted increasing
    for i in range(len(a)):
        if a[i] >= x: return i
    return len(a)-1

#
def _writeGeoTiff(fn, a, spatial_ref, geotransform):

    if os.path.exists(fn) :
       raise RuntimeError("%s already exists" % fn)

    gdt = { 
            numpy.dtype('bool')    : gdal.GDT_Byte, 
            numpy.dtype('int8')    : gdal.GDT_Byte, 
            numpy.dtype('uint8')   : gdal.GDT_Byte, 
            numpy.dtype('int16')   : gdal.GDT_Int16,
            numpy.dtype('uint16')  : gdal.GDT_UInt16,
            numpy.dtype('int32')   : gdal.GDT_Int32,
            numpy.dtype('float32') : gdal.GDT_Float32, 
            numpy.dtype('float64') : gdal.GDT_Float64, 
          }
    driver = gdal.GetDriverByName('GTiff')

    l = a
    if type(l) is not list:
       l = [a]

    rows, cols = l[0].shape
    raster = driver.Create(fn, cols, rows, len(l), gdt[l[0].dtype])
    raster.SetGeoTransform(geotransform)

    for i in range(len(l)):
        band = raster.GetRasterBand(i+1)
        band.WriteArray(l[i])

    raster.SetProjection(str(spatial_ref))
    raster = None

#
def _getGeoTransform(ds):
    return ds.GetGeoTransform()

#
def _getEPSGCode(ds):
    srs = osr.SpatialReference()
    srs.ImportFromWkt(ds.GetProjection())
    authority = srs.GetAuthorityName("PROJCS")
    if not authority == 'EPSG':
        raise RuntimeError('Unexpected authority %s' % (authority))

    return int(srs.GetAuthorityCode("PROJCS"))
    
#
def _createDataFile(field, mask_fn):
    global DATA_FILE_EPOCH

    file_name = field._getDataFilePath()

    try:

        mask_ds = gdal.Open(mask_fn)

        gt  = mask_ds.GetGeoTransform()
        prj = mask_ds.GetProjection()
        srs = osr.SpatialReference(wkt=prj)

        ds = netCDF4.Dataset(file_name, "w")
        ds.setncattr('Recording_Date', str(format(DATA_FILE_EPOCH, "%Y%m%dT%H%M%S")))
        ds.setncattr('Creation_Date',  str(format(datetime.datetime.now(), "%Y%m%dT%H%M%S")))
        ds.setncattr('field_id',       field.field_id)
        ds.setncattr('user_id',        field.user_id)
        ds.setncattr('description',    "NDVI data for field")
        ds.setncattr('GeoTransform',   "(%f, %f, %f, %f, %f, %f)" % (gt[0], gt[1], gt[2], gt[3], gt[4], gt[5]))
        ds.setncattr('spatial_ref',    srs.ExportToWkt())
    
        mask_b = mask_ds.GetRasterBand(1)
        mask   = mask_b.ReadAsArray()
        if mask.dtype <> numpy.dtype('bool'): mask = mask <> 0

        npixels   = numpy.count_nonzero(~mask)
        pixd      = ds.createDimension("pixels" , npixels)
        timed     = ds.createDimension("time" ,   None)
        acqdd     = ds.createDimension("acqd" ,   None)
    
        nX, nY = mask.shape
        dX     = ds.createDimension("X", nX)
        dY     = ds.createDimension("Y", nY)
    
        time_v  = ds.createVariable("time",           numpy.int64,   ("time"))
        acqd_v  = ds.createVariable("acqd",           numpy.int64,   ("acqd"))
        vpix_v  = ds.createVariable("n_valid_pixels", numpy.int32,   ("time"))
        ndvis_v = ds.createVariable("ndvi_sorted",    numpy.float32, ("time", "pixels"))
        scls_v  = ds.createVariable("scl_sorted",     numpy.int8,    ("time", "pixels"))
        scl_v   = ds.createVariable("scl",            numpy.int8,    ("time", "X", "Y"))
        ndvi_v  = ds.createVariable("ndvi",           numpy.float32, ("time", "X", "Y"))
    
        ds.close()

    except:
        os.remove(file_name)
        raise
    

#
def _updateFieldData(file_name, dt_stamp, B04, B08, SCL, ndvi):

    ds = netCDF4.Dataset(file_name, "a")
    time_v = ds.variables['time']
    acqd_v = ds.variables['acqd']
    ndvi_v = ds.variables['ndvi']
    vpix_v = ds.variables['n_valid_pixels']
    scl_v  = ds.variables['scl']
    ndvi_sorted_v = ds.variables['ndvi_sorted']
    scl_sorted_v  = ds.variables['scl_sorted']

    nacq, npixel = ndvi_sorted_v.shape

    record_date = datetime.datetime.strptime(ds.getncattr("Recording_Date"), "%Y%m%dT%H%M%S")
    delta_s     = (dt_stamp - record_date).total_seconds()

    # Insert acquisition datetime into array
    idx = None
    for i in range(len(acqd_v)):
        if acqd_v[i] >= delta_s: idx = i; break
    if idx is None: 
        idx = len(acqd_v)
    elif acqd_v[idx] <> delta_s:
        # shift data to make room for new acquisition
        acqd_v[idx+1:] = acqd_v[idx:nacq]
    acqd_v[idx] = numpy.int64(delta_s)

    # If the scene classification has no data at all 
    # we do not do any further processing. 
    # Note that a fully clouded image will still have 
    # values > 0 and we want to register this.
    if numpy.count_nonzero(SCL) == 0: 
        print "WARNING : addAcquisition no data for %s %s" % (file_name, dt_stamp)
        ds.close()
        return

    n_valid_pixels = numpy.count_nonzero(~numpy.isnan(ndvi))
    ndvi_sorted_i = numpy.argsort(ndvi.flatten())
    ndvi_sorted   = ndvi.flatten()[ndvi_sorted_i]
    scl_sorted    = SCL.flatten()[ndvi_sorted_i]
    
    if n_valid_pixels > npixel:
        raise RuntimeError("ERROR : unexpected number of valid pixels (%d > %d)." % (n_valid_pixels, npixel))

    if n_valid_pixels>0:
        # Have largest valid values first
        ndvi_sorted[0:n_valid_pixels] = ndvi_sorted[n_valid_pixels-1::-1]
        scl_sorted[0:n_valid_pixels]  = scl_sorted[n_valid_pixels-1::-1]

    record_date = datetime.datetime.strptime(ds.getncattr("Recording_Date"), "%Y%m%dT%H%M%S")
    delta_s     = (dt_stamp - record_date).total_seconds()

    idx = None
    for i in range(len(time_v)):
        if time_v[i] >= delta_s: idx = i; break

    if idx is None: 
        idx = nacq
    elif time_v[idx] <> delta_s:
        # shift data to make room for new acquisition
        time_v[idx+1:] = time_v[idx:nacq]
        vpix_v[idx+1:] = vpix_v[idx:nacq]
        scl_v[idx+1:]  = scl_v[idx:nacq]
        ndvi_v[idx+1:] = ndvi_v[idx:nacq]
        ndvi_sorted_v[idx+1:] = ndvi_sorted_v[idx:nacq]
        scl_sorted_v[idx+1:]  = scl_sorted_v[idx:nacq]
    
    # Save acquisition
    time_v[idx] = numpy.int64(delta_s)
    vpix_v[idx] = n_valid_pixels
    scl_v[idx]  = SCL
    ndvi_v[idx] = ndvi
    ndvi_sorted_v[idx] = ndvi_sorted[0:npixel]
    scl_sorted_v[idx]  = scl_sorted[0:npixel]

    ds.close()

def _saveProperties(properties, filepath):
    json.dump(properties, open(filepath, 'w'))

#
def _computeNDVI(B04, B08, scl):

    # Table 2-1 S2-PDGS-MPC-L2A-SUM, version 2.3.0, Issue 1, 2016-11-25
    # 0 NO_DATA
    # 1 SATURATED_OR_DEFECTIVE
    # 2 DARK_AREA_PIXELS
    # 3 CLOUD_SHADOWS
    # 4 VEGETATION
    # 5 BARE_SOILS
    # 6 WATER
    # 7 CLOUD_LOW_PROBABILITY
    # 8 CLOUD_MEDIUM_PROBABILITY
    # 9 CLOUD_HIGH_PROBABILITY
    # 10 THIN_CIRRUS
    # 11 SNOW
    #mask = ~( (scl == 4) | (scl == 5) | (scl == 6) )
    mask = (scl == 0)

    # Make sure we are not using unsigned arithmetic so convert to float
    s = numpy.float32(B08) + numpy.float32(B04)
    d = numpy.float32(B08) - numpy.float32(B04)

    # don't want infinity or negative NDVI
    mask = mask | (s <= 0) | (d<0)

    # prevent warning when doing divide
    s = numpy.where(mask, 1, s)
    ndvi = numpy.where(mask, numpy.float32('nan'), d / s)

    return ndvi


def acquireDeclarations():
    return []

def pushData():
    return -1


def getUserIDs():
    d = _getDatabasePath()
    l = glob.glob(os.path.join(d, 'user-*'))
    return map(lambda x : os.path.basename(x).replace('user-','',1), l)


class Field():

    user_id  = "EMPTY"
    group_id = "EMPTY"
    field_id = "EMPTY"

    def __init__(self, user_id, group_id, field_id):
        user_dir = _getUserPath(user_id)
        if not os.path.exists(user_dir):
           raise RuntimeError("ERROR : user %s does not exist" % user_id)

        group_dir = _getGroupPath(user_id, group_id)
        if not os.path.exists(group_dir) :
           raise RuntimeError("ERROR : group %s/%s does not exist" % (user_id, group_id))

        field_dir = _getFieldPath(user_id, group_id, field_id)
        if not os.path.exists(field_dir) :
           raise RuntimeError("ERROR : field %s/%s/%s does not exist" % (user_id, group_id, field_id))

        self.user_id  = user_id
        self.group_id = group_id
        self.field_id = field_id
    
    def _getDataFilePath(self):
        return _getFieldPath(self.user_id, self.group_id, self.field_id) + '/' + 'data.nc'

    def _getMaskFilePath(self):
        return _getFieldPath(self.user_id, self.group_id, self.field_id) + '/' + 'mask.tif'

    def _getFeatureFilePath(self):
        return _getFieldPath(self.user_id, self.group_id, self.field_id) + '/' + 'field.geojson'

    def _getPropertiesFilePath(self):
        return _getFieldPath(self.user_id, self.group_id, self.field_id) + '/' + 'properties.json'

    def _getFeatureCollection(self):
        fp = Field._getFeatureFilePath(self)
        return geojson.load(open(fp))

    def getTag(self):
        fc_fp = Field._getFeatureFilePath(self)
        fc = geojson.load(open(fc_fp))
        return fc['features'][0]['properties']['tag']

    def getTotalNumberOfPixels(self):
        data_fn = Field._getDataFilePath(self)
        if not os.path.exists(data_fn): return None
        ds = netCDF4.Dataset(data_fn, "r")
        ndvi_sorted_v = ds.variables['ndvi_sorted'][:]
        ds.close()

        return ndvi_sorted_v.shape[1]

    def getLastAcquisitionDate(self):
        data_fn = Field._getDataFilePath(self)
        if not os.path.exists(data_fn): return None
        ds = netCDF4.Dataset(data_fn, "r")
        record_date = datetime.datetime.strptime(ds.getncattr("Recording_Date"), "%Y%m%dT%H%M%S")
        time_v = ds.variables['time'][:]
        ds.close()
        if len(time_v) == 0: return None

        return record_date + datetime.timedelta(seconds=int(time_v[-1]))

    ####################
    ## Process Interface
    ####################

    def define(self, mask_fn, properties):

        field = self

        field_polygon_fn = field._getFeatureFilePath()
        if not os.path.exists(field_polygon_fn) :
           raise RuntimeError("ERROR : missing polygon file for %s/%s/%s." % (user_id, group_id, field_id))

        field_mask_fn     = field._getMaskFilePath()
        properties_fn     = field._getPropertiesFilePath()
    
        try:
            # Write mask for debug purposes
            shutil.copy(mask_fn, field_mask_fn)

            # Create data file
            _createDataFile(field, field_mask_fn)

            # Save properties
            _saveProperties(properties, properties_fn)

        except:
            if os.path.exists(field_mask_fn): os.remove(field_mask_fn)
            if os.path.exists(properties_fn): os.remove(properties_fn)
            raise

        return field

    def getGeoJSON(self):
        geojson_fn = Field._getFeatureFilePath(self)
        if not os.path.exists(geojson_fn) :
           raise RuntimeError("ERROR : GeoJSON %s does not exist" % (geojson_fn))
        return geojson.load(open(geojson_fn))
        
    def getProperties(self):
        fp = Field._getPropertiesFilePath(self)
        p  = json.load(open(fp))

        # Field properties
        d = {}
        d['user_id']          = self.user_id
        d['group_id']         = self.group_id
        d['field_id']         = self.field_id
        d['last_acquisition'] = Field.getLastAcquisitionDate(self)
        d['properties']       = p

        return d

    def addAcquisition(self, dt_stamp, B04, B08, SCL):

        data_file_name = Field._getDataFilePath(self)
        ndvi = _computeNDVI(B04, B08, SCL)
        _updateFieldData(data_file_name, dt_stamp, B04, B08, SCL, ndvi)
    
#####################
## Frontend Interface
#####################

    @classmethod
    def declare(cls, user_id, group_id, tag, features_fn, field_id=None):

        if field_id is None: field_id = str(uuid.uuid4())

        user_dir = _getUserPath(user_id)
        if not os.path.exists(user_dir):
            raise RuntimeError("ERROR : declare : user %s does not exist." % user_id)

        group_dir = _getGroupPath(user_id, group_id)

        field_dir = _getFieldPath(user_id, group_id, field_id)
        
        n_fields     = _getFieldCnt(user_id)
        max_n_fields = _getMaxNumberOfFields(user_id)
        if n_fields > max_n_fields:
            raise RuntimeError("ERROR : declare : too many fields (%d) for user %s." % (n_field, user_id))

        if os.path.exists(field_dir):
            raise RuntimeError("ERROR : declare : field_id %s/%s/%s does exist." % (user_id, group_id, field_id))

        feature_fn = ""

        try:
            os.umask(0)
            os.makedirs(field_dir, 0777)

            field = cls(user_id, group_id, field_id)
        
            features = geojson.load(open(features_fn))
            epsgcode = csutils.geojson.EPSGCode(features)
            if epsgcode is None:
                raise RuntimeError("ERROR : feature collection does not have a CRS.")

            geometry = features['features'][0]['geometry']
            if geometry['type'] not in [ 'MultiPolygon', 'Polygon']:
                raise RuntimeError("ERROR : unexpected feature type %s" % geometry['type'])

            # create new feature collection to not 
            # include any other stuff sent by the user
            feature_list       = [geojson.Feature(geometry=geometry)]
            feature_collection = geojson.FeatureCollection(feature_list)
            feature_collection['crs'] = {"type": "name", "properties": {"name": "urn:ogc:def:crs:EPSG::%s" % epsgcode}}
            feature_collection['features'][0]['properties']['user_id']  = user_id
            feature_collection['features'][0]['properties']['field_id'] = field_id
            feature_collection['features'][0]['properties']['group_id'] = group_id
            feature_collection['features'][0]['properties']['tag']      = tag

            feature_fn = field._getFeatureFilePath()
            geojson.dump(feature_collection, open(feature_fn, 'w'))

        except:
            if os.path.exists(feature_fn): os.remove(feature_fn)
            if os.path.exists(field_dir): os.rmdir(field_dir)
            raise

        return { "field_id" : field_id, "field_cnt" : n_fields+1 }

    @classmethod
    def getGroupList(cls, user_id, group_id):
        user_dir = _getUserPath(user_id)
        if not os.path.exists(user_dir):
            raise RuntimeError("ERROR : list : user %s does not exist." % user_id)

        group_dir = _getGroupPath(user_id, group_id)
        if not os.path.exists(group_dir):
            raise RuntimeError("ERROR : list : group %s does not exist." % group_id)

        field_list = os.listdir(group_dir)
        field_list = [ x.replace('field-', '',1) for x in field_list ]

        feature_collection_list = []
        for field_id in field_list:

            field = Field(user_id, group_id, field_id)
            fc   = field._getFeatureCollection()

            date = field.getLastAcquisitionDate()
            last_acquisition_date = None
            if date is not None:
                last_acquisition_date = date.isoformat()

            n_total_pixels        = field.getTotalNumberOfPixels()

            feature = fc['features'][0]
            feature['properties'] = {
                 "group_id"          : group_id,
                 "field_id"          : field_id,
                 "tag"               : feature['properties']['tag'],
                 "n_total_pixels"    : n_total_pixels,
                 "last_acquisition"  : last_acquisition_date,
            }
            fc['features'][0] = feature

            feature_collection_list.append(fc)

        return feature_collection_list


    @classmethod
    def getDeclaredFields(cls, user_id):
        l = _getFieldList(user_id)
        field_list = []
        for group_id, field_id in l:
            field = Field(user_id, group_id, field_id)
            if not field.isDefined(): field_list.append(field)
        return field_list

    @classmethod
    def getDefinedFields(cls, user_id):
        l = _getFieldList(user_id)
        field_list = []
        for group_id, field_id in l:
            field = Field(user_id, group_id, field_id)
            if field.isDefined(): field_list.append(field)
        return field_list

    def isDefined(self):
        return os.path.exists(Field._getPropertiesFilePath(self))

    def getNDVITimeSeries(self, start_date, end_date, lb_percentile=0.0, ub_percentile=100.0, lb_vpix_percentage=0.0, ag='AVG', mask_list=None):

        if mask_list is None:
            mask_list = [4,5,6]

        if lb_percentile<0.0 or lb_percentile>100.0:
            raise RuntimeError("ERROR : getNDVITimeSeries unexpected lb_pertentile %f" % lb_percentile)
        if ub_percentile<0.0 or ub_percentile>100.0:
            raise RuntimeError("ERROR : getNDVITimeSeries unexpected ub_pertentile %f" % ub_percentile)
        if start_date > end_date:
            raise RuntimeError("ERROR : getNDVITimeSeries unexpected dates (%s > %s)" % (str(start_date), str(end_date)))

        data_pn = Field._getDataFilePath(self)
        ds = netCDF4.Dataset(data_pn, "r")

        time_v        = ds.variables['time']
        vpix_v        = ds.variables['n_valid_pixels']
        ndvi_sorted_v = ds.variables['ndvi_sorted']
        scl_sorted_v  = ds.variables['scl_sorted']

        ntpix = ndvi_sorted_v.shape[1]

        # If no acquisitions
        if len(time_v[:]) == 0:
            result = { "user_id"             : self.user_id,
                       "field_id"            : self.field_id,
                       "group_id"            : self.group_id,
                       "tag"                 : Field.getTag(self),
                       "start_date"          : datetime.datetime.isoformat(start_date),
                       "end_date"            : datetime.datetime.isoformat(end_date),
                       "lb_percentile"       : lb_percentile,
                       "ub_percentile"       : ub_percentile,
                       "pixel_percentage"    : lb_vpix_percentage,
                       "aggregation"         : ag,
                       "n_total_pixels"      : ntpix,
                       "mask_list"           : mask_list,
                       "timeseries"     : {
                           "datetime"          : [],
                           "value"             : [],
                           "n_selected_pixels" : [],
                           "n_valid_pixels"    : [],
                       }
                     }
            return result

        record_date = datetime.datetime.strptime(ds.getncattr("Recording_Date"), "%Y%m%dT%H%M%S")

        sdt = (start_date - record_date).total_seconds()
        edt = (end_date - record_date).total_seconds()

        stidx = _getIndex(sdt, time_v)
        etidx = _getIndex(edt, time_v)
        if stidx<0: stidx=0; etidx=-1

        value_list          = []
        datetime_list       = []
        npix_list           = []
        n_valid_pixels_list = []
        n_selected_pixels_list = []

        for i in range(stidx, etidx+1):
            nvp   = vpix_v[i]

            # Mask selects the pixels ...
            data = ndvi_sorted_v[i]
            mask = numpy.isin(scl_sorted_v[i], mask_list) & ~numpy.isnan(data)
            nvp  = numpy.sum(mask)
            data = data[mask]

            # Skip acquisition when not enough valid pixels
            if (100.0 * float(nvp) / ntpix) < lb_vpix_percentage : continue
	 
            v = float('nan')
            if nvp <> float('nan') and nvp >0:

                # array is sorted large to small
                spidx = int(math.floor((nvp-1) * (1.0 - ub_percentile / 100.0)))
                epidx = int(math.ceil((nvp-1) * (1.0 - lb_percentile / 100.0)))

                # If ub percentile > lb percentile, take only lb_percentile
                if spidx>epidx : spidx = epidx

                data = data[spidx:epidx+1]

                npix = epidx - spidx + 1

                if ag == 'AVG': v = numpy.mean(data) 
                if ag == 'SUM': v = numpy.sum(data) 
                if ag == 'MAX': v = numpy.max(data) 
                if ag == 'MIN': v = numpy.min(data) 
                if ag == 'MED': v = numpy.mean(data[(npix-1)/2:(npix/2)+1])
            else:
                npix = 0           

            dt  = record_date + datetime.timedelta(seconds=int(time_v[i]))
            dts = dt.isoformat()

            value_list.append(float(v))
            datetime_list.append(dts)
            n_valid_pixels_list.append(int(nvp))
            n_selected_pixels_list.append(int(npix))
         
        result = { "user_id"             : self.user_id, 
                   "field_id"            : self.field_id,
                   "group_id"            : self.group_id,
		   "tag"		 : Field.getTag(self),
                   "start_date"          : datetime.datetime.isoformat(start_date),
                   "end_date"            : datetime.datetime.isoformat(end_date),
                   "lb_percentile"       : lb_percentile,
                   "ub_percentile"       : ub_percentile,
                   "pixel_percentage"    : lb_vpix_percentage,
                   "aggregation"         : ag,
                   "n_total_pixels"      : ntpix, 
                   "mask_list"           : mask_list,
                   "timeseries"     : {
                       "datetime"          : datetime_list,
                       "value"             : value_list,
                       "n_selected_pixels" : n_selected_pixels_list,
                       "n_valid_pixels"    : n_valid_pixels_list,
                   } 
                 }

        ds.close()

        return result

    def getNDVIDistribution(self, pdate=None, nbins=0, mask_list=None):

        if mask_list is None:
            mask_list = [4,5,6]

        data_pn = Field._getDataFilePath(self)
        ds = netCDF4.Dataset(data_pn, "r")

        time_v        = ds.variables['time']
        ndvi_sorted_v = ds.variables['ndvi_sorted']
        scl_sorted_v  = ds.variables['scl_sorted']
        vpix_v        = ds.variables['n_valid_pixels']

        # If no acquisitions
        if len(time_v[:]) == 0:
            result = { "user_id"             : self.user_id, 
                       "field_id"            : self.field_id,
                       "group_id"            : self.group_id,
		       "tag"	             : Field.getTag(self),
                       "nbins"               : nbins,
                       "datetime"            : None if pdate is None else pdate.isoformat(),
                       "mask_list"           : mask_list,
                       "aquisition_datetime" : None,
                       "n_total_pixels"      : None,
                       "n_valid_pixels"      : None,
                       "values"              : None,
                     }
            return result

        record_date = datetime.datetime.strptime(ds.getncattr("Recording_Date"), "%Y%m%dT%H%M%S")

        # len(time_v)> 0

        dt = time_v[-1]
        date = pdate
        if pdate is not None:
            dt = (pdate - record_date).total_seconds()
        else:
            date = record_date + datetime.timedelta(seconds=int(d))

        idx = _getIndex(dt, time_v)
        adate = record_date + datetime.timedelta(seconds=int(time_v[idx]))

        # Mask selects the pixels ...
        mask = numpy.isin(scl_sorted_v[idx], mask_list)
        nvp  = numpy.sum(mask)
        data = ndvi_sorted_v[idx]
        data = data[mask]

	if nbins > 0 and nbins<nvp:
            q = [50.0]
            if nbins>1: q = [ i * (100.0/(nbins-1)) for i in range(nbins) ]
	    data = numpy.nanpercentile(data, q)

        result = { "user_id"             : self.user_id, 
                   "field_id"            : self.field_id,
                   "group_id"            : self.group_id,
		   "tag"	         : Field.getTag(self),
                   "nbins"               : nbins,
                   "datetime"            : None if pdate is None else pdate.isoformat(),
                   "mask_list"           : mask_list,
                   "aquisition_datetime" : adate.isoformat(),
                   "n_total_pixels"      : int(ndvi_sorted_v.shape[1]),
                   "n_valid_pixels"      : int(nvp),
                   "values"              : map(float, data),
                 }

        ds.close()

        return result


    def getSCLDistribution(self, date=None):

        data_pn = Field._getDataFilePath(self)
        ds = netCDF4.Dataset(data_pn, "r")

        time_v        = ds.variables['time']
        scl_v         = ds.variables['scl']
        vpix_v        = ds.variables['n_valid_pixels']
        ndvi_sorted_v = ds.variables['ndvi_sorted']

        # If no acquisitions
        if len(time_v[:]) == 0:
            result = { "user_id"             : self.user_id, 
                       "field_id"            : self.field_id,
                       "group_id"            : self.group_id,
		       "tag"	             : Field.getTag(self),
                       "datetime"            : None if date is None else date.isoformat(),
                       "aquisition_datetime" : None,
                       "n_total_pixels"      : None,
                       "n_valid_pixels"      : None,
                       "values"              : None,
                     }
            return result

        record_date = datetime.datetime.strptime(ds.getncattr("Recording_Date"), "%Y%m%dT%H%M%S")

        # len(time_v) > 0

        dt = time_v[-1]
        if date is not None:
            dt = (date - record_date).total_seconds()
        else:
            date = record_date + datetime.timedelta(seconds=int(dt))

        idx = _getIndex(dt, time_v)
        adate = record_date + datetime.timedelta(seconds=int(time_v[idx]))

        mask_fn = Field._getMaskFilePath(self)
        mask_ds = gdal.Open(mask_fn)
        mask_b  = mask_ds.GetRasterBand(1)
        mask    = mask_b.ReadAsArray()

        scl     = scl_v[idx]
        scl     = scl[mask == False]

        scl_bin = numpy.bincount(scl.flatten(), minlength=12)

        result = { "user_id"             : self.user_id,
                   "field_id"            : self.field_id,
                   "group_id"            : self.group_id,
		   "tag"	         : Field.getTag(self),
                   "datetime"            : date.isoformat(),
                   "aquisition_datetime" : adate.isoformat(),
                   "n_total_pixels"      : int(ndvi_sorted_v.shape[1]),
                   "n_valid_pixels"      : int(vpix_v[idx]),
                   "values"              : map(int, scl_bin),
                 }

        ds.close()

        return result

    def getAcquisitionDateTimes(self, dtformat=lambda x: x, all_acq=False):

        data_pn = Field._getDataFilePath(self)
        ds = netCDF4.Dataset(data_pn, "r")

        record_date = datetime.datetime.strptime(ds.getncattr("Recording_Date"), "%Y%m%dT%H%M%S")

        # Get the time offset array
        # We may want the daetimes incl. ones without valid pixels
        time = ds.variables['time'][:]
        if all_acq: time = ds.variables['acqd'][:]

        ds.close()

        dtsl = []
        for time_offset in time:
            dt  = record_date + datetime.timedelta(seconds=int(time_offset))
            dtsl.append(dtformat(dt))

        return dtsl

    def getNDVIImage(self, image_fn, date=None):

        data_pn = Field._getDataFilePath(self)
        ds = netCDF4.Dataset(data_pn, "r")

        record_date = datetime.datetime.strptime(ds.getncattr("Recording_Date"), "%Y%m%dT%H%M%S")

        time_v = ds.variables['time']
        ndvi_v = ds.variables['ndvi']

        if time_v.shape[0] == 0: return None

        dt = time_v[-1]
        if date is not None:
            dt = (date - record_date).total_seconds()

        idx = _getIndex(dt, time_v)
        nX, nY = ndvi_v.shape[1:3]
        if idx>=0:
            ndvi_data = ndvi_v[idx]
        else:
            ndvi_data = numpy.array([float('nan')]*(nX*nY)).reshape(nX, nY)

        spatial_ref  = ds.getncattr("spatial_ref")
        geotransform = eval(ds.getncattr("GeoTransform"))

        _writeGeoTiff(image_fn, ndvi_data, spatial_ref, geotransform)
        
        ds.close()

    def getSCLImage(self, image_fn, date=None):

        data_pn = Field._getDataFilePath(self)
        ds = netCDF4.Dataset(data_pn, "r")

        record_date = datetime.datetime.strptime(ds.getncattr("Recording_Date"), "%Y%m%dT%H%M%S")

        time_v = ds.variables['time']
        scl_v  = ds.variables['scl']

        if time_v.shape[0] == 0: return None

        dt = time_v[-1]
        if date is not None:
            dt = (date - record_date).total_seconds()

        idx = _getIndex(dt, time_v)
        nX, nY = scl_v.shape[1:3]
        if idx>=0:
            scl_data = scl_v[idx]
        else:
            scl_data = numpy.array([0]*(nX*nY)).reshape(nX, nY)

        spatial_ref  = ds.getncattr("spatial_ref")
        geotransform = eval(ds.getncattr("GeoTransform"))

        _writeGeoTiff(image_fn, scl_data, spatial_ref, geotransform)

        ds.close()

    def delete(self):

        srcdir = _getFieldPath(self.user_id, self.group_id, self.field_id)
        dstdir = _getDeletedPath(self.user_id)
        if not os.path.exists(dstdir): 
            os.umask(0)
            os.makedirs(dstdir, 0777)
 
        # Add time stamp to prevent multiple create + delete
        # with same name to cause error
        dts    = str(format(datetime.datetime.now(), "%Y%m%dT%H%M%S"))
        dname  = '-'.join([self.user_id, self.group_id, self.field_id, dts])
        dstdir = os.path.join(dstdir, dname)
 
        shutil.move(srcdir, dstdir)
 
        field_cnt = _getFieldCnt(self.user_id)
 
        return field_cnt


def getDefinitions():
    return []
