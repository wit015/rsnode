################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

import os
import sys
import datetime
import hashlib
import json
import traceback

import numpy as np

from osgeo import gdal

import testlib

# Global script identifiers
SVN_REVISION = '$Rev: 7862 $'
SVN_DATE = '$Date: 2017-01-27 12:51:07 +0100 (Fri, 27 Jan 2017) $'
SVN_AUTHOR = '$Author: jb76278 $'
SCRIPT_FILE_NAME = os.path.basename(__file__)
NOW_STRING = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

gTEST_ROI_DIR = os.getenv('TEST_ROI_DIR', './ROI/')

import math

# Haversine distance to compute distance on a sphere
def hdistance(origin, destination):
    lat1, lon1 = origin
    lat2, lon2 = destination
    radius = 6371 # km
    dlat = math.radians(lat2-lat1)
    dlon = math.radians(lon2-lon1)
    a = math.sin(dlat/2) * math.sin(dlat/2) + math.cos(math.radians(lat1)) \
    * math.cos(math.radians(lat2)) * math.sin(dlon/2) * math.sin(dlon/2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
    d = radius * c
    return d

def issueRequest(request, testid, teststep, outFileName):
    returnArray = []
    returnGdalInfo = ""
    response = testlib.checkIssueRequest(request, testid, teststep)
    data = response.read()
    try:
        os.remove(outFileName)
    except:
        None
    g = open(outFileName, 'wb')
    g.write(data)
    g.close()

def getImageData(fileName):
    dataSet = gdal.Open(fileName)
    return np.array(dataSet.GetRasterBand(1).ReadAsArray())

def getImageNoDataValue(fileName):
    dataSet = gdal.Open(fileName)
    return dataSet.GetRasterBand(1).GetNoDataValue()

gNODATA_TEST = 25.0

# Parameters for the MODIS product cube
gRES_X  = None
gRES_Y  = None
gORG_X  = None
gORG_Y  = None

# For pixel value calc do not use exact border of pixel but 
# use small offset value.
gEPS_X  = None
gEPS_Y  = None

def setGeoTransform(orgX, resX, epsX, orgY, resY, epsY):
    global gRES_X, gRES_Y, gEPS_X, gORG_X, gORG_Y, gEPS_Y
    r = (gRES_X, gRES_Y, gEPS_X, gORG_X, gORG_Y, gEPS_Y)
    gORG_X = orgX
    gRES_X = resX
    gEPS_X = epsX
    gORG_Y = orgY
    gRES_Y = resY
    gEPS_Y = epsY
    return r

def getGeoTransform():
    global gRES_X, gRES_Y, gEPS_X, gORG_X, gORG_Y, gEPS_Y
    return (gORG_X, gRES_X, gEPS_X, gORG_Y, gRES_Y, gEPS_Y)

def setNoDataValue(x):
    global gNODATA_TEST
    r = gNODATA_TEST
    gNODATA_TEST = x
    return r

def getNoDataValue():
    return gNODATA_TEST

def REFSTART_1(date, delta=1):
   return datetime.date(date.year+delta, date.month, date.day)

def REFSTART_2(date, delta=1):
    nmonth = ((date.month-1+delta) % 12) + 1
    nyear  = date.year + ((date.month-1+delta) / 12)
    if nmonth == 2 and date.day > 28:
        return datetime.date(nyear, 3, 1) - datetime.timedelta(1)
    if nmonth in [4, 6, 9, 11] and date.day > 30:
        return datetime.date(nyear, nmonth, 30)
    return datetime.date(nyear, nmonth, date.day)

def REFSTART_3(date, delta=12):
    return date + datetime.timedelta(delta)

def REFSTART_4(date, delta=-1):
    return REFSTART_1(date, delta)

def REFSTART_5(date, delta=-1):
    return REFSTART_2(date, delta)

def REFSTART_6(date, delta=-12):
    return REFSTART_3(date, delta)

# Filled by testcase_0
gPeriodStartList = []

def setPeriodStartList(l):
    global gPeriodStartList
    gPeriodStartList = l

def getPeriodStartList():
    global gPeriodStartList
    return gPeriodStartList

def deltaMonth(date, delta):
    nmonth = ((date.month-1+delta) % 12) + 1
    nyear  = date.year + ((date.month-1+delta) / 12)
    if nmonth == 2 and date.day > 28:
        return datetime.date(nyear, 3, 1) - datetime.timedelta(1)
    if nmonth in [4, 6, 9, 11] and date.day > 30:
        return datetime.date(nyear, nmonth, 30)
    return datetime.date(nyear, nmonth, date.day)

def deltaYear(date, delta):
    return datetime.date(date.year+delta, date.month, date.day)

def world2pix(lon, lat):
    global gRES_X, gRES_Y, gORG_X, gORG_Y
    pX    = int((lon-gORG_X)/gRES_X)
    pY    = int((lat-gORG_Y)/gRES_Y)
    return (pX, pY)

def pix2world(x, y):
    global gRES_X, gRES_Y, gORG_X, gORG_Y
    return (gORG_X + x*gRES_X, gORG_Y + y*gRES_Y)
   
def getMeasurementIntervalStartIndex(date):
    global gPeriodStartList
    for i in range(len(gPeriodStartList)):
        startDate = gPeriodStartList[i]
        ndays = (date - startDate).days
        if ndays < 0: return max(0, i-1)
    return i
   
def getMeasurementIntervalStart(date):
    global gPeriodStartList
    i = getMeasurementIntervalStartIndex(date)
    return gPeriodStartList[i]

def testValue(lon, lat, date):
    global gRES_X, gRES_Y, gORG_X, gORG_Y, gNODATA_TEST
    # Compute origin of the pixel
    pX, pY = world2pix(lon, lat)
    pLon = gORG_X + gRES_X * pX
    pLat = gORG_Y + gRES_Y * pY
    # Compute expected value
    date  = getMeasurementIntervalStart(date)
    delta = date - datetime.date(2000, 1, 1)
    if (round(pLon*10000) + round(pLat*10000) + delta.days) % 2500 == gNODATA_TEST: print "NODATA at ", lon, lat, date
    return (round(pLon*10000) + round(pLat*10000) + delta.days) % 2500

def testValueList(lon, lat, period, by=2001, ey=2015, integrate='NONE', wLength=0, allowBreak=False):
  
    p = period
    if len(p) == 1: p = (period[0], period[0])

    l = []
    ed = getMeasurementIntervalStart(p[1])
    ei = gPeriodStartList.index(ed)
    sd = getMeasurementIntervalStart(p[0])
    si = gPeriodStartList.index(sd)
    di = ei - si + 1 # Number of intervals
    #print gPeriodStartList[si]

    wsize = wLength
    if wsize == 0 and integrate == 'DIF':
       wsize = 1
    elif wsize == 0 and integrate == 'SUM':
       wsize = di
    wsize = max(1, wsize)

    ii = [ i for i in range(0,len(gPeriodStartList)) if gPeriodStartList[i].month==1 and gPeriodStartList[i].day==1 ]
    nipy = np.diff(np.array(ii))[0]
    #print by, ey, ii, nipy

    for begin in [0] + ii: 
        if gPeriodStartList[begin].year >= by: break

    for end in ii: 
        if gPeriodStartList[end].year > ey: break
    if gPeriodStartList[end].year <= ey : 
        end = len(gPeriodStartList)-1
    else:
        end -= 1

    # Get earliest date
    d1  = (si - begin) % nipy
    d2  = (ei - begin) % nipy
    si = begin + d1
    ei = begin + d2

    #print begin, si, ei, end, di, allowBreak, integrate
    if ei<si:
        if allowBreak:
            si -= nipy
        else:
            ei += nipy

    yvl = []
    for si in range(si, end-di+2, nipy):

        vl = []

        if si<begin:
            r = gPeriodStartList[end+2+d2-di:end+1]
            r = r + gPeriodStartList[begin:ei+1]
            si += nipy
        else:
            r = gPeriodStartList[si:si+di]
            ei += nipy

        for d in r:
            v = testValue(lon, lat, d)
            #print si, d, v
            vl.append(v)

        if integrate == 'DIF': 
           vl = [vl[0]] + vl
           vl = np.diff(vl).tolist()
        if integrate <> 'NONE' and wsize > 1: 
           vl = np.apply_along_axis(np.convolve, 0, vl, np.ones(wsize), mode='full')[0:-wsize+1].tolist()

        yvl.append(vl)

    return yvl

def testValueBaseStats(lon, lat, period, by=2001, ey=2015, integrate='NONE', wLength=0):
    allowBreak = integrate == 'NONE'
    if (period[1]-period[0]).days > 364:
       period = (period[0], period[0] + datetime.timedelta(364))
    l = testValueList(lon, lat, period, by, ey, integrate, wLength, allowBreak)
    if len(l) == 0: np.ones(6,) * float('nan')
    s = np.percentile(l, [0.0, 25.0, 50.0, 75.0, 100.0], axis=0).tolist()
    s.append(np.mean(l, axis=0).tolist())
    return s

def testValueAggregate(lon, lat, period, integrate='NONE', aggregate='AVG', wLength=0):
    allowBreak = False
    l = testValueList(lon, lat, period, period[0].year, period[1].year, integrate, wLength, allowBreak)
    if   aggregate == 'MAX' : return np.max(l)
    elif aggregate == 'MIN' : return np.min(l)
    else:                    return np.average(l)

def testValueAggregateLTA(lon, lat, period, by=2001, ey=2015, integrate='NONE', aggregate='AVG', wLength=0): 
    allowBreak = integrate == 'NONE' and aggregate == 'NONE'
    if (period[1]-period[0]).days > 364:
       period = (period[0], period[0] + datetime.timedelta(364))
    l = testValueList(lon, lat, period, by, ey, integrate, wLength, allowBreak)
    if len(l) == 0: return float('nan')
    if   aggregate == 'MAX' : l = np.nanmax(l, axis=1).tolist()
    elif aggregate == 'MIN' : l = np.nanmin(l, axis=1).tolist()
    elif aggregate == 'AVG' : l = np.nanmean(l, axis=1).tolist()
    return np.nanmean(l, axis=0).tolist()

def testValueLTA(lon, lat, period, by=2001, ey=2015, integrate='NONE', wLength=0):
    return testValueAggregateLTA(lon, lat, period, by, ey, integrate, 'NONE', wLength)

def computeSignature(data):
    bdata = data.view(np.uint8)
    return hashlib.sha1(bdata).hexdigest()

def getMeasurementIntervals(testcase='none', process='VegetationIndex:GetDates', source='MODIS_500m', indicator='TEST'):

    request = testlib.buildWpsRequest(
        process=process,
        source=source,
        indicator=indicator,
        response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

    outFileName = ('/tmp/%s.json' % testcase)
    issueRequest(request, testcase, 1, outFileName)
    result = open(outFileName, 'r').read()
    response = json.loads(result)
    dates = map(lambda d : datetime.datetime.strptime(d, "%Y-%m-%d").date(), response['pstart'])
    setPeriodStartList(dates)
    return dates
