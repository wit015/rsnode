################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

import re
import os
import sys
import httplib
# Python 3 import used by Alterra.
# import http.client as httplib
from lxml import etree
import numpy as np
import traceback

gTrace = 1
gNChecks = 0
gNFails = 0
gLogging = False

_NAMESPACE = {'wcs': 'http://www.opengis.net/wcs/1.1.1',
              'gml': 'http://www.opengis.net/gml',
              'xlink': 'http://www.w3.org/1999/xlink',
              'wps': 'http://www.opengis.net/wps/1.0.0',
              None: 'http://www.opengis.net/wps/1.0.0',
              'ogc': 'http://www.opengis.net/ogc',
              'wfs': 'http://www.opengis.net/wfs',
              'xsi': 'http://schemas.opengis.net/wps/1.0.0/wpsExecute_request.xsd',
              'ows': 'http://www.opengis.net/ows/1.1',
              'ogr': 'http://ogr.maptools.org/'}


def getTrace():
    return gTrace


def _ns(ns):
    return '{' + _NAMESPACE.get(ns) + '}'


# Method will check the GDAL info of given tif file again expected value
def checkStringContent(aTestID, aTestStep, aString, aExpectedGdalString, aNegativeOutcomeExpected=False):
    global gNChecks, gNFails, gLogging
    gNChecks = gNChecks + 1

    res = aString == aExpectedGdalString != aNegativeOutcomeExpected
    if res:
        msg = "PASS"
    else:
        gNFails = gNFails + 1
        msg = "FAIL"
    if gLogging:
        print("TEST %s - %s : %s (%d/%d)" %
              (aTestID, aTestStep, msg, gNChecks, gNFails))
    return res


# Method will check if the two given arrays are equal to each other
def checkArrayContent(aTestID, aTestStep, aArrayOne, aArrayTwo, aNegativeOutcomeExpected=False):
    global gNChecks, gNFails, gLogging
    gNChecks = gNChecks + 1

    res = np.array_equal(aArrayOne, aArrayTwo) != aNegativeOutcomeExpected
    if res:
        msg = "PASS"
    else:
        gNFails = gNFails + 1
        msg = "FAIL"
    if gLogging:
        print("TEST %s - %s : %s (%d/%d)" %
              (aTestID, aTestStep, msg, gNChecks, gNFails))
    return res


def setLogging(b):
    global gLogging
    oldValue = gLogging
    gLogging = b
    return oldValue


def reset(n=0, m=0):
    global gNChecks, gNFails
    gNChecks = n
    gNFails = m


def check(id, step, value, expected, tol=None):
    global gNChecks, gNFails, gLogging
    gNChecks = gNChecks + 1
    if tol == None:
        res = value == expected
    else:
        if value == None: value = float('nan')
        res = abs(value - expected) <= tol * abs(expected)
    if res:
        msg = "PASS"
    else:
        gNFails = gNFails + 1
        msg = "FAIL"
    if res == False or gLogging:
        print("TEST %s - %s : 'value: %s' = 'expected: %s' %s (%d/%d)" %
              (id, step, value, expected, msg, gNChecks, gNFails))
    return res


def getCSSERVER():
    csserver = os.getenv('CSSERVER', "dsl-sandbox-cs1:8080")
    m = re.search('([^:]*):([0-9]*)(.*)', csserver)
    host = m.group(1)
    port = m.group(2)
    url  = m.group(3)
    if url == '': url = "/geoserver/wps"
    return host, port, url

# Get host to connect to (may be proxy)
def getConnectionHost(host, port, url):
    if os.getenv('http_proxy') is not None:
        chost, cport = os.getenv('http_proxy').replace('http://', '').split(':')
        url = "http://" + host + ":" + str(port) + url
    else:
        chost, cport = host, port
    return (chost, cport, url)


def checkIssueRequest(request, testid, teststep):
    host, port, url = getCSSERVER()
    headers = {'Content-type': 'xml'}
    chost, cport, url = getConnectionHost(host, port, url)
    conn = httplib.HTTPConnection(chost, cport)
    try:
        conn.request("POST", url, request, headers)
        response = conn.getresponse()
    except:
        check(testid, str(teststep) + ':conn.getresponse()', True, False)
        raise

    check(testid, str(teststep) + ":response.status", response.status, 200)
    check(testid, str(teststep) + ":response.reason", response.reason, 'OK')
    return response


def conclude():
    global gNChecks, gNFails
    print('TOTAL checks=%d fails=%d' % (gNChecks, gNFails))


def buildWpsRequest(**kwargs):
    def parseInputDef(s):
        value = s
        inputType = 'LiteralData'
        attributes = {}
        cdata = False
        if isinstance(s, list):
            value, inputType, attributes, cdata = s
        return [value, inputType, attributes, cdata]

    def parsOutputDef(s):
        name = s
        outputType = 'RawDataOutput'
        attributes = {}
        if isinstance(s, list):
            name, outputType, attributes = s
        return [name, outputType, attributes]

    Execute = etree.Element(
            _ns('wps') + "Execute",
            {"version": "1.0.0", "service": "WPS"},
            nsmap=_NAMESPACE)
    Process = etree.SubElement(Execute, _ns('ows') + 'Identifier')
    DataInputs = etree.SubElement(Execute, _ns('wps') + 'DataInputs')

    for key in kwargs:
        # Ignore optional values
        if (kwargs[key] != "Optional"):
            if key == 'process':
                Process.text = kwargs[key]
            elif key == 'response':
		value, outputType, attributes = parsOutputDef(kwargs[key])
		Response = etree.SubElement(Execute, _ns('wps') + 'ResponseForm')
		RespData = etree.SubElement(Response, _ns('wps') + outputType, attributes)
		Id = etree.SubElement(RespData, _ns('ows') + 'Identifier')
		Id.text = value
            else:
                Input = etree.SubElement(DataInputs, _ns('wps') + 'Input')
                InputId = etree.SubElement(Input, _ns('ows') + 'Identifier')
                InputId.text = key
                if isinstance(kwargs[key], list) and (kwargs[key])[1] == 'Reference':
                    body, inputType, ref, attributes = kwargs[key]
                    attributes[_ns('xlink') + 'href'] = ref
                    attributes['method'] = "POST"
                    Reference = etree.SubElement(Input, _ns('wps') + 'Reference', attributes)
                    CData = etree.SubElement(Reference, _ns('wps') + 'Body')
                    CData.append(etree.fromstring(body))
                else:
                    value, inputType, attributes, cdata = parseInputDef(kwargs[key])
                    Data = etree.SubElement(Input, _ns('wps') + 'Data')
                    CData = etree.SubElement(Data, _ns('wps') + inputType, attributes)
                    if cdata:
                        CData.text = etree.CDATA(value)
                    else:
                        CData.text = str(value)

    return etree.tostring(Execute, pretty_print=True)


def run(testcases, r):
    global gNChecks, gNFails

    for testCaseId in r:
        try:
            testcases[str(testCaseId)]()
            if getTrace():
                print("Testcase %s checks=%d fails=%d" % (testCaseId, gNChecks, gNFails))
        except KeyboardInterrupt:
            check(testCaseId, "KEYBOARD INTERRUPT", False, True)
            conclude()
            raise
        except:
            check(testCaseId, "EXCEPTION", False, True)
            traceback.print_exception(*sys.exc_info())


def main(testcases):
    global gTrace, gNFails

    host, port, url = getCSSERVER()
    chost, cport, url = getConnectionHost(host, port, url)
    print("Using host=%s, port=%s url=%s" % (chost, cport, url))

    # Get all functions names testcase_<nr> as default test cases
    r = []

    for arg in sys.argv[1:]:
        if arg.startswith('--'):
            l = arg.split('=')
            if len(l) == 2:
                key, value = l
            else:
                key, value = l[0], ''
            if key == "--log":
                setLogging(True)
            elif key == "--trace":
                gTrace = eval(value)
            elif key == "--range":
                design = "testcase_"
                if value != '':
                    r = r + [design + x for x in map(str, eval(value))]
                else:
                    r = r + [x for x in testcases.keys() if re.match(design + "_[0-9]*", x)]
            else:
                design = key[2:] + "_"
                if value != '':
                    r = r + [design + x for x in map(str, eval(value))]
                else:
                    r = r + [x for x in testcases.keys() if re.match(design + "[0-9]*", x)]

    if len(r) == 0:
        # Get all functions names testcase_<nr> as default test cases
        r = [x for x in testcases.keys() if x.startswith('testcase_')]

    r.sort()

    run(testcases, r)

    conclude()
    sys.exit(gNFails)
