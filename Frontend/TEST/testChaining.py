#!/usr/bin/python

import os
import sys
import httplib, urllib
import testlib
import testValues

testlib.setLogging(True)

roi = '{"type": "FeatureCollection", "features" : [ { "type": "Feature", "properties": { }, "geometry": { "type":"Polygon","coordinates":[[[36.00,6.00],[36.00,8.00],[38.00,8.00],[38.00,6.00],[36.00,6.00]]]} } ] }'

processName = 'VegetationIndex:PeriodAggregateMap'

# Minimal number of input parameters
request = testlib.buildWpsRequest(
             process=processName,
             source="MODIS_500m",
             ROI=[roi, 'ComplexData', {'mimeType' : 'application/json'}, 1],
             indicator="NDVI",
             periodStart="2010-01-01",
             periodEnd="2010-12-01",
             aggregateType="MAX",
             response=['geotiff', 'RawDataOutput', { 'mimeType' : 'image/tiff'}])

styleStr = '<?xml version="1.0" encoding="ISO-8859-1"?> <StyledLayerDescriptor version="1.0.0"><NamedLayer><Name>Two color gradient</Name><UserStyle><Title>SLD Cook Book: Two color gradient</Title> <FeatureTypeStyle> <Rule> <RasterSymbolizer> <ColorMap type="intervals"> <ColorMapEntry color="#0000FF" quantity="5000" /> <ColorMapEntry color="#00FF00" quantity="6000" /> <ColorMapEntry color="#FF0000" quantity="8000" /> </ColorMap> </RasterSymbolizer> </Rule> </FeatureTypeStyle> </UserStyle></NamedLayer></StyledLayerDescriptor>'

request2 = testlib.buildWpsRequest(
             process="ras:StyleCoverage",
             coverage=[request, 'Reference', 'http://geoserver/wps', {'mimeType' : 'image/tiff'}],
             style=[styleStr, 'ComplexData', {'mimeType' : 'application/xml; subtype=sld/1.0.0'}, 1],
             response=['result', 'RawDataOutput', { 'mimeType' : 'image/png'}])

testcase='chain-example'
outFileName = ('/tmp/%s.tif' % testcase)
testValues.issueRequest(request2, testcase, 1, outFileName)

testlib.conclude()

sys.exit(testlib.gNFails)
