##################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
##################################################################################

import datetime
import testValues

gFLOAT_TOLERANCE = 1.0e-6

# Parameters of the product cube
gRES_X  = 0.05
gRES_Y  = -0.05
gORG_X  = 32.0
gORG_Y  = 20.0

# For pixel value calc do not use exact border of pixel but 
# use small offset value.
gEPS_X  = gRES_X / 1000.0
gEPS_Y  = gRES_Y / 1000.0

gPERIOD_1  = { 'periodStart' : datetime.date(2010, 03, 11), 'periodEnd' : datetime.date(2010, 03, 11) }
gPERIOD_2  = { 'periodStart' : datetime.date(1981, 01, 01), 'periodEnd' : datetime.date(1981, 01, 01) }
gPERIOD_3  = { 'periodStart' : datetime.date(2012, 02, 29), 'periodEnd' : datetime.date(2012, 02, 29) }
gPERIOD_4  = { 'periodStart' : datetime.date(2009, 02, 26), 'periodEnd' : datetime.date(2009, 02, 28) }
gPERIOD_5  = { 'periodStart' : datetime.date(2008, 02, 26), 'periodEnd' : datetime.date(2008, 02, 29) }
gPERIOD_6  = { 'periodStart' : datetime.date(2009, 02, 21), 'periodEnd' : datetime.date(2009, 03, 01) }
gPERIOD_7  = { 'periodStart' : datetime.date(2008, 02, 21), 'periodEnd' : datetime.date(2008, 03, 01) }
gPERIOD_8  = { 'periodStart' : datetime.date(2008, 12, 11), 'periodEnd' : datetime.date(2008, 12, 31) }
gPERIOD_9  = { 'periodStart' : datetime.date(2012, 12, 21), 'periodEnd' : datetime.date(2013, 01, 05) }
gPERIOD_10 = { 'periodStart' : datetime.date(2013, 01, 01), 'periodEnd' : datetime.date(2013, 12, 31) }
gPERIOD_11 = { 'periodStart' : datetime.date(2012, 01, 01), 'periodEnd' : datetime.date(2012, 12, 31) }
gPERIOD_12 = { 'periodStart' : datetime.date(1981, 01, 01), 'periodEnd' : datetime.date(2015, 12, 31) }
gPERIOD_13 = { 'periodStart' : datetime.date(2008, 01, 01), 'periodEnd' : datetime.date(2010, 01, 31) }

testValues.setNoDataValue(-9999.0)

def getMeasurementIntervalNext(date):
   sdate = testValues.getMeasurementIntervalStart(date)
   if sdate.day < 26: return sdate + datetime.timedelta(5)
   if sdate.month == 12: return datetime.date(sdate.year+1, 1, 1)
   return datetime.date(sdate.year, sdate.month+1, 1)

def getMeasurementIntervalEnd(date):
   sdate = testValues.getMeasurementIntervalStart(date)
   if sdate.day < 26: return sdate + datetime.timedelta(4)
   if sdate.month == 12: return datetime.date(sdate.year, 12, 31)
   return datetime.date(sdate.year, sdate.month+1, 1) - datetime.timedelta(1)

def getMeasurementIndex(periodStart, periodEnd):
   sidx = min(5,(periodStart.day - 1) / 5) + 6 * (periodStart.month-1)
   eidx = min(5,(periodEnd.day - 1) / 5) + 6 * (periodEnd.month-1)
   return (sidx, eidx)

def getMeasurementDoy(periodStart, periodEnd):
   sidx, eidx = getMeasurementIndex(periodStart, periodEnd)
   doy  =  [0, 5, 10, 15, 20, 25, 31, 36, 41, 46, 51, 56, 59, 64, 69, 74, 79, 84, 90, 95, 100, 105, 110, 115, 120, 125, 130, 135, 140, 145, 151, 156, 161, 166, 171, 176, 181, 186, 191, 196, 201, 206, 212, 217, 222, 227, 232, 237, 243, 248, 253, 258, 263, 268, 273, 278, 283, 288, 293, 298, 304, 309, 314, 319, 324, 329, 334, 339, 344, 349, 354, 359, 365]
   sdoy = doy[sidx]+1
   edoy = doy[eidx+1]
   return (sdoy, edoy)
