#!/usr/bin/python

##################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
##################################################################################

import re
import os
import sys
import json
import csv
import datetime

import numpy as np

from osgeo import gdal

import testlib

from testValues import *
from common import *

setGeoTransform(gORG_X, gRES_X, gEPS_X, gORG_Y, gRES_Y, gEPS_Y)

# Global script identifiers
svn_revision = str('$Rev: 7872 $')
svn_date = str('$Date: 2017-01-30 21:38:29 +0100 (Mon, 30 Jan 2017) $')
svn_author = str('$Author: jb76278 $')
script_file_name = os.path.basename(__file__)
now_string = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

# To make sure the whole array is printed.
np.set_printoptions(threshold='nan')

def checkTimeSeriesByDate(testcase, response, label, pointList, period, integrate='SUM', windowLength=0, refType='NONE', refStart=None, maskID='NONE'):

   iwindow = windowLength/5

   periodStartList = getPeriodStartList()

   ts = response['timeseries']
   rq = response['request']

   sd = getMeasurementIntervalStart(period[0])
   ed = getMeasurementIntervalStart(period[1])

   sdi = periodStartList.index(sd)
   edi = periodStartList.index(ed)

   # Compute number of intervals per year
   ii = [ i for i in range(0,len(periodStartList)) if periodStartList[i].month==1 and periodStartList[i].day==1 ]
   nipy = np.diff(np.array(ii))[0]

   refPeriod = period
   if edi - sdi + 1 > nipy:
      refPeriod = (sd, periodStartList[edi - nipy])

   if refStart <> None:
      rsd = getMeasurementIntervalStart(refStart)  # measurement period start for ref date
      rdi = periodStartList.index(rsd)            # index in period start list
      red = periodStartList[rdi+(edi-sdi)]
      refPeriod = (rsd, red)

   testlib.check(testcase, "type  ", ts['type'],  'TimeSeriesByDate')
   testlib.check(testcase, "label ", ts['label'], label)
   testlib.check(testcase, "N     ", ts['N'], edi-sdi+1)

   bs = []
   rv = []
   v  = []
   for p in pointList:
      bs.append(testValueBaseStats(p[0], p[1], refPeriod, 1981, 2015, integrate, iwindow))
      v.append(testValueList(p[0], p[1], period, period[0].year, period[1].year, integrate, iwindow)[0])
      if   refType == 'ABS':
         rv.append(testValueList(p[0], p[1], refPeriod, refPeriod[0].year, refPeriod[1].year, integrate, iwindow)[0])
      elif refType == 'LTA':
         rv.append(testValueLTA(p[0], p[1], refPeriod, 1981, 2015,  integrate, iwindow))

   bs = np.array(bs)
   v  = np.array(v)
   if refType <> 'NONE':
      rv = np.array(rv)
      v = v - rv

   v  = np.mean(v, axis=0)
   bs = np.mean(bs, axis=0)

   minv = np.min(v)
   maxv = np.max(v)

   for j in range(0, v.shape[0]):

      ms = periodStartList[sdi+j]
      me = getMeasurementIntervalEnd(ms)
      testlib.check(testcase, "ts-pstart-%02d" % j, ts['pstart'][j], ms.isoformat())
      testlib.check(testcase, "ts-pend-%02d  " % j, ts['pend'][j],   me.isoformat())
      if v[j] <> gNODATA_TEST: 
         testlib.check(testcase, "ts-value-%02d " % j, ts['value'][j],  v[j], gFLOAT_TOLERANCE)
      else:
         testlib.check(testcase, "ts-value-%02d " % j, ts['value'][j],  None)
      testlib.check(testcase, "ts-P00-%02d   " % j, ts['P00'][j],    bs[0][j%nipy], gFLOAT_TOLERANCE)
      testlib.check(testcase, "ts-P25-%02d   " % j, ts['P25'][j],    bs[1][j%nipy], gFLOAT_TOLERANCE)
      testlib.check(testcase, "ts-P50-%02d   " % j, ts['P50'][j],    bs[2][j%nipy], gFLOAT_TOLERANCE)
      testlib.check(testcase, "ts-P75-%02d   " % j, ts['P75'][j],    bs[3][j%nipy], gFLOAT_TOLERANCE)
      testlib.check(testcase, "ts-P100-%02d  " % j, ts['P100'][j],   bs[4][j%nipy], gFLOAT_TOLERANCE)
      testlib.check(testcase, "ts-Avg-%02d   " % j, ts['Avg'][j],    bs[5][j%nipy], gFLOAT_TOLERANCE)

   testlib.check(testcase, "ts-min-%02d   " % j, ts['min'], minv, gFLOAT_TOLERANCE)
   testlib.check(testcase, "ts-max-%02d   " % j, ts['max'], maxv, gFLOAT_TOLERANCE)

   # Check request info
   testlib.check(testcase, "rq-source     ", rq['source'],        "CHIRPS_5000m")
   testlib.check(testcase, "rq-indicator  ", rq['indicator'],     "TEST")
   testlib.check(testcase, "rq-maskID     ", rq['maskID'],        maskID)
   testlib.check(testcase, "rq-periodStart", rq['periodStart'],   period[0].isoformat())
   testlib.check(testcase, "rq-periodEnd  ", rq['periodEnd'],     period[1].isoformat())
   testlib.check(testcase, "rq-reftype    ", rq['referenceType'], refType)
   testlib.check(testcase, "rq-integrate  ", rq['integrateType'], integrate)
   testlib.check(testcase, "rq-window     ", rq['windowLength'],  windowLength)


#######################################
# Test cases 
#######################################

#-----------------------------------------------------------

def init():

   testcase = __file__ + "_0_"
   dates = getMeasurementIntervals(testcase, 'Precipitation:GetDates', 'CHIRPS_5000m', 'PRECIP_PRELIM')

   # Check all intervals
   for i in range(0, len(dates)-1):
      delta = dates[i+1] - dates[i]
      expected_delta = 5

      # Last measurement interval in month is not 5 days.
      year = dates[i].year
      ndate = dates[i+1]
      if   dates[i] == datetime.date(year, 12, 26):
          expected_delta = 6
      elif dates[i].day == 26:
         ndate = datetime.date(dates[i].year, dates[i].month+1, 1)
         expected_delta = (ndate - dates[i]).days

      testlib.check(testcase, "2 - %s" % dates[i], delta.days, expected_delta)

#-----------------------------------------------------------

def testcase_1( testcase = __file__ + "_1_", intType = 'SUM'):

   for y in range(1981, 2014):

      periodStart = datetime.date(y, gPERIOD_1['periodStart'].month, gPERIOD_1['periodStart'].day)
      periodEnd   = datetime.date(y, gPERIOD_1['periodEnd'].month, gPERIOD_1['periodEnd'].day)
      polygon     = open(gTEST_ROI_DIR + 'ROI-1.geojson', 'r').read()

      request = testlib.buildWpsRequest(
         process='Precipitation:TimeSeriesByDate',
         source='CHIRPS_5000m',
         ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
         indicator='TEST',
         periodStart=periodStart.isoformat(),
         periodEnd=periodEnd.isoformat(),
         integrateType=intType,
         label=testcase,
         response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

      outFileName = ('/tmp/%s.json' % testcase)
      issueRequest(request, testcase, 1, outFileName)
      result   = open(outFileName, 'r').read()
      response = json.loads(result)
      if testlib.getTrace() > 1: print json.dumps(response)

      lon         = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
      lat         = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]

      checkTimeSeriesByDate(testcase + str(y), response, testcase, [(lon, lat)], (periodStart, periodEnd), intType)

#-----------------------------------------------------------

def testcase_2( testcase = __file__ + "_2_", intType = 'SUM'):
   
   periodStart = gPERIOD_8['periodStart']
   periodEnd   = gPERIOD_8['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-2.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:TimeSeriesByDate',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      integrateType=intType,
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]

   checkTimeSeriesByDate(testcase, response, testcase, [(lon, lat)], (periodStart, periodEnd), intType)

#-----------------------------------------------------------

def testcase_3(testcase = __file__ + "_3_", intType = 'SUM'):

   periodStart = gPERIOD_9['periodStart']
   periodEnd   = gPERIOD_9['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-3.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:TimeSeriesByDate',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      integrateType=intType,
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]

   checkTimeSeriesByDate(testcase, response, testcase, [(lon, lat)], (periodStart, periodEnd), intType)

#-----------------------------------------------------------

def testcase_4(testcase = __file__ + "_4_", intType = 'SUM'):

   periodStart = gPERIOD_10['periodStart']
   periodEnd   = gPERIOD_10['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-4.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:TimeSeriesByDate',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      integrateType=intType,
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]

   checkTimeSeriesByDate(testcase, response, testcase, [(lon, lat)], (periodStart, periodEnd), intType)

#-----------------------------------------------------------

def testcase_5(testcase = __file__ + "_5_", intType = 'SUM'):

   periodStart = gPERIOD_11['periodStart']
   periodEnd   = gPERIOD_11['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-2.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:TimeSeriesByDate',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      integrateType=intType,
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]

   checkTimeSeriesByDate(testcase, response, testcase, [(lon, lat)], (periodStart, periodEnd), intType)

#-----------------------------------------------------------

def testcase_6(testcase = __file__ + "_6_", intType = 'SUM'):

   periodStart = gPERIOD_11['periodStart']
   periodEnd   = gPERIOD_11['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-1.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:TimeSeriesByDate',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      integrateType=intType,
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]

   checkTimeSeriesByDate(testcase, response, testcase, [(lon, lat)], (periodStart, periodEnd), intType)

#-----------------------------------------------------------

def testcase_7(testcase = __file__ + "_7_", intType = 'SUM'):

   periodStart = gPERIOD_10['periodStart']
   periodEnd   = gPERIOD_10['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-6.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:TimeSeriesByDate',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      integrateType=intType,
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon0 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0]
   lat0 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][1]
   lon1 = json.loads(polygon)['features'][0]['geometry']['coordinates'][1][0]
   lat1 = json.loads(polygon)['features'][0]['geometry']['coordinates'][1][1]

   checkTimeSeriesByDate(testcase, response, testcase, [(lon0, lat0), (lon1, lat1)], (periodStart, periodEnd), intType)

#-----------------------------------------------------------

def testcase_8(testcase = __file__ + "_8_", intType = 'SUM'):

   periodStart = gPERIOD_11['periodStart']
   periodEnd   = gPERIOD_11['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-8.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:TimeSeriesByDate',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      integrateType=intType,
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon0 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][0]
   lat0 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][1]
   lon1 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][1][0]
   lat1 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][1][1]

   checkTimeSeriesByDate(testcase, response, testcase, [(lon0, lat0), (lon1, lat1)], (periodStart, periodEnd), intType)

#-----------------------------------------------------------

def testcase_9(testcase = __file__ + "_9_", intType = 'SUM'):

   periodStart = gPERIOD_9['periodStart']
   periodEnd   = gPERIOD_9['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-8.geojson', 'r').read()
   refStart    = REFSTART_2(periodStart, 2) # This should change the returned base statistics to refStart
   refType     = 'NONE'

   request = testlib.buildWpsRequest(
      process='Precipitation:TimeSeriesByDate',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      referenceStart=refStart.isoformat(),
      referenceType=refType,
      integrateType=intType,
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon0 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][0]
   lat0 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][1]
   lon1 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][1][0]
   lat1 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][1][1]

   checkTimeSeriesByDate(testcase, response, testcase, [(lon0, lat0), (lon1, lat1)], (periodStart, periodEnd), intType, 0, refType, refStart)

#-----------------------------------------------------------

def testcase_10(testcase = __file__ + "_10_", intType = 'SUM'):

   periodStart = gPERIOD_11['periodStart']
   periodEnd   = gPERIOD_11['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-8.geojson', 'r').read()
   refStart    = REFSTART_1(periodStart)
   refType     = 'ABS'

   request = testlib.buildWpsRequest(
      process='Precipitation:TimeSeriesByDate',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      referenceStart=refStart.isoformat(),
      referenceType=refType,
      integrateType=intType,
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon0 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][0]
   lat0 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][1]
   lon1 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][1][0]
   lat1 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][1][1]

   checkTimeSeriesByDate(testcase, response, testcase, [(lon0, lat0), (lon1, lat1)], (periodStart, periodEnd), intType, 0, refType, refStart)

#-----------------------------------------------------------

def testcase_11(testcase = __file__ + "_11_", intType = 'SUM'):

   periodStart = gPERIOD_9['periodStart']
   periodEnd   = gPERIOD_9['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-8.geojson', 'r').read()
   refStart    = REFSTART_2(periodStart)
   refType     = 'ABS'

   request = testlib.buildWpsRequest(
      process='Precipitation:TimeSeriesByDate',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      referenceStart=refStart.isoformat(),
      referenceType=refType,
      integrateType=intType,
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon0 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][0]
   lat0 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][1]
   lon1 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][1][0]
   lat1 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][1][1]

   checkTimeSeriesByDate(testcase, response, testcase, [(lon0, lat0), (lon1, lat1)], (periodStart, periodEnd), intType, 0, refType, refStart)

#-----------------------------------------------------------

def testcase_12(testcase = __file__ + "_12_", intType = 'SUM'):

   periodStart = gPERIOD_9['periodStart']
   periodEnd   = gPERIOD_9['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-8.geojson', 'r').read()
   refStart    = REFSTART_3(periodStart)
   refType     = 'ABS'

   request = testlib.buildWpsRequest(
      process='Precipitation:TimeSeriesByDate',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      referenceStart=refStart.isoformat(),
      referenceType=refType,
      integrateType=intType,
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon0 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][0]
   lat0 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][1]
   lon1 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][1][0]
   lat1 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][1][1]

   checkTimeSeriesByDate(testcase, response, testcase, [(lon0, lat0), (lon1, lat1)], (periodStart, periodEnd), intType, 0, refType, refStart)

#-----------------------------------------------------------

def testcase_13(testcase = __file__ + "_13_", intType = 'SUM'):

   periodStart = gPERIOD_9['periodStart']
   periodEnd   = gPERIOD_9['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-8.geojson', 'r').read()
   refStart    = REFSTART_4(periodStart)
   refType     = 'LTA'

   request = testlib.buildWpsRequest(
      process='Precipitation:TimeSeriesByDate',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      referenceStart=refStart.isoformat(),
      referenceType=refType,
      integrateType=intType,
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon0 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][0]
   lat0 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][1]
   lon1 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][1][0]
   lat1 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][1][1]

   checkTimeSeriesByDate(testcase, response, testcase, [(lon0, lat0), (lon1, lat1)], (periodStart, periodEnd), intType, 0, refType, refStart)

#-----------------------------------------------------------

def testcase_14(testcase = __file__ + "_14_", intType = 'SUM'):

   periodStart = gPERIOD_9['periodStart']
   periodEnd   = gPERIOD_9['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-8.geojson', 'r').read()
   refStart    = REFSTART_5(periodStart)
   refType     = 'LTA'

   request = testlib.buildWpsRequest(
      process='Precipitation:TimeSeriesByDate',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      referenceStart=refStart.isoformat(),
      referenceType=refType,
      integrateType=intType,
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon0 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][0]
   lat0 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][1]
   lon1 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][1][0]
   lat1 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][1][1]

   checkTimeSeriesByDate(testcase, response, testcase, [(lon0, lat0), (lon1, lat1)], (periodStart, periodEnd), intType, 0, refType, refStart)

#-----------------------------------------------------------

def testcase_15(testcase = __file__ + "_15_", intType = 'SUM'):

   periodStart = gPERIOD_9['periodStart']
   periodEnd   = gPERIOD_9['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-8.geojson', 'r').read()
   refStart    = REFSTART_6(periodStart)
   refType     = 'LTA'
 
   request = testlib.buildWpsRequest(
      process='Precipitation:TimeSeriesByDate',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      referenceStart=refStart.isoformat(),
      referenceType=refType,
      integrateType=intType,
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon0 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][0]
   lat0 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][1]
   lon1 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][1][0]
   lat1 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][1][1]

   checkTimeSeriesByDate(testcase, response, testcase, [(lon0, lat0), (lon1, lat1)], (periodStart, periodEnd), intType, 0, refType, refStart)

#-----------------------------------------------------------

def testcase_16(testcase = __file__ + "_16_", intType = 'SUM'):

   periodStart = gPERIOD_8['periodStart']
   periodEnd   = gPERIOD_8['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-9.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:TimeSeriesByDate',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      integrateType=intType,
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   l = []
   for i in range(0, 3):
      lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][i][0]
      lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][i][1]
      l.append((lon, lat))

   checkTimeSeriesByDate(testcase, response, testcase, l, (periodStart, periodEnd), intType)

#-----------------------------------------------------------

def testcase_17(testcase = __file__ + "_17_", intType = 'SUM'):

   periodStart = gPERIOD_8['periodStart']
   periodEnd   = gPERIOD_8['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-9.geojson', 'r').read()
   maskID      = 'TEST'

   request = testlib.buildWpsRequest(
      process='Precipitation:TimeSeriesByDate',
      source='CHIRPS_5000m',
      maskID=maskID,
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      integrateType=intType,
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][1]

   # The other 2 pixels in the ROI have NoData due to the mask

   checkTimeSeriesByDate(testcase, response, testcase, [(lon, lat)], (periodStart, periodEnd), intType, 0, 'NONE', None, maskID)

#-----------------------------------------------------------

def testcase_18():
   testcase_1(__file__ + "_18_", 'NONE')

#-----------------------------------------------------------

def testcase_19():
   testcase_2(__file__ + "_19_", 'NONE')

#-----------------------------------------------------------

def testcase_20():
   testcase_3(__file__ + "_20_", 'NONE')

#-----------------------------------------------------------

def testcase_21():
   testcase_4(__file__ + "_21_", 'NONE')

#-----------------------------------------------------------

def testcase_22():
   testcase_5(__file__ + "_22_", 'NONE')

#-----------------------------------------------------------

def testcase_23():
   testcase_6(__file__ + "_23_", 'NONE')

#-----------------------------------------------------------

def testcase_24():
   testcase_7(__file__ + "_24_", 'NONE')

#-----------------------------------------------------------

def testcase_25():
   testcase_8(__file__ + "_25_", 'NONE')

#-----------------------------------------------------------

def testcase_26():
   testcase_9(__file__ + "_26_", 'NONE')

#-----------------------------------------------------------

def testcase_27():
   testcase_10(__file__ + "_27_", 'NONE')

#-----------------------------------------------------------

def testcase_28():
   testcase_11(__file__ + "_28_", 'NONE')

#-----------------------------------------------------------

def testcase_29():
   testcase_12(__file__ + "_29_", 'NONE')

#-----------------------------------------------------------

def testcase_30():
   testcase_13(__file__ + "_30_", 'NONE')

#-----------------------------------------------------------

def testcase_31():
   testcase_14(__file__ + "_31_", 'NONE')

#-----------------------------------------------------------

def testcase_32():
   testcase_15(__file__ + "_32_", 'NONE')

#-----------------------------------------------------------

def testcase_33():
   testcase_16(__file__ + "_33_", 'NONE')

#-----------------------------------------------------------

def testcase_34():
   testcase_17(__file__ + "_34_", 'NONE')

#-----------------------------------------------------------

def testcase_35(testcase = __file__ + "_35_"):

   periodStart = gPERIOD_10['periodStart']
   periodEnd   = gPERIOD_10['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-4.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:TimeSeriesByDate',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      # Default integration is SUM
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]

   # Default integration is SUM
   checkTimeSeriesByDate(testcase, response, testcase, [(lon, lat)], (periodStart, periodEnd), 'SUM')

#-----------------------------------------------------------

def testcase_36(testcase = __file__ + "_36_"):

   periodStart = gPERIOD_10['periodStart']
   periodEnd   = gPERIOD_10['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-4.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:TimeSeriesByDate',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      # Default integration is SUM
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/csv'}])

   outFileName = ('/tmp/%s.csv' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   reader = csv.reader(open(outFileName, 'r'))
   ts = {}; rq = {}
   for r in reader:
      if len(r) > 1 :
         if   r[0] in [ 'P00', 'P25', 'P50', 'P75', 'P100', 'Avg', 'value' ]: 
            ts[r[0]] = map(float, r[1:])
         elif r[0] in [ 'type', 'label' ]: 
            ts[r[0]] = r[1]
         elif r[0] in [ 'min', 'max' ]: 
            ts[r[0]] = float(r[1])
         elif r[0] in [ 'pstart', 'pend' ]: 
            ts[r[0]] = r[1:]
         elif r[0] in [ 'N' ]: 
            ts[r[0]] = int(r[1])
         elif r[0] in [ 'windowLength' ]: 
            rq[r[0]] = int(r[1])
         else:
            rq[r[0]] = r[1]
   response = { 'timeseries' : ts, 'request' : rq }
      
   if testlib.getTrace() > 1: print str(response)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]

   # Default integration is SUM
   checkTimeSeriesByDate(testcase, response, testcase, [(lon, lat)], (periodStart, periodEnd), 'SUM')

#-----------------------------------------------------------

def testcase_37(testcase = __file__ + "_37_"):

   periodStart   = gPERIOD_13['periodStart']
   periodEnd     = gPERIOD_13['periodEnd']
   polygon       = open(gTEST_ROI_DIR + 'ROI-4.geojson', 'r').read()
   integrateType = 'NONE'

   request = testlib.buildWpsRequest(
      process='Precipitation:TimeSeriesByDate',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      integrateType=integrateType,
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]

   # Default integration is NONE
   checkTimeSeriesByDate(testcase, response, testcase, [(lon, lat)], (periodStart, periodEnd), integrateType)

#-----------------------------------------------------------

def testcase_38(testcase = __file__ + "_38_"):

   periodStart   = gPERIOD_13['periodStart']
   periodEnd     = gPERIOD_13['periodEnd']
   polygon       = open(gTEST_ROI_DIR + 'ROI-4.geojson', 'r').read()
   integrateType = 'SUM'
   window        = 20

   request = testlib.buildWpsRequest(
      process='Precipitation:TimeSeriesByDate',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      integrateType=integrateType,
      windowLength=window,
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]

   # Default integration is NONE
   checkTimeSeriesByDate(testcase, response, testcase, [(lon, lat)], (periodStart, periodEnd), integrateType, windowLength=window)

#-----------------------------------------------------------

def testcase_39():

    testcase = __file__ + "_39_"

    periodStart = gPERIOD_9['periodStart']
    periodEnd   = gPERIOD_9['periodEnd']
    polygon     = open(gTEST_ROI_DIR + 'ROI-14.geojson', 'r').read()
    integrateType = 'SUM'

    request = testlib.buildWpsRequest(
        process='Precipitation:TimeSeriesByDate',
        source='CHIRPS_5000m',
        ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
        indicator='PRECIP_FINAL',
        periodStart=periodStart.isoformat(),
        periodEnd=periodEnd.isoformat(),
        integrateType=integrateType,
        label=testcase,
        response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

    outFileName = ('/tmp/%s.json' % testcase)
    issueRequest(request, testcase, 1, outFileName)
    result   = open(outFileName, 'r').read()
    response = json.loads(result)
    if testlib.getTrace() > 1: print json.dumps(response)

    # Check all values are non-negative and less than 1000
    ts     = response['timeseries']
    values = ts['value']
    for i in range(len(values)):
       testlib.check(testcase, "value-%d %f" % (i,values[i]), (values[i] >= 0.0 and values[i] < 1000.0), True)
    values = ts['P00']
    for i in range(len(values)):
       testlib.check(testcase, "P00-%d %f" % (i, values[i]), (values[i] >= 0.0 and values[i] < 1000.0), True)
    values = ts['P25']
    for i in range(len(values)):
       testlib.check(testcase, "P25-%d %f" % (i, values[i]), (values[i] >= 0.0 and values[i] < 1000.0), True)
    values = ts['P50']
    for i in range(len(values)):
       testlib.check(testcase, "P50-%d %f" % (i, values[i]), (values[i] >= 0.0 and values[i] < 1000.0), True)
    values = ts['P75']
    for i in range(len(values)):
       testlib.check(testcase, "P75-%d %f" % (i, values[i]), (values[i] >= 0.0 and values[i] < 1000.0), True)
    values = ts['P100']
    for i in range(len(values)):
       testlib.check(testcase, "P100-%d %f" % (i, values[i]), (values[i] >= 0.0 and values[i] < 1000.0), True)
    values = ts['Avg']
    for i in range(len(values)):
       testlib.check(testcase, "Avg-%d %f" % (i, values[i]), (values[i] >= 0.0 and values[i] < 1000.0), True)

#-----------------------------------------------------------

def testcase_40():

    testcase = __file__ + "_40_"

    periodStart = gPERIOD_9['periodStart']
    periodEnd   = gPERIOD_9['periodEnd']
    polygon     = open(gTEST_ROI_DIR + 'ROI-15.geojson', 'r').read()
    integrateType = 'SUM'
    aggregateType = 'MAX'

    request = testlib.buildWpsRequest(
        process='Precipitation:TimeSeriesByDate',
        source='CHIRPS_5000m',
        ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
        indicator='PRECIP_FINAL',
        periodStart=periodStart.isoformat(),
        periodEnd=periodEnd.isoformat(),
        integrateType=integrateType,
        label=testcase,
        response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

    outFileName = ('/tmp/%s.json' % testcase)
    issueRequest(request, testcase, 1, outFileName)
    result   = open(outFileName, 'r').read()
    response = json.loads(result)
    if testlib.getTrace() > 1: print json.dumps(response)

    # Check all values are nan
    ts     = response['timeseries']
    values = ts['value']
    for i in range(len(values)):
       testlib.check(testcase, "value-%d %f" % (i,values[i]),  math.isnan(values[i]), True)
       testlib.check(testcase, "P00 %f " % (ts['P00'][i]),  math.isnan(ts['P00'][i]),  True)
       testlib.check(testcase, "P25 %f " % (ts['P25'][i]),  math.isnan(ts['P25'][i]),  True)
       testlib.check(testcase, "P50 %f " % (ts['P50'][i]),  math.isnan(ts['P50'][i]),  True)
       testlib.check(testcase, "P75 %f " % (ts['P75'][i]),  math.isnan(ts['P75'][i]),  True)
       testlib.check(testcase, "P100 %f" % (ts['P100'][i]), math.isnan(ts['P100'][i]), True)
       testlib.check(testcase, "Avg %f " % (ts['Avg'][i]),  math.isnan(ts['Avg'][i]),  True)
    testlib.check(testcase, "min %f " % (ts['min']),  math.isnan(ts['min']),  True)
    testlib.check(testcase, "max %f " % (ts['max']),  math.isnan(ts['max']),  True)

#-----------------------------------------------------------

def testcase_41():

   testcase = __file__ + "_41_"

   periodStart = gPERIOD_11['periodStart']
   periodEnd   = gPERIOD_11['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-2.geojson', 'r').read()
   intType     = 'SUM'
   ninterval   = 3
   window      = ninterval*5

   request = testlib.buildWpsRequest(
      process='Precipitation:TimeSeriesByDate',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      integrateType=intType,
      windowLength=window,
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]

   checkTimeSeriesByDate(testcase, response, testcase, [(lon, lat)], (periodStart, periodEnd), intType, windowLength=window)

#-----------------------------------------------------------

def datatest_1(testcase = __file__ + "datatest_1_", intType = 'SUM'):

   periodStart = gPERIOD_1['periodStart']
   periodEnd   = gPERIOD_1['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-Afar-Ewa.geojson', 'r').read()
   refStart    = REFSTART_1(periodStart)
   refType     = 'LTA'

   request = testlib.buildWpsRequest(
      process='Precipitation:TimeSeriesByDate',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='PRECIP_FINAL',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      referenceStart=refStart.isoformat(),
      referenceType=refType,
      integrateType=intType,
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

#-----------------------------------------------------------

def datatest_2(testcase = __file__ + "datatest_2_", intType = 'SUM'):

   periodStart = gPERIOD_1['periodStart']
   periodEnd   = gPERIOD_1['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-Afar-Ewa.geojson', 'r').read()
   refStart    = REFSTART_1(periodStart)
   refType     = 'LTA'

   request = testlib.buildWpsRequest(
      process='Precipitation:TimeSeriesByDate',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='PRECIP_PRELIM',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      referenceStart=refStart.isoformat(),
      referenceType=refType,
      integrateType=intType,
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

#--------------------------- end of testcases --------------------------------------
# Get all functions names testcase_<nr> as default test cases
testCaseIds = [ x for x in dir() if re.match('.+_[0-9]+', x) ]
testCaseIds.sort()
testcases = {}
for testCaseId in testCaseIds:
   testcases[testCaseId] = locals()[testCaseId]
init()
testlib.main(testcases)
