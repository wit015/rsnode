#!/usr/bin/python

##################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2015-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
##################################################################################

import re
import os
import sys
import json
import datetime
import traceback
import hashlib

import numpy as np

from osgeo import gdal

import testlib
import signatures

from testValues import *
from common import *

setGeoTransform(gORG_X, gRES_X, gEPS_X, gORG_Y, gRES_Y, gEPS_Y)

# Global script identifiers
svn_revision = str('$Rev: 7060 $')
svn_date = str('$Date: 2016-06-28 08:20:26 +0200 (Tue, 28 Jun 2016) $')
svn_author = str('$Author: jb76278 $')
script_file_name = os.path.basename(__file__)
now_string = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

# To make sure the whole array is printed.
np.set_printoptions(threshold='nan')

def checkInterval(testcase, response, origin, pointList, period, integrate='SUM', aggregate='NONE', window=0, threshold=0.0, statistic='VALUE', mask=None):

   (ox, oy) = world2pix(origin[0], origin[1])

   periodStartList = getPeriodStartList()

   sd = getMeasurementIntervalStart(period[0])
   ed = getMeasurementIntervalStart(period[1])

   sdi = periodStartList.index(sd)
   edi = periodStartList.index(ed)
   startDates = periodStartList[sdi:edi+1]
   startDates = startDates + [getMeasurementIntervalNext(startDates[-1])]
   startDates = np.array(startDates)
   durations = startDates - startDates[0]
   durations = np.append(durations[1:], durations[0])

   if   window == 0 and integrate == 'SUM':
      niWindow = (getMeasurementIntervalEnd(ed) - sd).days
   elif window == 0 and integrate == 'DIF':
      niWindow = 1
   else:
      niWindow = max(1, (window+4) / 5)

   sl = { 'VALUE' : 0, 'P00' : 0 , 'P25' : 1, 'P50' : 2, 'P75' : 3, 'P100' : 4 }
   if   statistic not in sl :
      testlib.check(testcase, "statistic %s not in %s" % (statistic, sl), True, False)
      return

   for p in pointList:
      if statistic == 'VALUE':
         v  = testValueList(p[0], p[1], period, period[0].year, period[1].year, 'NONE')
      else:
         v  = testValueList(p[0], p[1], period, 1981, 2015, 'NONE')
      r = np.array(v)

      if integrate <> 'NONE':
         cWindow = np.ones(niWindow)
         if integrate == 'DIF': 
            r = np.pad(v, ((0,0), (1,0), (0,0)), mode='edge')
            r = np.diff(r, axis=1)
         r = np.apply_along_axis(np.convolve, 1, r, cWindow, mode='full')
         r = r[:,:durations.shape[0]-1]
      
      if aggregate == 'THR':
         if threshold <= 0.0:
            b = r <= threshold
         else:
            b = r >= threshold
         b = np.pad(b, ((0,0), (0,1)), mode='constant', constant_values=True)
         idx = np.nanargmax(b, axis=1)
         #print r, b, idx
      elif aggregate == 'MIN':
         r = np.pad(r, ((0,0), (0,1)), mode='constant', constant_values=np.inf)
         idx = np.nanargmin(r, axis=1)
         #print r, idx
      else:
         r = np.pad(r, ((0,0), (0,1)), mode='constant', constant_values=-np.inf)
         idx = np.nanargmax(r, axis=1)
         #print r, idx

      #print idx

      si  = sl[statistic]
      if statistic  == 'VALUE' :
         idx = idx[si]
      else :
         idx  = np.percentile(idx, [0, 25, 50, 75, 100], interpolation='nearest')
         #print idx
         idx  = idx[si]

      nDays = durations[idx].days

      (x, y) = world2pix(p[0], p[1])
      x = x - ox; y = y - oy
      
      if mask is not None and mask[y, x] : nDays = 0

      testlib.check(testcase, "p lon=%f lat=%f (%d, %d)" % (p[0], p[1], x, y), response[y, x], nDays)

#######################################
# Test cases 
#######################################

#----------------------------------------------------------------

def init():

   testcase = __file__ + "_0_"
   dates = getMeasurementIntervals(testcase, 'Precipitation:GetDates', 'CHIRPS_5000m', 'PRECIP_PRELIM')

   # Check all intervals 
   for i in range(0, len(dates)-1):
      delta = dates[i+1] - dates[i]
      expected_delta = 5

      # Last measurement interval in month is not 10 days.
      year = dates[i].year
      ndate = dates[i+1]
      if   dates[i] == datetime.date(year, 12, 26): 
          expected_delta = 6
      elif dates[i].day == 26:
         ndate = datetime.date(dates[i].year, dates[i].month+1, 1)
         expected_delta = (ndate - dates[i]).days

      testlib.check(testcase, "2 - %s" % dates[i], delta.days, expected_delta)


#----------------------------------------------------------------

def _testcase_1(testcase, period, yrange=range(2000, 2016)):

   dy = period['periodEnd'].year - period['periodStart'].year 

   for y in yrange :

      periodStart  = datetime.date(y, period['periodStart'].month, period['periodStart'].day)
      periodEnd    = datetime.date(y+dy, period['periodEnd'].month, period['periodEnd'].day)
      polygon      = open(gTEST_ROI_DIR + 'ROI-1.geojson', 'r').read()
      integrate    = 'SUM'
      aggregate    = 'THR'
      threshold    = 1000.0
      window       = 0

      request = testlib.buildWpsRequest(
         process='Precipitation:IntervalMap',
         source='CHIRPS_5000m',
         ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
         indicator='TEST',
         # integrateType=integrate, SUM is default
         aggregateType=aggregate,
         threshold=threshold,
         # windowLength=window, 0 is default
         periodStart=str(periodStart),
         periodEnd=str(periodEnd),
         label=testcase,
         response=['geotiff', 'RawDataOutput', { 'mimeType' : 'image/tif'}])

      outFileName = ('/tmp/%s.tif' % testcase)
      issueRequest(request, testcase, 1, outFileName)
      data = getImageData(outFileName)
      testlib.check(testcase, 1, data.shape, (1,1))

      lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
      lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
      p = (periodStart, periodEnd)
      checkInterval(testcase, data, (lon, lat), [(lon, lat)], p, integrate, aggregate, window, threshold)

#----------------------------------------------------------------

def testcase_1():

   testcase = __file__ + "_1_"
   _testcase_1(testcase, gPERIOD_1)

#----------------------------------------------------------------

def testcase_2():

   testcase = __file__ + "_2_"
   _testcase_1(testcase, gPERIOD_2)
   
#----------------------------------------------------------------

def testcase_3():

   testcase = __file__ + "_3_"
   _testcase_1(testcase, gPERIOD_3, range(2000,2017,4))
   
#----------------------------------------------------------------

def testcase_4():

   testcase = __file__ + "_4_"
   _testcase_1(testcase, gPERIOD_4)

#----------------------------------------------------------------

def testcase_5():

   testcase = __file__ + "_5_"
   _testcase_1(testcase, gPERIOD_5, range(2000,2017,4))
   
#----------------------------------------------------------------

def testcase_6():

   testcase = __file__ + "_6_"
   _testcase_1(testcase, gPERIOD_6)

#----------------------------------------------------------------

def testcase_7():

   testcase = __file__ + "_7_"
   _testcase_1(testcase, gPERIOD_7)

#----------------------------------------------------------------

def testcase_8():

   testcase = __file__ + "_8_"
   _testcase_1(testcase, gPERIOD_8)

#----------------------------------------------------------------

def testcase_9():

   testcase = __file__ + "_9_"
   _testcase_1(testcase, gPERIOD_9)

#----------------------------------------------------------------

def testcase_10():

   testcase = __file__ + "_10_"
   _testcase_1(testcase, gPERIOD_10)

#----------------------------------------------------------------

def testcase_11():

   testcase = __file__ + "_11_"
   _testcase_1(testcase, gPERIOD_11)

#----------------------------------------------------------------

def _testcase_100(testcase, period, yrange=range(2000, 2016)):

   dy = period['periodEnd'].year - period['periodStart'].year

   for y in yrange :

      periodStart  = datetime.date(y, period['periodStart'].month, period['periodStart'].day)
      periodEnd    = datetime.date(y+dy, period['periodEnd'].month, period['periodEnd'].day)
      polygon      = open(gTEST_ROI_DIR + 'ROI-1.geojson', 'r').read()
      integrate    = 'SUM'
      aggregate    = 'THR'
      threshold    = 1000.0
      window       = 15

      request = testlib.buildWpsRequest(
         process='Precipitation:IntervalMap',
         source='CHIRPS_5000m',
         ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
         indicator='TEST',
         # integrateType=integrate, SUM is default
         aggregateType=aggregate,
         threshold=threshold,
         windowLength=window,
         periodStart=str(periodStart),
         periodEnd=str(periodEnd),
         label=testcase,
        response=['geotiff', 'RawDataOutput', { 'mimeType' : 'image/tif'}])

      outFileName = ('/tmp/%s.tif' % testcase)
      issueRequest(request, testcase, 1, outFileName)
      data = getImageData(outFileName)
      testlib.check(testcase, 1, data.shape, (1,1))

      lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
      lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
      p = (periodStart, periodEnd)
      checkInterval(testcase, data, (lon, lat), [(lon, lat)], p, integrate, aggregate, window, threshold)

#----------------------------------------------------------------

def testcase_101():

   testcase = __file__ + "_101_"
   _testcase_100(testcase, gPERIOD_1)

#----------------------------------------------------------------

def testcase_102():

   testcase = __file__ + "_102_"
   _testcase_100(testcase, gPERIOD_2)

#----------------------------------------------------------------

def testcase_103():

   testcase = __file__ + "_103_"
   _testcase_100(testcase, gPERIOD_3, range(2000,2017,4))

#----------------------------------------------------------------

def testcase_104():

   testcase = __file__ + "_104_"
   _testcase_100(testcase, gPERIOD_4)

#----------------------------------------------------------------

def testcase_105():

   testcase = __file__ + "_105_"
   _testcase_100(testcase, gPERIOD_5, range(2000,2017,4))

#----------------------------------------------------------------

def testcase_106():

   testcase = __file__ + "_106_"
   _testcase_100(testcase, gPERIOD_6)

#----------------------------------------------------------------

def testcase_107():

   testcase = __file__ + "_107_"
   _testcase_100(testcase, gPERIOD_7)

#----------------------------------------------------------------

def testcase_108():

   testcase = __file__ + "_108_"
   _testcase_100(testcase, gPERIOD_8)

#----------------------------------------------------------------

def testcase_109():

   testcase = __file__ + "_109_"
   _testcase_100(testcase, gPERIOD_9)

#----------------------------------------------------------------

def testcase_110():

   testcase = __file__ + "_110_"
   _testcase_100(testcase, gPERIOD_10)

#----------------------------------------------------------------

def testcase_111():

   testcase = __file__ + "_111_"
   _testcase_100(testcase, gPERIOD_11)


#----------------------------------------------------------------

def _testcase_200(testcase, period, yrange=range(2000, 2016)):

   dy = period['periodEnd'].year - period['periodStart'].year

   for y in yrange :

      periodStart  = datetime.date(y, period['periodStart'].month, period['periodStart'].day)
      periodEnd    = datetime.date(y+dy, period['periodEnd'].month, period['periodEnd'].day)
      polygon      = open(gTEST_ROI_DIR + 'ROI-1.geojson', 'r').read()
      integrate    = 'SUM'
      aggregate    = 'MAX'
      window       = 15

      request = testlib.buildWpsRequest(
         process='Precipitation:IntervalMap',
         source='CHIRPS_5000m',
         ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
         indicator='TEST',
         # integrateType=integrate, SUM is default
         # aggregateType=aggregate, MAX is default
         windowLength=window,
         periodStart=str(periodStart),
         periodEnd=str(periodEnd),
         label=testcase,
        response=['geotiff', 'RawDataOutput', { 'mimeType' : 'image/tif'}])

      outFileName = ('/tmp/%s.tif' % testcase)
      issueRequest(request, testcase, 1, outFileName)
      data = getImageData(outFileName)
      testlib.check(testcase, 1, data.shape, (1,1))

      lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
      lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
      p = (periodStart, periodEnd)
      checkInterval(testcase, data, (lon, lat), [(lon, lat)], p, integrate, aggregate, window)

#----------------------------------------------------------------

def testcase_201():

   testcase = __file__ + "_201_"
   _testcase_200(testcase, gPERIOD_1)

#----------------------------------------------------------------

def testcase_202():

   testcase = __file__ + "_202_"
   _testcase_200(testcase, gPERIOD_2)

#----------------------------------------------------------------

def testcase_203():

   testcase = __file__ + "_203_"
   _testcase_200(testcase, gPERIOD_3, range(2000,2017,4))

#----------------------------------------------------------------

def testcase_204():

   testcase = __file__ + "_204_"
   _testcase_200(testcase, gPERIOD_4)

#----------------------------------------------------------------

def testcase_205():

   testcase = __file__ + "_205_"
   _testcase_200(testcase, gPERIOD_5, range(2000,2017,4))

#----------------------------------------------------------------

def testcase_206():

   testcase = __file__ + "_206_"
   _testcase_200(testcase, gPERIOD_6)

#----------------------------------------------------------------

def testcase_207():

   testcase = __file__ + "_207_"
   _testcase_200(testcase, gPERIOD_7)

#----------------------------------------------------------------

def testcase_208():

   testcase = __file__ + "_208_"
   _testcase_200(testcase, gPERIOD_8)

#----------------------------------------------------------------

def testcase_209():

   testcase = __file__ + "_209_"
   _testcase_200(testcase, gPERIOD_9)

#----------------------------------------------------------------

def testcase_210():

   testcase = __file__ + "_210_"
   _testcase_200(testcase, gPERIOD_10)

#----------------------------------------------------------------

def testcase_211():

   testcase = __file__ + "_211_"
   _testcase_200(testcase, gPERIOD_11)


#----------------------------------------------------------------

def _testcase_300(testcase, period, yrange=range(2000, 2016)):

   dy = period['periodEnd'].year - period['periodStart'].year

   for y in yrange :

      periodStart  = datetime.date(y, period['periodStart'].month, period['periodStart'].day)
      periodEnd    = datetime.date(y+dy, period['periodEnd'].month, period['periodEnd'].day)
      polygon      = open(gTEST_ROI_DIR + 'ROI-1.geojson', 'r').read()
      integrate    = 'NONE'
      aggregate    = 'MAX'
      window       = 15

      request = testlib.buildWpsRequest(
         process='Precipitation:IntervalMap',
         source='CHIRPS_5000m',
         ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
         indicator='TEST',
         integrateType=integrate,
         # aggregateType=aggregate, MAX is default
         windowLength=window,
         periodStart=str(periodStart),
         periodEnd=str(periodEnd),
         label=testcase,
        response=['geotiff', 'RawDataOutput', { 'mimeType' : 'image/tif'}])

      outFileName = ('/tmp/%s.tif' % testcase)
      issueRequest(request, testcase, 1, outFileName)
      data = getImageData(outFileName)
      testlib.check(testcase, 1, data.shape, (1,1))

      lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
      lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
      p = (periodStart, periodEnd)
      checkInterval(testcase, data, (lon, lat), [(lon, lat)], p, integrate, aggregate, window)

#----------------------------------------------------------------

def testcase_301():

   testcase = __file__ + "_301_"
   _testcase_300(testcase, gPERIOD_1)

#----------------------------------------------------------------

def testcase_302():

   testcase = __file__ + "_302_"
   _testcase_300(testcase, gPERIOD_2)

#----------------------------------------------------------------

def testcase_303():

   testcase = __file__ + "_303_"
   _testcase_300(testcase, gPERIOD_3, range(2000,2017,4))

#----------------------------------------------------------------

def testcase_304():

   testcase = __file__ + "_304_"
   _testcase_300(testcase, gPERIOD_4)

#----------------------------------------------------------------

def testcase_305():

   testcase = __file__ + "_305_"
   _testcase_300(testcase, gPERIOD_5, range(2000,2017,4))

#----------------------------------------------------------------

def testcase_306():

   testcase = __file__ + "_306_"
   _testcase_300(testcase, gPERIOD_6)

#----------------------------------------------------------------

def testcase_307():

   testcase = __file__ + "_307_"
   _testcase_300(testcase, gPERIOD_7)

#----------------------------------------------------------------

def testcase_308():

   testcase = __file__ + "_308_"
   _testcase_300(testcase, gPERIOD_8)

#----------------------------------------------------------------

def testcase_309():

   testcase = __file__ + "_309_"
   _testcase_300(testcase, gPERIOD_9)

#----------------------------------------------------------------

def testcase_310():

   testcase = __file__ + "_310_"
   _testcase_300(testcase, gPERIOD_10)

#----------------------------------------------------------------

def testcase_311():

   testcase = __file__ + "_311_"
   _testcase_300(testcase, gPERIOD_11)


#----------------------------------------------------------------

def _testcase_400(testcase, period, yrange=range(2000, 2016)):

   dy = period['periodEnd'].year - period['periodStart'].year

   for y in yrange :

      periodStart  = datetime.date(y, period['periodStart'].month, period['periodStart'].day)
      periodEnd    = datetime.date(y+dy, period['periodEnd'].month, period['periodEnd'].day)
      polygon      = open(gTEST_ROI_DIR + 'ROI-1.geojson', 'r').read()
      integrate    = 'NONE'
      aggregate    = 'MIN'
      window       = 15

      request = testlib.buildWpsRequest(
         process='Precipitation:IntervalMap',
         source='CHIRPS_5000m',
         ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
         indicator='TEST',
         integrateType=integrate,
         aggregateType=aggregate,
         windowLength=window,
         periodStart=str(periodStart),
         periodEnd=str(periodEnd),
         label=testcase,
        response=['geotiff', 'RawDataOutput', { 'mimeType' : 'image/tif'}])

      outFileName = ('/tmp/%s.tif' % testcase)
      issueRequest(request, testcase, 1, outFileName)
      data = getImageData(outFileName)
      testlib.check(testcase, 1, data.shape, (1,1))

      lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
      lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
      p = (periodStart, periodEnd)
      checkInterval(testcase, data, (lon, lat), [(lon, lat)], p, integrate, aggregate, window)

#----------------------------------------------------------------

def testcase_401():

   testcase = __file__ + "_401_"
   _testcase_400(testcase, gPERIOD_1)

#----------------------------------------------------------------

def testcase_402():

   testcase = __file__ + "_402_"
   _testcase_400(testcase, gPERIOD_2)

#----------------------------------------------------------------

def testcase_403():

   testcase = __file__ + "_403_"
   _testcase_400(testcase, gPERIOD_3, range(2000,2017,4))

#----------------------------------------------------------------

def testcase_404():

   testcase = __file__ + "_404_"
   _testcase_400(testcase, gPERIOD_4)

#----------------------------------------------------------------

def testcase_405():

   testcase = __file__ + "_405_"
   _testcase_400(testcase, gPERIOD_5, range(2000,2017,4))

#----------------------------------------------------------------

def testcase_406():

   testcase = __file__ + "_406_"
   _testcase_400(testcase, gPERIOD_6)

#----------------------------------------------------------------

def testcase_407():

   testcase = __file__ + "_407_"
   _testcase_400(testcase, gPERIOD_7)

#----------------------------------------------------------------

def testcase_408():

   testcase = __file__ + "_408_"
   _testcase_400(testcase, gPERIOD_8)

#----------------------------------------------------------------

def testcase_409():

   testcase = __file__ + "_409_"
   _testcase_400(testcase, gPERIOD_9)

#----------------------------------------------------------------

def testcase_410():

   testcase = __file__ + "_410_"
   _testcase_400(testcase, gPERIOD_10)

#----------------------------------------------------------------

def testcase_411():

   testcase = __file__ + "_411_"
   _testcase_400(testcase, gPERIOD_11)


#----------------------------------------------------------------

def testcase_501():

   testcase = __file__ + "_501_"

   periodStart  = gPERIOD_10['periodStart']
   periodEnd    = gPERIOD_10['periodEnd']
   polygon      = open(gTEST_ROI_DIR + 'ROI-12.geojson', 'r').read()
   integrate    = 'NONE'
   aggregate    = 'MAX'
   statistic    = 'P00'
   window       = 0 # not used
   threshold    = 0 # not used
   maskID       = "TEST"

   request = testlib.buildWpsRequest(
      process='Precipitation:IntervalMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      maskID=maskID,
      indicator='TEST',
      integrateType=integrate,
      aggregateType=aggregate,
      # windowLength=window, 0 is default
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      statistic=statistic,
      label=testcase,
     response=['geotiff', 'RawDataOutput', { 'mimeType' : 'image/tif'}])

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)
   testlib.check(testcase, 1, data.shape, (10,10))

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][1]

   pointlist = []
   nX = 10; nY = 10
   for x in range(0, nX):
      for y in range(0, nY):
            pointlist.append((lon+x*gRES_X, lat+y*gRES_Y))

   period = (periodStart, periodEnd)
   #pointlist = [pointlist[0]]

   # Generate mask
   mask = np.array([False]*100).reshape(10,10)
   mask[0,:] = True

   checkInterval(testcase, data, (lon, lat), pointlist, period, integrate, aggregate, window, threshold, statistic, mask)


#----------------------------------------------------------------

def testcase_502():

   testcase = __file__ + "_502_"

   periodStart  = gPERIOD_10['periodStart']
   periodEnd    = gPERIOD_10['periodEnd']
   polygon      = open(gTEST_ROI_DIR + 'ROI-12.geojson', 'r').read()
   integrate    = 'SUM'
   aggregate    = 'THR'
   threshold    = 50000.0
   window       = 0
   statistic    = 'P25'

   request = testlib.buildWpsRequest(
      process='Precipitation:IntervalMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      # integrateType=integrate, SUM is default
      aggregateType=aggregate,
      threshold=threshold,
      # windowLength=window, 0 is default
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      statistic=statistic,
      label=testcase,
     response=['geotiff', 'RawDataOutput', { 'mimeType' : 'image/tif'}])

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)
   testlib.check(testcase, 1, data.shape, (10,10))

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][1]

   pointlist = []
   nX = 10; nY = 10
   for x in range(0, nX):
      for y in range(0, nY):
            pointlist.append((lon+x*gRES_X, lat+y*gRES_Y))

   period = (periodStart, periodEnd)
   checkInterval(testcase, data, (lon, lat), pointlist, period, integrate, aggregate, window, threshold, statistic)

#----------------------------------------------------------------

def testcase_503():

   testcase = __file__ + "_503_"

   periodStart  = gPERIOD_10['periodStart']
   periodEnd    = gPERIOD_10['periodEnd']
   polygon      = open(gTEST_ROI_DIR + 'ROI-12.geojson', 'r').read()
   integrate    = 'SUM'
   aggregate    = 'THR'
   threshold    = 3000.0
   window       = 6
   statistic    = 'P50'

   request = testlib.buildWpsRequest(
      process='Precipitation:IntervalMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      # integrateType=integrate, SUM is default
      aggregateType=aggregate,
      threshold=threshold,
      windowLength=window,
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      statistic=statistic,
      label=testcase,
     response=['geotiff', 'RawDataOutput', { 'mimeType' : 'image/tif'}])

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)
   testlib.check(testcase, 1, data.shape, (10,10))

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][1]

   pointlist = []
   nX = 10; nY = 10
   for x in range(0, nX):
      for y in range(0, nY):
            pointlist.append((lon+x*gRES_X, lat+y*gRES_Y))

   period = (periodStart, periodEnd)
   checkInterval(testcase, data, (lon, lat), pointlist, period, integrate, aggregate, window, threshold, statistic)


#----------------------------------------------------------------

def testcase_504():

   testcase = __file__ + "_504_"

   periodStart  = gPERIOD_10['periodStart']
   periodEnd    = gPERIOD_10['periodEnd']
   polygon      = open(gTEST_ROI_DIR + 'ROI-12.geojson', 'r').read()
   integrate    = 'SUM'
   aggregate    = 'THR'
   threshold    = 3000.0
   window       = 11
   statistic    = 'P75'

   request = testlib.buildWpsRequest(
      process='Precipitation:IntervalMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      # integrateType=integrate, SUM is default
      aggregateType=aggregate,
      threshold=threshold,
      windowLength=window,
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      statistic=statistic,
      label=testcase,
     response=['geotiff', 'RawDataOutput', { 'mimeType' : 'image/tif'}])

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)
   testlib.check(testcase, 1, data.shape, (10,10))

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][1]

   pointlist = []
   nX = 10; nY = 10
   for x in range(0, nX):
      for y in range(0, nY):
            pointlist.append((lon+x*gRES_X, lat+y*gRES_Y))

   period = (periodStart, periodEnd)
   checkInterval(testcase, data, (lon, lat), pointlist, period, integrate, aggregate, window, threshold, statistic)


#----------------------------------------------------------------

def testcase_505():

   testcase = __file__ + "_505_"

   periodStart  = gPERIOD_10['periodStart']
   periodEnd    = gPERIOD_10['periodEnd']
   polygon      = open(gTEST_ROI_DIR + 'ROI-12.geojson', 'r').read()
   integrate    = 'NONE'
   aggregate    = 'MIN'
   statistic    = 'P100'
   window       = 0 # not used
   threshold    = 0 # not used
   maskID       = "TEST"

   request = testlib.buildWpsRequest(
      process='Precipitation:IntervalMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      maskID=maskID,
      indicator='TEST',
      integrateType=integrate,
      aggregateType=aggregate,
      # windowLength=window, 0 is default
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      statistic=statistic,
      label=testcase,
     response=['geotiff', 'RawDataOutput', { 'mimeType' : 'image/tif'}])

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)
   testlib.check(testcase, 1, data.shape, (10,10))

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][1]

   pointlist = []
   nX = 10; nY = 10
   for x in range(0, nX):
      for y in range(0, nY):
            pointlist.append((lon+x*gRES_X, lat+y*gRES_Y))

   period = (periodStart, periodEnd)
   #pointlist = [pointlist[0]]

   # Generate mask
   mask = np.array([False]*100).reshape(10,10)
   mask[0,:] = True

   checkInterval(testcase, data, (lon, lat), pointlist, period, integrate, aggregate, window, threshold, statistic, mask)

#----------------------------------------------------------------

def testcase_506():

   # Check correspondence between IntervalMap and TimeSeriesDate
   testcase = __file__ + "_506_"

   periodStart = gPERIOD_11['periodStart']
   periodEnd   = gPERIOD_11['periodEnd']
   polygon      = open(gTEST_ROI_DIR + 'ROI-1.geojson', 'r').read()
   integrate    = 'NONE'
   aggregate    = 'MAX'

   request = testlib.buildWpsRequest(
      process='Precipitation:TimeSeriesByDate',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='PRECIP_PRELIM',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      integrateType=integrate,
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   ts       = json.loads(result)['timeseries']

   statistic = 'VALUE'

   request = testlib.buildWpsRequest(
      process='Precipitation:IntervalMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='PRECIP_FINAL',
      integrateType=integrate,
      # aggregateType=aggregate, MAX is default
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      statistic=statistic,
      label=testcase,
     response=['geotiff', 'RawDataOutput', { 'mimeType' : 'image/tif'}])

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)
   testlib.check(testcase, 1, data.shape, (1,1))

   indx     = int(np.argmax(ts['value']))
   ds       = datetime.datetime.strptime(ts['pstart'][0], "%Y-%m-%d").date()
   de       = datetime.datetime.strptime(ts['pend'][indx], "%Y-%m-%d").date()
   ndays    = (de - ds).days + 1

   testlib.check( testcase, "value %s %s %d %d %d" % (ds, de, indx, ndays, data[0,0]), data[0, 0], ndays)

#----------------------------------------------------------------

def testcase_507():

   # Check correspondence between IntervalMap and TimeSeriesDate
   testcase = __file__ + "_507_"

   periodStart = gPERIOD_11['periodStart']
   periodEnd   = gPERIOD_11['periodEnd']
   polygon      = open(gTEST_ROI_DIR + 'ROI-3.geojson', 'r').read()
   integrate    = 'SUM'
   aggregate    = 'MAX'
   ninterval    = 6
   window       = ninterval * 5
   maskID       = 'ESA_CCI_2010'

   request = testlib.buildWpsRequest(
      process='Precipitation:TimeSeriesByDate',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      maskID=maskID,
      indicator='PRECIP_PRELIM',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      integrateType=integrate,
      windowLength=window,
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   ts       = json.loads(result)['timeseries']

   statistic = 'VALUE'

   request = testlib.buildWpsRequest(
      process='Precipitation:IntervalMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      maskID=maskID,
      indicator='PRECIP_FINAL',
      integrateType=integrate,
      # aggregateType=aggregate, MAX is default
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      statistic=statistic,
      windowLength=window,
      label=testcase,
     response=['geotiff', 'RawDataOutput', { 'mimeType' : 'image/tif'}])

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)
   testlib.check(testcase, 1, data.shape, (1,1))

   indx     = int(np.argmax(ts['value']))
   ds       = datetime.datetime.strptime(ts['pstart'][0], "%Y-%m-%d").date()
   de       = datetime.datetime.strptime(ts['pend'][indx], "%Y-%m-%d").date()
   ndays    = (de - ds).days + 1

   testlib.check( testcase, "value %s %s %d %d %d" % (ds, de, indx, ndays, data[0,0]), data[0, 0], ndays)


#----------------------------------------------------------------

def testcase_601():

   # Generates error because DIF is not valid for Precipitation
   testcase = __file__ + "_601_"

   periodStart  = gPERIOD_1['periodStart']
   periodEnd    = gPERIOD_1['periodEnd']
   polygon      = open(gTEST_ROI_DIR + 'ROI-2.geojson', 'r').read()
   integrate    = 'DIF'
   aggregate    = 'THR'
   statistic    = 'VALUE'
   window       = 0
   threshold    = -0.1

   request = testlib.buildWpsRequest(
      process='Precipitation:IntervalMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      integrateType=integrate,
      aggregateType=aggregate,
      # windowLength=window, 0 is default
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      statistic=statistic,
      label=testcase,
     response=['geotiff', 'RawDataOutput', { 'mimeType' : 'image/tif'}])

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)

   f = open(outFileName, 'r')
   data = f.read()
   testlib.check(testcase, 1, re.match('.*integrateType.*DIF.*not a valid value.*', data, re.DOTALL) != None, True)

#----------------------------------------------------------------

def datatest_1():
   testcase_506()

#----------------------------------------------------------------

def datatest_2():
   testcase_507()

#--------------------------- end of testcases --------------------------------------


# Get all functions names testcase_<nr> as default test cases
testCaseIds = [ x for x in dir() if re.match('.+_[0-9]+', x) ]
testCaseIds.sort()
testcases = {}
for testCaseId in testCaseIds:
   testcases[testCaseId] = locals()[testCaseId]
init()
testlib.main(testcases)
