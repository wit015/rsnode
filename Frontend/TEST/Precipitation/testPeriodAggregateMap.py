#!/usr/bin/python

##################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2015-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
##################################################################################

import re
import os
import sys
import json
import datetime
import traceback
import hashlib

import numpy as np

from osgeo import gdal

import testlib
import signatures

from testValues import *
from common import *

setGeoTransform(gORG_X, gRES_X, gEPS_X, gORG_Y, gRES_Y, gEPS_Y)

# Global script identifiers
svn_revision = str('$Rev: 7872 $')
svn_date = str('$Date: 2017-01-30 21:38:29 +0100 (Mon, 30 Jan 2017) $')
svn_author = str('$Author: jb76278 $')
script_file_name = os.path.basename(__file__)
now_string = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

# To make sure the whole array is printed.
np.set_printoptions(threshold='nan')

#######################################
# Test cases 
#######################################

#----------------------------------------------------------------

def init():

   testcase = __file__ + "_0_"
   dates = getMeasurementIntervals(testcase, 'Precipitation:GetDates', 'CHIRPS_5000m', 'PRECIP_PRELIM')

   # Check all intervals 
   for i in range(0, len(dates)-1):
      delta = dates[i+1] - dates[i]
      expected_delta = 5

      # Last measurement interval in month is not 10 days.
      year = dates[i].year
      ndate = dates[i+1]
      if   dates[i] == datetime.date(year, 12, 26): 
          expected_delta = 6
      elif dates[i].day == 26:
         ndate = datetime.date(dates[i].year, dates[i].month+1, 1)
         expected_delta = (ndate - dates[i]).days

      testlib.check(testcase, "2 - %s" % dates[i], delta.days, expected_delta)


#----------------------------------------------------------------

def testcase_1():

   testcase = __file__ + "_1_"

   for y in range(2000, 2016):

      periodStart = datetime.date(y, gPERIOD_1['periodStart'].month, gPERIOD_1['periodStart'].day)
      periodEnd   = datetime.date(y, gPERIOD_1['periodEnd'].month, gPERIOD_1['periodEnd'].day)
      polygon     = open(gTEST_ROI_DIR + 'ROI-1.geojson', 'r').read()

      request = testlib.buildWpsRequest(
         process='Precipitation:PeriodAggregateMap',
         source='CHIRPS_5000m',
         ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
         indicator='TEST',
         aggregateType='AVG',
         periodStart=str(periodStart),
         periodEnd=str(periodEnd),
         label=testcase,
         response='RawDataOutput')

      outFileName = ('/tmp/%s.tif' % testcase)
      issueRequest(request, testcase, 1, outFileName)
      data = getImageData(outFileName)
      testlib.check(testcase, 1, data.shape, (1,1))

      lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
      lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
      testlib.check(testcase, "2 - %s" % periodStart, data[0,0], testValue(lon, lat, periodStart))
      if testlib.getTrace() > 1: print periodStart, testValue(lon, lat, periodStart)


#----------------------------------------------------------------

def testcase_2():

   testcase = __file__ + "_2_"
   
   periodStart = gPERIOD_1['periodStart']
   periodEnd   = gPERIOD_1['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-2.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType='AVG',
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      label=testcase,
      response='RawDataOutput')

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)
   testlib.check(testcase, 1, data.shape, (1,1))

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   testlib.check(testcase, 2, data[0,0], testValue(lon, lat, periodStart))
   

#----------------------------------------------------------------

def testcase_3():

   testcase = __file__ + "_3_"
   
   periodStart = gPERIOD_1['periodStart']
   periodEnd   = gPERIOD_1['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-3.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType='AVG',
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      label=testcase,
      response='RawDataOutput')
   
   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)
   testlib.check(testcase, 1, data.shape, (1,1))

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   testlib.check(testcase, 2, data[0,0], testValue(lon, lat, periodStart))

#----------------------------------------------------------------

def testcase_4():

   testcase = __file__ + "_4_"

   periodStart = gPERIOD_1['periodStart']
   periodEnd   = gPERIOD_1['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-4.geojson', 'r').read()
   
   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType='AVG',
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      label=testcase,
      response='RawDataOutput')
   
   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)
   testlib.check(testcase, 1, data.shape, (1,1))

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   testlib.check(testcase, 2, data[0,0], testValue(lon, lat, periodStart))


#----------------------------------------------------------------

def testcase_5():

   testcase = __file__ + "_5_"
   
   periodStart = gPERIOD_1['periodStart']
   periodEnd   = gPERIOD_1['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-5.geojson', 'r').read()
   
   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType='AVG',
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      label=testcase,
      response='RawDataOutput')
   
   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)
   testlib.check(testcase, 1, data.shape, (1,1))

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   testlib.check(testcase, 2, data[0,0], testValue(lon, lat, periodStart))
   

#----------------------------------------------------------------

def testcase_6():

   testcase = __file__ + "_6_"
   
   periodStart = gPERIOD_1['periodStart']
   periodEnd   = gPERIOD_1['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-6.geojson', 'r').read()
   
   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType='AVG',
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      label=testcase,
      response='RawDataOutput')

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)

   lon0 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0]
   lat0 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][1]
   lon1 = json.loads(polygon)['features'][0]['geometry']['coordinates'][1][0]
   lat1 = json.loads(polygon)['features'][0]['geometry']['coordinates'][1][1]

   # Pixel index in base image frame
   p0 = world2pix(lon0, lat0)
   p1 = world2pix(lon1, lat1)

   # Pixel index in returned image frame (smaller extent) with p0 in upper left corner
   p1 = (p1[0]-p0[0], p1[1]-p0[1]); p0 = (0,0)

   testlib.check(testcase, 1, data.shape, (p1[1]+1, p1[0]+1))
   testlib.check(testcase, 2, data[0,0], testValue(lon0, lat0, periodStart))
   testlib.check(testcase, 3, data[p1[1],p1[0]], testValue(lon1, lat1, periodStart))
   testlib.check(testcase, 4, data[p1[1]/2,p1[0]/2], testValues.getNoDataValue())

#----------------------------------------------------------------

def testcase_7():

   testcase = __file__ + "_7_"

   periodStart = gPERIOD_1['periodStart']
   periodEnd   = gPERIOD_1['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-7.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType='AVG',
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      label=testcase,
      response='RawDataOutput')

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][1]

   testlib.check(testcase, 1, data.shape, (1, 1))
   testlib.check(testcase, 2, data[0,0], testValue(lon, lat, periodStart))

#----------------------------------------------------------------

def testcase_8():

   testcase = __file__ + "_8_"

   periodStart = gPERIOD_1['periodStart']
   periodEnd   = gPERIOD_1['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-8.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType='AVG',
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      label=testcase,
      response='RawDataOutput')

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)

   testlib.check(testcase, 1, data.shape, (1, 2))

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][1]
   testlib.check(testcase, 2, data[0,0], testValue(lon, lat, periodStart))

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][1][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][1][1]
   testlib.check(testcase, 3, data[0,1], testValue(lon, lat, periodStart))


#----------------------------------------------------------------

def testcase_9():

   testcase = __file__ + "_9_"

   periodStart = gPERIOD_1['periodStart']
   periodEnd   = gPERIOD_1['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-9.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType='AVG',
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      label=testcase,
      response='RawDataOutput')

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)

   testlib.check(testcase, 1, data.shape, (2, 2))

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][1]
   testlib.check(testcase, 2, data[0,1], testValue(lon, lat, periodStart))

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][1][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][1][1]
   testlib.check(testcase, 3, data[1,0], testValue(lon, lat, periodStart))

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][2][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][2][1]
   testlib.check(testcase, 4, data[1,1], testValue(lon, lat, periodStart))

   testlib.check(testcase, 5, data[0,0], testValues.getNoDataValue())


#----------------------------------------------------------------

def testcase_10():

   testcase = __file__ + "_10_"

   periodStart = gPERIOD_1['periodStart']
   periodEnd   = gPERIOD_1['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-10.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType='AVG',
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      label=testcase,
      response='RawDataOutput')

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][1]
   p0 = world2pix(lon, lat)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][1][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][1][1]
   p1 = world2pix(lon, lat)

   nX = p1[0]-p0[0]+1; nY = 2*nX-1
   testlib.check(testcase, 1, data.shape, (nY, nX))

   for y in range(0, nX):
      for x in range(0, y+1):
         testlib.check(testcase, 2, data[y, x], testValue(lon+x*gRES_X, lat+y*gRES_Y, periodStart))
      for x in range(y+1, nX):
         testlib.check(testcase, 3, data[y, x], testValues.getNoDataValue())
   for y in range(nX, 2*nX-1):
      for x in range(0, 2*nX-y-1):
         testlib.check(testcase, 4, data[y, x], testValue(lon+x*gRES_X, lat+y*gRES_Y, periodStart))
      for x in range(2*nX-y-1, nX):
         testlib.check(testcase, 5, data[y, x], testValues.getNoDataValue())

#----------------------------------------------------------------

def testcase_11():

   testcase = __file__ + "_11_"

   periodStart = gPERIOD_1['periodStart']
   periodEnd   = gPERIOD_1['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-11.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType='AVG',
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      label=testcase,
      response='RawDataOutput')

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)

   polygon0 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   polygon1 = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   lon, lat = polygon0[0][0] # ring 0, point 0
   p0 = world2pix(lon, lat)
   lon, lat = polygon1[0][0] # ring 0, point 0
   p1 = world2pix(lon, lat)

   # Origin of returned image with small offset 
   # to prevent errors due to machine precision
   lon = gORG_X + p1[0]*gRES_X + gEPS_X
   lat = gORG_Y + p0[1]*gRES_Y + gEPS_Y

   # pixel extent in returned image
   p = (p0[0]-p1[0], 0)   # upper left first polygon
   q = (0, p1[1]-p0[1])   # upper left second polygon

   # First polygon 2x2, second polygon = 1x4
   nX = p[0]+1+1 ; nY = q[1]+3+1
   testlib.check(testcase, 2, data.shape, (nY, nX))

   x, y = p
   testlib.check(testcase, 3, data[y+0, x+0], testValue(lon+(x+0)*gRES_X, lat+(y+0)*gRES_Y, periodStart))
   testlib.check(testcase, 4, data[y+0, x+1], testValue(lon+(x+1)*gRES_X, lat+(y+0)*gRES_Y, periodStart))
   testlib.check(testcase, 5, data[y+1, x+0], testValue(lon+(x+0)*gRES_X, lat+(y+1)*gRES_Y, periodStart))
   testlib.check(testcase, 6, data[y+1, x+1], testValue(lon+(x+1)*gRES_X, lat+(y+1)*gRES_Y, periodStart))

   x, y = q
   testlib.check(testcase, 7, data[y+0, x+0], testValue(lon+(x+0)*gRES_X, lat+(y+0)*gRES_Y, periodStart))
   testlib.check(testcase, 8, data[y+1, x+0], testValue(lon+(x+0)*gRES_X, lat+(y+1)*gRES_Y, periodStart))
   testlib.check(testcase, 9, data[y+2, x+0], testValue(lon+(x+0)*gRES_X, lat+(y+2)*gRES_Y, periodStart))
   testlib.check(testcase, 10, data[y+3, x+0], testValue(lon+(x+0)*gRES_X, lat+(y+3)*gRES_Y, periodStart))


#----------------------------------------------------------------

def testcase_12():

   testcase = __file__ + "_12_"

   periodStart = gPERIOD_1['periodStart']
   periodEnd   = gPERIOD_1['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-12.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType='AVG',
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      label=testcase,
      response='RawDataOutput')

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][1]

   nX = 10; nY = 10
   testlib.check(testcase, 2, data.shape, (nX, nY))
   for x in range(0, nX):
      for y in range(0, nY):
         testlib.check(
            testcase, 
            "3-%d-%d" % (x,y), 
            data[y, x],  
            testValue(lon+x*gRES_X, lat+y*gRES_Y, periodStart))

#----------------------------------------------------------------

def testcase_13():

   testcase = __file__ + "_13_"

   periodStart = gPERIOD_2['periodStart']
   periodEnd   = gPERIOD_2['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-1.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType='AVG',
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      label=testcase,
      response='RawDataOutput')

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)
   testlib.check(testcase, 1, data.shape, (1,1))

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   testlib.check(testcase, 2, data[0,0], testValue(lon, lat, periodStart))

#----------------------------------------------------------------

def testcase_14():

   testcase = __file__ + "_14_"

   periodStart = gPERIOD_3['periodStart']
   periodEnd   = gPERIOD_3['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-1.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType='AVG',
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      label=testcase,
      response='RawDataOutput')

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)
   testlib.check(testcase, 1, data.shape, (1,1))

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   testlib.check(testcase, 2, data[0,0], testValue(lon, lat, periodStart))

#----------------------------------------------------------------

def testcase_15():

   testcase = __file__ + "_15_"

   periodStart = gPERIOD_4['periodStart']
   periodEnd   = gPERIOD_4['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-1.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType='AVG',
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      label=testcase,
      response='RawDataOutput')

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)
   testlib.check(testcase, 1, data.shape, (1,1))

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   testlib.check(testcase, 2, data[0,0], testValue(lon, lat, periodStart))


#----------------------------------------------------------------

def testcase_16():

   testcase = __file__ + "_16_"

   periodStart = gPERIOD_5['periodStart']
   periodEnd   = gPERIOD_5['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-1.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType='AVG',
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      label=testcase,
      response='RawDataOutput')

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)
   testlib.check(testcase, 1, data.shape, (1,1))

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   testlib.check(testcase, 2, data[0,0], testValue(lon, lat, periodStart))

#----------------------------------------------------------------

def testcase_17():

   testcase = __file__ + "_17_"

   periodStart = gPERIOD_6['periodStart']
   periodEnd   = gPERIOD_6['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-1.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType='AVG',
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      label=testcase,
      response='RawDataOutput')

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)
   testlib.check(testcase, 1, data.shape, (1,1))

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   v   = testValueAggregate(lon, lat, (periodStart, periodEnd), 'SUM')
   testlib.check(testcase, 2, data[0,0], v)


#----------------------------------------------------------------

def testcase_18():

   testcase = __file__ + "_18_"

   periodStart = gPERIOD_7['periodStart']
   periodEnd   = gPERIOD_7['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-1.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType='AVG',
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      label=testcase,
      response='RawDataOutput')

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)
   testlib.check(testcase, 1, data.shape, (1,1))

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   v   = testValueAggregate(lon, lat, (periodStart, periodEnd), 'SUM')
   testlib.check(testcase, 2, data[0,0], v, gFLOAT_TOLERANCE)


#----------------------------------------------------------------

def testcase_19():

   testcase = __file__ + "_19_"

   periodStart = gPERIOD_8['periodStart']
   periodEnd   = gPERIOD_8['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-1.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType='AVG',
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      label=testcase,
      response='RawDataOutput')

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)
   testlib.check(testcase, 1, data.shape, (1,1))

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   v   = testValueAggregate(lon, lat, (periodStart, periodEnd), 'SUM')
   testlib.check(testcase, 2, data[0,0], v, gFLOAT_TOLERANCE)

#----------------------------------------------------------------

def testcase_20():

   testcase = __file__ + "_20_"

   periodStart = gPERIOD_9['periodStart']
   periodEnd   = gPERIOD_9['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-1.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType='AVG',
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      label=testcase,
      response='RawDataOutput')

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)
   testlib.check(testcase, 1, data.shape, (1,1))

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   v   = testValueAggregate(lon, lat, (periodStart, periodEnd), 'SUM')
   testlib.check(testcase, 2, data[0,0], np.float32(v))

#----------------------------------------------------------------

def testcase_21():

   testcase = __file__ + "_21_"

   periodStart = gPERIOD_10['periodStart']
   periodEnd   = gPERIOD_10['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-1.geojson', 'r').read()
   integrate   = 'SUM'
   nintervals  = 4
   window      = nintervals * 5
   aggregate   = 'AVG'

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      # integrateType=integrate, SUM is default
      windowLength=window,
      aggregateType=aggregate,
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      label=testcase,
      response='RawDataOutput')

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)
   testlib.check(testcase, 1, data.shape, (1,1))

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]

   v   = testValueAggregate(lon, lat, (periodStart, periodEnd), integrate, aggregate, nintervals)
   # Values in result tif file are 32-bit
   testlib.check(testcase, 2, data[0,0], np.float32(v))


#----------------------------------------------------------------

def testcase_22():

   testcase = __file__ + "_22_"

   periodStart = gPERIOD_11['periodStart']
   periodEnd   = gPERIOD_11['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-1.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType='AVG',
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      label=testcase,
      response='RawDataOutput')

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)
   testlib.check(testcase, 1, data.shape, (1,1))

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]

   v   = testValueAggregate(lon, lat, (periodStart, periodEnd), 'SUM')
   # Values in result tif file are 32-bit
   testlib.check(testcase, 2, data[0,0], np.float32(v))

#----------------------------------------------------------------

def testcase_23():

   testcase = __file__ + "_23_"

   periodStart = gPERIOD_9['periodStart']
   periodEnd   = gPERIOD_9['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-12.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType='AVG',
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      label=testcase,
      response='RawDataOutput')

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][1]

   nX = 10; nY = 10
   testlib.check(testcase, 2, data.shape, (nX, nY))
   for x in range(0, nX):
      for y in range(0, nY):
         v   = testValueAggregate(lon+x*gRES_X, lat+y*gRES_Y, (periodStart, periodEnd), 'SUM')
         # Values in result tif file are 32-bit
         testlib.check(testcase, "3-%d-%d" % (x,y), data[y,x], np.float32(v))



#----------------------------------------------------------------

def testcase_24():

   testcase = __file__ + "_24_"

   periodStart   = gPERIOD_1['periodStart']
   periodEnd     = gPERIOD_1['periodEnd']
   polygon       = open(gTEST_ROI_DIR + 'ROI-1.geojson', 'r').read()
   integrateType = 'NONE'
   aggregateType = 'AVG'

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST_LTA',
      integrateType=integrateType,
      aggregateType=aggregateType,
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      label=testcase,
      response='RawDataOutput')

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)
   testlib.check(testcase, 1, data.shape, (1,1))

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]

   v = np.float32(testValueAggregateLTA(lon, lat, (periodStart, periodEnd), 1981, 2015, integrateType, aggregateType))
#----------------------------------------------------------------

def testcase_25():

   testcase = __file__ + "_25_"

   periodStart   = gPERIOD_3['periodStart']
   periodEnd     = gPERIOD_3['periodEnd']
   polygon       = open(gTEST_ROI_DIR + 'ROI-2.geojson', 'r').read()
   integrateType = 'NONE'
   aggregateType = 'AVG'

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST_LTA',
      integrateType=integrateType,
      aggregateType=aggregateType,
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      label=testcase,
      response='RawDataOutput')

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)
   testlib.check(testcase, 1, data.shape, (1,1))

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]

   v = np.float32(testValueAggregateLTA(lon, lat, (periodStart, periodEnd), 1981, 2015, integrateType, aggregateType))
   # Values in result tif file are 32-bit
   testlib.check(testcase, 2, data[0,0], np.float32(v))

#----------------------------------------------------------------

def testcase_26():

   testcase = __file__ + "_26_"

   periodStart   = gPERIOD_8['periodStart']
   periodEnd     = gPERIOD_8['periodEnd']
   polygon       = open(gTEST_ROI_DIR + 'ROI-2.geojson', 'r').read()
   integrateType = 'NONE'
   aggregateType = 'AVG'

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST_LTA',
      integrateType=integrateType,
      aggregateType=aggregateType,
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      label=testcase,
      response='RawDataOutput')

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)
   testlib.check(testcase, 1, data.shape, (1,1))

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]

   v = np.float32(testValueAggregateLTA(lon, lat, (periodStart, periodEnd), 1981, 2015, integrateType, aggregateType))
   # Values in tif file are 32-bit
   testlib.check(testcase, 2, data[0,0], np.float32(v))


#----------------------------------------------------------------

def testcase_27():

   testcase = __file__ + "_27_"

   periodStart   = gPERIOD_9['periodStart']
   periodEnd     = gPERIOD_9['periodEnd']
   polygon       = open(gTEST_ROI_DIR + 'ROI-2.geojson', 'r').read()
   integrateType = 'NONE'
   aggregateType = 'AVG'

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST_LTA',
      integrateType=integrateType,
      aggregateType=aggregateType,
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      label=testcase,
      response='RawDataOutput')

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)
   testlib.check(testcase, 1, data.shape, (1,1))

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]

   v = np.float32(testValueAggregateLTA(lon, lat, (periodStart, periodEnd), 1981, 2015, integrateType, aggregateType))
   # Values in result tif file are 32-bit
   testlib.check(testcase, 2, data[0,0], np.float32(v))


#----------------------------------------------------------------

def testcase_28():

   testcase = __file__ + "_28_"

   periodStart   = gPERIOD_9['periodStart']
   periodEnd     = gPERIOD_9['periodEnd']
   polygon       = open(gTEST_ROI_DIR + 'ROI-2.geojson', 'r').read()
   indicator     = 'TEST_LTA'
   integrateType = 'SUM'
   aggregateType = 'MAX'

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator=indicator,
      aggregateType=aggregateType,
      integrateType=integrateType,
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      label=testcase,
      response='RawDataOutput')

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)
   testlib.check(testcase, 1, data.shape, (1,1))

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]

   v = np.float32(testValueAggregateLTA(lon, lat, (periodStart, periodEnd), 1981, 2015, integrateType, aggregateType))
   # Values in result tif file are 32-bit
   testlib.check(testcase, 2, data[0,0], np.float32(v), gFLOAT_TOLERANCE)

#----------------------------------------------------------------

def testcase_29():

   testcase = __file__ + "_29_"

   periodStart = gPERIOD_1['periodStart']
   periodEnd   = gPERIOD_1['periodEnd']
   indicator   = 'TEST'
   refStart    = REFSTART_1(periodStart)
   refType     = 'LTA'
   polygon     = open(gTEST_ROI_DIR + 'ROI-1.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator=indicator,
      aggregateType='AVG',
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      referenceStart=str(refStart),
      referenceType=refType,
      label=testcase,
      response='RawDataOutput')

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)
   testlib.check(testcase, 1, data.shape, (1,1))

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]

   vp   = testValueAggregate(lon, lat, (periodStart, periodEnd), 'SUM')
   vlta = np.float32(testValueAggregateLTA(lon, lat, (refStart, refStart + (periodEnd - periodStart)), 1981, 2015, 'SUM'))

   # Values in tif file are 32-bit
   testlib.check(testcase, 2, data[0,0], np.float32(vp - vlta), gFLOAT_TOLERANCE)

#----------------------------------------------------------------

def testcase_30():

   testcase = __file__ + "_30_"

   periodStart = gPERIOD_1['periodStart']
   periodEnd   = gPERIOD_1['periodEnd']
   refStart    = REFSTART_2(periodStart)
   indicator   = 'TEST'
   refType     = 'LTA'
   polygon     = open(gTEST_ROI_DIR + 'ROI-1.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator=indicator,
      aggregateType='AVG',
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      referenceStart=str(refStart),
      referenceType=refType,
      label=testcase,
      response='RawDataOutput')

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)
   testlib.check(testcase, 1, data.shape, (1,1))

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]

   vp   = testValueAggregate(lon, lat, (periodStart, periodEnd), 'SUM')
   vlta = np.float32(testValueAggregateLTA(lon, lat, (refStart, refStart + (periodEnd - periodStart)), 1981, 2015, 'SUM'))

   # Values in tif file are 32-bit
   testlib.check(testcase, 2, data[0,0], np.float32(vp - vlta), gFLOAT_TOLERANCE)


#----------------------------------------------------------------

def testcase_31():

   testcase = __file__ + "_31_"

   periodStart = gPERIOD_1['periodStart']
   periodEnd   = gPERIOD_1['periodEnd']
   refStart    = REFSTART_3(periodStart)
   indicator   = 'TEST'
   refType     = 'LTA'
   polygon     = open(gTEST_ROI_DIR + 'ROI-1.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator=indicator,
      aggregateType='AVG',
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      referenceStart=str(refStart),
      referenceType=refType,
      label=testcase,
      response='RawDataOutput')

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)
   testlib.check(testcase, 1, data.shape, (1,1))

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]

   vp   = testValueAggregate(lon, lat, (periodStart, periodEnd), 'SUM')
   vlta = np.float32(testValueAggregateLTA(lon, lat, (refStart, refStart + (periodEnd - periodStart)), 1981, 2015, 'SUM'))

   # Values in tif file are 32-bit
   testlib.check(testcase, 2, data[0,0], np.float32(vp - vlta), gFLOAT_TOLERANCE)

#----------------------------------------------------------------

def testcase_32():

   noDataValue = testValues.getNoDataValue()

   testcase = __file__ + "_32_"

   periodStart   = gPERIOD_9['periodStart']
   periodEnd     = gPERIOD_9['periodEnd']
   polygon       = open(gTEST_ROI_DIR + 'ROI-11.geojson', 'r').read()
   integrateType = 'NONE'
   aggregateType = 'MIN'

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType=aggregateType,
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      integrateType=integrateType,
      label=testcase,
      response='RawDataOutput')

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)

   polygon0 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   polygon1 = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   lon, lat = polygon0[0][0] # ring 0, point 0
   p0 = world2pix(lon, lat)
   lon, lat = polygon1[0][0] # ring 0, point 0
   p1 = world2pix(lon, lat)

   # Origin of returned image with small offset
   # to prevent errors due to machine precision
   lon = gORG_X + p1[0]*gRES_X + gEPS_X
   lat = gORG_Y + p0[1]*gRES_Y + gEPS_Y

   # pixel extent in returned image
   p = (p0[0]-p1[0], 0)   # upper left first polygon
   q = (0, p1[1]-p0[1])   # upper left second polygon

   # First polygon 2x2, second polygon = 1x4
   nX = p[0]+1+1 ; nY = q[1]+3+1
   testlib.check(testcase, 2, data.shape, (nY, nX))

   mask = np.array( [[ False, True, False, False],
                     [ False, True, False, False],
                     [ False, True, True,  True ],
                     [ False, True, True,  True ]] )

   for x in range(0, 4):
      for y in range(0, 4):
         p = (lon+x*gRES_X, lat+y*gRES_Y)
         if not mask[y, x]:
            v = testValueAggregate(p[0], p[1], (periodStart, periodEnd), integrateType, aggregateType)
         else:
            v = noDataValue
         testlib.check(testcase, 3, data[y, x], v, gFLOAT_TOLERANCE)
      
#----------------------------------------------------------------

def testcase_33():

   noDataValue = testValues.getNoDataValue()

   testcase = __file__ + "_33_"

   periodStart   = gPERIOD_9['periodStart']
   periodEnd     = gPERIOD_9['periodEnd']
   polygon       = open(gTEST_ROI_DIR + 'ROI-11.geojson', 'r').read()
   integrateType = 'NONE'
   aggregateType = 'MAX'

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType=aggregateType,
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      integrateType=integrateType,
      label=testcase,
      response='RawDataOutput')

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)

   polygon0 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   polygon1 = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   lon, lat = polygon0[0][0] # ring 0, point 0
   p0 = world2pix(lon, lat)
   lon, lat = polygon1[0][0] # ring 0, point 0
   p1 = world2pix(lon, lat)

   # Origin of returned image with small offset
   # to prevent errors due to machine precision
   lon = gORG_X + p1[0]*gRES_X + gEPS_X
   lat = gORG_Y + p0[1]*gRES_Y + gEPS_Y

   # pixel extent in returned image
   p = (p0[0]-p1[0], 0)   # upper left first polygon
   q = (0, p1[1]-p0[1])   # upper left second polygon

   # First polygon 2x2, second polygon = 1x4
   nX = p[0]+1+1 ; nY = q[1]+3+1
   testlib.check(testcase, 2, data.shape, (nY, nX))

   mask = np.array( [[ False, True, False, False],
                     [ False, True, False, False],
                     [ False, True, True,  True ],
                     [ False, True, True,  True ]] )

   for x in range(0, 4):
      for y in range(0, 4):
         p = (lon+x*gRES_X, lat+y*gRES_Y)
         if not mask[y, x]:
            v = testValueAggregate(p[0], p[1], (periodStart, periodEnd), integrateType, aggregateType)
         else:
            v = noDataValue
         testlib.check(testcase, 3, data[y, x], v, gFLOAT_TOLERANCE)

#----------------------------------------------------------------

def testcase_34():

   noDataValue = testValues.getNoDataValue()

   testcase = __file__ + "_34_"

   periodStart   = gPERIOD_9['periodStart']
   periodEnd     = gPERIOD_9['periodEnd']
   polygon       = open(gTEST_ROI_DIR + 'ROI-11.geojson', 'r').read()
   aggregateType = 'AVG'
   integrateType = 'SUM'

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType=aggregateType,
      integrateType=integrateType,
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      label=testcase,
      response='RawDataOutput')

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)

   polygon0 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   polygon1 = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   lon, lat = polygon0[0][0] # ring 0, point 0
   p0 = world2pix(lon, lat)
   lon, lat = polygon1[0][0] # ring 0, point 0
   p1 = world2pix(lon, lat)

   # Origin of returned image with small offset
   # to prevent errors due to machine precision
   lon = gORG_X + p1[0]*gRES_X + gEPS_X
   lat = gORG_Y + p0[1]*gRES_Y + gEPS_Y

   # pixel extent in returned image
   p = (p0[0]-p1[0], 0)   # upper left first polygon
   q = (0, p1[1]-p0[1])   # upper left second polygon

   # First polygon 2x2, second polygon = 1x4
   nX = p[0]+1+1 ; nY = q[1]+3+1
   testlib.check(testcase, 2, data.shape, (nY, nX))

   mask = np.array( [[ False, True, False, False],
                     [ False, True, False, False],
                     [ False, True, True,  True ],
                     [ False, True, True,  True ]] )

   for x in range(0, 4):
      for y in range(0, 4):
         p = (lon+x*gRES_X, lat+y*gRES_Y)
         if not mask[y, x]:
            v = testValueAggregate(p[0], p[1], (periodStart, periodEnd), integrateType, aggregateType)
         else:
            v = noDataValue
         testlib.check(testcase, 3, data[y, x], v, gFLOAT_TOLERANCE)

#----------------------------------------------------------------

def testcase_35():

   noDataValue = testValues.getNoDataValue()

   testcase = __file__ + "_35_"

   periodStart   = gPERIOD_9['periodStart']
   periodEnd     = gPERIOD_9['periodEnd']  
   polygon       = open(gTEST_ROI_DIR + 'ROI-11.geojson', 'r').read()
   aggregateType = 'MAX'                                             
   integrateType = 'SUM'                                             

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateMap',
      source='CHIRPS_5000m',                     
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',                                                  
      aggregateType=aggregateType,                                       
      integrateType=integrateType,                                       
      periodStart=str(periodStart),                                      
      periodEnd=str(periodEnd),                                          
      label=testcase,                                                    
      response='RawDataOutput')                                          

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)               

   polygon0 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   polygon1 = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   lon, lat = polygon0[0][0] # ring 0, point 0                                
   p0 = world2pix(lon, lat)                                                   
   lon, lat = polygon1[0][0] # ring 0, point 0                                
   p1 = world2pix(lon, lat)                                                   

   # Origin of returned image with small offset
   # to prevent errors due to machine precision
   lon = gORG_X + p1[0]*gRES_X + gEPS_X        
   lat = gORG_Y + p0[1]*gRES_Y + gEPS_Y        

   # pixel extent in returned image
   p = (p0[0]-p1[0], 0)   # upper left first polygon
   q = (0, p1[1]-p0[1])   # upper left second polygon

   # First polygon 2x2, second polygon = 1x4
   nX = p[0]+1+1 ; nY = q[1]+3+1            
   testlib.check(testcase, 2, data.shape, (nY, nX))

   mask = np.array( [[ False, True, False, False],
                     [ False, True, False, False],
                     [ False, True, True,  True ],
                     [ False, True, True,  True ]] )

   for x in range(0, 4):
      for y in range(0, 4):
         p = (lon+x*gRES_X, lat+y*gRES_Y)
         if not mask[y, x]:              
            v = testValueAggregate(p[0], p[1], (periodStart, periodEnd), integrateType, aggregateType)
         else:                                                                                     
            v = noDataValue                                                                       
         testlib.check(testcase, 3, data[y, x], v, gFLOAT_TOLERANCE)

#----------------------------------------------------------------

def testcase_36():

   noDataValue = testValues.getNoDataValue()

   testcase = __file__ + "_36_"

   periodStart    = gPERIOD_9['periodStart']
   periodEnd      = gPERIOD_9['periodEnd']  
   polygon        = open(gTEST_ROI_DIR + 'ROI-11.geojson', 'r').read()
   aggregateType  = 'AVG'                                             
   integrateType  = 'SUM'                                             
   referenceType  = 'ABS'
   referenceStart = REFSTART_1(periodStart, 0)

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateMap',
      source='CHIRPS_5000m',                     
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',                                                  
      aggregateType=aggregateType,                                       
      integrateType=integrateType,                                       
      periodStart=str(periodStart),                                      
      periodEnd=str(periodEnd),                                          
      referenceType=referenceType,
      referenceStart=str(referenceStart),
      label=testcase,                                                    
      response='RawDataOutput')                                          

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)               

   polygon0 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   polygon1 = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   lon, lat = polygon0[0][0] # ring 0, point 0                                
   p0 = world2pix(lon, lat)                                                   
   lon, lat = polygon1[0][0] # ring 0, point 0                                
   p1 = world2pix(lon, lat)                                                   

   # Origin of returned image with small offset
   # to prevent errors due to machine precision
   lon = gORG_X + p1[0]*gRES_X + gEPS_X        
   lat = gORG_Y + p0[1]*gRES_Y + gEPS_Y        

   # pixel extent in returned image
   p = (p0[0]-p1[0], 0)   # upper left first polygon
   q = (0, p1[1]-p0[1])   # upper left second polygon

   # First polygon 2x2, second polygon = 1x4
   nX = p[0]+1+1 ; nY = q[1]+3+1            
   testlib.check(testcase, 2, data.shape, (nY, nX))

   mask = np.array( [[ False, True, False, False],
                     [ False, True, False, False],
                     [ False, True, True,  True ],
                     [ False, True, True,  True ]] )

   for x in range(0, 4):
      for y in range(0, 4):
         p = (lon+x*gRES_X, lat+y*gRES_Y)
         if not mask[y, x]:              
            referenceEnd = referenceStart + (periodEnd-periodStart)
            v  = testValueAggregate(p[0], p[1], (periodStart, periodEnd), integrateType, aggregateType)
            rv = testValueAggregate(p[0], p[1], (referenceStart, referenceEnd), integrateType, aggregateType)
            v = v - rv
         else:                                                                                     
            v = noDataValue                                                                       
         testlib.check(testcase, 3, data[y, x], v, gFLOAT_TOLERANCE)

#----------------------------------------------------------------

def testcase_37():

   noDataValue = testValues.getNoDataValue()

   testcase = __file__ + "_37_"

   periodStart    = gPERIOD_9['periodStart']
   periodEnd      = gPERIOD_9['periodEnd']  
   polygon        = open(gTEST_ROI_DIR + 'ROI-11.geojson', 'r').read()
   aggregateType  = 'AVG'                                             
   integrateType  = 'SUM'                                             
   referenceType  = 'LTA'                                             
   referenceStart = REFSTART_1(periodStart, 1)

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateMap',
      source='CHIRPS_5000m',                     
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',                                                  
      aggregateType=aggregateType,                                       
      integrateType=integrateType,                                       
      periodStart=str(periodStart),                                      
      periodEnd=str(periodEnd),                                          
      referenceType=referenceType,                                       
      referenceStart=str(referenceStart),                                
      label=testcase,                                                    
      response='RawDataOutput')                                          

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)               

   polygon0 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   polygon1 = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   lon, lat = polygon0[0][0] # ring 0, point 0                                
   p0 = world2pix(lon, lat)                                                   
   lon, lat = polygon1[0][0] # ring 0, point 0                                
   p1 = world2pix(lon, lat)                                                   

   # Origin of returned image with small offset
   # to prevent errors due to machine precision
   lon = gORG_X + p1[0]*gRES_X + gEPS_X        
   lat = gORG_Y + p0[1]*gRES_Y + gEPS_Y        

   # pixel extent in returned image
   p = (p0[0]-p1[0], 0)   # upper left first polygon
   q = (0, p1[1]-p0[1])   # upper left second polygon

   # First polygon 2x2, second polygon = 1x4
   nX = p[0]+1+1 ; nY = q[1]+3+1            
   testlib.check(testcase, 2, data.shape, (nY, nX))

   mask = np.array( [[ False, True, False, False],
                     [ False, True, False, False],
                     [ False, True, True,  True ],
                     [ False, True, True,  True ]] )

   for x in range(0, 4):
      for y in range(0, 4):
         p = (lon+x*gRES_X, lat+y*gRES_Y)
         if not mask[y, x]:              
            referenceEnd = referenceStart + (periodEnd-periodStart)
            v  = testValueAggregate(p[0], p[1], (periodStart, periodEnd), integrateType, aggregateType)
            rv = testValueAggregateLTA(p[0], p[1], (referenceStart, referenceEnd), 1981, 2015, integrateType, aggregateType)
            v = v - rv                                                                                    
         else:                                                                                            
            v = noDataValue                                                                              
         testlib.check(testcase, 3, data[y, x], v, gFLOAT_TOLERANCE)

#----------------------------------------------------------------

def testcase_38():

   noDataValue = testValues.getNoDataValue()

   testcase = __file__ + "_38_"

   periodStart    = gPERIOD_9['periodStart']
   periodEnd      = gPERIOD_9['periodEnd']
   polygon        = open(gTEST_ROI_DIR + 'ROI-11.geojson', 'r').read()
   maskID         = "TEST"
   aggregateType  = 'AVG'
   integrateType  = 'SUM'
   referenceType  = 'NONE'

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      maskID=maskID,
      indicator='TEST',
      aggregateType=aggregateType,
      integrateType=integrateType,
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      referenceType=referenceType,
      label=testcase,
      response='RawDataOutput')

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)

   polygon0 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   polygon1 = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   lon, lat = polygon0[0][0] # ring 0, point 0
   p0 = world2pix(lon, lat)
   lon, lat = polygon1[0][0] # ring 0, point 0
   p1 = world2pix(lon, lat)

   # Origin of returned image with small offset
   # to prevent errors due to machine precision
   lon = gORG_X + p1[0]*gRES_X + gEPS_X
   lat = gORG_Y + p0[1]*gRES_Y + gEPS_Y

   # pixel extent in returned image
   p = (p0[0]-p1[0], 0)   # upper left first polygon
   q = (0, p1[1]-p0[1])   # upper left second polygon

   # First polygon 2x2, second polygon = 1x4
   nX = p[0]+1+1 ; nY = q[1]+3+1
   testlib.check(testcase, 2, data.shape, (nY, nX))

   mask = np.array( [[ True,  True, True, False],
                     [ True,  True, True, False],
                     [ False, True, True, True ],
                     [ False, True, True, True ]] )

   for x in range(0, 4):
      for y in range(0, 4):
         p = (lon+x*gRES_X, lat+y*gRES_Y)
         if not mask[y, x]:
            v  = testValueAggregate(p[0], p[1], (periodStart, periodEnd), integrateType, aggregateType)
         else:
            v = noDataValue
         testlib.check(testcase, 3, data[y, x], v, gFLOAT_TOLERANCE)

#----------------------------------------------------------------

def testcase_39():

   noDataValue = testValues.getNoDataValue()

   testcase = __file__ + "_39_"

   periodStart    = gPERIOD_9['periodStart']
   periodEnd      = gPERIOD_9['periodEnd']
   polygon        = open(gTEST_ROI_DIR + 'ROI-11.geojson', 'r').read()
   maskID         = "ESA_CCI_2010"
   aggregateType  = 'AVG'
   integrateType  = 'SUM'
   referenceType  = 'NONE'

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      maskID=maskID,
      indicator='TEST',
      aggregateType=aggregateType,
      integrateType=integrateType,
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      referenceType=referenceType,
      label=testcase,
      response='RawDataOutput')

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)

   polygon0 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   polygon1 = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   lon, lat = polygon0[0][0] # ring 0, point 0
   p0 = world2pix(lon, lat)
   lon, lat = polygon1[0][0] # ring 0, point 0
   p1 = world2pix(lon, lat)

   # Origin of returned image with small offset
   # to prevent errors due to machine precision
   lon = gORG_X + p1[0]*gRES_X + gEPS_X
   lat = gORG_Y + p0[1]*gRES_Y + gEPS_Y

   # pixel extent in returned image
   p = (p0[0]-p1[0], 0)   # upper left first polygon
   q = (0, p1[1]-p0[1])   # upper left second polygon

   # First polygon 2x2, second polygon = 1x4
   nX = p[0]+1+1 ; nY = q[1]+3+1
   testlib.check(testcase, 2, data.shape, (nY, nX))

   mask = np.array( [[ False, True, False, False],
                     [ False, True, False, False],
                     [ False, True, True,  True ],
                     [ False, True, True,  True ]] )

   for x in range(0, 4):
      for y in range(0, 4):
         p = (lon+x*gRES_X, lat+y*gRES_Y)
         if not mask[y, x]:
            v  = testValueAggregate(p[0], p[1], (periodStart, periodEnd), integrateType, aggregateType)
         else:
            v = noDataValue
         testlib.check(testcase, 3, data[y, x], v, gFLOAT_TOLERANCE)


#----------------------------------------------------------------

def datatest_1():


   testcase = __file__ + "datatest_1_"

   periodStart    = datetime.date(2000, 1, 1)
   periodEnd      = datetime.date(2000,12,31)
   polygon        = open(gTEST_ROI_DIR + 'ROI-Afar-Ewa.geojson', 'r').read()
   maskID         = 'NONE'
   indicator      = 'PRECIP_FINAL'
   aggregateType  = 'MAX'
   integrateType  = 'SUM'
   referenceType  = 'NONE'

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      maskID=maskID,
      indicator=indicator,
      aggregateType=aggregateType,
      integrateType=integrateType,
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      referenceType=referenceType,
      label=testcase,
      response='RawDataOutput')

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)

   testlib.check(testcase, "data.shape", data.shape, (10,10))

#----------------------------------------------------------------

def datatest_2():


   testcase = __file__ + "datatest_2_"

   periodStart    = datetime.date(2000, 1, 1)
   periodEnd      = datetime.date(2000,12,31)
   polygon        = open(gTEST_ROI_DIR + 'ROI-Afar-Ewa.geojson', 'r').read()
   maskID         = 'ESA_CCI_2010'
   indicator      = 'PRECIP_PRELIM'
   aggregateType  = 'MAX'
   integrateType  = 'SUM'
   referenceType  = 'NONE'

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      maskID=maskID,
      indicator=indicator,
      aggregateType=aggregateType,
      integrateType=integrateType,
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      referenceType=referenceType,
      label=testcase,
      response='RawDataOutput')

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)

   testlib.check(testcase, "data.shape", data.shape, (10,10))

#----------------------------------------------------------------

def datatest_3():


   testcase = __file__ + "datatest_3_"

   periodStart    = datetime.date(2000, 1, 1)
   periodEnd      = datetime.date(2000,12,31)
   polygon        = open(gTEST_ROI_DIR + 'ROI-Afar-Ewa.geojson', 'r').read()
   maskID         = 'ESA_CCI_2010'
   indicator      = 'PRECIP_LTA'
   aggregateType  = 'MAX'
   integrateType  = 'SUM'
   referenceType  = 'NONE'

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateMap',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      maskID=maskID,
      indicator=indicator,
      aggregateType=aggregateType,
      integrateType=integrateType,
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      referenceType=referenceType,
      label=testcase,
      response='RawDataOutput')

   outFileName = ('/tmp/%s.tif' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)

   testlib.check(testcase, "data.shape", data.shape, (10,10))


#----------------------------------------------------------------

def signature_1():

   testcase = __file__ + "signature_1_"
   print "Checking PRECIP_PRELIM signatures ..."

   l = signatures.PRECIP_PRELIM.keys()
   l.sort()
   for d in l:

      periodStart = d
      periodEnd   = d
      polygon     = open(gTEST_ROI_DIR + 'ROI-13.geojson', 'r').read()

      request = testlib.buildWpsRequest(
         process='Precipitation:PeriodAggregateMap',
         source='CHIRPS_5000m',
         ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
         indicator='PRECIP_PRELIM',
         aggregateType='AVG',
         periodStart=str(periodStart),
         periodEnd=str(periodEnd),
         label=testcase,
         response='RawDataOutput')

      outFileName = ('/tmp/%s.tif' % testcase)
      issueRequest(request, testcase, 1, outFileName)
      data = getImageData(outFileName)
      s = computeSignature(data)
      testlib.check(testcase, "2 - %s" % str(d), s, signatures.PRECIP_PRELIM[d])

#----------------------------------------------------------------

def signature_2():

   testcase = __file__ + "signature_2_"
   print "Checking PRECIP_FINAL signatures ..."

   l = signatures.PRECIP_FINAL.keys()
   l.sort()
   for d in l:

      periodStart = d
      periodEnd   = d
      polygon     = open(gTEST_ROI_DIR + 'ROI-13.geojson', 'r').read()

      request = testlib.buildWpsRequest(
         process='Precipitation:PeriodAggregateMap',
         source='CHIRPS_5000m',
         ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
         indicator='PRECIP_FINAL',
         aggregateType='AVG',
         periodStart=str(periodStart),
         periodEnd=str(periodEnd),
         label=testcase,
         response='RawDataOutput')

      outFileName = ('/tmp/%s.tif' % testcase)
      issueRequest(request, testcase, 1, outFileName)
      data = getImageData(outFileName)
      s = computeSignature(data)
      testlib.check(testcase, "2 - %s" % str(d), s, signatures.PRECIP_FINAL[d])

#----------------------------------------------------------------

def signature_3():

   testcase = __file__ + "signature_3_"
   print "Checking PRECIP_LTA signatures ..."

   l = signatures.PRECIP_LTA.keys()
   l.sort()
   for d in l:

      periodStart = d
      periodEnd   = d
      polygon     = open(gTEST_ROI_DIR + 'ROI-13.geojson', 'r').read()

      request = testlib.buildWpsRequest(
         process='Precipitation:PeriodAggregateMap',
         source='CHIRPS_5000m',
         ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
         indicator='PRECIP_LTA',
         aggregateType='AVG',
         periodStart=str(periodStart),
         periodEnd=str(periodEnd),
         label=testcase,
         response='RawDataOutput')

      outFileName = ('/tmp/%s.tif' % testcase)
      issueRequest(request, testcase, 1, outFileName)
      data = getImageData(outFileName)
      s = computeSignature(data)
      testlib.check(testcase, "2 - %s" % str(d), s, signatures.PRECIP_LTA[d])

#--------------------------- end of testcases --------------------------------------


# Get all functions names testcase_<nr> as default test cases
testCaseIds = [ x for x in dir() if re.match('.+_[0-9]+', x) ]
testCaseIds.sort()
testcases = {}
for testCaseId in testCaseIds:
   testcases[testCaseId] = locals()[testCaseId]
init()
testlib.main(testcases)
