#!/usr/bin/python

##################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
##################################################################################

import re
import os
import sys
import json
import datetime
import math

import numpy as np

from osgeo import gdal

import testlib
import common
from testValues import *
from common import *

setGeoTransform(gORG_X, gRES_X, gEPS_X, gORG_Y, gRES_Y, gEPS_Y)

# Global script identifiers
svn_revision = str('$Rev: 7872 $')
svn_date = str('$Date: 2017-01-30 21:38:29 +0100 (Mon, 30 Jan 2017) $')
svn_author = str('$Author: jb76278 $')
script_file_name = os.path.basename(__file__)
now_string = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

# To make sure the whole array is printed.
np.set_printoptions(threshold='nan')

def checkResponse(testcase, response, label, pointList, period, indicator='TEST', refType='NONE', refDate=None, aggregate='AVG', integrate='SUM', maskID='NONE', window=0):

   iwindow = window / 5

   periodStart = period[0]
   periodEnd   = period[1]

   testlib.check(testcase, "type  ", response['type'],  'PeriodAggregateROI')
   testlib.check(testcase, "label ", response['label'], label)
   testlib.check(testcase, "N     ", response['N'], len(pointList))

   area = 0.0
   pvl = [] # Value list over all points
   # For all points
   for p in pointList:
      if indicator.endswith('_LTA'):
         v = np.float32(testValueAggregateLTA(p[0], p[1], (periodStart, periodEnd), 1981, 2015, integrate, aggregate, iwindow))
      else:
         # In service implementation data is read back from TIFF at 32-bit resolution.
         v = np.float32(testValueAggregate(p[0], p[1], (periodStart, periodEnd), integrate, aggregate, iwindow))
      x, y       = world2pix(p[0], p[1])
      pLon, pLat = pix2world(x, y)
      dY = hdistance((0.0, 0.0), (1.0, 0.0))
      # The reference point for a picel is lower left corner
      dX = hdistance((pLat+0.5*gRES_Y, 0.0), (pLat+0.5*gRES_Y, 1.0))
      if not math.isnan(v) : 
         area = area + abs(gRES_X) * abs(gRES_Y) * dX * dY
         if refType <> 'NONE':
            refStart = refDate
            refEnd   = refStart + (periodEnd - periodStart)
         if   refType == 'LTA':
            rv = np.float32(testValueAggregateLTA(p[0], p[1], (refStart, refEnd), 1981, 2015, integrate, aggregate, iwindow))
            if not math.isnan(rv): pvl.append(v - rv)
         elif refType == 'ABS':
            # In service implementation data is read back from TIFF at 32-bit resolution.
            rv = np.float32(testValueAggregate(p[0], p[1], (refStart, refEnd), integrate, aggregate, iwindow))
            if not math.isnan(rv): pvl.append(v - rv)
         else:
            pvl.append(v)

   area = round(area, 2)
   bs = np.percentile(pvl, [0.0, 25.0, 50.0, 75.0, 100.0], axis=0)
   value = np.average(pvl)
   asum  = np.sum(pvl)

   # Area computation is imprecise due to summation over many points
   testlib.check(testcase, "area  ", round(response['area'],1),   round(area,1))
   if indicator.endswith('_LTA'):
      pstart, pend = getMeasurementDoy(periodStart, periodEnd)
      testlib.check(testcase, "pstart", response['pstart'], pstart)
      testlib.check(testcase, "pend  ", response['pend'],   pend)
   else:
      pstart = getMeasurementIntervalStart(periodStart)
      pend   = getMeasurementIntervalEnd(periodEnd)
      testlib.check(testcase, "pstart", response['pstart'], pstart.isoformat())
      testlib.check(testcase, "pend  ", response['pend'],   pend.isoformat())
   testlib.check(testcase, "mean  ", response['mean'],   value, gFLOAT_TOLERANCE)
   testlib.check(testcase, "sum   ", response['sum'],    asum,  gFLOAT_TOLERANCE)
   testlib.check(testcase, "P00   ", response['P00'],    bs[0], gFLOAT_TOLERANCE)
   testlib.check(testcase, "P25   ", response['P25'],    bs[1], gFLOAT_TOLERANCE)
   testlib.check(testcase, "P50   ", response['P50'],    bs[2], gFLOAT_TOLERANCE)
   testlib.check(testcase, "P75   ", response['P75'],    bs[3], gFLOAT_TOLERANCE)
   testlib.check(testcase, "P100  ", response['P100'],   bs[4], gFLOAT_TOLERANCE)

   # Check request info
   rq = response['request']
   testlib.check(testcase, "rq-source        ", rq['source'],        "CHIRPS_5000m")
   testlib.check(testcase, "rq-indicator     ", rq['indicator'],     indicator)
   testlib.check(testcase, "rq-maskID        ", rq['maskID'],        maskID)
   testlib.check(testcase, "rq-periodStart   ", rq['periodStart'],   period[0].isoformat())
   testlib.check(testcase, "rq-periodEnd     ", rq['periodEnd'],     period[1].isoformat())
   testlib.check(testcase, "rq-integrateType ", rq['integrateType'], integrate)
   testlib.check(testcase, "rq-aggregateType ", rq['aggregateType'], aggregate)
   testlib.check(testcase, "rq-reftype       ", rq['referenceType'], refType)
   testlib.check(testcase, "rq-window        ", rq['windowLength'],  window)
   if refType <> 'NONE':
      testlib.check(testcase, "rq-refdate    ", rq['referenceDate'], refDate.isoformat())


#######################################
# Test cases 
#######################################

#-----------------------------------------------------------

def init():

   testcase = __file__ + "_0_"
   dates = getMeasurementIntervals(testcase, 'Precipitation:GetDates', 'CHIRPS_5000m', 'PRECIP_PRELIM')

   # Check all intervals
   for i in range(0, len(dates)-1):
      delta = dates[i+1] - dates[i]
      expected_delta = 5

      # Last measurement interval in month is not 10 days.
      year = dates[i].year
      ndate = dates[i+1]
      if   dates[i] == datetime.date(year, 12, 26):
          expected_delta = 6
      elif dates[i].day == 26:
         ndate = datetime.date(dates[i].year, dates[i].month+1, 1)
         expected_delta = (ndate - dates[i]).days

      testlib.check(testcase, "2 - %s" % dates[i], delta.days, expected_delta)

#-----------------------------------------------------------

def testcase_1():

   testcase = __file__ + "_1_"

   periodStart   = gPERIOD_1['periodStart']
   periodEnd     = gPERIOD_1['periodEnd']
   polygon       = open(gTEST_ROI_DIR + 'ROI-1.geojson', 'r').read()
   aggregateType = 'AVG'

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      aggregateType=aggregateType,
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]

   checkResponse(testcase, response, testcase, [(lon, lat)], (periodStart, periodEnd))

#----------------------------------------------------------------

def testcase_2():

   testcase = __file__ + "_2_"
   
   periodStart = gPERIOD_1['periodStart']
   periodEnd   = gPERIOD_1['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-2.geojson', 'r').read()
   aggregateType = 'AVG'

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      aggregateType=aggregateType,
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]

   checkResponse(testcase, response, testcase, [(lon, lat)], (periodStart, periodEnd))

#----------------------------------------------------------------

def testcase_3():

   testcase = __file__ + "_3_"
   
   periodStart = gPERIOD_1['periodStart']
   periodEnd   = gPERIOD_1['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-3.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType='AVG',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])
   
   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]

   checkResponse(testcase, response, testcase, [(lon, lat)], (periodStart, periodEnd))

#----------------------------------------------------------------

def testcase_4():

   testcase = __file__ + "_4_"

   periodStart = gPERIOD_1['periodStart']
   periodEnd   = gPERIOD_1['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-4.geojson', 'r').read()
   
   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType='AVG',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])
   
   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]

   checkResponse(testcase, response, testcase, [(lon, lat)], (periodStart, periodEnd))

#----------------------------------------------------------------

def testcase_5():

   testcase = __file__ + "_5_"
   
   periodStart = gPERIOD_1['periodStart']
   periodEnd   = gPERIOD_1['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-5.geojson', 'r').read()
   
   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType='AVG',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])
   
   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]

   checkResponse(testcase, response, testcase, [(lon, lat)], (periodStart, periodEnd))

#----------------------------------------------------------------

def testcase_6():

   testcase = __file__ + "_6_"
   
   periodStart = gPERIOD_1['periodStart']
   periodEnd   = gPERIOD_1['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-6.geojson', 'r').read()
   
   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType='AVG',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][1]
   pl = [(lon, lat)]
   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][1][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1][1]
   pl = pl + [(lon, lat)]

   checkResponse(testcase, response, testcase, pl, (periodStart, periodEnd))

#----------------------------------------------------------------

def testcase_7():

   testcase = __file__ + "_7_"

   periodStart = gPERIOD_1['periodStart']
   periodEnd   = gPERIOD_1['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-7.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType='AVG',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][1]
   pl = [(lon, lat)]

   checkResponse(testcase, response, testcase, pl, (periodStart, periodEnd))

#----------------------------------------------------------------

def testcase_8():

   testcase = __file__ + "_8_"

   periodStart = gPERIOD_1['periodStart']
   periodEnd   = gPERIOD_1['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-8.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType='AVG',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][1]
   pl = [(lon, lat)]

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][1][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][1][1]
   pl = pl + [(lon, lat)]

   checkResponse(testcase, response, testcase, pl, (periodStart, periodEnd))

#----------------------------------------------------------------

def testcase_9():

   testcase = __file__ + "_9_"

   periodStart = gPERIOD_1['periodStart']
   periodEnd   = gPERIOD_1['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-9.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType='AVG',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][1]
   pl = [(lon, lat)]

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][1][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][1][1]
   pl = pl + [(lon, lat)]

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][2][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][2][1]
   pl = pl + [(lon, lat)]

   checkResponse(testcase, response, testcase, pl, (periodStart, periodEnd))

#----------------------------------------------------------------

def testcase_10():

   testcase = __file__ + "_10_"

   periodStart = gPERIOD_1['periodStart']
   periodEnd   = gPERIOD_1['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-10.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType='AVG',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][1][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][1][1]
   p1 = world2pix(lon, lat)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][2][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][2][1]
   p0 = world2pix(lon, lat)

   pl = []
   nX = p1[0]-p0[0]+1; nY = 2*nX-1
   for y in range(0, nX):
      for x in range(0, y+1):
         pl = pl + [(lon+x*gRES_X, lat+y*gRES_Y)]
   for y in range(nX, 2*nX-1):
      for x in range(0, 2*nX-y-1):
         pl = pl + [(lon+x*gRES_X, lat+y*gRES_Y)]

   checkResponse(testcase, response, testcase, pl, (periodStart, periodEnd))

#----------------------------------------------------------------

def testcase_11():

   testcase = __file__ + "_11_"

   periodStart = gPERIOD_1['periodStart']
   periodEnd   = gPERIOD_1['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-11.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType='AVG',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   polygon0 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   polygon1 = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   lon, lat = polygon0[0][0] # ring 0, point 0
   p0 = world2pix(lon, lat)
   lon, lat = polygon1[0][0] # ring 0, point 0
   p1 = world2pix(lon, lat)

   # Origin of returned image with small offset 
   # to prevent errors due to machine precision
   lon = gORG_X + p1[0]*gRES_X + gEPS_X
   lat = gORG_Y + p0[1]*gRES_Y + gEPS_Y

   # pixel extent in returned image
   p = (p0[0]-p1[0], 0)   # upper left first polygon
   q = (0, p1[1]-p0[1])   # upper left second polygon

   # First polygon 2x2, second polygon = 1x4
   nX = p[0]+1+1 ; nY = q[1]+3+1

   pl = []

   x, y = p
   pl = pl + [(lon+(x+0)*gRES_X, lat+(y+0)*gRES_Y)]
   pl = pl + [(lon+(x+1)*gRES_X, lat+(y+0)*gRES_Y)]
   pl = pl + [(lon+(x+0)*gRES_X, lat+(y+1)*gRES_Y)]
   pl = pl + [(lon+(x+1)*gRES_X, lat+(y+1)*gRES_Y)]

   x, y = q
   pl = pl + [(lon+(x+0)*gRES_X, lat+(y+0)*gRES_Y)]
   pl = pl + [(lon+(x+0)*gRES_X, lat+(y+1)*gRES_Y)]
   pl = pl + [(lon+(x+0)*gRES_X, lat+(y+2)*gRES_Y)]
   pl = pl + [(lon+(x+0)*gRES_X, lat+(y+3)*gRES_Y)]

   checkResponse(testcase, response, testcase, pl, (periodStart, periodEnd))

#----------------------------------------------------------------

def testcase_12():

   testcase = __file__ + "_12_"

   periodStart = gPERIOD_1['periodStart']
   periodEnd   = gPERIOD_1['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-12.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType='AVG',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][1]

   pl = []
   nX = 10; nY = 10
   for x in range(0, nX):
      for y in range(0, nY):
         pl = pl + [(lon+x*gRES_X, lat+y*gRES_Y)]

   checkResponse(testcase, response, testcase, pl, (periodStart, periodEnd))
   
#----------------------------------------------------------------

def testcase_13():

   testcase = __file__ + "_13_"

   periodStart = gPERIOD_2['periodStart']
   periodEnd   = gPERIOD_2['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-1.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType='AVG',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   pl = [(lon, lat)]

   checkResponse(testcase, response, testcase, pl, (periodStart, periodEnd))

#----------------------------------------------------------------

def testcase_14():

   testcase = __file__ + "_14_"

   periodStart = gPERIOD_3['periodStart']
   periodEnd   = gPERIOD_3['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-1.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType='AVG',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   pl = [(lon, lat)]

   checkResponse(testcase, response, testcase, pl, (periodStart, periodEnd))

#----------------------------------------------------------------

def testcase_15():

   testcase = __file__ + "_15_"

   periodStart = gPERIOD_4['periodStart']
   periodEnd   = gPERIOD_4['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-1.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType='AVG',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   pl = [(lon, lat)]

   checkResponse(testcase, response, testcase, pl, (periodStart, periodEnd))

#----------------------------------------------------------------

def testcase_16():

   testcase = __file__ + "_16_"

   periodStart = gPERIOD_5['periodStart']
   periodEnd   = gPERIOD_5['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-1.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType='AVG',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   pl = [(lon, lat)]

   checkResponse(testcase, response, testcase, pl, (periodStart, periodEnd))

#----------------------------------------------------------------

def testcase_17():

   testcase = __file__ + "_17_"

   periodStart = gPERIOD_6['periodStart']
   periodEnd   = gPERIOD_6['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-1.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType='AVG',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   pl = [(lon, lat)]

   checkResponse(testcase, response, testcase, pl, (periodStart, periodEnd))

#----------------------------------------------------------------

def testcase_18():

   testcase = __file__ + "_18_"

   periodStart = gPERIOD_7['periodStart']
   periodEnd   = gPERIOD_7['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-1.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType='AVG',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   pl = [(lon, lat)]

   checkResponse(testcase, response, testcase, pl, (periodStart, periodEnd))

#----------------------------------------------------------------

def testcase_19():

   testcase = __file__ + "_19_"

   periodStart = gPERIOD_8['periodStart']
   periodEnd   = gPERIOD_8['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-1.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType='AVG',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   pl = [(lon, lat)]

   checkResponse(testcase, response, testcase, pl, (periodStart, periodEnd))

#----------------------------------------------------------------

def testcase_20():

   testcase = __file__ + "_20_"

   periodStart = gPERIOD_9['periodStart']
   periodEnd   = gPERIOD_9['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-1.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType='AVG',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   pl = [(lon, lat)]

   checkResponse(testcase, response, testcase, pl, (periodStart, periodEnd))

#----------------------------------------------------------------

def testcase_21():

   testcase = __file__ + "_21_"

   indicator   = 'TEST'
   periodStart = gPERIOD_10['periodStart']
   periodEnd   = gPERIOD_10['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-1.geojson', 'r').read()
   integrate   = 'SUM'
   aggregate   = 'AVG'
   nintervals  = 6
   window      = nintervals*5
   maskID      = 'NONE'
   refType     = 'NONE'
   refDate     = None

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator=indicator,
      # integrateType=integrate, SUM is default
      windowLength=window,
      aggregateType=aggregate,
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   pl = [(lon, lat)]

   checkResponse(testcase, response, testcase, pl, (periodStart, periodEnd), window=window)

#----------------------------------------------------------------

def testcase_22():

   testcase = __file__ + "_22_"

   periodStart = gPERIOD_11['periodStart']
   periodEnd   = gPERIOD_11['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-1.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType='AVG',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   pl = [(lon, lat)]

   checkResponse(testcase, response, testcase, pl, (periodStart, periodEnd))

#----------------------------------------------------------------

def testcase_23():

   testcase = __file__ + "_23_"

   periodStart = gPERIOD_9['periodStart']
   periodEnd   = gPERIOD_9['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-12.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='TEST',
      aggregateType='AVG',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][1]

   pl = []
   nX = 10; nY = 10
   for x in range(0, nX):
      for y in range(0, nY):
         pl = pl + [(lon+x*gRES_X, lat+y*gRES_Y)]

   checkResponse(testcase, response, testcase, pl, (periodStart, periodEnd))

#----------------------------------------------------------------

def testcase_24():

   testcase = __file__ + "_24_"

   periodStart = gPERIOD_1['periodStart']
   periodEnd   = gPERIOD_1['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-1.geojson', 'r').read()
   indicator   = 'TEST_LTA'

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator=indicator,
      aggregateType='AVG',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   pl = [(lon, lat)]

   checkResponse(testcase, response, testcase, pl, (periodStart, periodEnd), indicator)

#----------------------------------------------------------------

def testcase_25():

   testcase = __file__ + "_25_"

   periodStart = gPERIOD_3['periodStart']
   periodEnd   = gPERIOD_3['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-2.geojson', 'r').read()
   indicator   = 'TEST_LTA'

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator=indicator,
      aggregateType='AVG',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   pl = [(lon, lat)]

   checkResponse(testcase, response, testcase, pl, (periodStart, periodEnd), indicator)

#----------------------------------------------------------------

def testcase_26():

   testcase = __file__ + "_26_"

   periodStart = gPERIOD_8['periodStart']
   periodEnd   = gPERIOD_8['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-2.geojson', 'r').read()
   indicator   = 'TEST_LTA'

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator=indicator,
      aggregateType='AVG',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   pl = [(lon, lat)]

   checkResponse(testcase, response, testcase, pl, (periodStart, periodEnd), indicator)

#----------------------------------------------------------------

def testcase_27():

   testcase = __file__ + "_27_"

   periodStart = gPERIOD_9['periodStart']
   periodEnd   = gPERIOD_9['periodEnd']
   polygon     = open(gTEST_ROI_DIR + 'ROI-2.geojson', 'r').read()
   indicator   = 'TEST_LTA'

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator=indicator,
      aggregateType='AVG',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   pl = [(lon, lat)]

   checkResponse(testcase, response, testcase, pl, (periodStart, periodEnd), indicator)

#----------------------------------------------------------------

def testcase_28():

   testcase = __file__ + "_28_"

   periodStart   = gPERIOD_9['periodStart']
   periodEnd     = gPERIOD_9['periodEnd']
   polygon       = open(gTEST_ROI_DIR + 'ROI-2.geojson', 'r').read()
   indicator     = 'TEST_LTA'
   aggregateType = 'MAX'
   integrateType = 'SUM'
   refType       = 'NONE'
   refDate       = 'NONE'

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator=indicator,
      aggregateType=aggregateType,
      integrateType=integrateType,
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      referenceType=refType,
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]

   pl = [(lon, lat)]
   checkResponse(testcase, response, testcase, pl, (periodStart, periodEnd), 
                 indicator, refType, refDate, aggregateType, integrateType)

#----------------------------------------------------------------

def testcase_29():

   testcase = __file__ + "_29_"

   periodStart = gPERIOD_1['periodStart']
   periodEnd   = gPERIOD_1['periodEnd']
   indicator   = 'TEST'
   refStart    = REFSTART_1(periodStart)
   refType     = 'LTA'
   polygon     = open(gTEST_ROI_DIR + 'ROI-1.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator=indicator,
      aggregateType='AVG',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      referenceStart=refStart.isoformat(),
      referenceType=refType,
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   pl = [(lon, lat)]

   checkResponse(testcase, response, testcase, pl, (periodStart, periodEnd), indicator, refType, refStart)

#----------------------------------------------------------------

def testcase_30():

   testcase = __file__ + "_30_"

   periodStart = gPERIOD_1['periodStart']
   periodEnd   = gPERIOD_1['periodEnd']
   indicator   = 'TEST'
   refStart    = REFSTART_2(periodStart)
   refType     = 'LTA'
   polygon     = open(gTEST_ROI_DIR + 'ROI-1.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator=indicator,
      aggregateType='AVG',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      referenceStart=refStart.isoformat(),
      referenceType=refType,
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   pl = [(lon, lat)]

   checkResponse(testcase, response, testcase, pl, (periodStart, periodEnd), indicator, refType, refStart)

#----------------------------------------------------------------

def testcase_31():

   testcase = __file__ + "_31_"

   periodStart = gPERIOD_1['periodStart']
   periodEnd   = gPERIOD_1['periodEnd']
   indicator   = 'TEST'
   refStart    = REFSTART_3(periodStart)
   refType     = 'LTA'
   polygon     = open(gTEST_ROI_DIR + 'ROI-1.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator=indicator,
      aggregateType='AVG',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      referenceStart=refStart.isoformat(),
      referenceType=refType,
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   pl = [(lon, lat)]

   checkResponse(testcase, response, testcase, pl, (periodStart, periodEnd), indicator, refType, refStart)

#----------------------------------------------------------------

def testcase_32():


   testcase = __file__ + "_32_"

   periodStart   = gPERIOD_9['periodStart']
   periodEnd     = gPERIOD_9['periodEnd']  
   polygon       = open(gTEST_ROI_DIR + 'ROI-11.geojson', 'r').read()
   indicator     = 'TEST'
   aggregateType = 'MIN'
   refType       = 'NONE'
   refStart      = 'NONE'

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',                     
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator=indicator,                                                  
      aggregateType=aggregateType,                                       
      periodStart=str(periodStart),                                      
      periodEnd=str(periodEnd),                                          
      referenceType=refType,
      label=testcase,                                                    
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])


   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   polygon0 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   polygon1 = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   lon, lat = polygon0[0][0] # ring 0, point 0                                
   p0 = world2pix(lon, lat)                                                   
   lon, lat = polygon1[0][0] # ring 0, point 0                                
   p1 = world2pix(lon, lat)                                                   

   # Origin of returned image with small offset
   # to prevent errors due to machine precision
   lon = gORG_X + p1[0]*gRES_X + gEPS_X        
   lat = gORG_Y + p0[1]*gRES_Y + gEPS_Y        

   # pixel extent in returned image
   p = (p0[0]-p1[0], 0)   # upper left first polygon
   q = (0, p1[1]-p0[1])   # upper left second polygon

   # First polygon 2x2, second polygon = 1x4
   nX = p[0]+1+1 ; nY = q[1]+3+1            

   mask = np.array( [[ False, True, False, False],
                     [ False, True, False, False],
                     [ False, True, True,  True ],
                     [ False, True, True,  True ]] )

   pl = []
   for x in range(0, 4):
      for y in range(0, 4):
         p = (lon+x*gRES_X, lat+y*gRES_Y)
         if not mask[y, x]:              
            pl.append(p)

   checkResponse(testcase, response, testcase, pl, (periodStart, periodEnd), indicator, refType, refStart, aggregateType)
                                                                                    
#----------------------------------------------------------------                   

def testcase_33():


   testcase = __file__ + "_33_"

   periodStart   = gPERIOD_9['periodStart']
   periodEnd     = gPERIOD_9['periodEnd']  
   polygon       = open(gTEST_ROI_DIR + 'ROI-11.geojson', 'r').read()
   indicator     = 'TEST'
   aggregateType = 'MAX'                                             
   refType       = 'NONE'
   refStart      = 'NONE'

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',                     
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator=indicator,                                                  
      aggregateType=aggregateType,                                       
      periodStart=str(periodStart),                                      
      periodEnd=str(periodEnd),                                          
      referenceType=refType,
      label=testcase,                                                    
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   polygon0 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   polygon1 = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   lon, lat = polygon0[0][0] # ring 0, point 0                                
   p0 = world2pix(lon, lat)                                                   
   lon, lat = polygon1[0][0] # ring 0, point 0                                
   p1 = world2pix(lon, lat)                                                   

   # Origin of returned image with small offset
   # to prevent errors due to machine precision
   lon = gORG_X + p1[0]*gRES_X + gEPS_X        
   lat = gORG_Y + p0[1]*gRES_Y + gEPS_Y        

   # pixel extent in returned image
   p = (p0[0]-p1[0], 0)   # upper left first polygon
   q = (0, p1[1]-p0[1])   # upper left second polygon

   # First polygon 2x2, second polygon = 1x4
   nX = p[0]+1+1 ; nY = q[1]+3+1            

   mask = np.array( [[ False, True, False, False],
                     [ False, True, False, False],
                     [ False, True, True,  True ],
                     [ False, True, True,  True ]] )

   pl = []
   for x in range(0, 4):
      for y in range(0, 4):
         p = (lon+x*gRES_X, lat+y*gRES_Y)
         if not mask[y, x]:
            pl.append(p)

   checkResponse(testcase, response, testcase, pl, (periodStart, periodEnd), indicator, refType, refStart, aggregateType)

#----------------------------------------------------------------

def testcase_34():


   testcase = __file__ + "_34_"

   periodStart   = gPERIOD_9['periodStart']
   periodEnd     = gPERIOD_9['periodEnd']  
   polygon       = open(gTEST_ROI_DIR + 'ROI-11.geojson', 'r').read()
   indicator     = 'TEST'
   aggregateType = 'MAX'                                             
   integrateType = 'SUM'                                             
   refType       = 'NONE'
   refStart      = 'NONE'

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',                     
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator=indicator,                                                  
      aggregateType=aggregateType,                                       
      integrateType=integrateType,                                       
      periodStart=str(periodStart),                                      
      periodEnd=str(periodEnd),                                          
      referenceType=refType,
      label=testcase,                                                    
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   polygon0 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   polygon1 = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   lon, lat = polygon0[0][0] # ring 0, point 0                                
   p0 = world2pix(lon, lat)                                                   
   lon, lat = polygon1[0][0] # ring 0, point 0                                
   p1 = world2pix(lon, lat)                                                   

   # Origin of returned image with small offset
   # to prevent errors due to machine precision
   lon = gORG_X + p1[0]*gRES_X + gEPS_X        
   lat = gORG_Y + p0[1]*gRES_Y + gEPS_Y        

   # pixel extent in returned image
   p = (p0[0]-p1[0], 0)   # upper left first polygon
   q = (0, p1[1]-p0[1])   # upper left second polygon

   # First polygon 2x2, second polygon = 1x4
   nX = p[0]+1+1 ; nY = q[1]+3+1            

   mask = np.array( [[ False, True, False, False],
                     [ False, True, False, False],
                     [ False, True, True,  True ],
                     [ False, True, True,  True ]] )

   pl = []
   for x in range(0, 4):
      for y in range(0, 4):
         p = (lon+x*gRES_X, lat+y*gRES_Y)
         if not mask[y, x]:
            pl.append(p)

   checkResponse(testcase, response, testcase, 
                 pl, (periodStart, periodEnd), indicator, refType, refStart, aggregateType, integrateType)

#----------------------------------------------------------------

def testcase_35():


   testcase = __file__ + "_35_"

   periodStart   = gPERIOD_9['periodStart']
   periodEnd     = gPERIOD_9['periodEnd']  
   polygon       = open(gTEST_ROI_DIR + 'ROI-11.geojson', 'r').read()
   indicator     = 'TEST'
   aggregateType = 'MAX'                                             
   integrateType = 'SUM'                                             
   refType       = 'NONE'
   refStart      = 'NONE'

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',                     
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator=indicator,
      aggregateType=aggregateType,                                       
      integrateType=integrateType,                                       
      periodStart=str(periodStart),                                      
      periodEnd=str(periodEnd),                                          
      referenceType=refType,
      label=testcase,                                                    
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   polygon0 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   polygon1 = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   lon, lat = polygon0[0][0] # ring 0, point 0
   p0 = world2pix(lon, lat)
   lon, lat = polygon1[0][0] # ring 0, point 0
   p1 = world2pix(lon, lat)

   # Origin of returned image with small offset
   # to prevent errors due to machine precision
   lon = gORG_X + p1[0]*gRES_X + gEPS_X
   lat = gORG_Y + p0[1]*gRES_Y + gEPS_Y

   # pixel extent in returned image
   p = (p0[0]-p1[0], 0)   # upper left first polygon
   q = (0, p1[1]-p0[1])   # upper left second polygon

   # First polygon 2x2, second polygon = 1x4
   nX = p[0]+1+1 ; nY = q[1]+3+1

   mask = np.array( [[ False, True, False, False],
                     [ False, True, False, False],
                     [ False, True, True,  True ],
                     [ False, True, True,  True ]] )

   pl = []
   for x in range(0, 4):
      for y in range(0, 4):
         p = (lon+x*gRES_X, lat+y*gRES_Y)
         if not mask[y, x]:
            pl.append(p)

   checkResponse(testcase, response, testcase,
                 pl, (periodStart, periodEnd), indicator, refType, refStart, aggregateType, integrateType)

#----------------------------------------------------------------

def testcase_36():


   testcase = __file__ + "_36_"

   periodStart    = gPERIOD_9['periodStart']
   periodEnd      = gPERIOD_9['periodEnd']  
   polygon        = open(gTEST_ROI_DIR + 'ROI-11.geojson', 'r').read()
   indicator      = 'TEST'
   aggregateType  = 'AVG'                                             
   integrateType  = 'SUM'                                             
   referenceType  = 'ABS'                                             
   referenceStart = REFSTART_1(periodStart, 0)                        

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',                     
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator=indicator,
      aggregateType=aggregateType,                                       
      integrateType=integrateType,                                       
      periodStart=str(periodStart),                                      
      periodEnd=str(periodEnd),                                          
      referenceType=referenceType,                                       
      referenceStart=str(referenceStart),                                
      label=testcase,                                                    
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   polygon0 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   polygon1 = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   lon, lat = polygon0[0][0] # ring 0, point 0
   p0 = world2pix(lon, lat)
   lon, lat = polygon1[0][0] # ring 0, point 0
   p1 = world2pix(lon, lat)

   # Origin of returned image with small offset
   # to prevent errors due to machine precision
   lon = gORG_X + p1[0]*gRES_X + gEPS_X
   lat = gORG_Y + p0[1]*gRES_Y + gEPS_Y

   # pixel extent in returned image
   p = (p0[0]-p1[0], 0)   # upper left first polygon
   q = (0, p1[1]-p0[1])   # upper left second polygon

   # First polygon 2x2, second polygon = 1x4
   nX = p[0]+1+1 ; nY = q[1]+3+1

   mask = np.array( [[ False, True, False, False],
                     [ False, True, False, False],
                     [ False, True, True,  True ],
                     [ False, True, True,  True ]] )

   pl = []
   for x in range(0, 4):
      for y in range(0, 4):
         p = (lon+x*gRES_X, lat+y*gRES_Y)
         if not mask[y, x]:
            pl.append(p)

   checkResponse(testcase, response, testcase,
                 pl, (periodStart, periodEnd), indicator, referenceType, referenceStart, aggregateType, integrateType)

#----------------------------------------------------------------

def testcase_37():


   testcase = __file__ + "_37_"

   periodStart    = gPERIOD_9['periodStart']
   periodEnd      = gPERIOD_9['periodEnd']  
   polygon        = open(gTEST_ROI_DIR + 'ROI-11.geojson', 'r').read()
   indicator      = 'TEST'
   aggregateType  = 'AVG'                                             
   integrateType  = 'SUM'                                             
   referenceType  = 'LTA'                                             
   referenceStart = REFSTART_1(periodStart, 0)                        

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',                     
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator=indicator,
      aggregateType=aggregateType,                                       
      integrateType=integrateType,                                       
      periodStart=str(periodStart),                                      
      periodEnd=str(periodEnd),                                          
      referenceType=referenceType,                                       
      referenceStart=str(referenceStart),                                
      label=testcase,                                                    
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   polygon0 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   polygon1 = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   lon, lat = polygon0[0][0] # ring 0, point 0
   p0 = world2pix(lon, lat)
   lon, lat = polygon1[0][0] # ring 0, point 0
   p1 = world2pix(lon, lat)

   # Origin of returned image with small offset
   # to prevent errors due to machine precision
   lon = gORG_X + p1[0]*gRES_X + gEPS_X
   lat = gORG_Y + p0[1]*gRES_Y + gEPS_Y

   # pixel extent in returned image
   p = (p0[0]-p1[0], 0)   # upper left first polygon
   q = (0, p1[1]-p0[1])   # upper left second polygon

   # First polygon 2x2, second polygon = 1x4
   nX = p[0]+1+1 ; nY = q[1]+3+1

   mask = np.array( [[ False, True, False, False],
                     [ False, True, False, False],
                     [ False, True, True,  True ],
                     [ False, True, True,  True ]] )

   pl = []
   for x in range(0, 4):
      for y in range(0, 4):
         p = (lon+x*gRES_X, lat+y*gRES_Y)
         if not mask[y, x]:
            pl.append(p)

   checkResponse(testcase, response, testcase,
                 pl, (periodStart, periodEnd), indicator, referenceType, referenceStart, aggregateType, integrateType)

#----------------------------------------------------------------

def testcase_38():


   testcase = __file__ + "_38_"

   periodStart    = gPERIOD_9['periodStart']
   periodEnd      = gPERIOD_9['periodEnd']
   polygon        = open(gTEST_ROI_DIR + 'ROI-11.geojson', 'r').read()
   maskID         = 'TEST'
   indicator      = 'TEST'
   aggregateType  = 'AVG'
   integrateType  = 'SUM'
   referenceType  = 'NONE'

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      maskID=maskID,
      indicator=indicator,
      aggregateType=aggregateType,
      integrateType=integrateType,
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      referenceType=referenceType,
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   polygon0 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   polygon1 = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   lon, lat = polygon0[0][0] # ring 0, point 0
   p0 = world2pix(lon, lat)
   lon, lat = polygon1[0][0] # ring 0, point 0
   p1 = world2pix(lon, lat)

   # Origin of returned image with small offset
   # to prevent errors due to machine precision
   lon = gORG_X + p1[0]*gRES_X + gEPS_X
   lat = gORG_Y + p0[1]*gRES_Y + gEPS_Y

   # pixel extent in returned image
   p = (p0[0]-p1[0], 0)   # upper left first polygon
   q = (0, p1[1]-p0[1])   # upper left second polygon

   # First polygon 2x2, second polygon = 1x4
   nX = p[0]+1+1 ; nY = q[1]+3+1

   mask = np.array( [[ True,  True, True, False],
                     [ True,  True, True, False],
                     [ False, True, True, True ],
                     [ False, True, True, True ]] )

   pl = []
   for x in range(0, 4):
      for y in range(0, 4):
         p = (lon+x*gRES_X, lat+y*gRES_Y)
         if not mask[y, x]:
            pl.append(p)

   checkResponse(testcase, response, testcase,
                 pl, (periodStart, periodEnd), indicator, referenceType, None, aggregateType, integrateType, maskID)


#----------------------------------------------------------------

def testcase_39():


   testcase = __file__ + "_39_"

   periodStart    = gPERIOD_9['periodStart']
   periodEnd      = gPERIOD_9['periodEnd']
   polygon        = open(gTEST_ROI_DIR + 'ROI-11.geojson', 'r').read()
   maskID         = 'ESA_CCI_2010'
   indicator      = 'TEST'
   aggregateType  = 'AVG'
   integrateType  = 'SUM'
   referenceType  = 'NONE'

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      maskID=maskID,
      indicator=indicator,
      aggregateType=aggregateType,
      integrateType=integrateType,
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      referenceType=referenceType,
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   polygon0 = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
   polygon1 = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]
   lon, lat = polygon0[0][0] # ring 0, point 0
   p0 = world2pix(lon, lat)
   lon, lat = polygon1[0][0] # ring 0, point 0
   p1 = world2pix(lon, lat)

   # Origin of returned image with small offset
   # to prevent errors due to machine precision
   lon = gORG_X + p1[0]*gRES_X + gEPS_X
   lat = gORG_Y + p0[1]*gRES_Y + gEPS_Y

   # pixel extent in returned image
   p = (p0[0]-p1[0], 0)   # upper left first polygon
   q = (0, p1[1]-p0[1])   # upper left second polygon

   # First polygon 2x2, second polygon = 1x4
   nX = p[0]+1+1 ; nY = q[1]+3+1


   mask = np.array( [[ False, True, False, False],
                     [ False, True, False, False],
                     [ False, True, True,  True ],
                     [ False, True, True,  True ]] )

   pl = []
   for x in range(0, 4):
      for y in range(0, 4):
         p = (lon+x*gRES_X, lat+y*gRES_Y)
         if not mask[y, x]:
            pl.append(p)

   checkResponse(testcase, response, testcase,
                 pl, (periodStart, periodEnd), indicator, referenceType, None, aggregateType, integrateType, maskID)

#----------------------------------------------------------------

def datatest_1():


   testcase = __file__ + "datatest_1_"

   periodStart    = datetime.date(2000, 1, 1)
   periodEnd      = datetime.date(2000,12,31)
   polygon        = open(gTEST_ROI_DIR + 'ROI-Afar-Ewa.geojson', 'r').read()
   maskID         = 'NONE'
   indicator      = 'PRECIP_FINAL'
   aggregateType  = 'MAX'
   integrateType  = 'SUM'
   referenceType  = 'NONE'

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      maskID=maskID,
      indicator=indicator,
      aggregateType=aggregateType,
      integrateType=integrateType,
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      referenceType=referenceType,
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   testlib.check(testcase, "N     ", response['N'], 59)


#----------------------------------------------------------------

def datatest_2():


   testcase = __file__ + "datatest_2_"

   periodStart    = datetime.date(2000, 1, 1)
   periodEnd      = datetime.date(2000,12,31)
   polygon        = open(gTEST_ROI_DIR + 'ROI-Afar-Ewa.geojson', 'r').read()
   maskID         = 'ESA_CCI_2010'
   indicator      = 'PRECIP_PRELIM'
   aggregateType  = 'MAX'
   integrateType  = 'SUM'
   referenceType  = 'NONE'

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      maskID=maskID,
      indicator=indicator,
      aggregateType=aggregateType,
      integrateType=integrateType,
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      referenceType=referenceType,
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   testlib.check(testcase, "N     ", response['N'], 53)

#----------------------------------------------------------------

def datatest_3():

   testcase = __file__ + "datatest_3_"

   periodStart    = datetime.date(2000, 1, 1)
   periodEnd      = datetime.date(2000,12,31)
   polygon        = open(gTEST_ROI_DIR + 'ROI-Afar-Ewa.geojson', 'r').read()
   maskID         = 'ESA_CCI_2010'
   indicator      = 'PRECIP_LTA'
   aggregateType  = 'MAX'
   integrateType  = 'SUM'
   referenceType  = 'NONE'

   request = testlib.buildWpsRequest(
      process='Precipitation:PeriodAggregateROI',
      source='CHIRPS_5000m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      maskID=maskID,
      indicator=indicator,
      aggregateType=aggregateType,
      integrateType=integrateType,
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      referenceType=referenceType,
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result   = open(outFileName, 'r').read()
   response = json.loads(result)
   if testlib.getTrace() > 1: print json.dumps(response)

   testlib.check(testcase, "N     ", response['N'], 53)

#--------------------------- end of testcases --------------------------------------
# Get all functions names testcase_<nr> as default test cases
testCaseIds = [ x for x in dir() if re.match('.+_[0-9]+', x) ]
testCaseIds.sort()
testcases = {}
for testCaseId in testCaseIds:
   testcases[testCaseId] = locals()[testCaseId]
init()
testlib.main(testcases)
