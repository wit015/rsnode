#!/usr/bin/python

##################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
##################################################################################

import re
import os
import sys
import json
import csv
import datetime
import math

import numpy as np

from osgeo import gdal

import testlib

from testValues import *
from common import *

setGeoTransform(gORG_X, gRES_X, gEPS_X, gORG_Y, gRES_Y, gEPS_Y)


# Global script identifiers
svn_revision = str('$Rev: 7872 $')
svn_date = str('$Date: 2017-01-30 21:38:29 +0100 (Mon, 30 Jan 2017) $')
svn_author = str('$Author: jb76278 $')
script_file_name = os.path.basename(__file__)
now_string = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

# To make sure the whole array is printed.
np.set_printoptions(threshold='nan')

def checkTimeSeriesByYear(testcase, response, label, pointList, period, aggregate='AVG', integrate='SUM', refType='NONE', maskID='NONE', windowLength=0):

    iwindow = windowLength / 5

    periodStartList = getPeriodStartList()

    ni = [ i for i in range(0, len(periodStartList)) if periodStartList[i].timetuple().tm_yday == 1 ]
    nipy = np.diff(np.array(ni))[0]

    ts = response['timeseries']
    rq = response['request']

    # Get DOY for start and end of period
    sidx, eidx = getMeasurementIndex(period[0], period[1])
    if sidx > eidx:
        eidx = eidx+nipy

    testlib.check(testcase, "type  ", ts['type'],  'TimeSeriesByYear')
    testlib.check(testcase, "label ", ts['label'], label)

    rvl = []
    if refType == 'LTA':
        for p in pointList:
            rv = testValueAggregateLTA(p[0], p[1], (period[0], period[1]), 1981, 2015, integrate, aggregate, iwindow)
            if not math.isnan(rv): rvl.append(rv)

    # For all years
    j = 0
    yvl = [] # Year value list
    for di in range(ni[0]+sidx, len(periodStartList)-(eidx-sidx), nipy):

        if testlib.getTrace() > 1 : print "Checking year %d" % y

        # Check whether the start date is before the begin of first measurement interval
        sd = periodStartList[di]
        ed = periodStartList[di+(eidx-sidx)]

        i = 0
        pvl = [] # Value list over all points
        # For all points
        for p in pointList:
            v = testValueAggregate(p[0], p[1], (sd, ed), integrate, aggregate, iwindow)
            if not math.isnan(v) : 
                if refType == 'LTA':
                    if not math.isnan(rvl[i]): pvl.append(v - rvl[i])
                else:
                    pvl.append(v)
            i += 1

        yvl.append(np.average(pvl))

        pstart = sd
        pend   = getMeasurementIntervalEnd(ed)

        testlib.check(testcase, "ts-pstart-%02d" % j, ts['pstart'][j], pstart.isoformat())
        testlib.check(testcase, "ts-pend-%02d  " % j, ts['pend'][j],   pend.isoformat())
        testlib.check(testcase, "ts-value-%02d " % j, ts['value'][j],  yvl[-1], gFLOAT_TOLERANCE)
    
        j += 1

    bs  = np.percentile(yvl, [0.0, 25.0, 50.0, 75.0, 100.0])
    # print yvl
    avg = np.mean(yvl)

    # Check number of points in time series
    testlib.check(testcase, "N     ", ts['N'], len(yvl))

    # Check the base statistics returned
    testlib.check(testcase, "ts-P00-%02d   " % j, ts['P00'],  bs[0], gFLOAT_TOLERANCE)
    testlib.check(testcase, "ts-P25-%02d   " % j, ts['P25'],  bs[1], gFLOAT_TOLERANCE)
    testlib.check(testcase, "ts-P50-%02d   " % j, ts['P50'],  bs[2], gFLOAT_TOLERANCE)
    testlib.check(testcase, "ts-P75-%02d   " % j, ts['P75'],  bs[3], gFLOAT_TOLERANCE)
    testlib.check(testcase, "ts-P100-%02d  " % j, ts['P100'], bs[4], gFLOAT_TOLERANCE)
    testlib.check(testcase, "ts-Avg-%02d   " % j, ts['Avg'],  avg, gFLOAT_TOLERANCE)
    
    # Check request info
    testlib.check(testcase, "rq-source        ", rq['source'],        "CHIRPS_5000m")
    testlib.check(testcase, "rq-indicator     ", rq['indicator'],     "TEST")
    testlib.check(testcase, "rq-maskID        ", rq['maskID'],        maskID)
    testlib.check(testcase, "rq-periodStart   ", rq['periodStart'],   period[0].isoformat())
    testlib.check(testcase, "rq-periodEnd     ", rq['periodEnd'],     period[1].isoformat())
    testlib.check(testcase, "rq-aggregateType ", rq['aggregateType'], aggregate)
    testlib.check(testcase, "rq-integrateType ", rq['integrateType'], integrate)
    testlib.check(testcase, "rq-window        ", rq['windowLength'],  windowLength)
    if refType <> None:
        testlib.check(testcase, "rq-reftype       ", rq['referenceType'], refType)


#######################################
# Test cases 
#######################################

#-----------------------------------------------------------

def init():

    testcase = __file__ + "_0_"
    dates = getMeasurementIntervals(testcase, 'Precipitation:GetDates', 'CHIRPS_5000m', 'TEST')

    # Check all intervals
    for i in range(0, len(dates)-1):
        delta = dates[i+1] - dates[i]
        expected_delta = 5

        # Last measurement interval in month is not 5 days.
        year = dates[i].year
        ndate = dates[i+1]
        if   dates[i] == datetime.date(year, 12, 26):
            expected_delta = 6
        elif dates[i].day == 26:
            ndate = datetime.date(dates[i].year, dates[i].month+1, 1)
            expected_delta = (ndate - dates[i]).days

        testlib.check(testcase, "2 - %s" % dates[i], delta.days, expected_delta)

#-----------------------------------------------------------

def testcase_1():

    testcase = __file__ + "_1_"

    periodStart   = gPERIOD_1['periodStart']
    periodEnd     = gPERIOD_1['periodEnd']
    polygon       = open(gTEST_ROI_DIR + 'ROI-1.geojson', 'r').read()
    aggregateType = 'AVG'

    request = testlib.buildWpsRequest(
        process='Precipitation:TimeSeriesByYear',
        source='CHIRPS_5000m',
        ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
        indicator='TEST',
        periodStart=periodStart.isoformat(),
        periodEnd=periodEnd.isoformat(),
        aggregateType=aggregateType,
        label=testcase,
        response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

    outFileName = ('/tmp/%s.json' % testcase)
    issueRequest(request, testcase, 1, outFileName)
    result   = open(outFileName, 'r').read()
    response = json.loads(result)
    if testlib.getTrace() > 1: print json.dumps(response)

    lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
    lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]

    checkTimeSeriesByYear(testcase, response, testcase, [(lon, lat)], (periodStart, periodEnd))

#-----------------------------------------------------------

def testcase_2():

    testcase = __file__ + "_2_"

    periodStart   = gPERIOD_8['periodStart']
    periodEnd     = gPERIOD_8['periodEnd']
    polygon       = open(gTEST_ROI_DIR + 'ROI-2.geojson', 'r').read()
    aggregateType = 'AVG'

    request = testlib.buildWpsRequest(
        process='Precipitation:TimeSeriesByYear',
        source='CHIRPS_5000m',
        ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
        indicator='TEST',
        periodStart=periodStart.isoformat(),
        periodEnd=periodEnd.isoformat(),
        aggregateType=aggregateType,
        label=testcase,
        response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

    outFileName = ('/tmp/%s.json' % testcase)
    issueRequest(request, testcase, 1, outFileName)
    result   = open(outFileName, 'r').read()
    response = json.loads(result)
    if testlib.getTrace() > 1: print json.dumps(response)

    lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
    lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]

    checkTimeSeriesByYear(testcase, response, testcase, [(lon, lat)], (periodStart, periodEnd))


#-----------------------------------------------------------

def testcase_3():

    testcase = __file__ + "_3_"

    periodStart = gPERIOD_9['periodStart']
    periodEnd   = gPERIOD_9['periodEnd']
    polygon     = open(gTEST_ROI_DIR + 'ROI-3.geojson', 'r').read()
    aggregateType = 'AVG'

    request = testlib.buildWpsRequest(
        process='Precipitation:TimeSeriesByYear',
        source='CHIRPS_5000m',
        ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
        indicator='TEST',
        periodStart=periodStart.isoformat(),
        periodEnd=periodEnd.isoformat(),
        aggregateType=aggregateType,
        label=testcase,
        response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

    outFileName = ('/tmp/%s.json' % testcase)
    issueRequest(request, testcase, 1, outFileName)
    result   = open(outFileName, 'r').read()
    response = json.loads(result)
    if testlib.getTrace() > 1: print json.dumps(response)

    lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
    lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]

    checkTimeSeriesByYear(testcase, response, testcase, [(lon, lat)], (periodStart, periodEnd))


#-----------------------------------------------------------

def testcase_4():

    testcase = __file__ + "_4_"

    periodStart = gPERIOD_4['periodStart']
    periodEnd   = gPERIOD_4['periodEnd']
    polygon     = open(gTEST_ROI_DIR + 'ROI-9.geojson', 'r').read()
    aggregateType = 'AVG'

    request = testlib.buildWpsRequest(
        process='Precipitation:TimeSeriesByYear',
        source='CHIRPS_5000m',
        ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
        indicator='TEST',
        periodStart=periodStart.isoformat(),
        periodEnd=periodEnd.isoformat(),
        aggregateType=aggregateType,
        label=testcase,
        response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

    outFileName = ('/tmp/%s.json' % testcase)
    issueRequest(request, testcase, 1, outFileName)
    result   = open(outFileName, 'r').read()
    response = json.loads(result)
    if testlib.getTrace() > 1: print json.dumps(response)

    pl = []
    for i in range(0, 3):
        lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][i][0]
        lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][i][1]
        pl.append((lon, lat))

    checkTimeSeriesByYear(testcase, response, testcase, pl, (periodStart, periodEnd))

#-----------------------------------------------------------

def testcase_5():

    testcase = __file__ + "_5_"

    periodStart = gPERIOD_4['periodStart']
    periodEnd   = gPERIOD_4['periodEnd']
    polygon     = open(gTEST_ROI_DIR + 'ROI-12.geojson', 'r').read()
    aggregateType = 'AVG'

    request = testlib.buildWpsRequest(
        process='Precipitation:TimeSeriesByYear',
        source='CHIRPS_5000m',
        ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
        indicator='TEST',
        periodStart=periodStart.isoformat(),
        periodEnd=periodEnd.isoformat(),
        aggregateType=aggregateType,
        label=testcase,
        response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

    outFileName = ('/tmp/%s.json' % testcase)
    issueRequest(request, testcase, 1, outFileName)
    result   = open(outFileName, 'r').read()
    response = json.loads(result)
    if testlib.getTrace() > 1: print json.dumps(response)

    lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][0]
    lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][0][0][1]

    pl = []
    nX = 10; nY = 10
    for x in range(0, nX):
        for y in range(0, nY):
            pl.append((lon+x*gRES_X, lat+y*gRES_Y))

    checkTimeSeriesByYear(testcase, response, testcase, pl, (periodStart, periodEnd))

#-----------------------------------------------------------

def testcase_6():

    testcase = __file__ + "_6_"

    periodStart = gPERIOD_8['periodStart']
    periodEnd   = gPERIOD_8['periodEnd']
    polygon     = open(gTEST_ROI_DIR + 'ROI-3.geojson', 'r').read()
    aggregateType = 'AVG'
    integrateType = 'NONE'
    referenceType = 'LTA'

    request = testlib.buildWpsRequest(
        process='Precipitation:TimeSeriesByYear',
        source='CHIRPS_5000m',
        ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
        indicator='TEST',
        periodStart=periodStart.isoformat(),
        periodEnd=periodEnd.isoformat(),
        aggregateType=aggregateType,
        integrateType=integrateType,
        referenceType=referenceType,
        label=testcase,
        response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

    outFileName = ('/tmp/%s.json' % testcase)
    issueRequest(request, testcase, 1, outFileName)
    result   = open(outFileName, 'r').read()
    response = json.loads(result)
    if testlib.getTrace() > 1: print json.dumps(response)

    lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
    lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]

    checkTimeSeriesByYear(testcase, response, testcase, 
                          [(lon, lat)], (periodStart, periodEnd), aggregateType, integrateType, referenceType)


#-----------------------------------------------------------

def testcase_7():

    testcase = __file__ + "_7_"

    periodStart = gPERIOD_9['periodStart']
    periodEnd   = gPERIOD_9['periodEnd']
    polygon     = open(gTEST_ROI_DIR + 'ROI-3.geojson', 'r').read()
    aggregateType = 'AVG'
    integrateType = 'NONE'
    referenceType = 'LTA'

    request = testlib.buildWpsRequest(
        process='Precipitation:TimeSeriesByYear',
        source='CHIRPS_5000m',
        ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
        indicator='TEST',
        periodStart=periodStart.isoformat(),
        periodEnd=periodEnd.isoformat(),
        aggregateType=aggregateType,
        integrateType=integrateType,
        referenceType=referenceType,
        label=testcase,
        response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

    outFileName = ('/tmp/%s.json' % testcase)
    issueRequest(request, testcase, 1, outFileName)
    result   = open(outFileName, 'r').read()
    response = json.loads(result)
    if testlib.getTrace() > 1: print json.dumps(response)

    lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
    lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]

    checkTimeSeriesByYear(testcase, response, testcase, 
                          [(lon, lat)], (periodStart, periodEnd), aggregateType, integrateType, referenceType)

#-----------------------------------------------------------

def testcase_8():

    testcase = __file__ + "_8_"

    periodStart = gPERIOD_9['periodStart']
    periodEnd   = gPERIOD_9['periodEnd']
    polygon     = open(gTEST_ROI_DIR + 'ROI-3.geojson', 'r').read()
    aggregateType = 'MIN'
    integrateType = 'NONE'
    referenceType = 'LTA'

    request = testlib.buildWpsRequest(
        process='Precipitation:TimeSeriesByYear',
        source='CHIRPS_5000m',
        ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
        indicator='TEST',
        periodStart=periodStart.isoformat(),
        periodEnd=periodEnd.isoformat(),
        aggregateType=aggregateType,
        integrateType=integrateType,
        referenceType=referenceType,
        label=testcase,
        response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

    outFileName = ('/tmp/%s.json' % testcase)
    issueRequest(request, testcase, 1, outFileName)
    result   = open(outFileName, 'r').read()
    response = json.loads(result)
    if testlib.getTrace() > 1: print json.dumps(response)

    lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
    lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]

    checkTimeSeriesByYear(testcase, response, testcase,
                          [(lon, lat)], (periodStart, periodEnd), aggregateType, integrateType, referenceType)

#-----------------------------------------------------------

def testcase_9():

    testcase = __file__ + "_9_"

    periodStart = gPERIOD_9['periodStart']
    periodEnd   = gPERIOD_9['periodEnd']
    polygon     = open(gTEST_ROI_DIR + 'ROI-3.geojson', 'r').read()
    aggregateType = 'MAX'
    integrateType = 'NONE'
    referenceType = 'LTA'

    request = testlib.buildWpsRequest(
        process='Precipitation:TimeSeriesByYear',
        source='CHIRPS_5000m',
        ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
        indicator='TEST',
        periodStart=periodStart.isoformat(),
        periodEnd=periodEnd.isoformat(),
        aggregateType=aggregateType,
        integrateType=integrateType,
        referenceType=referenceType,
        label=testcase,
        response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

    outFileName = ('/tmp/%s.json' % testcase)
    issueRequest(request, testcase, 1, outFileName)
    result   = open(outFileName, 'r').read()
    response = json.loads(result)
    if testlib.getTrace() > 1: print json.dumps(response)

    lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
    lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]

    checkTimeSeriesByYear(testcase, response, testcase,
                          [(lon, lat)], (periodStart, periodEnd), aggregateType, integrateType, referenceType)

#-----------------------------------------------------------

def testcase_10():

    testcase = __file__ + "_10_"

    periodStart = gPERIOD_9['periodStart']
    periodEnd   = gPERIOD_9['periodEnd']
    polygon     = open(gTEST_ROI_DIR + 'ROI-3.geojson', 'r').read()
    integrateType = 'SUM'
    window        = 20
    aggregateType = 'AVG'
    referenceType = 'LTA'

    request = testlib.buildWpsRequest(
        process='Precipitation:TimeSeriesByYear',
        source='CHIRPS_5000m',
        ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
        indicator='TEST',
        periodStart=periodStart.isoformat(),
        periodEnd=periodEnd.isoformat(),
        integrateType=integrateType,
        windowLength=window,
        aggregateType=aggregateType,
        referenceType=referenceType,
        label=testcase,
        response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

    outFileName = ('/tmp/%s.json' % testcase)
    issueRequest(request, testcase, 1, outFileName)
    result   = open(outFileName, 'r').read()
    response = json.loads(result)
    if testlib.getTrace() > 1: print json.dumps(response)

    lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
    lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]

    checkTimeSeriesByYear(testcase, response, testcase, 
                          [(lon, lat)], (periodStart, periodEnd), aggregateType, integrateType, referenceType, windowLength=window)

#-----------------------------------------------------------

def testcase_11():

    testcase = __file__ + "_11_"

    periodStart = gPERIOD_9['periodStart']
    periodEnd   = gPERIOD_9['periodEnd']
    polygon     = open(gTEST_ROI_DIR + 'ROI-3.geojson', 'r').read()
    integrateType = 'SUM'
    aggregateType = 'MIN'
    referenceType = 'LTA'

    request = testlib.buildWpsRequest(
        process='Precipitation:TimeSeriesByYear',
        source='CHIRPS_5000m',
        ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
        indicator='TEST',
        periodStart=periodStart.isoformat(),
        periodEnd=periodEnd.isoformat(),
        aggregateType=aggregateType,
        integrateType=integrateType,
        referenceType=referenceType,
        label=testcase,
        response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

    outFileName = ('/tmp/%s.json' % testcase)
    issueRequest(request, testcase, 1, outFileName)
    result   = open(outFileName, 'r').read()
    response = json.loads(result)
    if testlib.getTrace() > 1: print json.dumps(response)

    lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
    lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]

    checkTimeSeriesByYear(testcase, response, testcase,
                          [(lon, lat)], (periodStart, periodEnd), aggregateType, integrateType, referenceType)

#-----------------------------------------------------------

def testcase_12():

    testcase = __file__ + "_12_"

    periodStart = gPERIOD_9['periodStart']
    periodEnd   = gPERIOD_9['periodEnd']
    polygon     = open(gTEST_ROI_DIR + 'ROI-3.geojson', 'r').read()
    integrateType = 'SUM'
    aggregateType = 'MAX'
    referenceType = 'LTA'

    request = testlib.buildWpsRequest(
        process='Precipitation:TimeSeriesByYear',
        source='CHIRPS_5000m',
        ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
        indicator='TEST',
        periodStart=periodStart.isoformat(),
        periodEnd=periodEnd.isoformat(),
        aggregateType=aggregateType,
        integrateType=integrateType,
        referenceType=referenceType,
        label=testcase,
        response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

    outFileName = ('/tmp/%s.json' % testcase)
    issueRequest(request, testcase, 1, outFileName)
    result   = open(outFileName, 'r').read()
    response = json.loads(result)
    if testlib.getTrace() > 1: print json.dumps(response)

    lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
    lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]

    checkTimeSeriesByYear(testcase, response, testcase,
                          [(lon, lat)], (periodStart, periodEnd), aggregateType, integrateType, referenceType)

#-----------------------------------------------------------

def testcase_13():

    testcase = __file__ + "_13_"

    periodStart = gPERIOD_9['periodStart']
    periodEnd   = gPERIOD_9['periodEnd']
    polygon     = open(gTEST_ROI_DIR + 'ROI-3.geojson', 'r').read()
    integrateType = 'SUM'
    aggregateType = 'MAX'
    referenceType = 'LTA'

    request = testlib.buildWpsRequest(
        process='Precipitation:TimeSeriesByYear',
        source='CHIRPS_5000m',
        ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
        indicator='TEST',
        periodStart=periodStart.isoformat(),
        periodEnd=periodEnd.isoformat(),
        aggregateType=aggregateType,
        integrateType=integrateType,
        referenceType=referenceType,
        label=testcase,
        response=['result', 'RawDataOutput', { 'mimeType' : 'application/csv'}])

    outFileName = ('/tmp/%s.csv' % testcase)
    issueRequest(request, testcase, 1, outFileName)
    reader = csv.reader(open(outFileName, 'r'))
    ts = {}; rq = {}
    for r in reader:
        if len(r) > 1 :
            if   r[0] in [ 'value' ]:
                ts[r[0]] = map(float, r[1:])
            elif r[0] in [ 'type', 'label' ]:
                ts[r[0]] = r[1]
            elif r[0] in [ 'P00', 'P25', 'P50', 'P75', 'P100', 'Avg' ]:
                ts[r[0]] = float(r[1])
            elif r[0] in [ 'pstart', 'pend' ]:
                ts[r[0]] = r[1:]
            elif r[0] in [ 'N' ]:
                ts[r[0]] = int(r[1])
            elif r[0] in [ 'windowLength' ]:
                rq[r[0]] = int(r[1])
            else:
                rq[r[0]] = r[1]
    response = { 'timeseries' : ts, 'request' : rq }

    if testlib.getTrace() > 1: print str(response)

    lon = json.loads(polygon)['features'][0]['geometry']['coordinates'][0]
    lat = json.loads(polygon)['features'][0]['geometry']['coordinates'][1]

    checkTimeSeriesByYear(testcase, response, testcase,
                          [(lon, lat)], (periodStart, periodEnd), aggregateType, integrateType, referenceType)

#-----------------------------------------------------------

def testcase_14():

    testcase = __file__ + "_14_"

    periodStart = gPERIOD_9['periodStart']
    periodEnd   = gPERIOD_9['periodEnd']
    polygon     = open(gTEST_ROI_DIR + 'ROI-14.geojson', 'r').read()
    integrateType = 'SUM'
    aggregateType = 'MAX'

    request = testlib.buildWpsRequest(
        process='Precipitation:TimeSeriesByYear',
        source='CHIRPS_5000m',
        ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
        indicator='PRECIP_FINAL',
        periodStart=periodStart.isoformat(),
        periodEnd=periodEnd.isoformat(),
        integrateType=integrateType,
        aggregateType=aggregateType,
        label=testcase,
        response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

    outFileName = ('/tmp/%s.json' % testcase)
    issueRequest(request, testcase, 1, outFileName)
    result   = open(outFileName, 'r').read()
    response = json.loads(result)
    if testlib.getTrace() > 1: print json.dumps(response)

    # Check all values are non-negative and less than 1000
    ts     = response['timeseries']
    values = ts['value']
    for i in range(len(values)):
       testlib.check(testcase, "value-%d %f" % (i,values[i]), (values[i] >= 0.0 and values[i] < 1000.0), True)
    testlib.check(testcase, "P00 %f" % (ts['P00']), (ts['P00'] >= 0.0 and ts['P00'] < 1000.0), True)
    testlib.check(testcase, "P25 %f" % (ts['P25']), (ts['P25'] >= 0.0 and ts['P25'] < 1000.0), True)
    testlib.check(testcase, "P50 %f" % (ts['P50']), (ts['P50'] >= 0.0 and ts['P50'] < 1000.0), True)
    testlib.check(testcase, "P75 %f" % (ts['P75']), (ts['P75'] >= 0.0 and ts['P75'] < 1000.0), True)
    testlib.check(testcase, "P100 %f" % (ts['P100']), (ts['P100'] >= 0.0 and ts['P100'] < 1000.0), True)
    testlib.check(testcase, "Avg %f" % (ts['Avg']), (ts['Avg'] >= 0.0 and ts['Avg'] < 1000.0), True)

#-----------------------------------------------------------

def testcase_15():

    testcase = __file__ + "_15_"

    periodStart = gPERIOD_9['periodStart']
    periodEnd   = gPERIOD_9['periodEnd']
    polygon     = open(gTEST_ROI_DIR + 'ROI-15.geojson', 'r').read()
    integrateType = 'SUM'
    aggregateType = 'MAX'

    request = testlib.buildWpsRequest(
        process='Precipitation:TimeSeriesByYear',
        source='CHIRPS_5000m',
        ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
        indicator='PRECIP_FINAL',
        periodStart=periodStart.isoformat(),
        periodEnd=periodEnd.isoformat(),
        integrateType=integrateType,
        aggregateType=aggregateType,
        label=testcase,
        response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

    outFileName = ('/tmp/%s.json' % testcase)
    issueRequest(request, testcase, 1, outFileName)
    result   = open(outFileName, 'r').read()
    response = json.loads(result)
    if testlib.getTrace() > 1: print json.dumps(response)

    # Check all values are nan
    ts     = response['timeseries']
    values = ts['value']
    for i in range(len(values)):
       testlib.check(testcase, "value-%d %f" % (i,values[i]), math.isnan(values[i]), True)
    testlib.check(testcase, "P00 %f " % (ts['P00']),   math.isnan(ts['P00']),  True)
    testlib.check(testcase, "P25 %f " % (ts['P25']),   math.isnan(ts['P25']),  True)
    testlib.check(testcase, "P50 %f " % (ts['P50']),   math.isnan(ts['P50']),  True)
    testlib.check(testcase, "P75 %f " % (ts['P75']),   math.isnan(ts['P75']),  True)
    testlib.check(testcase, "P100 %f" % (ts['P100']),  math.isnan(ts['P100']), True)
    testlib.check(testcase, "Avg %f " % (ts['Avg']),   math.isnan(ts['Avg']),  True)

#-----------------------------------------------------------

def datatest_1(testcase = __file__ + "datatest_1_", intType = 'SUM'):

    periodStart = gPERIOD_1['periodStart']
    periodEnd   = gPERIOD_1['periodEnd']
    polygon     = open(gTEST_ROI_DIR + 'ROI-Afar-Ewa.geojson', 'r').read()

    request = testlib.buildWpsRequest(
        process='Precipitation:TimeSeriesByYear',
        source='CHIRPS_5000m',
        ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
        indicator='PRECIP_FINAL',
        periodStart=periodStart.isoformat(),
        periodEnd=periodEnd.isoformat(),
        integrateType=intType,
        aggregateType='MAX',
        label=testcase,
        response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

    outFileName = ('/tmp/%s.json' % testcase)
    issueRequest(request, testcase, 1, outFileName)
    result   = open(outFileName, 'r').read()
    response = json.loads(result)
    if testlib.getTrace() > 1: print json.dumps(response)

#-----------------------------------------------------------

def datatest_2(testcase = __file__ + "datatest_2_", intType = 'SUM'):

    periodStart = gPERIOD_1['periodStart']
    periodEnd   = gPERIOD_1['periodEnd']
    polygon     = open(gTEST_ROI_DIR + 'ROI-Afar-Ewa.geojson', 'r').read()

    request = testlib.buildWpsRequest(
        process='Precipitation:TimeSeriesByYear',
        source='CHIRPS_5000m',
        ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
        indicator='PRECIP_PRELIM',
        periodStart=periodStart.isoformat(),
        periodEnd=periodEnd.isoformat(),
        integrateType=intType,
        aggregateType='MAX',
        label=testcase,
        response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

    outFileName = ('/tmp/%s.json' % testcase)
    issueRequest(request, testcase, 1, outFileName)
    result   = open(outFileName, 'r').read()
    response = json.loads(result)
    if testlib.getTrace() > 1: print json.dumps(response)


#--------------------------- end of testcases --------------------------------------

# Get all functions names testcase_<nr> as default test cases
testCaseIds = [ x for x in dir() if re.match('.+_[0-9]+', x) ]
testCaseIds.sort()
testcases = {}
for testCaseId in testCaseIds:
    testcases[testCaseId] = locals()[testCaseId]
init()
testlib.main(testcases)
