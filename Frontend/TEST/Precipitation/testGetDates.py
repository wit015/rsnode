#!/usr/bin/python

##################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
##################################################################################

import re
import os
import sys
import json
import datetime

import testlib

from testValues import *
from common import *

# Global script identifiers
svn_revision = str('$Rev: 6975 $')
svn_date = str('$Date: 2016-06-15 23:05:54 +0200 (Wed, 15 Jun 2016) $')
svn_author = str('$Author: jb76278 $')
script_file_name = os.path.basename(__file__)
now_string = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

#######################################
# Test cases 
#######################################

#-----------------------------------------------------------

def testcase_1():

   testcase = __file__ + "_1_"

   source    = 'CHIRPS_5000m'
   indicator = 'PRECIP_PRELIM'

   request = testlib.buildWpsRequest(
      process='Precipitation:GetDates',
      source=source,
      indicator=indicator,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result = open(outFileName, 'r').read()
   response = json.loads(result)

   pstart = map(lambda d : datetime.datetime.strptime(d, "%Y-%m-%d").date(), response['pstart'])
   pend   = map(lambda d : datetime.datetime.strptime(d, "%Y-%m-%d").date(), response['pend'])

   testlib.check(testcase, "type      ", response['type'], 'GetDates')
   testlib.check(testcase, "N         ", response['N'], len(pstart))
   testlib.check(testcase, "source    ", response['source'], source)
   testlib.check(testcase, "indicator ", response['indicator'], indicator)
  
   # Check all intervals
   for i in range(0, len(pstart)-1):
      delta = pstart[i+1] - pstart[i]
      expected_delta = 5

      # Last measurement interval in month is not 10 days.
      year  = pstart[i].year
      ndate = pstart[i+1]
      if   pstart[i] == datetime.date(year, 12, 26):
          expected_delta = 6
      elif pstart[i].day == 26:
         ndate = datetime.date(pstart[i].year, pstart[i].month+1, 1)
         expected_delta = (ndate - pstart[i]).days

      testlib.check(testcase, "pstart-%s-delta" % pstart[i], delta.days, expected_delta)
      testlib.check(testcase, "pend-%s-delta  " % pend[i], (pend[i]-pstart[i]).days, expected_delta-1)

   print "%s %s dates : %s - %s" % (source, indicator, pstart[0], pend[-1])

#-----------------------------------------------------------

def testcase_2():

   testcase = __file__ + "_2_"

   source    = 'CHIRPS_5000m'
   indicator = 'PRECIP_FINAL'

   request = testlib.buildWpsRequest(
      process='Precipitation:GetDates',
      source=source,
      indicator=indicator,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result = open(outFileName, 'r').read()
   response = json.loads(result)

   pstart = map(lambda d : datetime.datetime.strptime(d, "%Y-%m-%d").date(), response['pstart'])
   pend   = map(lambda d : datetime.datetime.strptime(d, "%Y-%m-%d").date(), response['pend'])

   testlib.check(testcase, "type      ", response['type'], 'GetDates')
   testlib.check(testcase, "N         ", response['N'], len(pstart))
   testlib.check(testcase, "source    ", response['source'], source)
   testlib.check(testcase, "indicator ", response['indicator'], indicator)

   # Check all intervals
   for i in range(0, len(pstart)-1):
      delta = pstart[i+1] - pstart[i]
      expected_delta = 5

      # Last measurement interval in month is not 10 days.
      year  = pstart[i].year
      ndate = pstart[i+1]
      if   pstart[i] == datetime.date(year, 12, 26):
          expected_delta = 6
      elif pstart[i].day == 26:
         ndate = datetime.date(pstart[i].year, pstart[i].month+1, 1)
         expected_delta = (ndate - pstart[i]).days

      testlib.check(testcase, "pstart-%s-delta" % pstart[i], delta.days, expected_delta)
      testlib.check(testcase, "pend-%s-delta  " % pend[i], (pend[i]-pstart[i]).days, expected_delta-1)

   print "%s %s dates : %s - %s" % (source, indicator, pstart[0], pend[-1])

#-----------------------------------------------------------

def testcase_3():

   testcase = __file__ + "_3_"

   source    = 'CHIRPS_5000m'
   indicator = 'PRECIP_LTA'

   request = testlib.buildWpsRequest(
      process='Precipitation:GetDates',
      source=source,
      indicator=indicator,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result = open(outFileName, 'r').read()
   response = json.loads(result)

   pstart = map(lambda d : datetime.datetime.strptime(d, "%Y-%m-%d").date(), response['pstart'])
   pend   = map(lambda d : datetime.datetime.strptime(d, "%Y-%m-%d").date(), response['pend'])

   testlib.check(testcase, "type      ", response['type'], 'GetDates')
   testlib.check(testcase, "N         ", response['N'], 72)
   testlib.check(testcase, "source    ", response['source'], source)
   testlib.check(testcase, "indicator ", response['indicator'], indicator)

   # Check all intervals
   for i in range(0, len(pstart)-1):
      delta = pstart[i+1] - pstart[i]
      expected_delta = 5

      # Last measurement interval in month is not 10 days.
      year  = pstart[i].year
      ndate = pstart[i+1]
      if   pstart[i] == datetime.date(year, 12, 26):
          expected_delta = 6
      elif pstart[i].day == 26:
         ndate = datetime.date(pstart[i].year, pstart[i].month+1, 1)
         expected_delta = (ndate - pstart[i]).days

      testlib.check(testcase, "pstart-%s-year  " % pstart[i], pstart[i].year, 1900)
      testlib.check(testcase, "pstart-%s-delta " % pstart[i], delta.days, expected_delta)
      testlib.check(testcase, "pend-%s-delta   " % pend[i], (pend[i]-pstart[i]).days, expected_delta-1)

   print "%s %s dates : %s - %s" % (source, indicator, pstart[0], pend[-1])

#--------------------------- end of testcases --------------------------------------

# Get all functions names testcase_<nr> as default test cases
testCaseIds = [ x for x in dir() if re.match('.+_[0-9]+', x) ]
testCaseIds.sort()
testcases = {}
for testCaseId in testCaseIds:
   testcases[testCaseId] = locals()[testCaseId]
testlib.main(testcases)
