#!/usr/bin/python

##################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
##################################################################################

import re
import os
import sys
import json
import datetime
import csv

import testlib
from testValues import *

#######################################
# Test cases 
#######################################

#-----------------------------------------------------------

def testcase_1():

   testcase = __file__ + "_1_"

   bbox      = "36.4233,13.5911,36.4306,13.5854"
   srs       = "epsg:4326"
   width     = 100
   height    = 200
   datetime  = '2018-01-01'
   user_id   = 'demo-1'
   group_id  = 'DEFAULT'
   field_id  = 'Azane_Lejalem'
   indicator = 'NDVI'

   request = testlib.buildWpsRequest(
      process='S2:FieldImage',
      bbox=bbox,
      srs=srs,
      width=width,
      height=height,
      user_id=user_id,
      group_id=group_id,
      field_id=field_id,
      indicator=indicator,
      response=['result', 'RawDataOutput', { 'mimeType' : 'image/tiff'}])

   outFileName = ('/tmp/%s.tiff' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)
   testlib.check(testcase, "shape", data.shape, (height,width))
   mask = ~np.isnan(data)
   testlib.check(testcase, "#pixels", np.sum(mask), 15028)

#-----------------------------------------------------------

def testcase_2():

   testcase = __file__ + "_2_"

   bbox      = "36.4233,13.5911,36.4306,13.5854"
   srs       = "epsg:4326"
   width     = 100
   height    = 200
   datetime  = '2018-01-01'
   user_id   = 'demo-1'
   group_id  = 'DEFAULT'
   field_id  = 'Azane_Lejalem'
   indicator = 'NDVI'

   request = testlib.buildWpsRequest(
      process='S2:FieldImage',
      bbox=bbox,
      srs=srs,
      width=width,
      height=height,
      user_id=user_id,
      group_id=group_id,
      field_id=field_id,
      indicator=indicator,
      response=['result', 'RawDataOutput', { 'mimeType' : 'image/png'}])

   outFileName = ('/tmp/%s.png' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)
   testlib.check(testcase, "shape", data.shape, (height,width))
   mask = data <> 0
   testlib.check(testcase, "#pixels", np.sum(mask), 15028)


#-----------------------------------------------------------

def testcase_3():

   testcase = __file__ + "_3_"

   bbox      = "36.4233,13.5911,36.4306,13.5854"
   srs       = "epsg:4326"
   width     = 200
   height    = 200
   datetime  = '2018-01-01'
   user_id   = 'demo-1'
   group_id  = 'DEFAULT'
   field_id  = 'Azane_Lejalem'
   indicator = 'SCL'

   request = testlib.buildWpsRequest(
      process='S2:FieldImage',
      bbox=bbox,
      srs=srs,
      width=width,
      height=height,
      user_id=user_id,
      group_id=group_id,
      field_id=field_id,
      indicator=indicator,
      response=['result', 'RawDataOutput', { 'mimeType' : 'image/tiff'}])

   outFileName = ('/tmp/%s.tiff' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)
   testlib.check(testcase, "shape", data.shape, (height,width))
   npix = [9954, 0, 0, 0, 0, 0, 0, 0, 18531, 0, 11515, 0, 0, 0, 0, 0, 0]
   for i in range(len(npix)):
       mask = data == i
       testlib.check(testcase, "#pixels value == %0d" %i, np.sum(mask), npix[i])

#--------------------------- end of testcases --------------------------------------

# Get all functions names testcase_<nr> as default test cases
testCaseIds = [ x for x in dir() if re.match('.+_[0-9]+', x) ]
testCaseIds.sort()
testcases = {}
for testCaseId in testCaseIds:
   testcases[testCaseId] = locals()[testCaseId]
testlib.main(testcases)
