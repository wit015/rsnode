#!/usr/bin/python

##################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
##################################################################################

import re
import os
import sys
import json
import datetime
import csv

import testlib
from testValues import *

#######################################
# Test cases 
#######################################

#-----------------------------------------------------------

def testcase_1():

   testcase = __file__ + "_1_"

   user_id  = 'demo-1'
   group_id = 'TEST'
   tag      = 'FIELD-TAG'
   features = open('field-1.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='S2:FieldDeclare',
      user_id=user_id,
      group_id=group_id,
      tag=tag,
      features=[features, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result = open(outFileName, 'r').read()
   testlib.check(testcase, "permission denied", re.search('.*Permission denied.*', result) is not None, True)

#-----------------------------------------------------------

def testcase_2():

   testcase = __file__ + "_2_"

   user_id  = 'demo-adsnl'
   group_id = 'TEST'
   tag      = 'FIELD-TAG'
   features = open('field-1.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='S2:FieldDeclare',
      user_id=user_id,
      group_id=group_id,
      tag=tag,
      features=[features, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result = open(outFileName, 'r').read()
   response = json.loads(result)
   field_id = response['field_id']
   testlib.check(testcase, "field-id %s length 36" % field_id, len(field_id), 36)

   request = testlib.buildWpsRequest(
      process='S2:FieldDelete',
      user_id=user_id,
      group_id=group_id,
      field_id=field_id,
      response='RawDataOutput')

   outFileName = ('/tmp/%s_delete.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result = open(outFileName, 'r').read()
   response = json.loads(result)
   del_field_id = response['field_id']
   testlib.check(testcase, "field-id %s" % del_field_id, del_field_id, field_id)
   testlib.check(testcase, "error", response['error'], None)

#-----------------------------------------------------------

def testcase_3():

   testcase = __file__ + "_3_"

   user_id  = 'demo-1'
   group_id = 'TEST'
   tag      = 'FIELD-TAG'
   features = open('field-2.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='S2:FieldDeclare',
      user_id=user_id,
      group_id=group_id,
      tag=tag,
      features=[features, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result = open(outFileName, 'r').read()
   testlib.check(testcase, "no matching granule", re.search('.*no matching granule.*', result) is not None, True)

#--------------------------- end of testcases --------------------------------------

# Get all functions names testcase_<nr> as default test cases
testCaseIds = [ x for x in dir() if re.match('.+_[0-9]+', x) ]
testCaseIds.sort()
testcases = {}
for testCaseId in testCaseIds:
   testcases[testCaseId] = locals()[testCaseId]
testlib.main(testcases)
