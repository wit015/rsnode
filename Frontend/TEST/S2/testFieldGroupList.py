#!/usr/bin/python

##################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
##################################################################################

import re
import os
import sys
import json
import datetime
import csv

import testlib
from testValues import *

#######################################
# Test cases 
#######################################

#-----------------------------------------------------------

def testcase_1():

   testcase = __file__ + "_1_"

   user_id  = 'demo-1'
   group_id = 'DEFAULT'

   request = testlib.buildWpsRequest(
      process='S2:FieldGroupList',
      user_id=user_id,
      group_id=group_id,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result = open(outFileName, 'r').read()
   response = json.loads(result)
   testlib.check(testcase, "nfeatures", len(response['features']), 2)
   testlib.check(testcase, "feature[0] tag", response['features'][0]['properties']['tag'], 'Mulualem_Melemelae')
   testlib.check(testcase, "feature[1] tag", response['features'][1]['properties']['tag'], 'Azane_Lejalem')

#-----------------------------------------------------------

def testcase_2():

   testcase = __file__ + "_2_"

   user_id  = 'demo-does-not-exist'
   group_id = 'DEFAULT'

   request = testlib.buildWpsRequest(
      process='S2:FieldGroupList',
      user_id=user_id,
      group_id=group_id,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result = open(outFileName, 'r').read()
   testlib.check(testcase, "error", re.search('.*user %s does not exist.*' % user_id, result) is not None, True)


#-----------------------------------------------------------

def testcase_3():

   testcase = __file__ + "_3_"

   user_id  = 'demo-1'
   group_id = 'does-not-exist'

   request = testlib.buildWpsRequest(
      process='S2:FieldGroupList',
      user_id=user_id,
      group_id=group_id,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result = open(outFileName, 'r').read()
   testlib.check(testcase, "error", re.search('.*group %s does not exist.*' % group_id, result) is not None, True)


#--------------------------- end of testcases --------------------------------------

# Get all functions names testcase_<nr> as default test cases
testCaseIds = [ x for x in dir() if re.match('.+_[0-9]+', x) ]
testCaseIds.sort()
testcases = {}
for testCaseId in testCaseIds:
   testcases[testCaseId] = locals()[testCaseId]
testlib.main(testcases)
