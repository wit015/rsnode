#!/usr/bin/python

##################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
##################################################################################

import re
import os
import sys
import json
import datetime
import csv

import testlib
from testValues import *

#######################################
# Test cases 
#######################################

#-----------------------------------------------------------

def testcase_1():

   testcase = __file__ + "_1_"

   user_id          = 'demo-1'
   group_id         = 'DEFAULT'
   field_id         = 'Azane_Lejalem'
   start_datetime   = '2017-01-01'
   end_datetime     = '2018-12-31'
   lb_percentile    = 0.0
   ub_percentile    = 100.0
   pixel_percentage = 0.0
   indicator        = 'NDVI'
   aggregator       = 'AVG'
   mask_list        = "'4,5,6'"

   request = testlib.buildWpsRequest(
      process='S2:FieldTimeSeries',
      user_id=user_id,
      group_id=group_id,
      field_id=field_id,
      start_datetime=start_datetime,
      end_datetime=end_datetime,
      lb_percentile=lb_percentile,
      ub_percentile=ub_percentile,
      pixel_percentage=pixel_percentage,
      indicator=indicator,
      aggregator=aggregator,
      mask_list=mask_list,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result = open(outFileName, 'r').read()
   response = json.loads(result)

   testlib.check(testcase, "group_id", response['group_id'], group_id)
   testlib.check(testcase, "field_id", response['field_id'], field_id)
   testlib.check(testcase, "n_total_pixels", response['n_total_pixels'], 3746)

#-----------------------------------------------------------

def testcase_2():

   testcase = __file__ + "_2_"

   user_id          = 'demo-1'
   group_id         = 'DEFAULT'
   field_id         = 'Azane_Lejalem'
   start_datetime   = '2017-01-01'
   end_datetime     = '2017-12-31'
   lb_percentile    = 0.0
   ub_percentile    = 100.0
   pixel_percentage = 0.0
   indicator        = 'NDVI'
   aggregator       = 'MED'
   mask_list        = "'4,5'"

   request = testlib.buildWpsRequest(
      process='S2:FieldTimeSeries',
      user_id=user_id,
      group_id=group_id,
      field_id=field_id,
      start_datetime=start_datetime,
      end_datetime=end_datetime,
      lb_percentile=lb_percentile,
      ub_percentile=ub_percentile,
      pixel_percentage=pixel_percentage,
      indicator=indicator,
      aggregator=aggregator,
      mask_list=mask_list,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/csv'}])

   outFileName = ('/tmp/%s.csv' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   response = csv.DictReader(open(outFileName, 'r'))
   response = list(response)

   testlib.check(testcase, "number of rows", len(response), 54)
   testlib.check(testcase, "group_id", response[0]['group_id'], group_id)
   testlib.check(testcase, "field_id", response[0]['field_id'], field_id)
   testlib.check(testcase, "aggregation", response[0]['aggregation'], aggregator)
   testlib.check(testcase, "n_total_pixels", int(response[0]['n_total_pixels']), 3746)


#--------------------------- end of testcases --------------------------------------

# Get all functions names testcase_<nr> as default test cases
testCaseIds = [ x for x in dir() if re.match('.+_[0-9]+', x) ]
testCaseIds.sort()
testcases = {}
for testCaseId in testCaseIds:
   testcases[testCaseId] = locals()[testCaseId]
testlib.main(testcases)
