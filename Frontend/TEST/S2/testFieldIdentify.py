#!/usr/bin/python

##################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
##################################################################################

import re
import os
import sys
import json
import datetime
import csv

import testlib
from testValues import *

#######################################
# Test cases 
#######################################

#-----------------------------------------------------------

def testcase_1():

   testcase = __file__ + "_1_"

   bbox     = "36.45331,13.10503,36.46000,13.09767"
   srs      = "epsg:4326"
   width    = 100
   height   = 200
   features = open('field-1.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='S2:FieldIdentify',
      bbox=bbox,
      srs=srs,
      width=width,
      height=height,
      features=[features, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      response=['result', 'RawDataOutput', { 'mimeType' : 'image/tiff'}])

   outFileName = ('/tmp/%s.tiff' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   data = getImageData(outFileName)
   testlib.check(testcase, "shape", data.shape, (height,width))
   mask = data <> 0
   testlib.check(testcase, "#pixels", np.sum(mask), 13307)

#-----------------------------------------------------------

def testcase_2():

   testcase = __file__ + "_2_"

   bbox     = "37.221,12.107,37.263,12.087"
   srs      = "epsg:4326"
   width    = 100
   height   = 200
   features = open('field-2.geojson', 'r').read()

   request = testlib.buildWpsRequest(
      process='S2:FieldIdentify',
      bbox=bbox,
      srs=srs,
      width=width,
      height=height,
      features=[features, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      response=['result', 'RawDataOutput', { 'mimeType' : 'image/tiff'}])

   outFileName = ('/tmp/%s.tiff' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result = open(outFileName,'r').read()
   testlib.check(testcase, "no matching granule", re.search('.*no matching granule.*', result) is not None, True)


#-----------------------------------------------------------



#--------------------------- end of testcases --------------------------------------

# Get all functions names testcase_<nr> as default test cases
testCaseIds = [ x for x in dir() if re.match('.+_[0-9]+', x) ]
testCaseIds.sort()
testcases = {}
for testCaseId in testCaseIds:
   testcases[testCaseId] = locals()[testCaseId]
testlib.main(testcases)
