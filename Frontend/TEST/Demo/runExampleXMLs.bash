#!/bin/bash

##################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
##################################################################################

export URL='http://dsl-sandbox-cs1:8080/geoserver/wps'
#export URL='http://104.155.9.160:8080/geoserver/wps'

for f in *.xml; do 
   b=`basename $f .xml`; 
   echo ${b}; 
   r=`grep 'RawDataOutput' ${b}.xml | head -1 | grep 'mimeType=' | sed 's/.*mimeType="\(.*\)".*/\1/'`
   case $r in
      application/json) outFileName=${b}.json ;;
      image/tiff)       outFileName=${b}.tif ;;
   esac
   echo "mimetype=$r $outFileName"
   curl -H 'Content-type: xml' -XPOST -d\@${b}.xml $URL > $outFileName
done
