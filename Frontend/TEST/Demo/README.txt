Descripion
==========

This demo contains the following example XML files:

-MAP_Tigray_CHIRPS_5000m_2012_SUM.xml
 Generate a raster map of the total (SUM) rain for 2012.
 This map is not 'rendered' and cannot be viewed in a picture viewer.
 Use QGIS instead.
 Output file is a tiff file.

-MAP_Tigray_with_SLD_CHIRPS_5000m_2012_SUM.xml
 Same as MAP_Tigray_CHIRPS_5000m_2012_SUM.xml but now 'rendered'
 to have a viewable picture by an image viewer.
 Output file is a tiff file.

-MAP_Tigray_MODIS_500m_2012_MAX.xml
 Generate a raster map of the max NDVI for 2012. The ESA_CCI_2010
 Land Cover map is ased as mask.
 This map is not 'rendered' and cannot be viewed in a picture viewer.
 Use QGIS instead.
 Output file is a tiff file.

-MAP_Tigray_with_SLD_MODIS_500m_2012_MAX.xml
 Same as MAP_Tigray_MODIS_500m_2012_MAX but now 'rendered'
 to have a viewable picture by an image viewer.
 Output file is a tiff file.
 
-TIMESERIES_BY_DATE_CHIRPS_5000m_2012.xml
 2012 CHIRPS cumulative time series for a province in Tigray. 
 Output file is a json timeseries file (.json)

-TIMESERIES_BY_YEAR_CHIRPS_5000m_2012.xml
 All year total rain for a province in Tigray.
 Output file is a json timeseries file (.json)

-TIMESERIES_BY_DATE_MODIS_500m_2012.xml
 2012 EVI Time series for a province in Tigray. 
 The ESA_CCI_2010 is used as mask.
 Output file is a json timeseries file (.json)

Run all XML examples:
=====================

% ./runExampleXMLs.bash
MAP_Tigray_CHIRPS_5000m_2012_SUM
mimetype=image/tiff MAP_Tigray_CHIRPS_5000m_2012_SUM.tif
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 13345    0 10804  100  2541  14211   3342 --:--:-- --:--:-- --:--:-- 14197
MAP_Tigray_MODIS_500m_2012_MAX
mimetype=image/tiff MAP_Tigray_MODIS_500m_2012_MAX.tif
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 2032k    0 2030k  100  2538   182k    228  0:00:11  0:00:11 --:--:--  494k
MAP_Tigray_with_SLD_CHIRPS_5000m_2012_SUM
mimetype=image/tiff MAP_Tigray_with_SLD_CHIRPS_5000m_2012_SUM.tif
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 11045    0  7220  100  3825    398    211  0:00:18  0:00:18 --:--:--   828
MAP_Tigray_with_SLD_MODIS_500m_2012_MAX
mimetype=image/tiff MAP_Tigray_with_SLD_MODIS_500m_2012_MAX.tif
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 1029k    0 1025k  100  3825  90381    329  0:00:11  0:00:11 --:--:--  283k
TIMESERIES_BY_DATE_CHIRPS_5000m_2012
mimetype=application/json TIMESERIES_BY_DATE_CHIRPS_5000m_2012.json
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 91523    0  6279  100 85244  11753   155k --:--:-- --:--:-- --:--:--  155k
TIMESERIES_BY_DATE_MODIS_500m_2012
mimetype=application/json TIMESERIES_BY_DATE_MODIS_500m_2012.json
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 93057    0  7977  100 85080   3362  35865  0:00:02  0:00:02 --:--:-- 35868
TIMESERIES_BY_YEAR_CHIRPS_5000m_2012
mimetype=application/json TIMESERIES_BY_YEAR_CHIRPS_5000m_2012.json
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 87042    0  1796  100 85246   4677   216k --:--:-- --:--:-- --:--:--  217k

OUTPUT
======
The expected output can be found in Results/

