#!/usr/bin/python

##################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
##################################################################################

import os
import sys
import httplib, urllib
import testlib

testlib.setLogging(True)

HOST = "dsl-sandbox-cs1"
#HOST = "104.155.9.160"
PORT = 8080
#PORT = 10000

addis_polygon = '{"type":"Polygon","coordinates":[[[38.64,9.10],[38.64,8.94],[38.77,8.81],[38.88,8.86],[38.92,9.00],[38.88,9.09],[38.78,9.10],[38.64,9.10]]]}'
tigray_envelope = '{ "type": "Polygon", "coordinates": [ [ [ 36.340510579095557, 14.475381020115776 ], [ 37.615765679567701, 14.560398026813919 ], [ 37.945206580523006, 15.102381444514581 ], [ 38.572207004921815, 14.613533656000257 ], [ 40.134394503000195, 14.836703298582883 ], [ 40.006868992952974, 12.158667587591378 ], [ 39.135444674297013, 12.14804046175411 ], [ 38.48718999822367, 13.210753045480898 ], [ 36.351137704932825, 13.168244542131827 ], [ 36.340510579095557, 14.475381020115776 ] ] ] }'


def issueRequest(request, testid, teststep):
   response = testlib.checkIssueRequest(HOST, PORT, request, testid, teststep)
   data = response.read()
   return data

#############################################

label = "MAP Tigray CHIRPS 5000m 2012 SUM".replace(' ', '_')
request = testlib.buildWpsRequest(
             process='Precipitation:PeriodAggregateMap',
             source="CHIRPS_5000m",
             ROI=[tigray_envelope, 'ComplexData', {'mimeType' : 'application/json'}, 1],
             maskID="NONE",
             indicator="PRECIP_PRELIM",
             periodStart="2012-01-01",
             periodEnd="2012-12-31",
             aggregateType="SUM",
             label=label,
             response=['geotiff', 'RawDataOutput', { 'mimeType' : 'image/tiff'}])

print label

f = open(label+'.xml', 'w')
f.write(request)
f.close()

data = issueRequest(request, label, 1)

f = open(label+'.tif', 'w')
f.write(data)
f.close()

#############################################

label = "MAP Tigray with SLD CHIRPS 5000m 2012 SUM".replace(' ', '_')

sldStr = '<?xml version="1.0" encoding="ISO-8859-1"?> <StyledLayerDescriptor version="1.0.0"><NamedLayer><Name>Two color gradient</Name><UserStyle><Title>SLD Cook Book: Two color gradient</Title> <FeatureTypeStyle> <Rule> <RasterSymbolizer> <ColorMap> <ColorMapEntry color="#FFFFFF" quantity="-9999" opacity="0" label="nodata"/> <ColorMapEntry color="#FFFFFF" quantity="0" /> <ColorMapEntry color="#000000" quantity="2500" /> </ColorMap> <Opacity>1.0</Opacity> </RasterSymbolizer> </Rule> </FeatureTypeStyle> </UserStyle></NamedLayer></StyledLayerDescriptor>'

request2 = testlib.buildWpsRequest(
             process="ras:StyleCoverage",
             coverage=[request, 'Reference', 'http://geoserver/wps', {'mimeType' : 'image/tiff'}],
             style=[sldStr, 'ComplexData', {'mimeType' : 'application/xml; subtype=sld/1.0.0'}, 1],
             response=['result', 'RawDataOutput', { 'mimeType' : 'image/tiff'}])

print label

f = open(label+'.xml', 'w')
f.write(request2)
f.close()

print "This may take a couple of seconds (~15) ..."
data = issueRequest(request2, label, 2)
print "Done!"

f = open(label+'.tif', 'w')
f.write(data)
f.close()

#############################################

label = "TIMESERIES BY DATE CHIRPS 5000m 2012".replace(' ', '_')
f = open('tigray_province.geojson')
polygon = f.read()
request = testlib.buildWpsRequest(
             process='Precipitation:TimeSeriesByDate',
             source="CHIRPS_5000m",
             ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
             maskID="NONE",
             indicator="PRECIP_PRELIM",
             periodStart="2012-01-01",
             periodEnd="2012-12-31",
             cumulative='True',
             label=label,
             response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

print label

f = open(label+'.xml', 'w')
f.write(request)
f.close()

data = issueRequest(request, label, 1)

f = open(label+'.json', 'w')
f.write(data)
f.close()

#############################################

label = "TIMESERIES BY YEAR CHIRPS 5000m 2012".replace(' ', '_')
f = open('tigray_province.geojson')
polygon = f.read()
request = testlib.buildWpsRequest(
             process='Precipitation:TimeSeriesByYear',
             source="CHIRPS_5000m",
             ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
             maskID="NONE",
             indicator="PRECIP_PRELIM",
             periodStart="2012-01-01",
             periodEnd="2012-12-31",
             aggregateType='SUM',
             label=label,
             response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

print label

f = open(label+'.xml', 'w')
f.write(request)
f.close()

data = issueRequest(request, label, 1)

f = open(label+'.json', 'w')
f.write(data)
f.close()

##############################################

label = "MAP Tigray MODIS 500m 2012 MAX".replace(' ', '_')
request = testlib.buildWpsRequest(
             process='VegetationIndex:PeriodAggregateMap',
             source="MODIS_500m",
             ROI=[tigray_envelope, 'ComplexData', {'mimeType' : 'application/json'}, 1],
             maskID="ESA_CCI_2010",
             indicator="NDVI",
             periodStart="2012-01-01",
             periodEnd="2012-12-31",
             aggregateType="MAX",
             label=label,
             response=['geotiff', 'RawDataOutput', { 'mimeType' : 'image/tiff'}])

print label

f = open(label+'.xml', 'w')
f.write(request)
f.close()

print "This may take a couple of seconds (~15) ..."
data = issueRequest(request, label, 1)
print "Done!"

f = open(label+'.tif', 'w')
f.write(data)
f.close()

#############################################

label = "MAP Tigray with SLD MODIS 500m 2012 MAX".replace(' ', '_')

sldStr = '<?xml version="1.0" encoding="ISO-8859-1"?> <StyledLayerDescriptor version="1.0.0"><NamedLayer><Name>Two color gradient</Name><UserStyle><Title>SLD Cook Book: Two color gradient</Title> <FeatureTypeStyle> <Rule> <RasterSymbolizer> <ColorMap> <ColorMapEntry color="#FFFFFF" quantity="-3000" opacity="0" label="nodata"/> <ColorMapEntry color="#FFFFFF" quantity="1100" /> <ColorMapEntry color="#000000" quantity="8300" /> </ColorMap> <Opacity>1.0</Opacity> </RasterSymbolizer> </Rule> </FeatureTypeStyle> </UserStyle></NamedLayer></StyledLayerDescriptor>'

request2 = testlib.buildWpsRequest(
             process="ras:StyleCoverage",
             coverage=[request, 'Reference', 'http://geoserver/wps', {'mimeType' : 'image/tiff'}],
             style=[sldStr, 'ComplexData', {'mimeType' : 'application/xml; subtype=sld/1.0.0'}, 1],
             response=['result', 'RawDataOutput', { 'mimeType' : 'image/tiff'}])

print label

f = open(label+'.xml', 'w')
f.write(request2)
f.close()

print "This may take a couple of seconds (~15) ..."
data = issueRequest(request2, label, 2)
print "Done!"

f = open(label+'.tif', 'w')
f.write(data)
f.close()

#############################################

label = "TIMESERIES BY DATE MODIS 500m 2012".replace(' ', '_')

f = open('tigray_province.geojson')
polygon = f.read()
request = testlib.buildWpsRequest(
             process='VegetationIndex:TimeSeriesByDate',
             source="MODIS_500m",
             ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
             maskID="ESA_CCI_2010",
             indicator="EVI",
             periodStart="2012-01-01",
             periodEnd="2012-12-31",
             label=label,
             response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

print label

f = open(label+'.xml', 'w')
f.write(request)
f.close()

data = issueRequest(request, label, 1)

f = open(label+'.json', 'w')
f.write(data)
f.close()
