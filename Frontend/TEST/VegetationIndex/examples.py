#!/usr/bin/python

##################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
##################################################################################
#
# This script generates the examples in the product description
#
##################################################################################

import re
import os
import sys
import json
import csv
import datetime
from lxml import etree

import testlib
from testValues import *
from common import *

# Global script identifiers
svn_revision = str('$Rev: 7867 $')
svn_date = str('$Date: 2017-01-30 15:05:14 +0100 (Mon, 30 Jan 2017) $')
svn_author = str('$Author: jb76278 $')
script_file_name = os.path.basename(__file__)
now_string = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

#######################################
# Test cases 
#######################################

#-----------------------------------------------------------

def testcase_1():

   testcase = __file__ + "_1_"

   source    = 'MODIS_500m'
   indicator = 'NDVI'

   request = testlib.buildWpsRequest(
      process='VegetationIndex:GetDates',
      source=source,
      indicator=indicator,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result = open(outFileName, 'r').read()
   response = json.loads(result)

   print "VegetationIndex:GetDates"
   print "REQUEST"
   print request
   print "RESPONSE"
   print json.dumps(response, sort_keys=True, indent=4, separators=(',', ': '))

#-----------------------------------------------------------

def testcase_2():

   testcase = __file__ + "_2_"

   periodStart    = gPERIOD_9['periodStart']
   periodEnd      = gPERIOD_9['periodEnd']
   polygon        = open(gTEST_ROI_DIR + 'ROI-11.geojson', 'r').read()
   maskID         = 'NONE'
   aggregateType  = 'MAX'
   integrateType  = 'NONE'
   referenceType  = 'ABS'
   referenceStart = REFSTART_3(periodStart, 30)

   request = testlib.buildWpsRequest(
      process='VegetationIndex:PeriodAggregateMap',
      source='MODIS_500m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      maskID=maskID,
      indicator='EVI',
      aggregateType=aggregateType,
      integrateType=integrateType,
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      referenceType=referenceType,
      referenceStart=str(referenceStart),
      label=testcase,
      response=['geotiff', 'ResponseDocument', {'mimeType' : 'image/tiff'}])

   outFileName = ('/tmp/%s.xml' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   reponse = open(outFileName, 'r').read()
   xml = etree.fromstring(reponse)

   print "VegetationIndex:PeriodAggregateMap"
   print "REQUEST"
   print request
   print "RESPONSE"
   print etree.tostring(xml, pretty_print=True)

#-----------------------------------------------------------

def testcase_3():

   testcase = __file__ + "_3_"

   periodStart    = gPERIOD_9['periodStart']
   periodEnd      = gPERIOD_9['periodEnd']
   polygon        = open(gTEST_ROI_DIR + 'ROI-11.geojson', 'r').read()
   aggregateType  = 'MAX'
   integrateType  = 'NONE'
   referenceType  = 'LTA'
   referenceStart = REFSTART_3(periodStart, 30)
   maskID         = 'ESA_CCI_2010'

   request = testlib.buildWpsRequest(
      process='VegetationIndex:PeriodAggregateROI',
      source='MODIS_500m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      maskID=maskID,
      indicator='EVI',
      aggregateType=aggregateType,
      integrateType=integrateType,
      periodStart=str(periodStart),
      periodEnd=str(periodEnd),
      referenceType=referenceType,
      referenceStart=str(referenceStart),
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result = open(outFileName, 'r').read()
   response = json.loads(result)

   print "VegetationIndex:PeriodAggregateROI"
   print "REQUEST"
   print request
   print "RESPONSE"
   print json.dumps(response, sort_keys=True, indent=4, separators=(',', ': '))

#-----------------------------------------------------------

def testcase_4():

   testcase = __file__ + "_4_"

   periodStart    = datetime.date(2012, 6, 1)
   periodEnd      = datetime.date(2014, 5,31)
   polygon        = open(gTEST_ROI_DIR + 'ROI-6.geojson', 'r').read()
   integrateType  = 'NONE'
   referenceType  = 'NONE'
   referenceStart = REFSTART_3(periodStart, 30)
   maskID         = 'NONE'

   request = testlib.buildWpsRequest(
      process='VegetationIndex:TimeSeriesByDate',
      source='MODIS_500m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='NDVI',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      referenceStart=referenceStart.isoformat(),
      referenceType=referenceType,
      integrateType=integrateType,
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

   outFileName = ('/tmp/%s.json' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result = open(outFileName, 'r').read()
   response = json.loads(result)

   print "VegetationIndex:TimeSeriesByDate"
   print "REQUEST"
   print request
   print "RESPONSE"
   print json.dumps(response, sort_keys=True, indent=4, separators=(',', ': '))

#-----------------------------------------------------------

def testcase_5():

   testcase = __file__ + "_5_"

   periodStart    = datetime.date(2012, 1, 1)
   periodEnd      = datetime.date(2012,12,31)
   polygon        = open(gTEST_ROI_DIR + 'ROI-6.geojson', 'r').read()
   integrateType  = 'NONE'
   aggregateType  = 'MAX'
   referenceType  = 'LTA'
   referenceType  = 'NONE'
   maskID         = 'NONE'

   request = testlib.buildWpsRequest(
      process='VegetationIndex:TimeSeriesByYear',
      source='MODIS_500m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='EVI',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      referenceType=referenceType,
      integrateType=integrateType,
      aggregateType=aggregateType,
      label=testcase,
      response=['result', 'RawDataOutput', { 'mimeType' : 'application/csv'}])

   outFileName = ('/tmp/%s.csv' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   result = open(outFileName, 'r').read()

   print "VegetationIndex:TimeSeriesByYear"
   print "REQUEST"
   print request
   print "RESPONSE"
   print result

#-----------------------------------------------------------

def testcase_6():

   testcase = __file__ + "_5_"

   periodStart    = datetime.date(2012, 1, 1)
   periodEnd      = datetime.date(2012,12,31)
   polygon        = open(gTEST_ROI_DIR + 'ROI-11.geojson', 'r').read()
   integrateType  = 'DIF'
   aggregateType  = 'MAX'
   maskID         = 'NONE'

   request = testlib.buildWpsRequest(
      process='VegetationIndex:IntervalMap',
      source='MODIS_500m',
      ROI=[polygon, 'ComplexData', {'mimeType' : 'application/json'}, 1],
      indicator='EVI',
      periodStart=periodStart.isoformat(),
      periodEnd=periodEnd.isoformat(),
      integrateType=integrateType,
      windowLength=16,
      aggregateType=aggregateType,
      label=testcase,
      response=['geotiff', 'ResponseDocument', {'mimeType' : 'image/tiff'}])

   outFileName = ('/tmp/%s.xml' % testcase)
   issueRequest(request, testcase, 1, outFileName)
   reponse = open(outFileName, 'r').read()
   xml = etree.fromstring(reponse)

   print "VegetationIndex:IntervalMap"
   print "REQUEST"
   print request
   print "RESPONSE"
   print etree.tostring(xml, pretty_print=True)

#--------------------------- end of testcases --------------------------------------

# Get all functions names testcase_<nr> as default test cases
testCaseIds = [ x for x in dir() if x.startswith('testcase_') ]
testCaseIds.sort()
testcases = {}
for testCaseId in testCaseIds:
   testcases[testCaseId] = locals()[testCaseId]
testlib.main(testcases)
