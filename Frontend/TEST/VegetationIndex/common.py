##################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
##################################################################################

import datetime
import testValues

gFLOAT_TOLERANCE = 1.0e-3

# Parameters for the MODIS product cube
gRES_X  = 0.0045
gRES_Y  = -0.0045
gORG_X  = 32.0
gORG_Y  = 20.0

# For pixel value calc do not use exact border of pixel but
# use small offset value.
gEPS_X  = gRES_X / 1000.0
gEPS_Y  = gRES_Y / 1000.0

gPERIOD_1  = { 'periodStart' : datetime.date(2010, 03, 11), 'periodEnd' : datetime.date(2010, 03, 11) }
gPERIOD_2  = { 'periodStart' : datetime.date(2000, 02, 18), 'periodEnd' : datetime.date(2000, 02, 18) }
gPERIOD_3  = { 'periodStart' : datetime.date(2012, 02, 29), 'periodEnd' : datetime.date(2012, 02, 29) }
gPERIOD_4  = { 'periodStart' : datetime.date(2009, 07, 04), 'periodEnd' : datetime.date(2009, 07, 11) }
gPERIOD_5  = { 'periodStart' : datetime.date(2008, 07, 03), 'periodEnd' : datetime.date(2008, 07, 10) }
gPERIOD_6  = { 'periodStart' : datetime.date(2009, 07, 04), 'periodEnd' : datetime.date(2009, 07, 12) }
gPERIOD_7  = { 'periodStart' : datetime.date(2008, 07, 03), 'periodEnd' : datetime.date(2008, 07, 11) }
gPERIOD_8  = { 'periodStart' : datetime.date(2008, 12, 18), 'periodEnd' : datetime.date(2008, 12, 31) }
gPERIOD_9  = { 'periodStart' : datetime.date(2012, 12, 26), 'periodEnd' : datetime.date(2013, 01, 05) }
gPERIOD_10 = { 'periodStart' : datetime.date(2013, 01, 01), 'periodEnd' : datetime.date(2013, 12, 31) }
gPERIOD_11 = { 'periodStart' : datetime.date(2012, 01, 01), 'periodEnd' : datetime.date(2012, 12, 31) }
gPERIOD_12 = { 'periodStart' : datetime.date(2000, 02, 18), 'periodEnd' : datetime.date(2015, 12, 31) }
gPERIOD_13 = { 'periodStart' : datetime.date(2008, 01, 01), 'periodEnd' : datetime.date(2010, 01, 31) }

testValues.setNoDataValue(-3000.0)

def getMeasurementIntervalNext(date):
   sdate = testValues.getMeasurementIntervalStart(date)
   sdate = sdate + datetime.timedelta(8)
   if sdate.year <> date.year: return datetime.date(date.year+1, 1, 1)
   return sdate

def getMeasurementIntervalEnd(date):
   sdate = testValues.getMeasurementIntervalStart(date)
   sdate = sdate + datetime.timedelta(7)
   if sdate.year <> date.year: return datetime.date(date.year, 12, 31)
   return sdate

def getMeasurementIndex(periodStart, periodEnd):
   sidx = (periodStart.timetuple().tm_yday-1) / 8
   eidx = (periodEnd.timetuple().tm_yday-1) / 8
   return (sidx, eidx)

def getMeasurementDoy(periodStart, periodEnd):
   pstart = (periodStart.timetuple().tm_yday - 1) / 8
   sdoy   = pstart*8+1
   pend   = (periodEnd.timetuple().tm_yday - 1) / 8
   edoy   = min(365, pend*8+1+7)
   return (sdoy, edoy)
