#!/usr/bin/python
################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2018, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################


import os
import sys
import httplib, urllib
import testlib
import testValues

testlib.setLogging(True)

multiPoints = '{ "type": "FeatureCollection", "features": [ { "type": "Feature", "properties": { }, "geometry": { "type": "MultiPoint", "coordinates": [ [ 36.459376310154937, 12.924434924467198 ], [ 36.464172025920988, 12.933198934248622 ], [ 36.557688483359058, 12.929693367265232 ], [ 36.563683128066629, 12.932614676504746 ] ] } } ]}'

testcase = 'buffer'
requestBuffer = testlib.buildWpsRequest(
             process='vec:BufferFeatureCollection',
             features=[multiPoints, 'ComplexData', {'mimeType' : 'application/json'}, 1],
             # 0,1 degrees is about 10 km.
             distance='0.1',
             response=['result', 'RawDataOutput', { 'mimeType' : 'application/json'}])

testcase = 'TimeSeriesByDateBuffered'
request = testlib.buildWpsRequest(
             process='VegetationIndex:TimeSeriesByDate',
             source="MODIS_500m",
             ROI=[requestBuffer, 'Reference', 'http://geoserver/wps', {'mimeType' : 'application/json'}],
             indicator="NDVI",
             periodStart="2016-01-01",
             periodEnd="2016-12-31",
             response=['result', 'RawDataOutput', { 'mimeType' : 'application/csv'}])

outFileName = ('/tmp/%s.csv' % testcase)
testValues.issueRequest(request, testcase, 1, outFileName)

testcase = 'PeriodAggregateMapBuffered'
request = testlib.buildWpsRequest(
             process='VegetationIndex:PeriodAggregateMap',
             source="MODIS_500m",
             ROI=[requestBuffer, 'Reference', 'http://geoserver/wps', {'mimeType' : 'application/json'}],
             indicator="NDVI",
             periodStart="2016-01-01",
             periodEnd="2016-12-31",
             aggregateType='MAX',
             response=['result', 'RawDataOutput', { 'mimeType' : 'image/tiff'}])

outFileName = ('/tmp/%s.tif' % testcase)
testValues.issueRequest(request, testcase, 1, outFileName)
