#!/bin/bash
#
# usage $0 [-u] [<date>]
# -u     : update logfile
# <date> : YYYY-MM-DD 
#
LOGPATH=/usr/share/tomcat6/logs
d=`date '+%F'`
y='20[0-9][0-9]-[0-9][0-9]-[0-9][0-9]'
while (( "$#" )); do
   case "$1" in
      -u)
         shift
         LOGFILE=$1
         shift
         ;;
      *)
         d=$1
         shift
         ;;
  esac
done
access_log=$LOGPATH/localhost_access_log.${d}.txt
hosts=`awk ' { print $1; }' $access_log | sort -u`
echo "hosts:"
for h in $hosts; do n=`grep $h $access_log | wc -l`; echo "$h  $n"; done | \
   sort -n -k 2 -r | \
   awk '{printf("%-40s\t%8d\n", $1, $2); total+=$2} END {printf("%-40s\t%8d\n", "TOTAL hosts", total);}'

GSLOGPATH=/usr/share/tomcat6/webapps/geoserver/data/logs
echo "=================================="
services='Precipitation:GetDates Precipitation:PeriodAggregateMap Precipitation:PeriodAggregateROI Precipitation:TimeSeriesByDate Precipitation:TimeSeriesByYear Precipitation:IntervalMap VegetationIndex:GetDates VegetationIndex:PeriodAggregateMap VegetationIndex:PeriodAggregateROI VegetationIndex:TimeSeriesByDate VegetationIndex:TimeSeriesByYear VegetationIndex:IntervalMap Other'
echo "services:"
t=0
if [[ -n "$LOGFILE" ]]; then echo -n "$d, " >> $LOGFILE; fi
for s in $services; do 
   if [[ "$s" == 'Other' ]]; then s=""; fi
   n=`egrep -h "value = |$y" $GSLOGPATH/geoserver.log* | \
      awk '/^20/ { d = $1; } /value =/ { print d, $3; }' | \
      egrep $d | \
      egrep "$s" | \
      wc -l`
   if [[ -z "$s" ]]; then
      s="Other"
      let n=$n-$t
   fi
   echo $s $n | \
      awk '{printf("%-40s\t%8d\n", $1, $2);}'
   if [[ -n "$LOGFILE" ]]; then echo -n "$n, " >> $LOGFILE; fi
   let t=t+$n
done
echo "TOTAL $t" | \
      awk '{printf("%-40s\t%8d\n", $1, $2);}'
echo "=================================="
if [[ -n "$LOGFILE" ]]; then echo "$t" >> $LOGFILE; fi
