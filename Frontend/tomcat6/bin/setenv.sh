#export JAVA_OPTS=$JAVA_OPTS:" -Xms2048m -Xmx2048m -XX:MaxPermSize=512m -XX:PermSize=512m"
export JAVA_OPTS="-Xms2048m -Xmx2048m -XX:MaxPermSize=1024m -XX:PermSize=1024m"
echo $JAVA_OPTS

# specific settings for CommonSense server
export MASK_TEMPLATES_PATH="/mnt/masks-release/"
export PROCESS_SCRIPTS_PATH="/usr/share/tomcat6/webapps/geoserver/data/scripts/lib/cpy/"
export DATA_CUBES_PATH="/mnt/datacubes/"

