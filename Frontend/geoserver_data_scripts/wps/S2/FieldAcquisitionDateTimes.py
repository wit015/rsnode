################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

import os
import re
import sys
import traceback

import subprocess

from geoserver.wps import process
from org.geoserver.wps.process import StringRawData

from java.util import Date
from java.text import SimpleDateFormat

import handleException 

@process (
   title = 'FieldAcquisitonDateTimes',
   description = 'Get the acquisition times for a field',
   inputs = {
      'user_id'   : (str,  'user id'),
      'group_id'  : (str,  'group id'),
      'field_id'  : (str,  'field id'),
      'outputMimeType': (str,  'The user chosen output mime type' , {'min': 0} )
   },
   outputs = {
      'result': (StringRawData, 'acquisition dates list', {'mimeTypes':'application/json,application/csv', 'chosenMimeType':'outputMimeType'} )
   }
)

def run(user_id, group_id, field_id, outputMimeType='application/json'):

   PROCESS_SCRIPTS_PATH = os.getenv('PROCESS_SCRIPTS_PATH')

   argv = [ PROCESS_SCRIPTS_PATH + '/processS2FieldAcquisitionDateTimes.py',
            user_id,
            field_id,            
            '--format', outputMimeType,
            '--group_id', group_id,
          ]

   p = subprocess.Popen(argv, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
   out, err = p.communicate()

   if not err == '':
       debugLogStr = "Command:\n" + str(argv) + "\n"
       debugLogStr += "StdOut:\n" + out + "\nStdErr\n" + err + "\n"
       raise Exception(debugLogStr)

   return StringRawData(out, outputMimeType)
