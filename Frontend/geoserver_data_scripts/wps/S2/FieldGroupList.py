################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

import os
import re
import sys
import traceback
g_svn_revision = str('$Rev: 7838 $').replace('$', '').strip(' \t\n\r')
g_svn_date = re.sub('\) \$', ' ' ,re.sub('^.*\(', 'Date: ', '$Date: 2017-01-23 12:39:07 +0100 (Mon, 23 Jan 2017) $')).strip(' \t\n\r')
g_svn_author = str('$Author: jb76278 $').replace('$', '').strip(' \t\n\r')
g_script_file_name = 'TimeSeriesByDate.py'

import uuid
import urllib
import subprocess

from geoserver.wps import process
from org.geoserver.wps.process import StringRawData
from org.geotools.process.vector import VectorToRasterProcess
from org.geotools.data import DataUtilities
from org.geotools.data.simple import SimpleFeatureCollection
from org.geotools.data.collection import ListFeatureCollection
from org.geotools.gce.geotiff import GeoTiffReader
from org.geotools.gce.geotiff import GeoTiffWriter
from org.geotools.geometry.jts import ReferencedEnvelope

from org.geotools.parameter import Parameter

from org.geotools.feature import FeatureCollection
from org.geotools.geojson.feature import FeatureJSON
from org.geotools.feature.simple import SimpleFeatureBuilder

from org.geotools.referencing import CRS

from org.opengis.metadata.spatial import Dimension
from org.opengis.feature.simple import SimpleFeature
from org.opengis.feature.simple import SimpleFeatureType

from org.geotools.coverage.grid import GridCoverage2D
from org.locationtech.jts.geom import Geometry
from org.locationtech.jts.geom import GeometryFactory
from org.geotools.geometry.jts import WKTReader2
from org.locationtech.jts.geom import Polygon

from org.geoserver.wps.process import RawData
from org.geoserver.wps.process import StreamRawData

from java.awt import Dimension
from java.lang import Boolean
from java.lang import System

import handleException 


@process (
   title = 'FieldList',
   description = 'Resturn GeoJSON will field lists',
   inputs = {
      'user_id'  : (str, 'user_id'),
      'group_id' : (str, 'group_id'),
      'srs'      : (str, 'target SRS', { 'min' : 0 } )
   },
   outputs = {
      'result': (StringRawData, 'geojson of field list', {'mimeTypes':'application/json'} )
   }
)

def run(user_id, group_id, srs='EPSG:4326'):

   PROCESS_SCRIPTS_PATH = os.getenv('PROCESS_SCRIPTS_PATH')
   curr_env = os.environ
   #raise Exception(str(curr_env))

   argv = [ PROCESS_SCRIPTS_PATH + 'processS2FieldGroupList.py',
            '--group_id', group_id,
            '--t_srs', srs,
            user_id]

   errorStr = ""
   debugLogStr = ""
   p = subprocess.Popen(argv, stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=curr_env)
   out, err = p.communicate()

   if not err == '':
       debugLogStr = "Command:\n" + str(argv) + "\n"
       debugLogStr += "StdOut:\n" + out + "\nStdErr\n" + err + "\n"
       raise Exception(debugLogStr)

   return StringRawData(out, 'application/json')
