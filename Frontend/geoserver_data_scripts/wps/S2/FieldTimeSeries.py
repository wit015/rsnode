################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

import os
import re
import sys
import traceback

import subprocess

from geoserver.wps import process
from org.geoserver.wps.process import StringRawData

from java.util import Date
from java.text import SimpleDateFormat

import handleException 

def dateToIsoFormat(date):
   fmt = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
   return fmt.format(date)

gINDICATORS  = ['NDVI']
gAGGREGATORS = ['AVG', 'SUM', 'MAX', 'MIN', 'MED' ]

@process (
   title = 'FieldTimeSeries',
   description = 'Get the time series of the values in a field',
   inputs = {
      'user_id'          : (str,   'user id'),
      'group_id'         : (str,   'group id'),
      'field_id'         : (str,   'field id'),
      'start_datetime'   : (Date,  'datetime YYYY-MM-DD[THH:MM:SS]', {'min': 0} ),
      'end_datetime'     : (Date,  'datetime YYYY-MM-DD[THH:MM:SS]', {'min': 0} ),
      'lb_percentile'    : (float, 'lower bound percentile', { 'min': 0 } ),
      'ub_percentile'    : (float, 'upper bound percentile', { 'min': 0 } ),
      'pixel_percentage' : (float, 'lower bound for percentage of valid pixels', { 'min': 0 } ),
      'indicator'        : (str,   'indicator', { 'min' : 0, 'domain' : gINDICATORS } ),
      'aggregator'       : (str,   'aggregator for values', { 'min' : 0, 'domain' : gAGGREGATORS } ),
      'mask_list'        : (str,   'list of SCL mask values', { 'min' : 0 } ),
      'outputMimeType'   : (str,   'The user chosen output mime type' , {'min': 0} )

   },
   outputs = {
      'result': (StringRawData, 'field list', {'mimeTypes':'application/json,application/csv', 'chosenMimeType':'outputMimeType'} )
   }
)

def run(user_id, 
        group_id, 
        field_id, 
        start_datetime=Date(100,1,1), 
        end_datetime=Date(1099,1,1),
        lb_percentile=0.0,
        ub_percentile=100.0,
        pixel_percentage=0.0,
        indicator=gINDICATORS[0],
        aggregator=gAGGREGATORS[0], 
        mask_list=None,
        outputMimeType='application/json'):

   PROCESS_SCRIPTS_PATH = os.getenv('PROCESS_SCRIPTS_PATH')

   argv = [ PROCESS_SCRIPTS_PATH + '/processS2FieldNDVITimeSeries.py',
            user_id,
            field_id,            
            '--group_id', group_id,
            '--start_date', dateToIsoFormat(start_datetime),
            '--end_date', dateToIsoFormat(end_datetime),
            '--lbp', str(lb_percentile),
            '--ubp', str(ub_percentile),
            '--vpp', str(pixel_percentage),
            '--ag', aggregator,
            '--format', outputMimeType,
          ]

   if mask_list is not None:
       mask_list=mask_list[1:-1]
       l = re.split('[ ,]', mask_list)
       argv.extend(["--mask_list"] + l)

   p = subprocess.Popen(argv, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
   out, err = p.communicate()

   if not err == '':
       debugLogStr = "Command:\n" + str(argv) + "\n"
       debugLogStr += "StdOut:\n" + out + "\nStdErr\n" + err + "\n"
       raise Exception(debugLogStr)

   return StringRawData(out, outputMimeType)
