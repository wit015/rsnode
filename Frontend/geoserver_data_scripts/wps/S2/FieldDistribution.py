################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

import os
import re
import sys
import traceback

import subprocess

from geoserver.wps import process
from org.geoserver.wps.process import StringRawData

from java.util import Date
from java.text import SimpleDateFormat

import handleException 

def dateToIsoFormat(date):
   fmt = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
   return fmt.format(date)


@process (
   title = 'FieldDistribution',
   description = 'Get the distribution of the values in a field',
   inputs = {
      'user_id'       : (str,  'user id'),
      'group_id'      : (str,  'group id'),
      'field_id'      : (str,  'field id'),
      'datetime'      : (Date, 'datetime YYYY-MM-DD[THH:MM:SS]', {'min': 0} ),
      'indicator'     : (str,  'indicator NDVI, SCL', {'min': 0} ),
      'nbins'         : (int,  'number of bins', {'min': 0} ),
      'mask_list'     : (str,  'SCL mask list', {'min': 0} ),
      'outputMimeType': (str,  'The user chosen output mime type' , {'min': 0} )

   },
   outputs = {
      'result': (StringRawData, 'field list', {'mimeTypes':'application/json,application/csv', 'chosenMimeType':'outputMimeType'} )
   }
)

def run(user_id, group_id, field_id, datetime=None, indicator=None, nbins=0, mask_list=None, outputMimeType='application/json'):

   PROCESS_SCRIPTS_PATH = os.getenv('PROCESS_SCRIPTS_PATH')

   argv = [ PROCESS_SCRIPTS_PATH + '/processS2FieldDistribution.py',
            user_id,
            field_id,            
            '--nbins', str(nbins),
            '--group_id', group_id,
            '--format', outputMimeType,
          ]

   if datetime is not None:
       argv.extend(['--datetime', dateToIsoFormat(datetime)])

   if indicator is not None:
       argv.extend(['--indicator', indicator])

   if mask_list is not None:
       mask_list=mask_list[1:-1]
       l = re.split('[ ,]', mask_list)
       argv.extend(["--mask_list"] + l)

   p = subprocess.Popen(argv, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
   out, err = p.communicate()

   if not err == '':
       debugLogStr = "Command:\n" + str(argv) + "\n"
       debugLogStr += "StdOut:\n" + out + "\nStdErr\n" + err + "\n"
       raise Exception(debugLogStr)

   return StringRawData(out, outputMimeType)
