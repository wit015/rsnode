################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

import os
import re
import sys
import traceback
g_svn_revision = str('$Rev: 7838 $').replace('$', '').strip(' \t\n\r')
g_svn_date = re.sub('\) \$', ' ' ,re.sub('^.*\(', 'Date: ', '$Date: 2017-01-23 12:39:07 +0100 (Mon, 23 Jan 2017) $')).strip(' \t\n\r')
g_svn_author = str('$Author: jb76278 $').replace('$', '').strip(' \t\n\r')
g_script_file_name = 'TimeSeriesByDate.py'

import uuid
import urllib
import subprocess

from geoserver.wps import process
from org.geoserver.wps.process import StringRawData
from org.geotools.process.vector import VectorToRasterProcess
from org.geotools.data import DataUtilities
from org.geotools.data.simple import SimpleFeatureCollection
from org.geotools.data.collection import ListFeatureCollection
from org.geotools.gce.geotiff import GeoTiffReader
from org.geotools.gce.geotiff import GeoTiffWriter
from org.geotools.geometry.jts import ReferencedEnvelope

from org.geotools.parameter import Parameter

from org.geotools.feature import FeatureCollection
from org.geotools.geojson.feature import FeatureJSON
from org.geotools.feature.simple import SimpleFeatureBuilder

from org.geotools.referencing import CRS

from org.opengis.metadata.spatial import Dimension
from org.opengis.feature.simple import SimpleFeature
from org.opengis.feature.simple import SimpleFeatureType

from org.geotools.coverage.grid import GridCoverage2D
from org.locationtech.jts.geom import Geometry
from org.locationtech.jts.geom import GeometryFactory
from org.geotools.geometry.jts import WKTReader2
from org.locationtech.jts.geom import Polygon

from org.geoserver.wps.process import RawData
from org.geoserver.wps.process import StreamRawData

from java.awt import Dimension
from java.io import ByteArrayInputStream
from java.io import File
from org.python.modules import jarray
from java.io import FileInputStream
from java.io import FileOutputStream
from java.lang import Boolean
from java.lang import System

import handleException 


@process (
   title = 'FieldIdentify',
   description = 'Identify field in S2',
   inputs = {
      'bbox'      : (str,               'bbox'),
      'srs'       : (str,               'srs'),
      'width'     : (int,               'width in pixels'),
      'height'    : (int,               'height in pixels'),
      'features'  : (FeatureCollection, 'feature collection including CRS'),
      'outputMimeType': (str, 'The user chosen output mime type' , {'min': 0, 'domain' : ['image/tiff', 'image/png'] } )
   },
   outputs = {
      'result': (RawData, 'image defining field', {'mimeTypes':'image/tiff,image/png', 'chosenMimeType':'outputMimeType'} )
   }
)

def run(features, outputMimeType='image/tiff', bbox=None, srs=None, width=None, height=None):

   #--------------------------------------

   def getByteArray(fileUrl):
       file = File(fileUrl)
       inputStream = FileInputStream(file)
       length = file.length()
       bytes = jarray.zeros(length, 'b')

       #Read in the bytes
       offset = 0
       numRead = 0
       while offset<length:
           if numRead>= 0:
               print numRead
   	       numRead=inputStream.read(bytes, offset, length-offset)
               offset = offset + numRead

       return bytes

   #--------------------------------------


   if bbox is not None:
       box = map(float, bbox.split(","))
       boxstr = "%f %f %f %f" % (box[0], box[1], box[2], box[3])       
   else:
       boxstr = None
   
   suuid = str(uuid.uuid4())

   ext = '.tif'
   if outputMimeType == 'image/png': ext = '.png'
   outfile = '/tmp/out-' + suuid + ext

   tmpfile  = '/tmp/tmp-'  + suuid + '.tif'
   jsonfile = '/tmp/json-' + suuid + '.json'
   cmdfile  = '/tmp/cmd-'  + suuid + '.txt'

   io = FeatureJSON();
   io.setEncodeFeatureCollectionCRS(True)
   io.writeFeatureCollection(features, jsonfile)
  
   PROCESS_SCRIPTS_PATH = os.getenv('PROCESS_SCRIPTS_PATH')

   argv = [ PROCESS_SCRIPTS_PATH + '/processS2FieldIdentify.py',
            jsonfile,
            outfile,
            '--fill', '250', '254', '254']

   if boxstr is not None:
       argv.extend(['--bbox', str(box[0]), str(box[1]), str(box[2]), str(box[3])])
            
   if srs is not None:
       argv.extend(['--t_srs', srs])

   if (width is not None) and (height is not None):
       argv.extend(['--outsize',str(width),str(height)])

   f = open(cmdfile, 'w')
   f.write(str(argv))
   f.close()
   
   p = subprocess.Popen(argv, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
   out, err = p.communicate()

   if not err == '':
       debugLogStr = "Command:\n" + str(argv) + "\n"
       debugLogStr += "StdOut:\n" + out + "\nStdErr\n" + err + "\n"
       raise Exception(debugLogStr)

   os.remove(jsonfile)
   os.remove(cmdfile)

   bdata = getByteArray(outfile)
   os.remove(outfile)

   return StreamRawData(outputMimeType, ByteArrayInputStream(bdata));
