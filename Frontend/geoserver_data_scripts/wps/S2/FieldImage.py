################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

import os
import re
import sys
import traceback

import uuid
import urllib
import subprocess

from geoserver.wps import process
from org.geoserver.wps.process import StringRawData
from org.geotools.process.vector import VectorToRasterProcess
from org.geotools.data import DataUtilities
from org.geotools.data.simple import SimpleFeatureCollection
from org.geotools.data.collection import ListFeatureCollection
from org.geotools.gce.geotiff import GeoTiffReader
from org.geotools.gce.geotiff import GeoTiffWriter
from org.geotools.geometry.jts import ReferencedEnvelope

from org.geotools.parameter import Parameter

from org.geotools.feature import FeatureCollection
from org.geotools.geojson.feature import FeatureJSON
from org.geotools.feature.simple import SimpleFeatureBuilder

from org.geotools.referencing import CRS

from org.opengis.metadata.spatial import Dimension
from org.opengis.feature.simple import SimpleFeature
from org.opengis.feature.simple import SimpleFeatureType

from org.geotools.coverage.grid import GridCoverage2D
from org.locationtech.jts.geom import Geometry
from org.locationtech.jts.geom import GeometryFactory
from org.geotools.geometry.jts import WKTReader2
from org.locationtech.jts.geom import Polygon

from org.geoserver.wps.process import RawData
from org.geoserver.wps.process import StreamRawData

from java.util import Date
from java.text import SimpleDateFormat

from java.awt import Dimension
from java.io import File
from java.io import FileInputStream
from java.io import FileOutputStream
from java.io import ByteArrayInputStream
from java.lang import Boolean
from java.lang import System

from org.python.modules import jarray

import handleException 

def dateToIsoFormat(date):
   fmt = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
   return fmt.format(date)


@process (
   title = 'FieldImage',
   description = 'Image of field in S2 - NDVI/SCL',
   inputs = {
      'bbox'      : (str,         'bbox', { 'min' : 0 } ),
      'srs'       : (str,         'srs', { 'min' : 0 } ),
      'width'     : (int,         'width in pixels', { 'min' : 0 } ),
      'height'    : (int,         'height in pixels', { 'min' : 0 } ),
      'datetime'  : (Date,        'datetime YY-MM-DD[THH:MM:SS]', { 'min' : 0 } ),
      'user_id'   : (str,         'user id'),
      'group_id'  : (str,         'group id'),
      'field_id'  : (str,         'field id'),
      'indicator'  : (str,        'NDVI/SCL', { 'min' : 0 } ),
      'outputMimeType': (str, 'The user chosen output mime type' , {'min': 0})
   },
   outputs = {
      'result': (RawData, 'image of field', {'mimeTypes':'image/tiff,image/png', 'chosenMimeType':'outputMimeType'} )
   }
)

def run(user_id, field_id, outputMimeType='image/tiff', bbox=None, indicator='NDVI', group_id='DEFAULT', datetime=None, srs="EPSG:4326", width=None, height=None):

   #--------------------------------------

   def getByteArray(fileUrl):
       file = File(fileUrl)
       inputStream = FileInputStream(file)
       length = file.length()
       bytes = jarray.zeros(length, 'b')

       #Read in the bytes
       offset = 0
       numRead = 0
       while offset<length:
           if numRead>= 0:
               print numRead
   	       numRead=inputStream.read(bytes, offset, length-offset)
               offset = offset + numRead

       return bytes

   #--------------------------------------

   suuid = str(uuid.uuid4())
   outfile = '/tmp/out-' + suuid + '.png'

   PROCESS_SCRIPTS_PATH = os.getenv('PROCESS_SCRIPTS_PATH')

   argv = [ PROCESS_SCRIPTS_PATH + '/processS2FieldImage.py',
            user_id,
            field_id,            
            outfile,
            '--group_id', group_id,
            '--indicator', indicator,
          ]

   if datetime is not None:
       argv.extend(['--datetime', dateToIsoFormat(datetime)])

   if srs is not None:
       argv.extend(['--t_srs', srs])

   if outputMimeType.lower() == "image/png":
       argv.extend(['--png'])

   if (width is not None) and (height is not None):
       argv.extend(['--outsize',str(width),str(height)])

   if bbox is not None:
       box = bbox.split(",")
       argv.extend(['--bbox', str(box[0]), str(box[1]), str(box[2]), str(box[3])])

   p = subprocess.Popen(argv, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
   out, err = p.communicate()

   if not err == '':
       debugLogStr = "Command:\n" + str(argv) + "\n"
       debugLogStr += "StdOut:\n" + out + "\nStdErr\n" + err + "\n"
       raise Exception(debugLogStr)

   bdata = getByteArray(outfile)
   os.remove(outfile)   

   return StreamRawData(outputMimeType, ByteArrayInputStream(bdata));
