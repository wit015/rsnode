################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2018, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

import os
import re
import sys
import traceback

import uuid
import urllib
import subprocess

from geoserver.wps import process
from org.geoserver.wps.process import StringRawData

from org.geotools.feature import FeatureCollection
from org.geotools.geojson.feature import FeatureJSON

import handleException 



@process (
   title = 'FieldDeclare',
   description = 'Declare a field for which S2 NDVI values are required',
   inputs = {
      'group_id'      : (str,               'group_id'),
      'user_id'       : (str,               'user_id'),
      'features'      : (FeatureCollection, 'feature collection including CRS'),
      'tag'           : (str,               'user defined tag string')
   },
   outputs = {
      'result': (StringRawData, 'json of field metadata', {'mimeTypes':'application/json'} )
   }
)

def run(group_id, user_id, features, tag):

   suuid = str(uuid.uuid4())
   featuresfile = '/tmp/json-' + suuid + '.json'

   io = FeatureJSON();
   io.setEncodeFeatureCollectionCRS(True)
   io.writeFeatureCollection(features, featuresfile)

   curr_env = os.environ
   PROCESS_SCRIPTS_PATH = os.getenv('PROCESS_SCRIPTS_PATH')

   # create temporary file
  
   argv = [ PROCESS_SCRIPTS_PATH + 'processS2FieldDeclare.py',
            user_id,
            tag,
            featuresfile,
            '--group_id', group_id
          ]

   errorStr = ""
   debugLogStr = ""
   p = subprocess.Popen(argv, stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=curr_env)
   out, err = p.communicate()

   if not err == '':
       debugLogStr = "Command:\n" + str(argv) + "\n"
       debugLogStr += "StdOut:\n" + out + "\nStdErr\n" + err + "\n"
       raise Exception(debugLogStr)

   return StringRawData(out, 'application/json')
