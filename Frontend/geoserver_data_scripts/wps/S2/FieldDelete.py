################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

import os
import re
import sys
import traceback

g_svn_revision = str('$Rev: 7838 $').replace('$', '').strip(' \t\n\r')
g_svn_date = re.sub('\) \$', ' ' ,re.sub('^.*\(', 'Date: ', '$Date: 2017-01-23 12:39:07 +0100 (Mon, 23 Jan 2017) $')).strip(' \t\n\r')
g_svn_author = str('$Author: jb76278 $').replace('$', '').strip(' \t\n\r')
g_script_file_name = 'processS2FieldDelete.py'

import subprocess

from geoserver.wps import process
from org.geoserver.wps.process import StringRawData

import handleException 


@process (
   title = 'FieldDelete',
   description = 'Delete a field',
   inputs = {
      'user_id'  : (str, 'user ID string'),
      'group_id' : (str, 'group ID string'),
      'field_id' : (str, 'field ID string')
   },
   outputs = {
      'result': (StringRawData, 'field ID + field count on success', {'mimeTypes':'application/json'} )
   }
)

def run(user_id, group_id, field_id):

   PROCESS_SCRIPTS_PATH = os.getenv('PROCESS_SCRIPTS_PATH')
   curr_env = os.environ
   #raise Exception(str(curr_env))

   argv = [ PROCESS_SCRIPTS_PATH + 'processS2FieldDelete.py',
            user_id, 
            field_id,
            '--group_id', group_id,
          ]

   errorStr = ""
   debugLogStr = ""
   p = subprocess.Popen(argv, stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=curr_env)
   out, err = p.communicate()

   if not err == '':
       debugLogStr = "Command:\n" + str(argv) + "\n"
       debugLogStr += "StdOut:\n" + out + "\nStdErr\n" + err + "\n"
       raise Exception(debugLogStr)

   return StringRawData(out, 'application/json')
