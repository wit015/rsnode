################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

import os
import re
import sys
import traceback
g_svn_revision = str('$Rev: 7838 $').replace('$', '').strip(' \t\n\r')
g_svn_date = re.sub('\) \$', ' ' ,re.sub('^.*\(', 'Date: ', '$Date: 2017-01-23 12:39:07 +0100 (Mon, 23 Jan 2017) $')).strip(' \t\n\r')
g_svn_author = str('$Author: jb76278 $').replace('$', '').strip(' \t\n\r')
g_script_file_name = 'IntervalMap.py'

import uuid
import urllib
import subprocess

from java.io import File
from java.util import Date
from java.lang import Boolean

from geoserver.wps import process
from org.geoserver.wps.process import StringRawData
from org.geotools.coverage.grid import GridCoverage2D
from org.geotools.coverage.grid.io import AbstractGridFormat
from org.geotools.gce.geotiff import GeoTiffReader
from org.geotools.feature import FeatureCollection
from org.geotools.parameter import Parameter

import handleROI
import handleException

# For 200 x 200 pixel image at 4-bytes per pixel + some overhead
gFILE_SIZE_TILE_LIMIT = 165000

# We have only one source, but WPS interface needs at least two in a set ...
gSOURCE_DICT         = { 'CHIRPS_5000m' : '/CHIRPS/', 'CHIRPS_5000m' : '/CHIRPS/' }
gINDICATOR_DICT      = { 'PRECIP_PRELIM' : 'PRECIP_PRELIM', 
                         'PRECIP_FINAL'  : 'PRECIP_FINAL',
                         'TEST'          : 'TEST' }
gMASK_DICT           = { 'NONE' : 'NONE', 'ESA_CCI_2010' : 'ESA_CCI_2010', 'TEST' : 'TEST' }
gSTATISTIC_TYPE_SET  = ( 'VALUE', 'P00', 'P25', 'P50', 'P75', 'P100' )
gINTEGRATE_TYPE_SET  = ( 'SUM', 'NONE' )
gAGGREGATE_TYPE_SET  = ( 'MAX', 'MIN', 'THR' )

gLTA_START_DATE = "1981-01-01"
gLTA_END_DATE   = "2015-12-31"

#########################################################
# Definition of the WPS interface (input/output)
#########################################################
@process (
   title = 'Compute threshold interval map',
   description = 'Compute map by calculating the number of days it takes (interval) to reach a threshold precipitation',
   inputs = {
      'source'         : (str, 'Data source identifier', { 'domain' : gSOURCE_DICT.keys() } ),
      'ROI'            : (FeatureCollection, 'Region of interest (Polygon/Point WGS84)' ),
      'maskID'         : (str, 'Mask to be applied', { 'min' : 0, 'domain' : gMASK_DICT.keys() } ),
      'indicator'      : (str, 'observered variable to use', { 'domain' : gINDICATOR_DICT.keys() } ),
      'integrateType'  : (str, 'type of temporal integration', { 'min' : 0, 'domain' : gINTEGRATE_TYPE_SET } ),
      'aggregateType'  : (str, 'type of temporal aggregation', { 'min' : 0, 'domain' : gAGGREGATE_TYPE_SET } ),
      'statistic'      : (str, 'multi year statistic requested', { 'min' : 0, 'domain' : gSTATISTIC_TYPE_SET } ),
      'threshold'      : (float, 'limit for THReshold aggregation', { 'min' : 0 } ),
      'periodStart'    : (Date, 'start date of aggregation period (ISO 8601)'),
      'periodEnd'      : (Date, 'end date of aggregation period (ISO 8601)'),
      'windowLength'   : (int, 'number of days for temporal integration', { 'min' : 0 } ),
      'label'          : (str, 'Label echoed in output', { 'min': 0 } )
   },
   outputs = {
      'geotiff': (GridCoverage2D, 'Geotiff with http header', {'mimeTypes':'image/tiff'} )
   }
)

#########################################################
# Handling of a WPS request
#########################################################
def run(source=None, ROI=None, maskID='NONE', indicator=None, integrateType='SUM', aggregateType='MAX', statistic='VALUE', threshold=0.0, periodStart=None, periodEnd=None, windowLength=0, label='no label'):

   global gFILE_SIZE_TILE_LIMIT, gSOURCE_DICT, gINDICATOR_DICT, gMASK_DICT, gSTATISTIC_TYPE_SET, gINTEGRATE_TYPE_SET, gAGGREGATE_TYPE_SET, gLTA_START_DATE, gLTA_END_DATE

   PROCESS_SCRIPTS_PATH = os.getenv('PROCESS_SCRIPTS_PATH')
   MASK_TEMPLATES_PATH  = os.getenv('MASK_TEMPLATES_PATH')
   DATA_CUBES_PATH      = os.getenv('DATA_CUBES_PATH')

   def toYYMMDD(date):
      return "%04d-%02d-%02d" % (date.year+1900, date.month+1, date.date)

   # errorStr is for end-user (to fix input they provide), debugLogStr is for developers (to debug code)
   errorStr = ""
   debugLogStr = ""

   # Try if it is possible to open a debug log file for developers
   try:
      logFileName = os.path.join('/tmp',g_script_file_name + "_" + str(uuid.uuid4()) + ".log")
      logFile = open(logFileName, 'w')
      logFile.close()
      os.remove(logFileName)
   except:
      # Cannot inform developers. Exit and tell users what the problem is.
      errorStr += "\nUnable to open log file (" + str(logFileName) + ") for writing."
      sys.exit(errorStr)

   try:
      # Determine source extension
      if source in gSOURCE_DICT:
         sourceExt = gSOURCE_DICT[source]
      else:
         sys.exit("\nError: source \'" + source + "\' not valid (" + str(gSOURCE_DICT.keys()) + ").\n")

      # Determine indicator extension
      if indicator in gINDICATOR_DICT:
         indicatorExt = gINDICATOR_DICT[indicator]
      else:
         sys.exit("\nError: indicator \'" + indicator + "\' not a valid value. " + str(gINDICATOR_DICT.keys()) + ".\n")

      # Get mask template from which to crop the ROI
      # Determine maskID extension
      if maskID in gMASK_DICT:
         maskIdExt = gMASK_DICT[maskID]
      else:
         sys.exit("\nError: maskID \'" + maskID + "\' not a valid value. " + str(gMASK_DICT.keys()) + ".\n")

      # Determine input filenames
      baseFileName = DATA_CUBES_PATH + sourceExt + 'base-' + indicatorExt + '.hdf5'
      maskTemplate = MASK_TEMPLATES_PATH + sourceExt + maskIdExt + ".tif"

      # Mask to be used for product data generation
      maskFileName = "/tmp/" + "mask-" + str(uuid.uuid4()) + ".tif"

      ###################
      # Get the mask from the ROI and store it in a geotiff
      ###################
      handleROI.main(ROI, maskTemplate, maskFileName)

      # Create Date strings
      periodStart = toYYMMDD(periodStart)
      periodEnd   = toYYMMDD(periodEnd)

      # Script in CPython to process request
      processScript = PROCESS_SCRIPTS_PATH + 'processIntervalMap.py'

      # Generate temp filename for output
      outFileName = "/tmp/" + "result-" + str(uuid.uuid4()) + ".tif"

      argv = [ processScript,
               outFileName,
               baseFileName,
               maskFileName,
               periodStart,
               periodEnd,
               '--source='    + source,
               '--window='    + str(windowLength),
               '--lta-start=' + str(gLTA_START_DATE),
               '--lta-end='   + str(gLTA_END_DATE),
               '--label='     + '"'+label+'"'
             ]

      # Integrator Type flag
      if integrateType in gINTEGRATE_TYPE_SET:
          argv.append("--integrate=" + str(integrateType))
      else:
          sys.exit("\nError: integrateType value \'" + integrateType + "\' not a valid value. " + str(gINTEGRATE_TYPE_SET) + ".\n")

      # Aggregate Type flag
      if not aggregateType in gAGGREGATE_TYPE_SET:
          sys.exit("\nError: aggregateType value \'" + aggregateType + "\' not a valid value. " + str(gAGGREGATE_TYPE_SET) + ".\n")

      if aggregateType == 'THR':
          argv.append("--threshold=" + str(threshold))
      if aggregateType == 'MAX':
          argv.append("--max")
      if aggregateType == 'MIN':
          argv.append("--min")

      # Statistics Type flag
      if statistic in gSTATISTIC_TYPE_SET:
          argv.append("--stats=" + str(statistic))
      else:
          sys.exit("\nError: statistic value \'" + statistic + "\' not a valid value. " + str(gSTATISTIC_TYPE_SET) +".\n")

      p = subprocess.Popen(argv, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
      out, err = p.communicate()

      if not err == '':
         debugLogStr += "Command:\n" + str(argv) + "\n"
         debugLogStr += "StdOut:\n" + out + "\nStdErr\n" + err + "\n"
         raise Exception()

      # Clean-up generated mask
      os.remove(maskFileName)

      reader = GeoTiffReader(outFileName)
 
      hints = []
      # Coverage2D has default 512x512 size resulting in large TIF files
      # For 'small' sized results set tile size to 1x1, This results in
      # larger java memory usage.
      if os.path.getsize(outFileName) < gFILE_SIZE_TILE_LIMIT:
         p = Parameter(AbstractGridFormat.SUGGESTED_TILE_SIZE)
         # 1,1 does not work for single pixel TIF file.
         p.setValue("2,2")
         hints.append(p)

      result =  reader.read(hints)

      # Clean-up generated file
      os.remove(outFileName)

      # If debugging is needed
      if not debugLogStr == '':
         scriptStr = str(g_script_file_name) + " Revision: " + str(g_svn_revision)
         handleException.writeLogFile(logFileName, scriptStr, "", debugLogStr)

      return result

   except:
      # Log the exception. As much output as possible to help in debugging
      scriptStr = str(g_script_file_name) + " Revision: " + str(g_svn_revision)
      handleException.writeLogFile(logFileName, scriptStr, errorStr, debugLogStr)
      errorStr += "%s\n" % logFileName
      f = open(logFileName, 'r')
      errorStr += f.read()
      sys.exit(errorStr)
