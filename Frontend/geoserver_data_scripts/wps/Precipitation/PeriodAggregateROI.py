################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

import os
import re
import sys
import traceback
g_svn_revision = str('$Rev: 7856 $').replace('$', '').strip(' \t\n\r')
g_svn_date = re.sub('\) \$', ' ' ,re.sub('^.*\(', 'Date: ', '$Date: 2017-01-26 17:37:46 +0100 (Thu, 26 Jan 2017) $')).strip(' \t\n\r')
g_svn_author = str('$Author: jb76278 $').replace('$', '').strip(' \t\n\r')
g_script_file_name = 'PeriodAggregateROI.py'

import uuid
import subprocess

from java.lang import Boolean
from java.util import Date

from geoserver.wps import process
from org.geoserver.wps.process import StringRawData
from org.geotools.feature import FeatureCollection

import handleROI
import handleException

gSOURCE_DICT         = { 'CHIRPS_5000m' : '/CHIRPS/', 'CHIRPS_5000m' : '/CHIRPS/' }
gINDICATOR_DICT      = { 'PRECIP_PRELIM' : 'PRECIP_PRELIM',
                         'PRECIP_FINAL'  : 'PRECIP_FINAL',
                         'PRECIP_LTA'    : 'PRECIP_FINAL',
                         'TEST'          : 'TEST',
                         'TEST_LTA'      : 'TEST' }
gMASK_DICT            = { 'NONE' : 'NONE', 'ESA_CCI_2010' : 'ESA_CCI_2010', 'TEST' : 'TEST' }
gREFERENCE_TYPE_SET   = ( 'NONE', 'ABS', 'LTA' )
gAGGREGATE_TYPE_SET   = ( 'AVG', 'MIN', 'MAX' )
gINTEGRATE_TYPE_SET   = ( 'NONE', 'SUM' )

gLTA_START_DATE     = "1981-01-01"
gLTA_END_DATE       = "2015-12-31"

@process (
   title = 'Compute aggregated precipitation value for ROI',
   description = 'Compute aggregated precipitation value for ROI',
   inputs = {
      'source'         : (str, 'Data source identifier', { 'domain' : gSOURCE_DICT.keys() } ),
      'ROI'            : (FeatureCollection, 'Region of interest (Polygon/Point WGS84)' ),
      'indicator'      : (str, 'observered variable to use', { 'domain' : gINDICATOR_DICT.keys() } ),
      'aggregateType'  : (str, 'Type of aggregation', { 'domain' : gAGGREGATE_TYPE_SET } ),
      'periodStart'    : (Date, 'Start date of aggregation period (ISO 8601)'),
      'periodEnd'      : (Date, 'End date of aggregation period (ISO 8601)'),
      'integrateType'  : (str, 'Type of integration', { 'min': 0, 'domain' : gINTEGRATE_TYPE_SET } ),
      'windowLength'   : (int, 'number of days for temporal integration', { 'min' : 0 } ),
      'maskID'         : (str, 'Mask to be applied', { 'min' : 0, 'domain' : gMASK_DICT.keys() } ),
      'referenceStart' : (Date, 'Start date of reference period (ISO 8601)', { 'min': 0 } ),
      'referenceType'  : (str, 'Type of reference data', { 'min': 0, 'domain' : gREFERENCE_TYPE_SET } ),
      'label'          : (str, 'Label echoed in output', { 'min': 0 } )
   },
   outputs = {
     'result': (StringRawData, 'result of computation in JSON format', { 'mimeTypes' : 'application/json' } )
   }
)

def run(source, ROI, indicator, aggregateType, periodStart, periodEnd, integrateType='SUM', windowLength=0, maskID='NONE', referenceStart=None, referenceType='NONE', label=None):

   global gMASK_DICT, gSOURCE_DICT, gINDICATOR_DICT, gREFERENCE_TYPE_SET, gAGGREGATE_TYPE_SET, gINTEGRATE_TYPE_SET, gLTA_START_DATE, gLTA_END_DATE

   PROCESS_SCRIPTS_PATH = os.getenv('PROCESS_SCRIPTS_PATH')
   MASK_TEMPLATES_PATH  = os.getenv('MASK_TEMPLATES_PATH')
   DATA_CUBES_PATH      = os.getenv('DATA_CUBES_PATH')

   def toYYMMDD(date):
      return "%04d-%02d-%02d" % (date.year+1900, date.month+1, date.date)

   # errorStr is for end-user (to fix input they provide), debugLogStr is for developers (to debug code)
   errorStr = ''
   debugLogStr = ''


   # Try if it is possible to open a log file for developers
   try:
      logFileName = os.path.join('/tmp',g_script_file_name + "_" + str(uuid.uuid4()) + ".log")
      logFile = open(logFileName, 'w')
      logFile.close()
      os.remove(logFileName)
   except:
      errorStr += "\nUnable to open log file (" + str(logFileName) + ") for writing."
      sys.exit(errorStr)

   try:
      # Determine source extension
      if source in gSOURCE_DICT:
         sourceExt = gSOURCE_DICT[source]
      else:
         sys.exit("\nError: source \'" + source + "\' not valid value (" + str(gSOURCE_DICT.keys()) + ").\n")

      # Determine indicator extension
      if indicator in gINDICATOR_DICT:
         indicatorExt = gINDICATOR_DICT[indicator]
      else:
         sys.exit("\nError: indicator \'" + indicator + "\' not a valid value. " + str(gINDICATOR_DICT.keys()) + ".\n")

      # Determine maskID extension
      if maskID in gMASK_DICT:
         maskIdExt = gMASK_DICT[maskID]
      else:
         sys.exit("\nError: maskID \'" + maskID + "\' not a valid value. " + str(gMASK_DICT.keys()) + ".\n")

      # Determine input filenames
      baseFileName = DATA_CUBES_PATH + sourceExt + 'base-' + indicatorExt + '.hdf5'
      if periodStart == periodEnd:
         ltaFileName  = DATA_CUBES_PATH + sourceExt + 'lta-' + indicatorExt + '.hdf5'
      else:
         ltaFileName  = ''
      maskTemplate = MASK_TEMPLATES_PATH + sourceExt + maskIdExt + ".tif"

      # Create writer to write resulting mask
      maskFileName = "/tmp/" + "mask-" + str(uuid.uuid4()) + ".tif"

      ###################
      # Get region of interest
      ###################
      handleROI.main(ROI, maskTemplate, maskFileName)

      # Create Date strings
      periodStart = toYYMMDD(periodStart)
      periodEnd   = toYYMMDD(periodEnd)

      # Script in CPython to process request
      processScript = PROCESS_SCRIPTS_PATH + 'processPeriodAggregateROI.py'

      argv = [ processScript,
               baseFileName,
               ltaFileName,
               maskFileName,
               periodStart,
               periodEnd,
               '--window='    + str(windowLength),
               '--lta-start=' + gLTA_START_DATE,
               '--lta-end='   + gLTA_END_DATE,
               '--source='    + source,
               '--indicator=' + indicator,
               '--maskID='    + maskID
             ]

      if indicator.endswith('_LTA'):
         argv.append("--lta")

      if referenceType in gREFERENCE_TYPE_SET:
         if referenceType <> 'NONE':
            argv.append('--reftype=' + referenceType)
            argv.append('--refdate=' + toYYMMDD(referenceStart))
      else:
         sys.exit("\nError: referenceType \'" + referenceType + "\' not a valid value. " + str(gREFERENCE_TYPE_SET) + ".\n")

      if aggregateType in gAGGREGATE_TYPE_SET:
         argv.append('--aggregate=' + aggregateType)
      else:
         sys.exit("\nError: aggregateType \'" + aggregateType + "\' not a valid value. " + str(gAGGREGATE_TYPE_SET) + ".\n")

      if integrateType in gINTEGRATE_TYPE_SET:
         argv.append('--integrate=' + integrateType)
      else:
         sys.exit("\nError: integrateType \'" + integrateType + "\' not a valid value. " + str(gINTEGRATE_TYPE_SET) + ".\n")

      if label <> None:
         argv.append('--label=' + label)

      p = subprocess.Popen(argv, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
      out, err = p.communicate()

      if not err == '':
         debugLogStr += "Command:\n" + str(argv) + "\n"
         debugLogStr += "StdOut:\n" + out + "\nStdErr\n" + err + "\n"
         raise Exception()

      # Clean-up generated file
      os.remove(maskFileName)

      # If debugging is needed
      if not debugLogStr == '':
         scriptStr = str(g_script_file_name) + " Revision: " + str(g_svn_revision)
         handleException.writeLogFile(logFileName, scriptStr, "", debugLogStr)

      return StringRawData(out, 'application/json')

   except:
      # Log the exception. As much output as possible to help in debugging
      scriptStr = str(g_script_file_name) + " Revision: " + str(g_svn_revision)
      handleException.writeLogFile(logFileName, scriptStr, errorStr, debugLogStr)
      errorStr += "%s\n" % logFileName
      f = open(logFileName, 'r')
      errorStr += f.read()
      sys.exit(errorStr)
