################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

import os
import re
import sys
import traceback
g_svn_revision = str('$Rev: 7856 $').replace('$', '').strip(' \t\n\r')
g_svn_date = re.sub('\) \$', ' ' ,re.sub('^.*\(', 'Date: ', '$Date: 2017-01-26 17:37:46 +0100 (Thu, 26 Jan 2017) $')).strip(' \t\n\r')
g_svn_author = str('$Author: jb76278 $').replace('$', '').strip(' \t\n\r')
g_script_file_name = 'TimeSeriesByDate.py'

import uuid
import urllib
import subprocess

from java.io import File
from java.lang import Boolean
from java.util import Date
from java.text import SimpleDateFormat

from geoserver.wps import process
from org.geoserver.wps.process import StringRawData
from org.geoserver.wps.process import StreamRawData
from org.geoserver.wps.process import RawData
from org.apache.commons.io import IOUtils
from org.geotools.feature import FeatureCollection

import handleROI
import handleException

gSOURCE_DICT          = { 'MODIS_500m' : '/MODIS500/', 'MODIS_500m' : '/MODIS500/' }
gINDICATOR_DICT       = { 'NDVI' : 'NDVI', 'EVI' : 'EVI', 'TEST' : 'TEST' }
gMASK_DICT            = { 'NONE' : 'NONE', 'ESA_CCI_2010' : 'ESA_CCI_2010', 'TEST' : 'TEST' }
gREFERENCE_TYPE_SET   = ( 'NONE', 'ABS', 'LTA' )
gINTEGRATE_TYPE_SET   = ( 'NONE', 'DIF' )
gOUTPUT_MIMETYPES     = ('application/json', 'application/csv')

gLTA_START_DATE = "2001-01-01"
gLTA_END_DATE   = "2015-12-31"

def dateToIsoFormat(date):
   fmt = SimpleDateFormat("yyyy-MM-dd")
   return fmt.format(date)

@process (
   title = 'Compute vegetation index time series for ROI',
   description = 'Compute vegetation index time series for ROI',
   inputs = {
      'source'         : (str, 'Data source identifier', { 'domain' : gSOURCE_DICT.keys() } ),
      'ROI'            : (FeatureCollection, 'Region of interest (Polygon/Point WGS84)' ),
      'indicator'      : (str, 'Observered variable to use', { 'domain' : gINDICATOR_DICT.keys() } ),
      'periodStart'    : (Date, 'Start date of time series period (ISO 8601)'),
      'periodEnd'      : (Date, 'End date of time series period (ISO 8601)'),
      'maskID'         : (str, 'Mask to be applied', { 'min' : 0, 'domain' : gMASK_DICT.keys() } ),
      'referenceStart' : (Date, 'start date of reference period (ISO 8601)', { 'min': 0 } ),
      'referenceType'  : (str, 'Type of reference data', { 'min': 0, 'domain' : gREFERENCE_TYPE_SET } ),
      'integrateType'  : (str, 'Compute integrateType in vegetation index values', { 'min': 0, 'domain' : gINTEGRATE_TYPE_SET } ),
      'windowLength'   : (int, 'integrate window in days', { 'min': 0, } ),
      'label'          : (str, 'Label echoed in output', { 'min': 0 } ),
      'outputMimeType' : (str, 'output mimetype', { 'min': 0 } )
   },
   outputs = {
      'result': (RawData, 'result of computation', { 'mimeTypes' : 'application/json,application/csv', 'chosenMimeType': 'outputMimeType' }),
   }
)

def run(source, ROI, indicator, periodStart, periodEnd, maskID='NONE', referenceStart=None, referenceType='NONE', integrateType='NONE', windowLength=0, label=None, outputMimeType='application/json'):

   global gMASK_DICT, gSOURCE_DICT, gINDICATOR_DICT, gREFERENCE_TYPE_SET, gINTEGRATE_TYPE_SET, gLTA_START_DATE, gLTA_END_DATE

   PROCESS_SCRIPTS_PATH = os.getenv('PROCESS_SCRIPTS_PATH')
   MASK_TEMPLATES_PATH  = os.getenv('MASK_TEMPLATES_PATH')
   DATA_CUBES_PATH      = os.getenv('DATA_CUBES_PATH')

   # errorStr is for end-user (to fix input they provide), debugLogStr is for developers (to debug code)
   errorStr = ""
   debugLogStr = ""

   # Try if it is possible to open a log file for developers
   try:
      logFileName = os.path.join('/tmp',g_script_file_name + "_" + str(uuid.uuid4()) + ".log")
      logFile = open(logFileName, 'w')
      logFile.close()
      os.remove(logFileName)
   except:
      errorStr += "\nUnable to open log file (" + str(logFileName) + ") for writing."
      sys.exit(errorStr)

   try:
      # Determine source extension
      if source in gSOURCE_DICT:
         sourceExt = gSOURCE_DICT[source]
      else:
         sys.exit("\nError: source \'" + source + "\' not valid (" + str(gSOURCE_DICT.keys()) + ").\n")

      # Determine indicator extension
      if indicator in gINDICATOR_DICT:
         indicatorExt = gINDICATOR_DICT[indicator]
      else:
         sys.exit("\nError: indicator \'" + indicator + "\' not a valid value. " + str(gINDICATOR_DICT.keys()) + ".\n")

      # Determine maskID extension
      if maskID in gMASK_DICT:
         maskIdExt = gMASK_DICT[maskID]
      else:
         sys.exit("\nError: maskID \'" + maskID + "\' not a valid value. " + str(gMASK_DICT.keys()) + ".\n")

      if not integrateType in gINTEGRATE_TYPE_SET:
         sys.exit("\nError: integrateType \'" + integrateType + "\' not a valid value. " + str(gINTEGRATE_TYPE_SET) + ".\n")

      # Determine input filenames
      baseFileName = DATA_CUBES_PATH + sourceExt + 'base-' + indicatorExt + '.hdf5'
      if integrateType <> 'NONE' :
         # For integrateType time series we need to recompute the base statistics.
         ltaFileName  = "''"
      else:
         ltaFileName  = DATA_CUBES_PATH + sourceExt + 'lta-' + indicatorExt + '.hdf5'
      maskTemplate = MASK_TEMPLATES_PATH + sourceExt + maskIdExt + ".tif"

      # Get the mask from the ROI and store it in a geotiff
      maskFileName = "/tmp/" + "mask-" + str(uuid.uuid4()) + ".tif"

      # Get region of interest 
      handleROI.main(ROI, maskTemplate, maskFileName)

      # Construct command with the appropriate flags and parameters to get the time series
      commandStr = "python " + PROCESS_SCRIPTS_PATH + "processTimeSeriesByDate.py"

      # Reference Type flag
      if referenceType in gREFERENCE_TYPE_SET:
         commandStr += " --reftype=" + str(referenceType)
      else:
         errorStr += "\nError: Reference Type value \'" + referenceType + "\' not a valid value. " + str(gREFERENCE_TYPE_SET) + ".\n"

      # Reference Start Date flag
      if referenceStart <> None:
         commandStr += " --refdate=" + dateToIsoFormat(referenceStart)

      # Difference flag
      if integrateType <> 'NONE':
         # Cannot use precomputed base statistics
         ltaFileName = "''"
         commandStr += " --integrate=" + integrateType
      commandStr += " --window="    + str(windowLength)
      commandStr += " --lta-start=" + gLTA_START_DATE
      commandStr += " --lta-end="   + gLTA_END_DATE

      # Label to be echoed in output
      commandStr+= " --label=\"" + str(label) + "\""

      # Provide additional service arguments for echo in returned json
      commandStr+= " --source="    + source
      commandStr+= " --indicator=" + indicator
      commandStr+= " --maskID="    + maskID

      # Mime Type flag
      if outputMimeType in gOUTPUT_MIMETYPES:
         commandStr += " --mimetype=" + outputMimeType
      else:
         errorStr += "\nError: outputMimeType value \'" + outputMimeType + "\' not a valid value. " + str(gOUTPUT_MIMETYPES) + ".\n"

      # Base file
      commandStr += " " + baseFileName

      # LTA file
      commandStr += " " + ltaFileName

      # ROI-mask geotiff file
      commandStr += " " + maskFileName

      # Period Start Date
      if not periodStart == None:
         commandStr += " " + dateToIsoFormat(periodStart)
      else:
         errorStr += "\nError: No valid period start date given (format YYYY-MM-DD)"

      # Period End Date
      if not periodEnd == None:
         commandStr += " " + dateToIsoFormat(periodEnd)
      else:
         errorStr += "\nError: No valid period end date given (format YYYY-MM-DD)"

      # Now execute the command string
      p = subprocess.Popen(commandStr, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
      out, err = p.communicate()
      if not err == '':
         debugLogStr += "Command:\n" + commandStr + "\n"
         debugLogStr += "StdOut:\n" + out + "\nStdErr\n" + err + "\n"
         raise Exception()

      # Remove temp files
      os.remove(maskFileName)

      # If debugging is needed
      if not debugLogStr == '':
         scriptStr = str(g_script_file_name) + " Revision: " + str(g_svn_revision)
         handleException.writeLogFile(logFileName, scriptStr, "", debugLogStr)

      extension='json'
      if outputMimeType == 'application/csv': extension = 'csv'
      return StreamRawData(outputMimeType, IOUtils.toInputStream(out), extension)

   except:
      # Log the exception. As much output as possible to help in debugging
      scriptStr = str(g_script_file_name) + " Revision: " + str(g_svn_revision)
      handleException.writeLogFile(logFileName, scriptStr, errorStr, debugLogStr)
      errorStr += "%s\n" % logFileName
      f = open(logFileName, 'r')
      errorStr += f.read()
      sys.exit(errorStr)
