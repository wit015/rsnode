################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

import os
import re
import sys
import traceback
g_svn_revision = str('$Rev: 7838 $').replace('$', '').strip(' \t\n\r')
g_svn_date = re.sub('\) \$', ' ' ,re.sub('^.*\(', 'Date: ', '$Date: 2017-01-23 12:39:07 +0100 (Mon, 23 Jan 2017) $')).strip(' \t\n\r')
g_svn_author = str('$Author: jb76278 $').replace('$', '').strip(' \t\n\r')
g_script_file_name = 'TimeSeriesByDate.py'

import uuid
import urllib
import subprocess

from geoserver.wps import process
from org.geoserver.wps.process import StringRawData

import handleException 

gSOURCE_DICT          = { 'MODIS_500m' : '/MODIS500/', 'MODIS_500m' : '/MODIS500/' }
gINDICATOR_DICT       = { 'NDVI' : 'NDVI', 'EVI' : 'EVI', 'NDVI_LTA' : 'NDVI', 'EVI_LTA' : 'EVI', 
                          'TEST' : 'TEST', 'TEST_LTA' : 'TEST' }

@process (
   title = 'Return available dates for product',
   description = 'Return available dates for product',
   inputs = {
      'source'         : (str, 'Data source identifier', { 'domain' : gSOURCE_DICT.keys() } ),
      'indicator'      : (str, 'Observered variable to use', { 'domain' : gINDICATOR_DICT.keys() } )
   },
   outputs = {
      'result': (StringRawData, 'JSON format dates', { 'mimeTypes' : 'application/json' })
   }
)

def run(source, indicator):

   global gSOURCE_DICT, gINDICATOR_DICT

   PROCESS_SCRIPTS_PATH = os.getenv('PROCESS_SCRIPTS_PATH')
   DATA_CUBES_PATH      = os.getenv('DATA_CUBES_PATH')

   # errorStr is for end-user (to fix input they provide), debugLogStr is for developers (to debug code)
   errorStr = ""
   debugLogStr = ""

   # Try if it is possible to open a log file for developers
   try:
      logFileName = os.path.join('/tmp',g_script_file_name + "_" + str(uuid.uuid4()) + ".log")
      logFile = open(logFileName, 'w')
      logFile.close()
      os.remove(logFileName)
   except:
      errorStr += "\nUnable to open log file (" + str(logFileName) + ") for writing."
      sys.exit(errorStr)

   try:
      # Determine source extension
      if source in gSOURCE_DICT:
         sourceExt = gSOURCE_DICT[source]
      else:
         sys.exit("\nError: source \'" + source + "\' not valid (" + str(gSOURCE_DICT.keys()) + ").\n")

      # Determine indicator extension
      if indicator in gINDICATOR_DICT:
         indicatorExt = gINDICATOR_DICT[indicator]
      else:
         sys.exit("\nError: indicator \'" + indicator + "\' not a valid option. " + str(gINDICATOR_DICT.keys()) + ".\n")

      # Determine input filenames
      if indicator.endswith("LTA"):
         cubeFileName = DATA_CUBES_PATH + sourceExt + 'lta-' + indicatorExt + '.hdf5'
      else:
         cubeFileName = DATA_CUBES_PATH + sourceExt + 'base-' + indicatorExt + '.hdf5'

      # Script in CPython to process request
      processScript = PROCESS_SCRIPTS_PATH + 'processGetDates.py'

      argv = [ processScript,
               cubeFileName,
               source,
               indicator
             ]

      p = subprocess.Popen(argv, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
      out, err = p.communicate()

      if not err == '':
         debugLogStr += "Command:\n" + str(argv) + "\n"
         debugLogStr += "StdOut:\n" + out + "\nStdErr\n" + err + "\n"
         raise Exception()

      # If debugging is needed
      if not debugLogStr == '':
         scriptStr = str(g_script_file_name) + " Revision: " + str(g_svn_revision)
         handleException.writeLogFile(logFileName, scriptStr, "", debugLogStr)

      return StringRawData(out, 'application/json')

   except:
      # Log the exception. As much output as possible to help in debugging
      scriptStr = str(g_script_file_name) + " Revision: " + str(g_svn_revision)
      handleException.writeLogFile(logFileName, scriptStr, errorStr, debugLogStr)
      errorStr += "%s\n" % logFileName
      f = open(logFileName, 'r')
      errorStr += f.read()
      sys.exit(errorStr)
