################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

import os
import re
import sys
import traceback
g_svn_revision = str('$Rev: 7856 $').replace('$', '').strip(' \t\n\r')
g_svn_date = re.sub('\) \$', ' ' ,re.sub('^.*\(', 'Date: ', '$Date: 2017-01-26 17:37:46 +0100 (Thu, 26 Jan 2017) $')).strip(' \t\n\r')
g_svn_author = str('$Author: jb76278 $').replace('$', '').strip(' \t\n\r')
g_script_file_name = 'PeriodAggregateMap.py'

import uuid
import urllib
import subprocess

from java.io import File
from java.util import Date
from java.lang import Boolean

from geoserver.wps import process
from org.geoserver.wps.process import StringRawData
from org.geotools.coverage.grid import GridCoverage2D
from org.geotools.gce.geotiff import GeoTiffReader
from org.geotools.parameter import Parameter
from org.geotools.coverage.grid.io import AbstractGridFormat
from org.geotools.feature import FeatureCollection

import handleROI
import handleException

# For 200 x 200 pixel image at 4-bytes per pixel + some overhead
gFILE_SIZE_TILE_LIMIT = 165000

# We have only one source, but WPS interface needs at least two in a set ...
gSOURCE_DICT         = { 'MODIS_500m' : '/MODIS500/', 'MODIS_500m' : '/MODIS500/' }
gINDICATOR_DICT      = { 'NDVI' : 'NDVI', 'EVI' : 'EVI', 'TEST' : 'TEST',
                         'NDVI_LTA' : 'NDVI', 'EVI_LTA' : 'EVI', 'TEST_LTA' : 'TEST' }
gMASK_DICT           = { 'NONE' : 'NONE', 'ESA_CCI_2010' : 'ESA_CCI_2010', 'TEST' : 'TEST' }
gAGGREGATE_TYPE_SET  = ( 'AVG', 'MIN', 'MAX')
gREFERENCE_TYPE_SET  = ( 'NONE', 'ABS', 'LTA')
gINTEGRATE_TYPE_SET  = ( 'NONE', 'DIF' )

gLTA_START_DATE = "2001-01-01"
gLTA_END_DATE   = "2015-12-31"

#########################################################
# Definition of the WPS interface (input/output)
#########################################################
@process (
    title = 'compute map with aggregated vegetation index data',
    description = 'Compute map by aggregating vegetation indicator for specified date/doy interval',
    inputs = {
        'source'         : (str, 'Data source identifier', { 'domain' : gSOURCE_DICT.keys() } ),
        'ROI'            : (FeatureCollection, 'Region of interest (Polygon/Point WGS84)' ),
        'indicator'      : (str, 'Observered variable to use', { 'domain' : gINDICATOR_DICT.keys() } ),
        'aggregateType'  : (str, 'Type of aggregation', { 'domain' : gAGGREGATE_TYPE_SET } ),
        'periodStart'    : (Date, 'Start date of aggregation period (ISO 8601)'),
        'periodEnd'      : (Date, 'End date of aggregation period (ISO 8601)'),
        'maskID'         : (str, 'Mask to be applied', { 'min' : 0, 'domain' : gMASK_DICT.keys() } ),
        'referenceStart' : (Date, 'Start date of reference period (ISO 8601)', { 'min': 0 } ),
        'referenceType'  : (str, 'Type of reference data', { 'min': 0, 'domain' : gREFERENCE_TYPE_SET } ),
        'integrateType'  : (str, 'type of temporal integration', { 'min' : 0, 'domain' : gINTEGRATE_TYPE_SET } ),
        'windowLength'   : (int, 'number of days for temporal integration', { 'min' : 0 } ),
        'label'          : (str, 'Label echoed in output', { 'min': 0 } )
    },
    outputs = {
        'map': (GridCoverage2D, 'Geotiff with http header', {'mimeTypes':'image/tiff'} )
    }
)

#########################################################
# Handling of a WPS request
#########################################################
def run(source, ROI, indicator, aggregateType, periodStart, periodEnd, maskID='NONE', referenceStart=None, referenceType='NONE', integrateType='NONE', windowLength=0, label=''):

    global gFILE_SIZE_TILE_LIMIT, gSOURCE_DICT, gINDICATOR_DICT, gMASK_DICT, gREFERENCE_TYPE_SET, gAGGREGATE_TYPE_SET, gLTA_START_DATE, gLTA_END_DATE

    def toYYMMDD(date):
       return "%04d-%02d-%02d" % (date.year+1900, date.month+1, date.date)

    PROCESS_SCRIPTS_PATH = os.getenv('PROCESS_SCRIPTS_PATH')
    MASK_TEMPLATES_PATH  = os.getenv('MASK_TEMPLATES_PATH')
    DATA_CUBES_PATH      = os.getenv('DATA_CUBES_PATH')

    # errorStr is for end-user (to fix input they provide), debugLogStr is for developers (to debug code)
    errorStr = ""
    debugLogStr = ""

    # Try if it is possible to open a debug log file for developers
    try:
        logFileName = os.path.join('/tmp',g_script_file_name + "_" + str(uuid.uuid4()) + ".log")
        logFile = open(logFileName, 'w')
        logFile.close()
        os.remove(logFileName)
    except:
        # Cannot inform developers. Exit and tell users what the problem is.
        errorStr += "\nUnable to open log file (" + str(logFileName) + ") for writing."
        sys.exit(errorStr)

    try:
        # Determine source extension
        if source in gSOURCE_DICT:
           sourceExt = gSOURCE_DICT[source]
        else:
           sys.exit("\nError: source \'" + source + "\' not valid (" + str(gSOURCE_DICT.keys()) + ").\n")
  
        # Determine indicator extension
        if indicator in gINDICATOR_DICT:
           indicatorExt = gINDICATOR_DICT[indicator]
        else:
           sys.exit("\nError: indicator \'" + indicator + "\' not a valid option. " + str(gINDICATOR_DICT.keys()) + ".\n")
  
        # Determine maskID extension
        if maskID in gMASK_DICT:
           maskIdExt = gMASK_DICT[maskID]
        else:
           sys.exit("\nError: maskID \'" + maskID + "\' not a valid option. " + str(gMASK_DICT.keys()) + ".\n")
  
        # Determine input filenames
        baseFileName = DATA_CUBES_PATH + sourceExt + 'base-' + indicatorExt + '.hdf5'
        if periodStart == periodEnd:
           ltaFileName  = DATA_CUBES_PATH + sourceExt + 'lta-' + indicatorExt + '.hdf5'
        else:
           ltaFileName  = "''"
        maskTemplate = MASK_TEMPLATES_PATH + sourceExt + maskIdExt + ".tif"

        # Create writer to write resulting mask
        maskFileName = "/tmp/" + "mask-" + str(uuid.uuid4()) + ".tif"

        ###################
        # Get the mask from the ROI and store it in a geotiff
        ###################
        handleROI.main(ROI, maskTemplate, maskFileName)
 
        commandStr = "python " + PROCESS_SCRIPTS_PATH + "processPeriodAggregateMap.py"

        # Aggregate flag
        if aggregateType in gAGGREGATE_TYPE_SET:
            commandStr += " --aggregate=" + str(aggregateType)
        else:
            errorStr += "\nError: aggregateType value \'" + aggregateType + "\' not a valid value. " + str(gAGGREGATE_TYPE_SET) + ".\n"

        # Integrator Type flag
        if integrateType in gINTEGRATE_TYPE_SET:
           commandStr += " --integrate=" + str(integrateType)
        else:
            errorStr += "\nError: integrateType value \'" + integrateType + "\' not a valid value. " + str(gINTEGRATE_TYPE_SET) + ".\n"

        # Reference Type flag
        if referenceType in gREFERENCE_TYPE_SET:
            commandStr += " --reftype=" + str(referenceType)
        else:
            errorStr += "\nError: referenceType value \'" + referenceType + "\' not a valid value. " + str(gREFERENCE_TYPE_SET) + ".\n"

        # Reference Start Date flag
        if not referenceStart == None:
            commandStr += " --refdate=" + toYYMMDD(referenceStart)

        # Check whether referenceStart is set when referenceType <> NONE
        if referenceType <> 'NONE' and referenceStart == None:
           sys.exit("\nError: referenceType <> NONE but referenceStart is not set.")

        # Label to be echoed in output
        if not label == None:
            commandStr+= " --label=\"" + label + "\""

        if indicator.endswith('_LTA'):
            commandStr+= " --lta"

        
        commandStr+= " --window="    + str(windowLength)
        commandStr+= " --lta-start=" + gLTA_START_DATE
        commandStr+= " --lta-end="   + gLTA_END_DATE
        commandStr+= " --source="    + source
        commandStr+= " --indicator=" + indicator
        commandStr+= " --maskID="    + maskID

        # Output file
        tifFile = "/tmp/" + 'result-' + str(uuid.uuid4()) + ".tif"
        commandStr += " " + str(tifFile)

        # Source file
        commandStr += " " + baseFileName

        # Base statistics file
        commandStr += " " + ltaFileName

        # ROI-mask geotiff file
        commandStr += " " + maskFileName

        # Period Start Date
        if not periodStart == None:
            commandStr += " " + str(periodStart.year + 1900) + "-" + str(periodStart.month + 1).zfill(2) + "-" + str(periodStart.date).zfill(2)
        else:
            errorStr += "\nError: No valid aggregate start date given (format YYYY-MM-DD)"
 
        # Period End Date
        if not periodEnd == None:
            commandStr += " " + str(periodEnd.year + 1900) + "-" + str(periodEnd.month + 1).zfill(2) + "-" + str(periodEnd.date).zfill(2)
        else:
            errorStr += "\nError: No valid aggregate end date given (format YYYY-MM-DD)"

        # Now execute the command string
        p = subprocess.Popen(commandStr, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = p.communicate()

        if not err == '':
            debugLogStr += "Command:\n" + commandStr + "\n"
            debugLogStr += "StdOut:\n" + out + "\nStdErr\n" + err + "\n"
            raise Exception()

        # Get the tif file
        reader = GeoTiffReader(tifFile)

        hints = []
        # Coverage2D has default 512x512 size resulting in large TIF files
        # For 'small' sized results set tile size to 1x1, This results in
        # larger java memory usage.
        if os.path.getsize(tifFile) < gFILE_SIZE_TILE_LIMIT:
           p = Parameter(AbstractGridFormat.SUGGESTED_TILE_SIZE)
           # 1,1 does not work for single pixel TIF file.
           p.setValue("2,2")
           hints.append(p)

        result =  reader.read(hints)

        # Clean-up generated file
        os.remove(maskFileName)
        os.remove(tifFile)

        # If debugging is needed
        if not debugLogStr == '':
           scriptStr = str(g_script_file_name) + " Revision: " + str(g_svn_revision)
           handleException.writeLogFile(logFileName, scriptStr, "", debugLogStr)

        return result

    except:

       # Log the exception. As much output as possible to help in debugging
       scriptStr = str(g_script_file_name) + " Revision: " + str(g_svn_revision)
       handleException.writeLogFile(logFileName, scriptStr, errorStr, debugLogStr)
       errorStr += "%s\n" % logFileName
       f = open(logFileName, 'r')
       errorStr += f.read()
       sys.exit(errorStr)
