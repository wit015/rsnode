################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

# Applicable Product Description: CS-ADSNL-RP-0007, Issue 1.0 (draft) dd. 24 Jan 2016

import os
import re
import sys
import traceback
g_svn_revision = str('$Rev: 7856 $').replace('$', '').strip(' \t\n\r')
g_svn_date = re.sub('\) \$', ' ' ,re.sub('^.*\(', 'Date: ', '$Date: 2017-01-26 17:37:46 +0100 (Thu, 26 Jan 2017) $')).strip(' \t\n\r')
g_svn_author = str('$Author: jb76278 $').replace('$', '').strip(' \t\n\r')
g_script_file_name = 'TimeSeriesByYear.py'

import subprocess

from java.io import File
from java.util import Date
from java.lang import Boolean

from geoserver.wps import process
from org.geoserver.wps.process import StringRawData
from org.geoserver.wps.process import StreamRawData
from org.geoserver.wps.process import RawData
from org.apache.commons.io import IOUtils
from org.geotools.feature import FeatureCollection

import uuid
import urllib

import handleROI
import handleException

gSOURCE_DICT          = { 'MODIS_500m' : '/MODIS500/', 'MODIS_500m' : '/MODIS500/' }
gINDICATOR_DICT       = { 'NDVI' : 'NDVI', 'EVI' : 'EVI', 'TEST' : 'TEST' }
gMASK_DICT            = { 'NONE' : 'NONE', 'ESA_CCI_2010' : 'ESA_CCI_2010', 'TEST' : 'TEST' }
gREFERENCE_TYPE_SET   = ( 'NONE', 'LTA' )
gAGGREGATE_TYPE_SET   = ( 'AVG', 'MIN', 'MAX' )
gINTEGRATE_TYPE_SET   = ( 'NONE', 'DIF' )
gOUTPUT_MIMETYPES     = ('application/json', 'application/csv')

gLTA_START_DATE     = "2001-01-01"
gLTA_END_DATE       = "2015-12-31"


@process (
   title = 'Compute aggregated precipitation time series for ROI and DOY period',
   description = 'Compute aggregated precipitation time series for ROI and DOY period',
   inputs = {
      'source'         : (str, 'Data source identifier', { 'domain' : gSOURCE_DICT.keys() } ),
      'ROI'            : (FeatureCollection, 'Region of interest (Polygon/Point WGS84)' ),
      'maskID'         : (str, 'Mask to be applied (default NONE)', { 'min' : 0, 'domain' : gMASK_DICT.keys() } ),
      'indicator'      : (str, 'observered variable to use', { 'domain' : gINDICATOR_DICT.keys() } ),
      'periodStart'    : (Date, 'start date of aggregation period (ISO 8601) (converted to DOY)'),
      'periodEnd'      : (Date, 'end date of aggregation period (ISO 8601) (converted to DOY)'),
      'referenceType'  : (str, 'type of reference data (default NONE)', { 'min': 0, 'domain' : gREFERENCE_TYPE_SET } ),
      'aggregateType'  : (str, 'type of aggregation', { 'domain' : gAGGREGATE_TYPE_SET } ),
      'integrateType'  : (str, 'type of integration', { 'min': 0, 'domain' : gINTEGRATE_TYPE_SET } ),
      'windowLength'   : (int, 'number of days for temporal integration', { 'min' : 0 } ),
      'label'          : (str, 'Label echoed in output', { 'min': 0 }),
      'outputMimeType' : (str, 'output mimetype', { 'min': 0 } )
   },
   outputs = {
      'result': (RawData, 'result of computation', { 'mimeTypes' : 'application/json,application/csv', 'chosenMimeType': 'outputMimeType' }),
   }
)

def run(source=None, ROI=None, maskID='NONE', indicator=None, periodStart=None, periodEnd=None, referenceType='NONE', aggregateType=None, integrateType='NONE', windowLength=0, label='no label', outputMimeType = 'application/json'):

   global gSOURCE_DICT, gINDICATOR_DICT, gMASK_DICT, gREFERENCE_TYPE_SET, gAGGREGATE_TYPE_SET, gINTEGRATE_TYPE, gOUTPUT_MIMETYPES, gLTA_START_DATE, gLTA_END_DATE


   PROCESS_SCRIPTS_PATH = os.getenv('PROCESS_SCRIPTS_PATH')
   MASK_TEMPLATES_PATH  = os.getenv('MASK_TEMPLATES_PATH')
   DATA_CUBES_PATH      = os.getenv('DATA_CUBES_PATH')

   def toYYMMDD(date):
      return "%04d-%02d-%02d" % (date.year+1900, date.month+1, date.date)

   # errorStr is for end-user (to fix input they provide), debugLogStr is for developers (to debug code)
   errorStr = ""
   debugLogStr = ""

   # Try if it is possible to open a log file for developers
   try:
      logFileName = os.path.join('/tmp',g_script_file_name + "_" + str(uuid.uuid4()) + ".log")
      logFile = open(logFileName, 'w')
      logFile.close()
      os.remove(logFileName)
   except:
      errorStr += "\nUnable to open log file (" + str(logFileName) + ") for writing."
      sys.exit(errorStr)

   try:
      # Determine source extension
      if source in gSOURCE_DICT:
         sourceExt = gSOURCE_DICT[source]
      else:
         sys.exit("\nError: source \'" + source + "\' not a valid value (" + str(gSOURCE_DICT.keys()) + ").\n")

      # Determine indicator extension
      if indicator in gINDICATOR_DICT:
         indicatorExt = gINDICATOR_DICT[indicator]
      else:
         sys.exit("\nError: indicator \'" + indicator + "\' not a valid value. " + str(gINDICATOR_DICT.keys()) + ".\n")

      # Determine maskID extension
      if maskID in gMASK_DICT:
         maskIdExt = gMASK_DICT[maskID]
      else:
         sys.exit("\nError: maskID \'" + maskID + "\' not a valid value. " + str(gMASK_DICT.keys()) + ".\n")

      # Determine validity aggregateType
      if not aggregateType in gAGGREGATE_TYPE_SET:
         sys.exit("\nError: aggregateType \'" + aggregateType + "\' not a valid value. " + str(gAGGREGATE_TYPE_SET) + ".\n")

      # Determine validity integrateType
      if not integrateType in gINTEGRATE_TYPE_SET:
         sys.exit("\nError: integrateType \'" + integrateType + "\' not a valid value. " + str(gINTEGRATE_TYPE_SET) + ".\n")

      # Determine input filenames
      baseFileName = DATA_CUBES_PATH + sourceExt + 'base-' + indicatorExt + '.hdf5'
      ltaFileName  = ''
      maskTemplate = MASK_TEMPLATES_PATH + sourceExt + maskIdExt + ".tif"

      if outputMimeType not in gOUTPUT_MIMETYPES:
         errorStr += '\nIllegal mimetype="' + outputMimeType + '"'

      # Create writer to write resulting mask
      maskFileName = "/tmp/" + "mask-" + str(uuid.uuid4()) + ".tif"

      ###################
      # Get region of interest
      ###################
      handleROI.main(ROI, maskTemplate, maskFileName)

      # Create Date strings
      periodStart = toYYMMDD(periodStart)
      periodEnd   = toYYMMDD(periodEnd)

      # Script in CPython to process request
      processScript = PROCESS_SCRIPTS_PATH + 'processTimeSeriesByYear.py'

      argv = [ processScript,
               baseFileName,
               ltaFileName,
               maskFileName,
               periodStart,
               periodEnd,
               '--mimetype='  + outputMimeType,
               '--reftype='   + referenceType,
               '--aggregate=' + aggregateType,
               '--integrate=' + integrateType,
               '--window='    + str(windowLength),
               '--lta-start=' + gLTA_START_DATE,
               '--lta-end='   + gLTA_END_DATE,
               '--label='     + label,
               '--source='    + source,
               '--indicator=' + indicator,
               '--maskID='    + maskID
             ]

      p = subprocess.Popen(argv, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
      out, err = p.communicate()
      if not err == '':
         debugLogStr += "Command:\n" + str(argv) + "\n"
         debugLogStr += "StdOut:\n" + out + "\nStdErr\n" + err + "\n"
         raise Exception()

      # Clean-up generated mask
      os.remove(maskFileName)

      # If debugging is needed
      if not debugLogStr == '':
         scriptStr = str(g_script_file_name) + " Revision: " + str(g_svn_revision)
         handleException.writeLogFile(logFileName, scriptStr, "", debugLogStr)

      extension='json'
      if outputMimeType == 'application/csv': extension = 'csv'
      return StreamRawData(outputMimeType, IOUtils.toInputStream(out), extension)

   except:
      # Log the exception. As much output as possible to help in debugging
      scriptStr = str(g_script_file_name) + " Revision: " + str(g_svn_revision)
      handleException.writeLogFile(logFileName, scriptStr, errorStr, debugLogStr)
      errorStr += "%s\n" % logFileName
      f = open(logFileName, 'r')
      errorStr += f.read()
      sys.exit(errorStr)
