################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################
# Common module to save exception information to a file.
#

import sys
import traceback

def writeLogFile(logFileName, scriptStr, errorStr, debugLogStr):
   logFile = open(logFileName, 'w')
   logFile.write('-'*60 + "\n")
   logFile.write("Script    : " + scriptStr + "\n")
   logFile.write('-'*60 + "\n")
   logFile.write("Error     :\n" + errorStr + "\n")
   logFile.write('-'*60 + "\n")
   logFile.write("Debug     :\n" + debugLogStr + "\n")
   logFile.write('-'*60 + "\n")
   logFile.write("Exception :\n" + str(sys.exc_info) + "\n")
   exc_type, exc_value, exc_traceback = sys.exc_info()
   logFile.write("*** print_exception:")
   traceback.print_exception(exc_type, exc_value, exc_traceback, limit=9, file=logFile)
   logFile.write('-'*60 + "\n")
   logFile.flush()
   logFile.close()
