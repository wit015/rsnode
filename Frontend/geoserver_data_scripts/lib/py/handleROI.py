################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

import os
import sys
import uuid
import subprocess

from java.io import File
from java.util import Date
from java.lang import Boolean

from org.geotools.gce.geotiff import GeoTiffReader
from org.geotools.gce.geotiff import GeoTiffWriter
from org.geotools.process.raster import CropCoverage
from org.geotools.util import NullProgressListener
#from com.vividsolutions.jts.geom import Geometry
from org.locationtech.jts.geom import Geometry

def main(ROI, aMaskTemplate, aMaskFileName):

   PROCESS_SCRIPTS_PATH = os.getenv('PROCESS_SCRIPTS_PATH')

   errorStr = ''

   ROI = (ROI.toArray())[0].getDefaultGeometry()

   if ROI.getGeometryType() not in ["Point", "MultiPoint", "Polygon", "MultiPolygon"]:
      sys.exit("ERROR : unexpected geometry type '" + ROI.getGeometryType() + "'")

   # Write ROI to file 
   roiFileName = "/tmp/" + "roi-" + str(uuid.uuid4()) + ".txt"
   f = open(roiFileName, 'w')
   f.write(str(ROI))
   f.close()

   # Generate mask based on ROI
   if ROI.getGeometryType() in ["Point", "MultiPoint"] :
      #############
      # Crop mask to selected (multi)points
      #############

      cmd = [PROCESS_SCRIPTS_PATH + "genPixelMask.py", aMaskTemplate, aMaskFileName, roiFileName]
      subprocess.call(cmd)
   else:
      #############
      # Crop mask to selected (multi)polygon
      #############

      # Generate boundary mask
      pointsMaskFileName = "/tmp/" + "pmask-" + str(uuid.uuid4()) + ".tif"
      cmd = [PROCESS_SCRIPTS_PATH + "genPixelMask.py", aMaskTemplate, pointsMaskFileName, roiFileName]
      subprocess.call(cmd)

      # Generate area mask
      cropMaskFileName = "/tmp/" + "cmask-" + str(uuid.uuid4()) + ".tif"
      fileHandle = File(cropMaskFileName)
      tifWriter  = GeoTiffWriter(fileHandle)
      reader = GeoTiffReader(aMaskTemplate)

      # Perform cropping
      cov = CropCoverage()
      tifWriter.write(cov.execute(reader.read([]), ROI, NullProgressListener()), None)

      # Use the nodata value of the crop mask. Otherwise merge will be incorrect
      cmask  = GeoTiffReader(cropMaskFileName)
      noDataValue = cmask.getMetadata().getNoData()

      # Merge boundary mask and area mask raster
      cmd = ["/usr/bin/gdal_merge.py", 
                "-n", str(noDataValue), 
                "-a_nodata", str(noDataValue),
                "-o", aMaskFileName, 
                pointsMaskFileName, 
                cropMaskFileName]
      subprocess.call(cmd)

      # Remove temp files
      os.remove(pointsMaskFileName)
      os.remove(cropMaskFileName)

   # Remove temp files
   os.remove(roiFileName)
