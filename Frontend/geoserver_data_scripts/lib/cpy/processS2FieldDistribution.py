#!/usr/bin/env python
#
################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2018, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

import json
import argparse
import datetime

import traceback

import fieldDB
import csutils
import csutils.dict2csv

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("user_id",      help="user ID")
    parser.add_argument("field_id",     help="field ID")
    parser.add_argument("--group_id",   help="group ID", default="DEFAULT", type=str)
    parser.add_argument("--datetime",   help="date time string YYYY-MM-DD[THH:MM:SS]", default=None, type=str)
    parser.add_argument("--format",     help="output format", default="application/json", type=str)
    parser.add_argument("--indicator",  help="indicator (NDVI, SCL)", default="NDVI", type=str)
    parser.add_argument("--nbins",      help="number of bins", default=0, type=int)
    parser.add_argument("--mask_list",  help="SCL mask values", nargs="+", type=int, default=None)
    
    args = parser.parse_args()

    try: 

        dt = args.datetime
        if dt is not None:
            if 'T' in args.datetime:
                dt = datetime.datetime.strptime(args.datetime, "%Y-%m-%dT%H:%M:%S")
            else:
                dt = datetime.datetime.strptime(args.datetime, "%Y-%m-%d")
    
        f = fieldDB.Field(args.user_id, args.group_id, args.field_id)

        if args.indicator == 'NDVI':
            result = f.getNDVIDistribution(dt, args.nbins, args.mask_list)
        elif args.indicator == 'SCL':
            result = f.getSCLDistribution(dt)
        else:
            raise RuntimeError("ERROR : unexpected indicator %s" % args.indicactor)

        if   args.format == "application/json":
            print json.dumps(result)
        elif args.format == "application/csv":
            print csutils.dict2csv.dict2csv(result)
        else:
            raise RuntimeError("ERROR %s : unknown format %s" % (sys.argv[0], args.format))

    except RuntimeError as re:

        print str(re.args)

    except :

        print traceback.format_exc()
