#!/usr/bin/env python
#
################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2018, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

import json
import argparse
import datetime
import numpy

import traceback

import fieldDB
import csutils
import csutils.dict2csv

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("user_id",      help="user ID")
    parser.add_argument("field_id",     help="field ID")
    parser.add_argument("--group_id",   help="group ID", default="DEFAULT", type=str)
    parser.add_argument("--start_date", help="date time string YYYY-MM-DD[THH:MM:SS]", default="2000-01-01", type=str)
    parser.add_argument("--end_date",   help="date time string YYYY-MM-DD[THH:MM:SS]", default="2999-01-01", type=str)
    parser.add_argument("--lbp",        help="lower bound percentile", default=0.0, type=float)
    parser.add_argument("--ubp",        help="lower bound percentile", default=100.0, type=float)
    parser.add_argument("--ag",         help="aggregator",             default="AVG", type=str)
    parser.add_argument("--vpp",        help="lower bound valid pixels percentage", default=0.0, type=float)
    parser.add_argument("--format",     help="output format", default="application/json", type=str)
    parser.add_argument("--mask_list",  help="SCL mask values", nargs="+", type=int, default=None)

    args = parser.parse_args()

    try:

        if 'T' in args.start_date:
            start_date = datetime.datetime.strptime(args.start_date, "%Y-%m-%dT%H:%M:%S")
        else:
            start_date = datetime.datetime.strptime(args.start_date, "%Y-%m-%d")
        if 'T' in args.end_date:
            end_date = datetime.datetime.strptime(args.end_date, "%Y-%m-%dT%H:%M:%S")
        else:
            end_date = datetime.datetime.strptime(args.end_date, "%Y-%m-%d")

        lbp = args.lbp
        ubp = args.ubp
        ag  = args.ag
        vpp = args.vpp

        f = fieldDB.Field(args.user_id, args.group_id, args.field_id)

        result = f.getNDVITimeSeries(
                    start_date, 
                    end_date, 
                    lb_percentile=lbp, 
                    ub_percentile=ubp, 
                    lb_vpix_percentage=vpp, 
                    mask_list=args.mask_list,
                    ag=ag)

        if   args.format == "application/json":
            # Replace NaN by null as NaN is not a legal value
            values = result['timeseries']['value']
            result['timeseries']['value'] = map(lambda x : None if numpy.isnan(x) else x, values)
            print json.dumps(result)
        elif args.format == "application/csv":
            print csutils.dict2csv.dict2csv(result)
        else:
            raise RuntimeError("ERROR %s : unknown format %s" % (sys.argv[0], args.format))

    except RuntimeError as re:

        print str(re.args[0])

    except :

        print traceback.format_exc()
