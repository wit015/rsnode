#!/usr/bin/env python
#
################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2018, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

import json
import argparse
import datetime
import traceback

import fieldDB

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("user_id",      help="user ID")
    parser.add_argument("field_id",     help="field ID")
    parser.add_argument("--group_id",   help="group ID", default="DEFAULT", type=str)

    args = parser.parse_args()

    error = None
    result = None

    try:

        f = fieldDB.Field(args.user_id, args.group_id, args.field_id)
        result = f.delete()

    except RuntimeError as re:

        error = str(re.args[0])

    except :
        error = traceback.format_exc()

    print json.dumps( { "field_id" : args.field_id, "field_cnt" : result, "error" : error } )
