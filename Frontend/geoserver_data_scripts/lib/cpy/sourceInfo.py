################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################
# Purpose:
# This python script knows about MODIS and CHIRPS data(cubes). All other scripts
# should not have any dependancy on the MODIS or CHIRPS types of cubes. If they
# need it they need to ask this script for the needed info.
################################################################################
# Last commit:
# $Rev: 7845 $
# $Date: 2017-01-24 23:16:51 +0100 (Tue, 24 Jan 2017) $
# $Author: jb76278 $
################################################################################

import datetime

# Global array
global gSOURCE_TYPE_SET
gSOURCE_TYPE_SET = [ 'CHIRPS_5000m', 'MODIS_500m' ]

################################################################################
# Function returns either 'MODIS_500m', 'CHIRPS_5000m' or 'UNKNOWN' as source.
# This is based on the input argument with the full filename and path.
def getSourceFromFileName(fileNameAndPath):
   returnValue = 'UNKNOWN'
   if ("-PRECIP_" in fileNameAndPath) or ("chirps" in fileNameAndPath):
      returnValue = gSOURCE_TYPE_SET[0]
   elif ("500/" in fileNameAndPath) and (("-EVI." in fileNameAndPath) or ("-NDVI." in fileNameAndPath)):
      returnValue = gSOURCE_TYPE_SET[1]
   return returnValue

##################################################################################
# Function returns the end-date of the measurement that started on the 'date' argument.
# isLastInterval shall be set to True for very last interval in non-LTA time cube.
def endDate(source, date, isLastInterval=False):
   if   source.startswith('MODIS'):
      # MODIS cube has basically 8 day interval
      ndate =  date + datetime.timedelta(7)
      # The very last interval has 16 days
      if isLastInterval : 
         ndate =  date + datetime.timedelta(15)
      # The last interval in a year is shorter
      if ndate.year <> date.year:
         ndate = datetime.date(date.year, 12, 31)
      return ndate
   elif source.startswith('CHIRPS'):
      if date.day == 26:
         if date.month == 12:
            d = datetime.date(date.year, 12, 31)
         else:
            d = datetime.date(date.year, date.month+1, 1) - datetime.timedelta(1)
      else:
         d = date + datetime.timedelta(4)
      return d
   else:
      # Don't know source. Return same date.
      return date

##################################################################################
# Function returns the index (on the time-axis) in the Long Time Average data for
# the given argument date. (The argument date can be year specific, the output is
# generic day-of-year converted to index.)
def indexLTA(source, date):
   if source.startswith('MODIS'):
      return (date.timetuple().tm_yday - 1) / 8
   if source.startswith('CHIRPS'):
      return (date.month - 1) * 6 + min(5, (date.day - 1) / 5)
   return -1

##################################################################################
# Returns the number days for a measurement interval
def measurementInterval(source):
   if source.startswith('MODIS'):
      return 8
   if source.startswith('CHIRPS'):
      return 5
   return -1

##################################################################################
# Returns the number of time-axis measurements in the LTA data (one year)
def lengthLTA(source):
   if source.startswith('MODIS'):
      return 46
   if source.startswith('CHIRPS'):
      return 72
   return -1

##################################################################################
def periodLTA(source, period, offset=None):
   ltaStart  = indexLTA(source, period[0])
   ltaLength = lengthLTA(source)
   if (period[1]-period[0]).days < 365:
      ltaEnd = indexLTA(source, period[1])
   else:
      ltaEnd = ltaStart + ltaLength - 1

   if offset <> None:
      # The reference interval must contain the same number of intervals
      # as the selected period. When crossing between leap years we
      # cannot just add the periodEnd - periodStart (which may be 366 days)
      ltaDelta  = ltaEnd - ltaStart
      ltaStart  = indexLTA(source, offset)
      ltaEnd    = (ltaStart + ltaDelta) % ltaLength # Note that perhaps ltaEnd < ltaStart

   return (ltaStart, ltaEnd, ltaLength)
