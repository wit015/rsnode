##################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
##################################################################################
# Purpose:
# This python script publishes basic 'statistical' functions that operate on
# numpy arrays. These functions know nothing about the content of the arrays.
# Not what the values represent, not if axis are lat, long or date, they just
# see a multidimensional array and perform their mathematical function on it.
##################################################################################
# Last commit:
# $Rev: 7861 $
# $Date: 2017-01-27 12:50:14 +0100 (Fri, 27 Jan 2017) $
# $Author: jb76278 $
##################################################################################

import math
import datetime
import numpy as np

################################################
# Gather selections from a 3D data-cube
# Returns a 4D data-cube with the selected data
#
# cube     : input data 3D cube
# interval : tuple with time intervals definition
# ySlice   : selected y-axis indices
# xSlice   : selected x-axis indices
def gatherData(cube, interval, ySlice, xSlice):

   begin, start, end, nint, nipy = interval
   startIndices = getBeginIndices(begin, start, end, nint, nipy)

   # Number of years to process
   nyear = startIndices.shape[0]

   # Make cube
   data = np.ma.empty((nyear, nint, ySlice.stop-ySlice.start, xSlice.stop-xSlice.start))

   # Gather data from measurement intervals
   for yi in range(0, nyear):
      y = startIndices[yi]
      data[yi] = np.ma.array(cube[y:y+nint, ySlice, xSlice])

   return data

################################################
# Get the array of starting indices from the
# given constraints.
#
# begin : minimum value the selected index can get
# start :
# end   : maximum value the selected index can get
# nint  : number of indices in one year selection (so this defines the shape[1] value of the output)
# nipy  : number of indices per year
def getBeginIndices(begin, start, end, nint, nipy):

   # Minimize start index
   start = begin + (start - begin) % nipy

   # Compute number of years to process
   nyear = (end - start - nint + 1) / nipy + 1

   output = np.ma.empty((nyear), dtype=int)
   for yi in range(0, nyear):
      output[yi] = start + yi * nipy
   return output

################################################
# Computes base statistics for a multi-dim cube
# in the axis=0 direction
# Return a cube with same number of dimensions as
# the input, but with:
# returnCube[0] percentile 0 (minimum)
# returnCube[1] percentile 25
# returnCube[2] percentile 50
# returnCube[3] percentile 75
# returnCube[4] percentile 100 (maximum)
# returnCube[5] average
def computeBaseStats(cube):
   bs = np.empty((6,) + cube.shape[1:])

   # Do not use nanpercentile as it is very slow
   # NoDataValues will appear in the max/min percentile.
   bs[0:5] = np.percentile(cube, [0.0, 25.0, 50.0, 75.0, 100.0], axis=0)
   bs[5]   = np.nanmean(cube, axis=0)

   return bs

################################################
def computeBaseStatsInterval(cube, interval, ySlice, xSlice, mask, integrator, wLength, noDataValue):

   data = gatherData(cube, interval, ySlice, xSlice)

   data.mask = mask

   if noDataValue <> None:
      data   = np.ma.masked_where(data == noDataValue, data, copy=False)

   if integrator == 'DIF':
      data = np.pad(data, ((0,0), (1,0), (0,0), (0,0)), mode='edge')
      data = np.ma.array(np.diff(data, axis=1))

   if integrator <> 'NONE' and wLength > 1:
      window = np.ones(wLength)
      data = np.apply_along_axis(np.convolve, 1, data, window, mode='full')
      data = np.ma.array(data[:, 0:-wLength+1])

   return computeBaseStats(data)

################################################
def computeLTA(cube, interval, ySlice, xSlice, mask, integrator, wLength, aggregate='AVG', noDataValue=None):

   data = gatherData(cube, interval, ySlice, xSlice)

   data.mask = mask

   if noDataValue <> None:
      data   = np.ma.masked_where(data == noDataValue, data, copy=False)

   if integrator == 'DIF':
      data = np.pad(data, ((0,0), (1,0), (0,0), (0,0)), mode='edge')
      data = np.ma.array(np.diff(data, axis=1))

   if integrator <> 'NONE' and wLength > 1:
      window = np.ones(wLength)
      data = np.apply_along_axis(np.convolve, 1, data, window, mode='full')
      data = np.ma.array(data[:, 0:-wLength+1])

   if   aggregate == 'MIN':
      data = np.nanmin(data, axis=1)
   elif aggregate == 'MAX':
      data = np.nanmax(data, axis=1)
   elif aggregate == 'AVG':
      data = np.nanmean(data, axis=1)

   return np.nanmean(data, axis=0)

################################################
def computeYear(cube, interval, ySlice, xSlice, mask, refData, integrator, wLength, aggregate, noDataValue):

   data = gatherData(cube, interval, ySlice, xSlice)

   data.mask = mask

   if noDataValue <> None:
      data   = np.ma.masked_where(data == noDataValue, data, copy=False)

   if integrator == 'DIF':
      data = np.pad(data, ((0,0), (1,0), (0,0), (0,0)), mode='edge')
      data = np.ma.array(np.diff(data, axis=1))

   if integrator <> 'NONE' and wLength > 1:
      window = np.ones(wLength)
      data = np.apply_along_axis(np.convolve, 1, data, window, mode='full')
      data = np.ma.array(data[:, 0:-wLength+1])

   if   aggregate == 'MIN':
      data = np.nanmin(data, axis=1)
   elif aggregate == 'MAX':
      data = np.nanmax(data, axis=1)
   elif aggregate == 'AVG':
      data = np.nanmean(data, axis=1)

   if not refData is None:
      data = data - refData

   return data
