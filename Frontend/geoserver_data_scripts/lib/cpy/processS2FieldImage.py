#!/usr/bin/env python
#
################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2018, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

import os
import argparse
import datetime
import uuid
import fieldDB
import csutils.subprocess
import csutils.os

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("user_id",      help="user ID")
    parser.add_argument("field_id",     help="field ID")
    parser.add_argument("image_fn",     help="image filename", type=str)
    parser.add_argument("--bbox",       help="bounding box ", nargs=4, type=float)
    parser.add_argument("--group_id",   help="group ID", default="DEFAULT", type=str)
    parser.add_argument("--datetime",   help="date time string YYYY-MM-DD[THH:MM:SS]", default=None, type=str)
    parser.add_argument("--indicator",  help="indicator (NDVI, SCL)", default="NDVI", type=str)
    parser.add_argument("--outsize",    help="npixels-X npixels-Y", nargs=2, type=int, default=None)
    parser.add_argument("--t_srs",      help="output SRS", default=None, type=str)
    parser.add_argument("--fieldonly",  action='store_true')
    parser.add_argument("--png",        action='store_true')

    args = parser.parse_args()
    suuid = str(uuid.uuid4())
    tmpfile = '/tmp/fi-' + suuid + '.tif'
    tmpfilew = '/tmp/fiw-' + suuid + '.tif'    

    dt = args.datetime
    if dt is not None:
        if 'T' in args.datetime:
            dt = datetime.datetime.strptime(args.datetime, "%Y-%m-%dT%H:%M:%S")
        else:
            dt = datetime.datetime.strptime(args.datetime, "%Y-%m-%d")

    f = fieldDB.Field(args.user_id, args.group_id, args.field_id)

    if args.fieldonly:
        tmpfile = args.image_fn
        

    if args.indicator.upper() == 'NDVI':
        f.getNDVIImage(tmpfile, dt)
    elif args.indicator.upper() == 'SCL':
        f.getSCLImage(tmpfile, dt)
    else:
        raise RuntimeError("ERROR : unexpected indicator %s" % args.indicactor)

    if args.fieldonly:
        quit()
    
    ## make the final image
    if not os.path.isfile(tmpfile):
        raise RuntimeError("Temporary file " + tmpfile + " not created/found.")

    if not args.png:
        tmpfilew = args.image_fn

    argv = [ 'gdalwarp',
             tmpfile, tmpfilew,
            '-srcnodata', 'nan',
            '-dstalpha',
            '-overwrite']

    if args.bbox is not None:
        bboxstr = "%f %f %f %f" % (args.bbox[0], args.bbox[1], args.bbox[2], args.bbox[3])    
        argv.extend(['-te', bboxstr])

    if args.t_srs is not None:
        argv.extend(['-t_srs', args.t_srs])

    if args.outsize is not None:
        osstr = "%d %d" % (args.outsize[0], args.outsize[1])
        argv.extend(['-ts', osstr])

    csutils.subprocess.callcmd(argv)

    if args.png:
        scales = { 'NDVI' : [ 0.0, 1.0 ], 'SCL' : [ 0, 11 ] }
        scale = scales[args.indicator]
        argv = [ 'gdal_translate',
                 '--config', 'GDAL_PAM_ENABLED', 'NO', # Prevent generation aux.xml
                 '-of', 'PNG',
                 '-a_nodata', 'nan',
                 '-ot', 'Byte','-scale_1', str(scale[0]), str(scale[1]),  # we need to scale values, we are going from a Float32 -> Uint16
                 tmpfilew, args.image_fn]

        csutils.subprocess.callcmd(argv)
        os.remove(tmpfilew)

    os.remove(tmpfile)
