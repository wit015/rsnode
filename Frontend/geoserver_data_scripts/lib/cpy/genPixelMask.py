#!/usr/bin/env python

##################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
##################################################################################

import re
import sys
import numpy as np
from osgeo import gdal              
from osgeo import osr
import traceback


def _world2Pixel(gt, x, y):
  ulX = gt[0]
  ulY = gt[3]
  xDist = gt[1]
  yDist = gt[5]
  rtnX = gt[2]
  rtnY = gt[4]
  pixel = int((x - ulX) / xDist)
  line  = int((y - ulY) / yDist)
  return (pixel, line)

def _pixel2World(gt, y, x):
   returnLat = gt[3] + float(y) * gt[5]
   returnLong = gt[0] + float(x) * gt[1]
   return (returnLat, returnLong)

def interpolate(gm, aLon, aLat, bLon, bLat):

   # O  : Origin of the grid
   # R  : Resolution grid
   # P  : Pixel
   # A  : begin point for line piece
   # AP : begin pixel for line piece
   # B  : end point for line piece
   # BP : end pixel for line piece

   AP = _world2Pixel(gm, aLon, aLat);
   BP = _world2Pixel(gm, bLon, bLat);

   O = (gm[0], gm[3]) # Origin of the geoMatrix
   R = (gm[1], gm[5]) # Resolution of the geoMatrix
   A = (aLon, aLat)   # Begin point
   B = (bLon, bLat)   # End point
   P = AP             # First pixel
   L = []             # Pixel list

   Sx = 1 # Step direction in X
   if (R[0]<0) ^ (aLon > bLon): Sx = -1
   Sy = 1 # Step direction in Y
   if (R[1]<0) ^ (aLat > bLat): Sy = -1

   # The longitude is the 'left' of a pixel. So offset when moving -lon is 0.
   if Sx<0:
      Dx = 0
   else:
      Dx = 1

   # Compute slope
   try:
      Slope = (B[1]-A[1])/(B[0]-A[0])
   except:
      # Line runs vertical
      Slope = Sy * float('inf')

   while True:
     # (Cx,Cy) = intersection AB and y = Oy + (Py+Sy)*Ry = crossing next line
     Cx = O[0] + (P[0]+Dx)*R[0];
     Cy = A[1] + (Cx - A[0]) * Slope;
     # convert to pixel and if infinite X take end point pixel
     if abs(Cy) < sys.maxint:
        CP = _world2Pixel(gm, Cx, Cy)
     else :
        CP = BP
     # Loop in Y while not crossing border with next line
     while True :
        L.append(P)
        if P[1] == CP[1] or P[1] == BP[1]: break
        P = (P[0], P[1]+Sy)
     if P[0] == BP[0]: break
     # Go to next line
     P = (P[0]+Sx, P[1])

   return L

def genPixelMask(aMaskFileName, aPixelMaskFileName, aPointFileName):

   # Get origin for pixel (upper left corner
   mask  = gdal.Open(aMaskFileName)
   gm    = mask.GetGeoTransform()

   # Acquire the noDataValue
   band = mask.GetRasterBand(1)
   noDataValue = mask.GetRasterBand(1).GetNoDataValue()
   if noDataValue == None:
      noDataValue = 0.0

   # Get mask template data
   maskData = band.ReadAsArray()

   # Initialise new mask data
   newMaskData = np.full(maskData.shape, noDataValue)

   # Get pixel list
   pointString = open(aPointFileName,'r').read()
   isPolygon = re.match('POLYGON|MULTIPOLYGON', pointString)
   pointList = [(float(p[0]),float(p[1])) 
                  for p in map(str.split, re.sub('[^0-9. ,]','', pointString).split(','))]

   # Loop over all points and maintain bounding box
   maxX = 0; maxY = 0
   minX = sys.maxsize; minY = sys.maxsize; 
   minLon = 180.0; maxLat = -90.0

   pp = None
   for p in pointList:
      pixelList = []
      xIndex, yIndex = _world2Pixel(gm, p[0], p[1])
      if isPolygon:
         if pp == None : 
            pp = p  # previous point
            fp = p  # First point
         pixelList = interpolate(gm, pp[0], pp[1], p[0], p[1])

         # For multi-polygons check if end of polygon is reached to start new polygon
         if p <> pp and p == fp :
           pp = None;
         else :
           pp = p
      else:
         pixelList = [(xIndex, yIndex)]

      # Copy mask data for pixels in pixel list
      for v in pixelList:
         newMaskData[v[1], v[0]] = maskData[v[1], v[0]]

      # Determine bounding box
      if xIndex < minX: minX = xIndex
      if yIndex < minY: minY = yIndex
      if xIndex > maxX: maxX = xIndex
      if yIndex > maxY: maxY = yIndex
      if p[0] < minLon : 
         minLon = p[0]; minLonX = xIndex
      if p[1] > maxLat : 
         maxLat = p[1]; maxLatY = yIndex

   # Generate geometry matrix with new origin
   oLat, oLon = _pixel2World(gm, maxLatY, minLonX)
   gm = (oLon, gm[1], gm[2], oLat, gm[4], gm[5])

   # Crop mask data to bounding box
   newMaskData = newMaskData[minY:maxY+1, minX:maxX+1]
 

   driver = gdal.GetDriverByName('GTiff')
   dataset = driver.Create(aPixelMaskFileName, newMaskData.shape[1], newMaskData.shape[0], 1, gdal.GDT_Float32)
   # Configure the projection
   proj = osr.SpatialReference()
   proj.SetWellKnownGeogCS("WGS84")
   dataset.SetProjection(proj.ExportToWkt())
   # Change extent
   dataset.SetGeoTransform(gm)
   band = dataset.GetRasterBand(1)
   band.WriteArray(newMaskData)
   band.SetNoDataValue(noDataValue)
   band.ComputeStatistics(0)
   dataset.FlushCache()  # Write to disk.
   dataset = None

def usage():
   sys.exit("usage: %s <mask filename> <output filename> <point filename>" % sys.argv[0])

if __name__ == "__main__":
   if len(sys.argv) <> 4:
      usage()
   try:
      aMaskFileName      = sys.argv[1]
      aPixelMaskFileName = sys.argv[2]
      aPointFileName     = sys.argv[3]
      genPixelMask(aMaskFileName, aPixelMaskFileName, aPointFileName)
   except:
      exc_type, exc_value, exc_traceback = sys.exc_info()
      traceback.print_exception(exc_type, exc_value, exc_traceback)
      usage()
