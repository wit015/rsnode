#!/usr/bin/env python
##################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
##################################################################################

import re
import os
import sys
import datetime

# Global script identifiers
svn_revision = str('$Rev: 7861 $').replace('$', '').strip(' \t\n\r')
svn_date = re.sub('\) \$', ' ' ,re.sub('^.*\(', 'Date: ', '$Date: 2017-01-27 12:50:14 +0100 (Fri, 27 Jan 2017) $')).strip(' \t\n\r')
svn_author = str('$Author: jb76278 $').replace('$', '').strip(' \t\n\r')
script_file_name = os.path.basename(__file__)
now_string = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

local_path = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, local_path + '/code')
sys.path.insert(0, local_path + '/pyLibs')

#import arrayToPng as arr2png
from netCDF4 import Dataset
from netCdfGeoMetaData import NetCdfGeoMetaData
import numpy as np

# Handling of nan will result in run-time warnings to be printed.
# Prevent these messages as it will fail the process.
import warnings
warnings.filterwarnings('ignore', '.*', RuntimeWarning, '.*nanfunctions.*')

# The GDAL imports for geoTiff (a.o.)
from osgeo import gdal
from osgeo import osr
from osgeo.gdalconst import *

import baseStats
import sourceInfo

# Handling of nan will result in run-time warnings to be printed.
# Prevent these messages as it will fail the process.
import warnings
logWarnings = open('/tmp/' + os.path.basename(__file__) + '.log', 'a')
def customwarn(message, category, filename, lineno, file=None, line=None):
    now = str(datetime.datetime.now())
    logWarnings.write(now + ':' + warnings.formatwarning(message, category, filename, lineno))
warnings.showwarning = customwarn


gLTA_START_DATE = datetime.date(2001,  1,  1)
gLTA_END_DATE   = datetime.date(2014, 12, 31)

gINTEGRATE_TYPE_SET  = [ 'SUM', 'NONE', 'DIF' ]
gSTATISTICS_TYPE_SET  = [ 'P50', 'P00', 'P25', 'P75', 'P100', 'VALUE' ]

#############################
# Return index in days array of largest element less or equal to the selected day
# If no element exists return 0 (index of first element)
#############################
def lookup(dataArray, value):
   return max(np.where(dataArray <= value, range(dataArray.shape[0]), 0))

######################################################################################################
# Method will extract the requested map data and put it in a geoTiff file
######################################################################################################
def processIntervalMap(aOutputFileName, aBaseFileName, aRoiFileName, aPeriodStart, aPeriodEnd, aIntegrateType, aAggregateType, aThreshold, aStatsType, aWindowLength, aLtaStartDate, aLtaEndDate, aSource, aLabel):
   global svn_revision, svn_date, svn_author, now_string, script_file_name

   # Objects for the meta data of the input
   metaData    = NetCdfGeoMetaData()
   roiMetaData = NetCdfGeoMetaData()

   ###################
   # Open the base file
   ###################
   baseDataset = Dataset(os.path.join(aBaseFileName), 'r')

   metaData.copyFromNetCdfObject(baseDataset)
   latSize  = len(baseDataset.dimensions['latitude'])
   longSize = len(baseDataset.dimensions['longitude'])

   noDataValueCube = None
   if metaData.getNoDataValue() <> '<None>':
      noDataValueCube = float(metaData.getNoDataValue())
   
   ###################
   # Retrieve the ROI mask and bounding box indices from the geotif file
   ###################
   roiFile = gdal.Open(aRoiFileName)
   roiMetaData.copyFromGdalData(roiFile)
   upperLat, leftLong = roiMetaData.getLatLongFromPixelYx(0,0)
   xOrg, xRes = metaData.getLongitudeTransform()
   yOrg, yRes = metaData.getLatitudeTransform()
   # Take mid of upper left mask pixel to prevent precision errors.
   upperIndex,leftIndex = metaData.getPixelYxFromLatLong(upperLat+0.5*yRes, leftLong+0.5*xRes)
   lowerIndex = upperIndex + roiFile.RasterYSize
   rightIndex = leftIndex  + roiFile.RasterXSize

   upperIndex = min(max(0, upperIndex), latSize-1)
   leftIndex  = min(max(0, leftIndex), longSize-1)
   lowerIndex = min(max(0, lowerIndex), latSize-1)
   rightIndex = min(max(0, rightIndex), longSize-1)

   ySlice = slice(upperIndex, lowerIndex)
   xSlice = slice(leftIndex, rightIndex)

   # Create true/false array from roi-tif-file to use as mask later.
   noDataValueMask  = roiFile.GetRasterBand(1).GetNoDataValue()
   roiMaskDataArray = np.ma.array(roiFile.GetRasterBand(1).ReadAsArray())
   roiMaskDataArray = np.ma.masked_where(roiMaskDataArray == noDataValueMask, roiMaskDataArray, copy=False)
   roiMaskDataArray = np.ma.masked_where(roiMaskDataArray <= 0.0, roiMaskDataArray, copy=False)
   del roiFile

   # Adjust the meta data to the new reference (to be used for the geotiff output)
   metaData.adjustPixelOriginGeoTransform(upperIndex, leftIndex)

   ###################
   # Retrieve the time indices for the base datacube to start with
   ###################
   timeArray = baseDataset.variables['time'][:]

   # Offset to compute start and end dates for periods
   epoch = metaData.getDate()

   startDay = max(0, (aPeriodStart - epoch).days)
   endDay   = min(timeArray[-1], (aPeriodEnd - epoch).days)
   startIndex = lookup(timeArray, startDay)
   endIndex   = max(lookup(timeArray, endDay), startIndex)
   key, value = baseDataset.variables.items()[3]  # Index 3 is the VI or Precip data in a cube
   indicesInYear = sourceInfo.lengthLTA(aSource)

   # Start and end dates to be echoed back to caller
   startDate       = epoch + datetime.timedelta(int(timeArray[startIndex]))
   isLastInterval  = endIndex == timeArray.shape[0]
   endDate         = sourceInfo.endDate(aSource, epoch + datetime.timedelta(int(timeArray[endIndex])), isLastInterval)
   duration        = (endDate - startDate).days + 1

   # Gather all the start indices of the data selections   
   if aStatsType == 'VALUE':
      startIndices = baseStats.getBeginIndices(startIndex, startIndex, endIndex, endIndex-startIndex+1, indicesInYear)
   else:   # aStatsType in [ 'P50', 'P00', 'P25', 'P75', 'P100' ]
      # Determine upper and lower bounds for base statistics dates
      ltaStart, ltaEnd, ltaLength = sourceInfo.periodLTA(aSource, (aPeriodStart, aPeriodEnd))
      ltaNInt = (ltaEnd-ltaStart) % ltaLength + 1
      ltaStartIndex = lookup(timeArray, (aLtaStartDate-epoch).days)
      ltaEndIndex   = lookup(timeArray, (aLtaEndDate-epoch).days)
      ltaInterval = (ltaStartIndex, ltaStartIndex+ltaStart, ltaEndIndex, ltaNInt, ltaLength)
      startIndices = baseStats.getBeginIndices(*ltaInterval)

   # Reserve space for the interval output cube(s)
   intervalOutput = np.ma.empty((startIndices.shape[0], ySlice.stop-ySlice.start, xSlice.stop-xSlice.start), dtype=int)

   # The interval array is a 1D array with all possible outcomes for the interval 
   # (last two values are 'duration' and duration plus measerement-interval)
   intervalArray = np.append(timeArray[startIndex+1:endIndex+1] - timeArray[startIndex], duration)
   intervalArray = np.append(intervalArray, 0)

   # For DIF we skip the first interval
   # if aIntegrateType == 'DIF':  
   #    if intervalArray.shape[0]<3:
   #       sys.exit("ERROR : period must cover at least 2 measurement intervals for integrate=DIF");
   #    intervalArray = intervalArray[1:]

   # Calculate the interval indices number for the different interval and integration type calculations
   if   aWindowLength == 0 and aIntegrateType == 'DIF':
      windowIndexes = 1
   elif aWindowLength == 0 and aIntegrateType == 'SUM':
      windowIndexes = endIndex - startIndex + 1
   else : 
      windowIndexes = int(np.ceil(aWindowLength / float(sourceInfo.measurementInterval(aSource))))
      windowIndexes = max(1, windowIndexes)

   # Now loop through all the intervals
   for indx in range(0, startIndices.shape[0]):

      # Make selection of the base data and create a duration array for that selection
      data = baseDataset.variables[key]
      selectedRawData = data[startIndices[indx]:(startIndices[indx]+(endIndex-startIndex+1)), ySlice, xSlice]

      # Create threshold data cube based on integration type
      if aIntegrateType == 'DIF':
         iData   = np.pad(selectedRawData, ((1,0), (0,0), (0,0)), mode='edge')
         iData   = np.diff(iData, axis=0)
         cWindow = np.ones(windowIndexes)
         thresholdData = np.apply_along_axis(np.convolve, 0, iData, cWindow, mode='full')
         thresholdData = thresholdData[:intervalArray.shape[0]-1,:,:]

      elif aIntegrateType == 'SUM':
         iData   = selectedRawData
         cWindow = np.ones(windowIndexes)
         thresholdData = np.apply_along_axis(np.convolve, 0, iData, cWindow, mode='full')
         thresholdData = thresholdData[:intervalArray.shape[0]-1,:,:]

      else: # aIntegrateType == 'NONE'
         thresholdData = selectedRawData  # Default when integration type is 'NONE'

      if aAggregateType == 'THR':
         # Get the indices at which the threshold is first exceeded 
         # (the first 'true' value in boolean array)
         if aThreshold >= 0.0:
            b = thresholdData >= aThreshold
         else:
            b = thresholdData <= aThreshold
         # Create matrix with all True values
         t = np.full((1,) + b.shape[1:], True, dtype=bool)
         # Concatenate True matrix to make sure a max is found which is True
         b = np.concatenate((b, t), axis=0)
         rIdx = np.nanargmax(b, axis=0)

      elif aAggregateType == 'MIN':
         # Add layer to prevent exception when slice is all-Nan
         thresholdData = np.lib.pad(thresholdData, ((0,1), (0,0), (0,0)), 'constant', constant_values=np.finfo(np.float32).max)
         rIdx = np.nanargmin(thresholdData, axis = 0)

      else : # aAggregateType == 'MAX'
         # Add layer to prevent exception when slice is all-Nan
         thresholdData = np.lib.pad(thresholdData, ((0,1), (0,0), (0,0)), 'constant', constant_values=np.finfo(np.float32).min)
         rIdx = np.nanargmax(thresholdData, axis = 0)

      # Convert the threshold-indices to 'interval days'
      intervalOutput[indx] = rIdx

   baseDataset.close()

   # Select the (statistical) data for the output
   if aStatsType == 'VALUE':
      outputData = intervalArray[intervalOutput[0]]
   else:
      statistics = np.percentile(intervalOutput, [0.0, 25.0, 50.0, 75.0, 100.0], axis=0, interpolation='nearest')
      if   aStatsType == 'P00' : si = 0
      elif aStatsType == 'P25' : si = 1
      elif aStatsType == 'P50' : si = 2
      elif aStatsType == 'P75' : si = 3
      elif aStatsType == 'P100': si = 4
      else :
         print "Unexpected statistic %s" % aStatsType
         return -1

      outputData = intervalArray[statistics[si]]

   ###################
   # Set NODATA (0) for masked pixels
   ###################
   roiMaskDataArray = np.ma.masked_where(np.isnan(outputData), roiMaskDataArray, copy=False)
   outputData[np.where(roiMaskDataArray.mask)] = 0

   ###################
   # Put the gifOutputData grid in a geotiff
   ###################
   driver = gdal.GetDriverByName('GTiff')
   dataset = driver.Create(aOutputFileName, outputData.shape[1], outputData.shape[0], 1, gdal.GDT_Int32)
   dataset.SetMetadataItem('TIFFTAG_SOFTWARE', aLabel)
   dataset.SetMetadataItem('COMMON_SENSE', aLabel)
   # Configure the projection
   proj = osr.SpatialReference()
   proj.SetWellKnownGeogCS("WGS84")
   dataset.SetProjection(proj.ExportToWkt())
   latOrigin, latPixSize = metaData.getLatitudeTransform()
   longOrigin, longPixSize = metaData.getLongitudeTransform()
   geotransform = (longOrigin, longPixSize, 0, latOrigin, 0, latPixSize)
   dataset.SetGeoTransform(geotransform)  
   dataset.GetRasterBand(1).WriteArray(outputData)
   dataset.GetRasterBand(1).SetNoDataValue(0)
   dataset.FlushCache()  # Write to disk.
   dataset = None

   return (startDate, endDate)

def usage():
   print "Usage:"
   print "python %s [optional flags] --source=<sourcename> <output-file-name> <base-file-name> <roi-polygon-geotif-file> <period-begin> <period-end>" % sys.argv[0]
   print ""
   print "This method returns the number of day it takes to reach a threshold value. Arguments:"
   print " The output file name should have the .tif extension."
   print " The base file must be a netCDF cube type file."
   print " The roi polygon geotif file specifies the region of interest."
   print " The period begin and end dates specify the period of interest (maximum is one year)."
   print ""
   print "Allowed flag examples:"
   print "--source=CHIRPS_5000m The data source name."
   print "--integrate=SUM       The integrate type flag can have 3 possible values: SUM, NONE or DIF. The default value is SUM."
   print "--stats=P50           The statistics value returned can have 7 possible values: VALUE P00 P25 P50 P75 P100.  The default value (if flag is omitted) is P50."
   print "--window=60        The maximum window (in days) in which the threshold value should be reached."
   print "--label               The content of this flag is returned in the output."
   print "--threshold=<float>   Find threshold"
   print "--max                 Find max"
   print "--min                 Find min"
   print ""
   print "Example(s):"
   print "%s ~/Jelle/data/output.tif ~/Jelle/data/base-PRECIP_FINAL.hdf5 ~/Jelle/data/lta-PRECIP_FINAL.hdf5 ~/Jelle/data/mask.tif 2015-03-01 2015-04-01" % sys.argv[0]
   print ""

######################################################################################################
# Check and process the input parameters
######################################################################################################
def main(argv):

   global gREFERENCE_TYPE_SET, gINTEGRATE_TYPE_SET, gAGGREGATE_TYPE_SET, gLTA_START_DATE, gLTA_END_DATE

   if len(sys.argv) < 6:
      usage()
      sys.exit("ERROR unexpected number of arguments (argc=%d)" % len(sys.argv))

   flags_dict     = {}

   # Initialise defaults
   outputFileName = ''
   baseFileName   = ''
   roiFileName    = ''
   threshold      = 0.0
   periodStart    = None
   periodEnd      = None
   integrateType  = gINTEGRATE_TYPE_SET[0]
   aggregateType  = 'MAX'
   statsType      = gSTATISTICS_TYPE_SET[0]
   windowLength      = 0
   ltaStartDate   = gLTA_START_DATE
   ltaEndDate     = gLTA_END_DATE
   label          = ''
   source         = ''

   # Extract all arguments
   mArgCount = 0 # Number of mandatory arguments
   for arg in argv[1:]:
      # Optional arguments
      if arg.startswith('--'):
         l = arg.split('=')
         if len(l) == 2:
            key, value = l
         else:
            key, value = l[0], None
         flags_dict[key] = value
      else:
         # Mandatory arguments
         if   mArgCount == 0:
            outputFileName = arg
            mArgCount += 1
         elif mArgCount == 1:
            baseFileName = arg
            mArgCount += 1
         elif mArgCount == 2:
            roiFileName = arg
            mArgCount += 1
         elif mArgCount == 3:
            periodStart = datetime.datetime.strptime(arg, "%Y-%m-%d").date()
            mArgCount += 1
         elif mArgCount == 4:
            periodEnd = datetime.datetime.strptime(arg, "%Y-%m-%d").date()
            # Maximum duration is one year between begin and end-date
            if (periodStart.month == 2) and (periodStart.day == 29):
               cutoffDay = datetime.date(periodStart.year + 1, periodStart.month, periodStart.day - 1)
            else:
               cutoffDay = datetime.date(periodStart.year + 1, periodStart.month, periodStart.day) - datetime.timedelta(days=1)
            if (periodEnd > cutoffDay):
               periodEnd = cutoffDay
            mArgCount += 1
         else:
            usage("ERROR : unkown argument '" + arg + "'")

   # Check the flags and extract their values
   for key, value in flags_dict.items():
      if key == '--integrate':
         if value in gINTEGRATE_TYPE_SET:
            integrateType = value
         else:
            print "Possible values: " + str(gINTEGRATE_TYPE_SET)
            sys.exit("Error: integrate ('" + value + "') not valid ('" + str(gINTEGRATE_TYPE_SET) + ").")
      elif key == '--threshold':
         aggregateType = 'THR'
         threshold = float(value)
      elif key == '--max':
         aggregateType = 'MAX'
      elif key == '--min':
         aggregateType = 'MIN'
      elif key == '--stats':
         if value in gSTATISTICS_TYPE_SET:
            statsType = value
         else:
            print "Possible values: " + str(gSTATISTICS_TYPE_SET)
            sys.exit("Error: Value \'" + value + "\' for Key \'" + key + "\' is not valid.")
      elif key == '--window':
         windowLength = int(value)
      elif key == '--label':
         label = str(value)
      elif key == '--source':
         if value in sourceInfo.gSOURCE_TYPE_SET:
            source = value
         else:
            print "Possible values: " + str(sourceInfo.gSOURCE_TYPE_SET)
            sys.exit("Error: Value \'" + value + "\' for Key \'" + key + "\' is not valid.")
      elif key == '--lta-start':
         ltaStartDate = datetime.datetime.strptime(value, "%Y-%m-%d").date()
      elif key == '--lta-end':
         ltaEndDate = datetime.datetime.strptime(value, "%Y-%m-%d").date()
      else:
         print "Warning: Key \'" + key + "\' is unknown and ignored."

   # 
   processIntervalMap(
      outputFileName, 
      baseFileName, 
      roiFileName,
      periodStart, 
      periodEnd, 
      integrateType,
      aggregateType,
      threshold,
      statsType, 
      windowLength,
      ltaStartDate,
      ltaEndDate,
      source,
      label)

######################################################################################################
# Only execute when run as a script
######################################################################################################
if __name__ == "__main__":
   main(sys.argv[:])

