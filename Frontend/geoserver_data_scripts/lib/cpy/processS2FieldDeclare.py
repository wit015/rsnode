#!/usr/bin/env python
#
################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2018, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

import argparse
import geopandas as gpd
import json

import S2DB
import fieldDB

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("user_id",     help="user ID")
    parser.add_argument("--group_id",  help="group ID", default="DEFAULT", type=str)
    parser.add_argument("tag",         help="tag")
    parser.add_argument("features_fn", help="polygon filename")

    args = parser.parse_args()

    features_fn = args.features_fn
    features_df    = gpd.read_file(features_fn)
    
    # get all granules containing the field
    if S2DB.getContainingGranuleId(features_df) is None :
        raise RuntimeError("ERROR : no matching granule")

    # Convert polygon to internal SRS   
    fid =fieldDB.Field.declare(args.user_id, args.group_id, args.tag, features_fn)

    print(json.dumps(fid))
