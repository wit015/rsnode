#!/usr/bin/env python
#
################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2018, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

import os
import argparse

import geojson

import csutils.os
import csutils.subprocess

import csutils.geojson

import fieldDB

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("user_id",    help="user ID")
    parser.add_argument("--group_id", help="group ID", default="DEFAULT", type=str)
    parser.add_argument("--t_srs",    help="target SRS", default="EPSG:4326", type=str)

    args = parser.parse_args()

    t_auth, t_epsg_str = (args.t_srs).split(':')
    if t_auth.upper() <> 'EPSG':
        raise RuntimeError("ERROR : unexpected SRS authority %s" % t_auth)
    t_epsg = int(t_epsg_str)

    feature_collection_list = fieldDB.Field.getGroupList(args.user_id, args.group_id)
 
    # reproject all feature collections to same SRS
    # and put all features in one collection.

    feature_list = []
    for fc in feature_collection_list:

        s_epsg = csutils.geojson.EPSGCode(fc)

        for i in range(len(fc['features'])):
            feature = fc['features'][i]
            g = feature['geometry']
            ng = csutils.geojson.reproject(g, s_epsg, t_epsg)
            feature['geometry'] = ng
            feature_list.append(feature)

    feature_collection = geojson.FeatureCollection(feature_list)
    feature_collection['crs'] = csutils.geojson.EPSGCrs(t_epsg)

    print geojson.dumps(feature_collection)
