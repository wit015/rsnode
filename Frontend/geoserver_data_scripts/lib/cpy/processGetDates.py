#!/usr/bin/env python
##################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
##################################################################################

import re
import os
import sys
import datetime

# Global script identifiers
svn_revision = str('$Rev: 7845 $').replace('$', '').strip(' \t\n\r')
svn_date = re.sub('\) \$', ' ' ,re.sub('^.*\(', 'Date: ', '$Date: 2017-01-24 23:16:51 +0100 (Tue, 24 Jan 2017) $')).strip(' \t\n\r')
svn_author = str('$Author: jb76278 $').replace('$', '').strip(' \t\n\r')
script_file_name = os.path.basename(__file__)
now_string = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

local_path = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, local_path + '/code')
sys.path.insert(0, local_path + '/pyLibs')

import json
import numpy as np
from collections import OrderedDict
from netCDF4 import Dataset
from netCdfGeoMetaData import NetCdfGeoMetaData

import sourceInfo

# Handling of nan will result in run-time warnings to be printed.
# Prevent these messages as it will fail the process.
import warnings
logWarnings = open('/tmp/' + os.path.basename(__file__) + '.log', 'a')
def customwarn(message, category, filename, lineno, file=None, line=None):
    now = str(datetime.datetime.now())
    logWarnings.write(now + ':' + warnings.formatwarning(message, category, filename, lineno))
warnings.showwarning = customwarn


def main(argv):

   cubeFilename = sys.argv[1]
   source       = sys.argv[2]
   indicator    = sys.argv[3]

   dataset   = Dataset(sys.argv[1], 'r')
   dateArray = dataset.variables['time'][:]
   metaData  = NetCdfGeoMetaData()
   metaData.copyFromNetCdfObject(dataset)

   epoch = metaData.getDate()

   def addRefDate(x):
      return (epoch + datetime.timedelta(days=int(x)))

   # LTA data has DOY 1 - 365. 
   # We need the modified DOY (0-364) to get correct dates
   if indicator.endswith('LTA'):
      dateArray = dateArray - 1

   # Get all measurement interval start dates
   startDates   = np.vectorize(addRefDate)(dateArray)
   endDates     = np.roll(startDates, -1) - datetime.timedelta(1)
   endDates[-1] = sourceInfo.endDate(source, startDates[-1], not indicator.endswith('LTA'))

   startDates = np.vectorize(datetime.date.isoformat)(startDates)
   endDates   = np.vectorize(datetime.date.isoformat)(endDates)

   response              = OrderedDict()
   response['type']      = 'GetDates'
   response['N']         = len(startDates)
   response['pstart']    = startDates.tolist()
   response['pend']      = endDates.tolist()
   response['source']    = sys.argv[2]
   response['indicator'] = sys.argv[3]

   print json.dumps(response)

if __name__ == "__main__":
   if len(sys.argv) <> 4:
      print "usage: %s <file-name> <source> <indicator>" % sys.argv[0]
      sys.exit(-1)
   main(sys.argv[:])
