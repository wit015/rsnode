#!/usr/bin/env python
#
# Module to select polygon from raster.
#
################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2018, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

import argparse
import geopandas as gpd

import S2DB

import polygonSelection

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("polygon_fn",  help="polygon filename")
    parser.add_argument("output_fn",   help="output filename")
    parser.add_argument("--bbox",      help="bounding box", nargs="+", type=float)
    parser.add_argument("--fill",      help="fill fill", nargs="+", type=int)
    parser.add_argument("--oformat",   help="output format", default="auto", type=str)
    parser.add_argument("--dstalpha",  help="create alpha band", action="store_true")
    parser.add_argument("--dstnodata", help="no data in target file", default=None, type=str)
    parser.add_argument("--outsize",   help="npixels-X npixels-Y", nargs=2, type=int, default=None)
    parser.add_argument("--t_srs",     help="output SRS", default=None, type=str)

    args = parser.parse_args()

    features_fn = args.polygon_fn
    features_df = gpd.read_file(features_fn)

    # get all granules containing the field
    gid = S2DB.getContainingGranuleId(features_df)
    if gid is None :
        raise RuntimeError("ERROR : no matching granule")

    template_pn = S2DB.getGranuleTemplateFilePath(gid)

    polygonSelection.clipPolygonFromRaster(
       template_pn, 
       args.polygon_fn, 
       args.output_fn,
       outsize=args.outsize,
       fill=args.fill,
       oformat=args.oformat,
       dstalpha=args.dstalpha, 
       dstnodata=args.dstnodata, 
       bbox=args.bbox,
       t_srs=args.t_srs)
