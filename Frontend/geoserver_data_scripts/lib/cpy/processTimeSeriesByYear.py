#!/usr/bin/env python
################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

import math
import re
import os
import sys
import datetime
import csv, io

# Global script identifiers
svn_revision = str('$Rev: 7855 $').replace('$', '').strip(' \t\n\r')
svn_date = re.sub('\) \$', ' ' ,re.sub('^.*\(', 'Date: ', '$Date: 2017-01-26 17:37:17 +0100 (Thu, 26 Jan 2017) $')).strip(' \t\n\r')
svn_author = str('$Author: jb76278 $').replace('$', '').strip(' \t\n\r')
script_file_name = os.path.basename(__file__)
now_string = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

local_path = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, local_path + '/code')
sys.path.insert(0, local_path + '/pyLibs')

#import arrayToPng as arr2png
from netCDF4 import Dataset
from netCdfGeoMetaData import NetCdfGeoMetaData
import numpy as np
import json
from collections import OrderedDict

# The GDAL imports for geoTiff (a.o.)
from osgeo import gdal
from osgeo import osr
from osgeo.gdalconst import *

import baseStats
import sourceInfo

# Handling of nan will result in run-time warnings to be printed.
# Prevent these messages as it will fail the process.
import warnings
logWarnings = open('/tmp/' + os.path.basename(__file__) + '.log', 'a')
def customwarn(message, category, filename, lineno, file=None, line=None):
    now = str(datetime.datetime.now())
    logWarnings.write(now + ':' + warnings.formatwarning(message, category, filename, lineno))
warnings.showwarning = customwarn

gREFERENCE_TYPE_SET = ['NONE', 'LTA']
gAGGREGATE_TYPE_SET = ['AVG', 'MIN', 'MAX']
gINTEGRATE_TYPE_SET = ['NONE', 'DIF', 'SUM']
gLTA_START_DATE     = datetime.date(1900, 1, 1)
gLTA_END_DATE       = datetime.date(2100, 1, 1)

def _world2Pixel(geoMatrix, x, y):
  ulX = geoMatrix[0]
  ulY = geoMatrix[3]
  xDist = geoMatrix[1]
  yDist = geoMatrix[5]
  rtnX = geoMatrix[2]
  rtnY = geoMatrix[4]
  pixel = (x - ulX) / xDist
  line  = (y - ulY) / yDist
  return (int(pixel), int(line))

#############################
# Return index in days array of largest element less or equal to the selected day
# If no element exists return 0 (index of first element)
#############################
def lookup(darray, value):
   return max(np.where(darray <= value, range(darray.shape[0]), 0))

###############################################################################################
# Method will extract the requested map data and put it in a geoTiff file
##############################################################################################
def _processTimeSeriesByYear(aBaseFileName, aLtaFileName, aMaskFileName, aAggregateType, aPeriodStart, aPeriodEnd, aIntegrateType, aWindowLength, aReferenceType, aLtaStartDate, aLtaEndDate, aLabel, aSource, aIndicator, aMaskID, aMimeType):

   # Object for the meta data of the input (will be similar to that of the output)
   metaData = NetCdfGeoMetaData()

   ###################
   # Open the base file and the long-time-average file for reference if needed
   ###################
   baseDataset = Dataset(os.path.join(aBaseFileName), 'r')
   metaData.copyFromNetCdfObject(baseDataset)
   noDataValueCube = eval(metaData.getNoDataValue())

   # Create function to add offset to a date array
   def addRefDate(x):
      return metaData.getDate() + datetime.timedelta(days=int(x))
   bDaysArray  = baseDataset.variables['time'][:]
   mStartDates = np.vectorize(addRefDate)(bDaysArray)
   startIndex  = lookup(mStartDates, aPeriodStart)
   endIndex    = lookup(mStartDates, aPeriodEnd)

   ###################
   # Determine size of the integration window
   ###################
   windowLength = math.ceil(float(aWindowLength)/sourceInfo.measurementInterval(aSource))
   if windowLength == 0 :
      if aIntegrateType == 'DIF': windowLength = 1
      if aIntegrateType == 'SUM': windowLength = endIndex - startIndex + 1
   windowLength = max(1, windowLength)

   ###################
   # Retrieve the bounding box indices for the datacube to start with
   ###################
   # Get Cube GeoTransform 
   cgt = eval(metaData.geoTransform)
   ds  = gdal.Open(aMaskFileName)
   d   = ds.ReadAsArray()

   # Get offset of roi in cube CRS
   rMinX, rResX, rRotX, rMaxY, rRotY, rResY = ds.GetGeoTransform()
   # Take middle of pixel to prevent numerical precision errors 
   ulX, ulY = _world2Pixel(cgt, rMinX+0.5*rResX, rMaxY+0.5*rResY)
   # Create mask
   mask = np.ma.masked_array(d)
   mask = np.ma.masked_where(mask <= 0, mask, copy=False)
   noDataValueMask = ds.GetRasterBand(1).GetNoDataValue()
   if noDataValueMask <> None:
      mask = np.ma.masked_where(d == noDataValueMask, mask, copy=False)
   nmy  = mask.shape[0] # number of mask pixels in Y
   nmx  = mask.shape[1] # number of mask pixels in X

   upperIndex = ulY;
   leftIndex  = ulX;
   lowerIndex = ulY+nmy;
   rightIndex = ulX+nmx;
   # Spatial indices to use
   ySlice = slice(upperIndex, lowerIndex)
   xSlice = slice(leftIndex, rightIndex)

   ###################
   # Determine the temportal aggregation function
   ###################
   if aAggregateType == 'AVG':
      taf = np.nanmean
   elif aAggregateType == 'MIN':
      taf = np.nanmin
   elif aAggregateType == 'MAX':
      taf = np.nanmax
   else:
      sys.exit("Error: illegal aggregateType: '" + aAggregateType + "'")

   ###################
   # Determine the spatial aggregation function
   ###################
   # Only average for the moment
   saf = np.nanmean

   ######################
   # Get LTA  data
   ######################
   ltaStart, ltaEnd, ltaLength = sourceInfo.periodLTA(aSource, (aPeriodStart, aPeriodEnd))
   nint = (ltaEnd-ltaStart) % ltaLength + 1 # Number of measurement intervals 

   ltaData = None

   if aReferenceType <> 'NONE':

      if aLtaFileName <> '':
   
         referenceDataset = Dataset(os.path.join(aLtaFileName), 'r')
         key,  value   = referenceDataset.variables.items()[8]  # Index 8 is the Average VI data in a multi-cube
         ltaData       = referenceDataset.variables[key]
   
         if ltaStart <= ltaEnd:
            ltaData = np.ma.array(ltaData[ltaStart:ltaEnd+1, ySlice, xSlice])
         else:
            ltaData = np.ma.array(
                         np.concatenate(
                            [ltaData[ltaStart:, ySlice, xSlice],
                             ltaData[0:ltaEnd+1, ySlice, xSlice]]))
   
         referenceDataset.close()
   
      else:
   
         # There is no precomputed base statistics cube, so compute the base statistics from the base data
         key, value = baseDataset.variables.items()[3]  # Index 3 is the indicator data in a cube
         baseData = baseDataset.variables[key]
   
         # Determine upper and lower bounds for base statistics dates
         ltaStartIndex = lookup(mStartDates, aLtaStartDate)
         ltaEndIndex   = lookup(mStartDates, aLtaEndDate)

         start    = ltaStartIndex + ltaStart
         interval = (ltaStartIndex, start, ltaEndIndex, nint, ltaLength)
         # The aggregation is done after taking the difference
         ltaData =  baseStats.computeLTA(baseData, interval, ySlice, xSlice, mask.mask, 
                                         aIntegrateType, windowLength, aAggregateType, noDataValueCube)
   
   ###################
   # Process the base data 
   ###################
   key, value = baseDataset.variables.items()[3]  # Index 3 is the indicator data in a cube
   baseData   = baseDataset.variables[key]
   interval = (startIndex % ltaLength, startIndex, baseData.shape[0]-1, nint, ltaLength)
   bdata      = baseStats.computeYear(
                   baseData,
                   interval,
                   ySlice,
                   xSlice,
                   mask.mask,
                   ltaData,
                   aIntegrateType,
                   windowLength,
                   aAggregateType,
                   noDataValueCube)

   bdata.mask = mask.mask

   # Reshape to have columsn and rows equal weight in masked array for spatial aggregation
   bdata = bdata.reshape((bdata.shape[0], bdata.shape[1]*bdata.shape[2]))

   # Compute mean over selected area
   bdata = np.nanmean(bdata, axis=1)

   # We're done so close data cubes
   baseDataset.close()

   ###################
   # Create period start and end dates
   ###################
   si = startIndex % ltaLength
   pstart = mStartDates[si::ltaLength]
   # If last interval is not complete, drop it
   if si+ltaLength*(len(pstart)-1)+nint-1 > len(mStartDates):
      pstart = pstart[0:-1]
   pend = []
   for i in range(si+nint-1, len(mStartDates), ltaLength):
      pend.append(sourceInfo.endDate(aSource, mStartDates[i], i == len(mStartDates)-1))
      
   ###################
   # Create JSON return object
   ###################

   ts = OrderedDict()
   ts['type']   = 'TimeSeriesByYear'
   ts['label']  = aLabel
   ts['N']      = len(bdata)
   ts['pstart'] = map(str, pstart)
   ts['pend']   = map(str, pend)
   ts['value']  = map(float, bdata)

   bs = np.percentile(bdata, [0.0, 25.0, 50.0, 75.0, 100.0])
   ts['P00']    = float(bs[0])
   ts['P25']    = float(bs[1])
   ts['P50']    = float(bs[2])
   ts['P75']    = float(bs[3])
   ts['P100']   = float(bs[4])
   ts['Avg']    = float(np.mean(bdata))
   

   # Echo request 
   rq = OrderedDict()
   rq['source']         = aSource
   rq['maskID']         = aMaskID
   rq['indicator']      = aIndicator
   rq['periodStart']    = aPeriodStart.isoformat()
   rq['periodEnd']      = aPeriodEnd.isoformat()
   rq['integrateType']  = aIntegrateType
   rq['windowLength']   = aWindowLength
   rq['aggregateType']  = aAggregateType
   rq['referenceType']  = aReferenceType
   rq['integrateType']  = aIntegrateType
   rq['ltaStartDate']   = aLtaStartDate.isoformat()
   rq['ltaEndDate']     = aLtaEndDate.isoformat()

   returnDict = OrderedDict()
   returnDict['timeseries'] = ts
   returnDict['request']    = rq

   ########################
   # Generate output string
   ########################
   returnString = 'ERROR'
   if aMimeType == 'application/json':
      returnString = json.dumps(returnDict)
   elif aMimeType == 'application/csv':
      # function to output CSV string (wrapper for csv writer)
      def csv2string(data):
          si = io.BytesIO()
          cw = csv.writer(si)
          cw.writerow(data)
          return si.getvalue()
      returnString = ''
      header = [ 'type', 'label', 'N', 'P00', 'P25', 'P50', 'P75', 'P100', 'Avg']
      for r in header:
         returnString = returnString + csv2string([r] + [ts[r]])
      header = [ 'pstart', 'pend', 'value']
      for r in header:
         returnString = returnString + csv2string([r] + ts[r])
      for r in rq.keys():
         returnString = returnString + csv2string([r] + [rq[r]])
   else:
      sys.exit("Illegal aMimeType="+aMimeType)

   print returnString

   return

def usage():
   global gAGGREGATE_TYPE_SET, gREFERENCE_TYPE_SET
   print "Usage:"
   print "python %s [flags] <base-file-name> <lta-file-name> <mask-file-name> <period-begin> <period-end>" % sys.argv[0]
   print ""
   print "The <base-file-name> must be the file path name of a netCDF cube type file."
   print "The <lta-file-name> must be the file path name of a netCDF cube type file."
   print "The <mask-file-name> must be the file path name of a TIFF type file which will be used as mask."
   print "Allowed flag examples:"
   print "--aggregate=AVG      Temporal aggragation, possible values: %s" % gAGGREGATE_TYPE_SET
   print "--reftype=LTA        The reference type. Allowed string values: %s" % gREFERENCE_TYPE_SET
   print "--label='string'     The content of this flag is returned in the output."
   print "--source=<string>    Name of source to be echoed in request field."
   print "--indicator=<string> Name of indicator to be echoed in request field."
   print "--maskID=<string>    Name of mask to be echoed in request field."
   print ""
   print "Example(s):"
   print "python %s /net/satarch/CommonSense/DataCubes/Released/MODIS/cube_EthiopiaNdvi500m.hdf5 /net/satarch/CommonSense/DataCubes/Released/MODIS/multi_cube_EthiopiaNdvi500mStats.hdf5 /tmp/mask.tiff 2010-01-01 2010-04-01" % sys.argv[0]
   print "python %s --label=\"this is the label\" --aggregate=AVG /net/satarch/CommonSense/DataCubes/Released/CHIRPS/cube_EthiopiaRain.hdf5 /net/satarch/CommonSense/DataCubes/Released/MODIS/multi_cube_EthiopiaNdvi500mStats.hdf5 /tmp/mask.tif 2010-01-01 2010-04-01" % sys.argv[0]

def processTimeSeriesByYear(argv):

   global gAGGREGATE_TYPE_SET, gREFERENCE_TYPE_SET, gINTEGRATE_TYPE_SET, gLTA_START_DATE, gLTA_END_DATE

   ######################################################################################################
   # Check and process the input parameters
   ######################################################################################################
   if len(argv) < 4: 
      usage()
      sys.exit("ERROR : not enough arguments (argc=%d)" % len(argv))

   flags_dict = {}

   # Extract all arguments
   mArgCount = 0 # Number of mandatory arguments
   for arg in argv[1:]:
      # Optional arguments
      if arg.startswith('--'):
         l = arg.split('=')
         if len(l) == 2:
            key, value = l
         else:
            key, value = l[0], None
         flags_dict[key] = value
      else:
         # Mandatory arguments
         if   mArgCount == 0:
            baseFileName = arg
            mArgCount += 1
         elif mArgCount == 1:
            ltaFileName = arg
            mArgCount += 1
         elif mArgCount == 2:
            maskFileName = arg
            mArgCount += 1
         elif mArgCount == 3:
            periodStart = datetime.datetime.strptime(arg, "%Y-%m-%d").date()
            mArgCount += 1
         elif mArgCount == 4:
            periodEnd = datetime.datetime.strptime(arg, "%Y-%m-%d").date()
            mArgCount += 1
         else:
            usage()
            print "ERROR : unkown argument '" + arg + "'"

   # Check number of mandatory arguments
   if mArgCount <> 5: usage(); print "ERROR : unexpected mandatory arguments (argc=%d)" % mArgCount

   if periodStart > periodEnd:
      sys.exit("ERROR : periodStart(%s) must be <= periodEnd (%s)." % (periodStart.isoformat(), periodEnd.isoformat()))

   # Initialise optional arguments
   integrate       = 'NONE'
   windowLength    = 0
   aggregateType   = gAGGREGATE_TYPE_SET[0]
   refStartDate    = periodStart
   referenceType   = gREFERENCE_TYPE_SET[0]
   label           = ''
   source          = ''
   indicator       = ''
   maskID          = ''
   mimeType        = 'application/json'
   ltaStartDate    = gLTA_START_DATE
   ltaEndDate      = gLTA_END_DATE

   if not os.path.isfile(baseFileName):
      sys.exit("Error: Input base file " + baseFileName + " does not exist.")

   if not os.path.isfile(maskFileName):
      sys.exit("Error: Input mask file " + maskFileName + " does not exist.")

   ##########################################
   # Check the flags and extract their values
   ##########################################

   for key, value in flags_dict.items():
      if   key == '--refdate':
         refStartDate = datetime.datetime.strptime(value, "%Y-%m-%d").date()
      elif key == '--reftype':
         if value in gREFERENCE_TYPE_SET:
            referenceType = value
         else:
            sys.exit("Error: referenceType ('" + value + "') not valid ('" + str(gREFERENCE_TYPE_SET) + ").")
      elif key == '--aggregate':
         if value in gAGGREGATE_TYPE_SET:
            aggregateType = value
         else:
            sys.exit("Error: aggregate ('" + value + "') not valid ('" + str(gAGGREGATE_TYPE_SET) + ").")
      elif key == '--integrate':
         if value in gINTEGRATE_TYPE_SET:
            integrate = value
      elif key == '--window':
         windowLength = int(value)
      elif key == '--label':
         label = value
      elif key == '--source':
         source = value
      elif key == '--indicator':
         indicator = value
      elif key == '--maskID':
         maskID = value
      elif key == '--mimetype':
         mimeType = str(value)
      elif key == '--lta-start':
         ltaStartDate = datetime.datetime.strptime(value, "%Y-%m-%d").date()
      elif key == '--lta-end':
         ltaEndDate = datetime.datetime.strptime(value, "%Y-%m-%d").date()
      else:
         print "Warning: Key \'" + key + "\' is unknown and ignored."
   
   _processTimeSeriesByYear(
      baseFileName,
      ltaFileName,
      maskFileName,
      aggregateType,
      periodStart,
      periodEnd,
      integrate,
      windowLength,
      referenceType, 
      ltaStartDate,
      ltaEndDate,
      label,
      source,
      indicator, 
      maskID,
      mimeType)

if __name__ == "__main__":
   processTimeSeriesByYear(sys.argv[:])
