#!/usr/bin/env python
##################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
##################################################################################

import math
import re
import os
import sys
import datetime

# Global script identifiers
svn_revision = str('$Rev: 7861 $').replace('$', '').strip(' \t\n\r')
svn_date = re.sub('\) \$', ' ' ,re.sub('^.*\(', 'Date: ', '$Date: 2017-01-27 12:50:14 +0100 (Fri, 27 Jan 2017) $')).strip(' \t\n\r')
svn_author = str('$Author: jb76278 $').replace('$', '').strip(' \t\n\r')
script_file_name = os.path.basename(__file__)
now_string = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

local_path = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, local_path + '/code')
sys.path.insert(0, local_path + '/pyLibs')

#import arrayToPng as arr2png
from netCDF4 import Dataset
from netCdfGeoMetaData import NetCdfGeoMetaData
import numpy as np

# Handling of nan will result in run-time warnings to be printed.
# Prevent these messages as it will fail the process.
import warnings
warnings.filterwarnings('ignore', '.*', RuntimeWarning, '.*nanfunctions.*')

# The GDAL imports for geoTiff (a.o.)
from osgeo import gdal
from osgeo import osr
from osgeo.gdalconst import *

import baseStats
import sourceInfo

# Handling of nan will result in run-time warnings to be printed.
# Prevent these messages as it will fail the process.
import warnings
logWarnings = open('/tmp/' + os.path.basename(__file__) + '.log', 'a')
def customwarn(message, category, filename, lineno, file=None, line=None):
    now = str(datetime.datetime.now())
    logWarnings.write(now + ':' + warnings.formatwarning(message, category, filename, lineno))
warnings.showwarning = customwarn


gINTEGRATE_TYPE_SET  = [ 'NONE', 'SUM', 'DIF' ]
gREFERENCE_TYPE_SET  = [ 'NONE', 'ABS', 'LTA' ]
gAGGREGATE_TYPE_SET  = [ 'AVG', 'MIN', 'MAX']

gLTA_START_DATE = datetime.date(2001,  1,  1)
gLTA_END_DATE   = datetime.date(2014, 12, 31)

#############################
# Return index in days array of largest element less or equal to the selected day
# If no element exists return 0 (index of first element)
#############################
def lookup(dataArray, value):
   return max(np.where(dataArray <= value, range(dataArray.shape[0]), 0))

######################################################################################################
# Method will extract the requested map data and put it in a geoTiff file
######################################################################################################
def processPeriodAggregateMap(aOutputFileName, aBaseFileName, aLtaFileName, aRoiFileName, aIndicatorIsLTA, aPeriodStart, aPeriodEnd, aAggregateType, aReferenceType, aRefStartDate, aIntegrateType, aWindowLength, aLtaStartDate, aLtaEndDate, aSource, aIndicator, aMaskID, aLabel):
   global svn_revision, svn_date, svn_author, now_string, script_file_name

   if (aReferenceType <> 'NONE') and (aRefStartDate == None):
      sys.exit("ERROR : reference type <> NONE but no reference date set.")

   # Objects for the meta data of the input
   metaData    = NetCdfGeoMetaData()
   roiMetaData = NetCdfGeoMetaData()

   ###################
   # Open the base file and the long-time-average file for reference if needed
   ###################
   if aIndicatorIsLTA and aLtaFileName <> '':
      baseDataset = Dataset(os.path.join(aLtaFileName), 'r')
   else:
      baseDataset = Dataset(os.path.join(aBaseFileName), 'r')

   metaData.copyFromNetCdfObject(baseDataset)
   latSize  = len(baseDataset.dimensions['latitude'])
   longSize = len(baseDataset.dimensions['longitude'])

   noDataValueCube = None
   if metaData.getNoDataValue() <> '<None>':
      noDataValueCube = float(metaData.getNoDataValue())
   
   ###################
   # Retrieve the ROI mask and bounding box indices from the geotif file
   ###################
   roiFile = gdal.Open(aRoiFileName)
   roiMetaData.copyFromGdalData(roiFile)
   upperLat, leftLong = roiMetaData.getLatLongFromPixelYx(0,0)
   xOrg, xRes = metaData.getLongitudeTransform()
   yOrg, yRes = metaData.getLatitudeTransform()
   # Take mid of upper left mask pixel to prevent precision errors.
   upperIndex,leftIndex = metaData.getPixelYxFromLatLong(upperLat+0.5*yRes, leftLong+0.5*xRes)
   lowerIndex = upperIndex + roiFile.RasterYSize
   rightIndex = leftIndex  + roiFile.RasterXSize

   upperIndex = min(max(0, upperIndex), latSize-1)
   leftIndex  = min(max(0, leftIndex), longSize-1)
   lowerIndex = min(max(0, lowerIndex), latSize-1)
   rightIndex = min(max(0, rightIndex), longSize-1)

   ySlice = slice(upperIndex, lowerIndex)
   xSlice = slice(leftIndex,rightIndex)

   # Create true/false array from roi-tif-file to use as mask later.
   noDataValueMask  = roiFile.GetRasterBand(1).GetNoDataValue()
   roiMaskDataArray = np.ma.array(roiFile.GetRasterBand(1).ReadAsArray())
   roiMaskDataArray = np.ma.masked_where(roiMaskDataArray == noDataValueMask, roiMaskDataArray, copy=False)
   roiMaskDataArray = np.ma.masked_where(roiMaskDataArray <= 0.0, roiMaskDataArray, copy=False)
   del roiFile

   # Adjust the meta data to the new reference (to be used for the geotiff output)
   metaData.adjustPixelOriginGeoTransform(upperIndex, leftIndex)

   ###################
   # Retrieve the time indices for the base datacube to start with
   ###################
   timeArray = baseDataset.variables['time'][:]
   
   if aIndicatorIsLTA and aLtaFileName <> '':
      # The periods for LTA are in DOY, so year is don't care as long as it is no leap year
      epoch = datetime.date(1900, 1, 1)
      # The time axis of MODIS is not consistent with CHIRPS. Make sure they all start with 0.
      if timeArray[0] <> 0:
         timeArray = timeArray - timeArray[0]
   else:
      # Offset to compute start and end dates for periods
      epoch = metaData.getDate()

   ###################
   # Determine indices for dates
   ###################
   startDay = max(0, (aPeriodStart - epoch).days)
   endDay   = min(timeArray[-1], (aPeriodEnd - epoch).days)
   startIndex = lookup(timeArray, startDay)
   endIndex   = lookup(timeArray, endDay)

   ###################
   # Determine size of the integration window
   ###################
   windowLength = int(math.ceil(float(aWindowLength)/sourceInfo.measurementInterval(aSource)))
   if windowLength == 0 :
      if aIntegrateType == 'DIF': windowLength = 1
      if aIntegrateType == 'SUM': windowLength = endIndex - startIndex + 1
   windowLength = max(1, windowLength)

   if aIndicatorIsLTA:

      ltaStart, ltaEnd, ltaLength = sourceInfo.periodLTA(aSource, (aPeriodStart, aPeriodEnd))

      if aLtaFileName <> '':

         key, value = baseDataset.variables.items()[8]  # Index for average in LTA datacube
         aggregateData = baseDataset.variables[key]

         startIndex = ltaStart
         endIndex   = ltaEnd
         if startIndex <= endIndex:
            aggregateData = aggregateData[startIndex:endIndex+1, ySlice, xSlice]
         else:
            aggregateData = np.concatenate(
                               [aggregateData[startIndex:, ySlice, xSlice],
                                aggregateData[0:endIndex+1, ySlice, xSlice]
                               ])
      else:
         # There is no precomputed base statistics cube, so compute the base statistics from the base data
         key, value = baseDataset.variables.items()[3]  # Index 3 is the indicator data in a cube
         baseData = baseDataset.variables[key]

         # Determine upper and lower bounds for base statistics dates
         ltaStartIndex = lookup(timeArray, (aLtaStartDate-epoch).days)
         ltaEndIndex   = lookup(timeArray, (aLtaEndDate-epoch).days)

         startIndex = ltaStartIndex+ltaStart
         endIndex   = ltaStartIndex+ltaEnd
         interval   = (ltaStartIndex, startIndex, ltaEndIndex, (ltaEnd-ltaStart) % ltaLength + 1, ltaLength)
         aggregateData = baseStats.computeLTA(baseData, interval, ySlice, xSlice, roiMaskDataArray.mask, 
                                              aIntegrateType, windowLength, aAggregateType, noDataValueCube)

   else:
      key, value = baseDataset.variables.items()[3]  # Index 3 is the VI data in a cube
      aggregateData = baseDataset.variables[key][startIndex:endIndex+1, ySlice, xSlice]


   # Start and end dates to be echoed back to caller
   startDate       = epoch + datetime.timedelta(int(timeArray[startIndex]))
   isLastInterval  = endIndex == timeArray.shape[0]
   endDate         = sourceInfo.endDate(aSource, epoch + datetime.timedelta(int(timeArray[endIndex])), isLastInterval)

   ########################
   # Get LTA data
   ########################

   if aReferenceType == 'LTA':

      ltaStart, ltaEnd, ltaLength = sourceInfo.periodLTA(aSource, (aPeriodStart, aPeriodEnd), aRefStartDate)

      if aLtaFileName <> '':

         referenceDataset = Dataset(os.path.join(aLtaFileName), 'r')
         key,  value      = referenceDataset.variables.items()[8]  # Index 8 is the Average VI data in a multi-cube
         ltaData          = referenceDataset.variables[key]

         if ltaStart <= ltaEnd:
            referenceData = np.ma.array(ltaData[ltaStart:ltaEnd+1, ySlice, xSlice])
         else:
            referenceData = np.ma.array(
                               np.concatenate(
                                  [ltaData[ltaStart:, ySlice, xSlice], 
                                   ltaData[0:ltaEnd+1, ySlice, xSlice]]))

         if aIntegrateType == 'DIF':
            referenceData = np.pad(referenceData, ((1,0), (0,0), (0,0)), mode='edge')
            referenceData = np.diff(referenceData, axis=0)

         if aIntegrateType <> 'NONE' and windowLength > 1:
            window = np.ones(windowLength)
            referenceData = np.apply_along_axis(np.convolve, 0, referenceData, window, mode='full')
            referenceData = referenceData[0:-wLength+1]

         if aAggregateType == 'MIN':
            referenceData = np.nanmin(referenceData, axis=0)
         elif aAggregateType == 'MAX':
            referenceData = np.nanmax(referenceData, axis=0)
         else:
            # aAggregateType == 'AVG':
            referenceData = np.nanmean(referenceData, axis=0)

      else:
         # There is no precomputed base statistics cube, so compute the base statistics from the base data
         key, value = baseDataset.variables.items()[3]  # Index 3 is the indicator data in a cube
         baseData = baseDataset.variables[key]

         # Determine upper and lower bounds for base statistics dates
         ltaStartIndex = lookup(timeArray, (aLtaStartDate-epoch).days)
         ltaEndIndex   = lookup(timeArray, (aLtaEndDate-epoch).days)

         start = lookup(timeArray, (aRefStartDate-epoch).days) % ltaLength

         interval = (ltaStartIndex, start, ltaEndIndex, (ltaEnd-ltaStart) % ltaLength + 1, ltaLength)
         referenceData =  baseStats.computeLTA(baseData, interval, ySlice, xSlice, roiMaskDataArray.mask, 
                                               aIntegrateType, windowLength, aAggregateType, noDataValueCube)

   # Situation: ABSolute reference type and Reference Start date is set
   elif aReferenceType == 'ABS':

      refStartDay = (aRefStartDate - metaData.getDate()).days
      if refStartDay < 0: sys.exit("Error: Reference start date " + str(aRefStartDate) + " not in time axis range.")
      refStartIndex = lookup(timeArray, refStartDay)
      refEndIndex   = refStartIndex + (endIndex - startIndex)
      if refEndIndex > len(timeArray):
         sys.exit("Error: Reference end date beyond last measurement date. " + 
                  "Index is " + str(refEndIndex) + " while max index is: " + str(len(bDaysArray) - 1))

      referenceData = baseDataset.variables[key][refStartIndex:refEndIndex+1, ySlice, xSlice]

   baseDataset.close()

   ###################
   # Process base data
   ###################
   aggregateGrid = []
   # The integration and aggregation for LTA is done in the LTA part.
   if not (aIndicatorIsLTA and aLtaFileName == ''):

      if aIntegrateType == 'DIF':
         aggregateData = np.pad(aggregateData, ((1,0), (0,0), (0,0)), mode='edge')
         aggregateData = np.diff(aggregateData, axis=0)
   
      if aIntegrateType <> 'NONE' and windowLength > 1:
         window = np.ones(windowLength)
         aggregateData = np.apply_along_axis(np.convolve, 0, aggregateData, window, mode='full')
         aggregateData = aggregateData[0:-windowLength+1]

      if aAggregateType == 'MIN':
         aggregateData = np.nanmin(aggregateData, axis=0)
      elif aAggregateType == 'MAX':
         aggregateData = np.nanmax(aggregateData, axis=0)
      else:
         # aAggregateType == 'AVG':
         aggregateData = np.nanmean(aggregateData, axis=0)

   ###########################################
   # Process reference data when not using LTA
   ###########################################
   # The integration and aggregation for LTA reference 
   # is done in the LTA part.
   if aReferenceType == 'ABS':

      if aIntegrateType == 'DIF':
         referenceData = np.pad(referenceData, ((1,0), (0,0), (0,0)), mode='edge')
         referenceData = np.diff(referenceData, axis=0)

      if aIntegrateType <> 'NONE' and windowLength > 1:
         window = np.ones(windowLength)
         referenceData = np.apply_along_axis(np.convolve, 0, referenceData, window, mode='full')
         referenceData = referenceData[0:-windowLength+1]

      if aAggregateType == 'MIN':
         referenceData = np.nanmin(referenceData, axis=0)
      elif aAggregateType == 'MAX':
         referenceData = np.nanmax(referenceData, axis=0)
      else:
         # aAggregateType == 'AVG':
         referenceData = np.nanmean(referenceData, axis=0)

   ###################
   # Calculate the resulting grid
   ###################
   resultGrid = []
   if aReferenceType <> 'NONE':
      resultGrid = (aggregateData - referenceData)
   else:
      resultGrid = np.copy(aggregateData)

   ###################
   # Set 'not-a-number' values, based on 'no-a-number' values and the selected ROI
   ###################
   if noDataValueCube <> None:
      roiMaskDataArray = np.ma.masked_where(np.isnan(resultGrid), roiMaskDataArray, copy=False)
      resultGrid[np.where(roiMaskDataArray.mask)] = noDataValueCube

   ###################
   # Put the result grid in a geotiff
   ###################
   driver = gdal.GetDriverByName('GTiff')
   dataset = driver.Create(aOutputFileName, resultGrid.shape[1], resultGrid.shape[0], 1, gdal.GDT_Float32)
   dataset.SetMetadataItem('TIFFTAG_SOFTWARE', aLabel)
   dataset.SetMetadataItem('COMMON_SENSE', aLabel)
   # Configure the projection
   proj = osr.SpatialReference()
   proj.SetWellKnownGeogCS("WGS84")
   dataset.SetProjection(proj.ExportToWkt())
   latOrigin, latPixSize = metaData.getLatitudeTransform()
   longOrigin, longPixSize = metaData.getLongitudeTransform()
   geotransform = (longOrigin, longPixSize, 0, latOrigin, 0, latPixSize)
   dataset.SetGeoTransform(geotransform)  
   dataset.GetRasterBand(1).WriteArray(resultGrid)
   if noDataValueCube <> None: dataset.GetRasterBand(1).SetNoDataValue(noDataValueCube)
   dataset.FlushCache()  # Write to disk.
   dataset = None

   return (startDate, endDate)

def usage():
   print "Usage:"
   print "python %s [flags] <output-file-name> <base-file-name> <lta-file-name> <roi-polygon-geotif-file> <period-begin> <period-end>" % sys.argv[0]
   print ""
   print "The base file must be a netCDF cube type file!"
   print "Allowed flag examples:"
   print "--aggregate=AVG The diff type flag can have 4 possible values: AVG MIN MAX or DELTA. The default value (if flag is omitted) is AVG."
   print "--refdate=2012-01-01 The reference period begin date. (The reference period end date is derived from the period length.)"
   print "--reftype=LTA The reference type. Allowed values: ABS and LTA. ABS takes reference within the (smoothed) data, LTA takes reference from long time average values."
   print "--normalize=True If this flag is set then the output is given as a normalized wrt the reference. Otherwise the absolute values are given."
   print "--label The content of this flag is returned in the output."
   print ""
   print "Example(s):"
   print "python %s ./output.tif /net/satarch/CommonSense/DataCubes/Released/MODIS/base-500m-NDVI.hdf5 /net/satarch/CommonSense/DataCubes/Released/MODIS/lta-500m-NDVI.hdf5 /tmp/roi.tif 2015-03-01 2015-04-01" % sys.argv[0]
   print ""

######################################################################################################
# Check and process the input parameters
######################################################################################################
def main(argv):

   global gREFERENCE_TYPE_SET, gINTEGRATE_TYPE_SET, gAGGREGATE_TYPE_SET, gLTA_START_DATE, gLTA_END_DATE

   if len(sys.argv) < 6:
      usage()
      sys.exit("ERROR unexpected number of arguments (argc=%d)" % len(sys.argv))

   flags_dict     = {}

   # Initialise defaults
   outputFileName = ''
   baseFileName   = ''
   ltaFileName    = ''
   roiFileName    = ''
   periodStart    = None
   periodEnd      = None
   aggregateType  = 'AVG'
   refStartDate   = None
   referenceType  = 'NONE'
   integrate      = 'NONE'
   ltaStartDate   = gLTA_START_DATE
   windowLength   = 0
   ltaEndDate     = gLTA_END_DATE
   label          = ''
   indicatorIsLTA = False
   source         = ''
   indicator      = ''
   maskID         = ''

   # Extract all arguments
   mArgCount = 0 # Number of mandatory arguments
   for arg in argv[1:]:
      # Optional arguments
      if arg.startswith('--'):
         l = arg.split('=')
         if len(l) == 2:
            key, value = l
         else:
            key, value = l[0], None
         flags_dict[key] = value
      else:
         # Mandatory arguments
         if   mArgCount == 0:
            outputFileName = arg
            mArgCount += 1
         elif mArgCount == 1:
            baseFileName = arg
            mArgCount += 1
         elif mArgCount == 2:
            ltaFileName = arg
            mArgCount += 1
         elif mArgCount == 3:
            roiFileName = arg
            mArgCount += 1
         elif mArgCount == 4:
            periodStart = datetime.datetime.strptime(arg, "%Y-%m-%d").date()
            mArgCount += 1
         elif mArgCount == 5:
            periodEnd = datetime.datetime.strptime(arg, "%Y-%m-%d").date()
            mArgCount += 1
         else:
            usage("ERROR : unkown argument '" + arg + "'")

   # Check the flags and extract their values
   for key, value in flags_dict.items():
      if key == '--aggregate':
         if value in gAGGREGATE_TYPE_SET:
            aggregateType = value
         else:
            sys.exit("Error: Value \'" + value + "\' for Key \'" + key + "\' is not valid.")
      elif key == '--refdate':
         refStartDate = datetime.datetime.strptime(value, "%Y-%m-%d").date()
      elif key == '--reftype':
         # If base data is LTA only LTA is allowed as reference
         if baseFileName == '': gREFERENCE_TYPE_SET = [ 'LTA' ]
         if value in gREFERENCE_TYPE_SET:
            referenceType = value
         else:
            sys.exit("Error: Value \'" + value + "\' for Key \'" + key + "\' is not valid.")
      elif key == '--integrate':
         if value in gINTEGRATE_TYPE_SET:
            integrate = value
         else:
            sys.exit("Error: integrate ('" + value + "') not valid ('" + str(gINTEGRATE_TYPE_SET) + ").")
      elif key == '--lta':
         indicatorIsLTA = True
      elif key == '--label':
         label = str(value)
      elif key == '--window':
         windowLength = int(value)
      elif key == '--source':
         source = value
      elif key == '--indicator':
         indicator = value
      elif key == '--maskID':
         maskID = value
      elif key == '--lta-start':
         ltaStartDate = datetime.datetime.strptime(value, "%Y-%m-%d").date()
      elif key == '--lta-end':
         ltaEndDate = datetime.datetime.strptime(value, "%Y-%m-%d").date()
      else:
         print "Warning: Key \'" + key + "\' is unknown and ignored."
      
   processPeriodAggregateMap(
      outputFileName, 
      baseFileName, 
      ltaFileName, 
      roiFileName, 
      indicatorIsLTA, 
      periodStart, 
      periodEnd, 
      aggregateType, 
      referenceType, 
      refStartDate, 
      integrate,
      windowLength,
      ltaStartDate,
      ltaEndDate,
      source,
      indicator, 
      maskID,
      label)

######################################################################################################
# Only execute when run as a script
######################################################################################################
if __name__ == "__main__":
   main(sys.argv[:])

