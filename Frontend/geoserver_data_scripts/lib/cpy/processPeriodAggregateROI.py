#!/usr/bin/env python
##################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
##################################################################################

import re
import os
import sys
import datetime
import uuid

# Global script identifiers
svn_revision = str('$Rev: 7882 $').replace('$', '').strip(' \t\n\r')
svn_date = re.sub('\) \$', ' ' ,re.sub('^.*\(', 'Date: ', '$Date: 2017-02-01 09:19:23 +0100 (Wed, 01 Feb 2017) $')).strip(' \t\n\r')
svn_author = str('$Author: jb76278 $').replace('$', '').strip(' \t\n\r')
script_file_name = os.path.basename(__file__)
now_string = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

local_path = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, local_path + '/code')
sys.path.insert(0, local_path + '/pyLibs')

#import arrayToPng as arr2png
import json
import numpy as np
from collections import OrderedDict

# The GDAL imports for geoTiff (a.o.)
from osgeo import gdal
from osgeo import osr
from osgeo.gdalconst import *

import processPeriodAggregateMap
import haversine

# Handling of nan will result in run-time warnings to be printed.
# Prevent these messages as it will fail the process.
import warnings
logWarnings = open('/tmp/' + os.path.basename(__file__) + '.log', 'a')
def customwarn(message, category, filename, lineno, file=None, line=None):
    now = str(datetime.datetime.now())
    logWarnings.write(now + ':' + warnings.formatwarning(message, category, filename, lineno))
warnings.showwarning = customwarn


gREFERENCE_TYPE_SET = [ 'NONE', 'ABS', 'LTA']
gAGGREGATE_TYPE_SET = [ 'AVG', 'MIN', 'MAX']
gINTEGRATE_TYPE_SET = [ 'NONE', 'SUM', 'DIF' ]

gLTA_START_DATE = datetime.date(1900, 1, 1)
gLTA_END_DATE = datetime.date(2100, 1, 1)

def _computeArea(gt, d):
   resX, resY = gt[1], gt[5]
   orgX, orgY = gt[0], gt[3]
   nPixelsX = np.ma.count(d, axis=1)
   rY       = np.array(range(0, nPixelsX.shape[0]))
   # Y range values. Take middle of pixel to average longitudal 
   # distance for upper and lower boundary of pixel.
   rY       = orgY + rY * resY + 0.5*resY
   # Compute distance for 1 degree in lattitude
   dLat     = haversine.distance((0.0, 0.0), (1.0, 0.0))
   # Compute distance for 1 degree in longitude for each lattitude
   dLon     = np.vectorize(lambda l : haversine.distance((l,0), (l,1.0)))(rY)
   area     = nPixelsX * abs(resX) * abs(resY) * dLat * dLon
   return np.sum(area)

###############################################################################################
# Method will extract the requested map data and put it in a geoTiff file
##############################################################################################
def processPeriodAggregateROI(aBaseFileName, aLtaFileName, aMaskFileName, aIndicatorIsLTA, aAggregateType, aPeriodStart, aPeriodEnd, aReferenceType, aReferenceStart, aIntegrate, aWindowLength, aLtaStartDate, aLtaEndDate, aLabel, aSource, aIndicator, aMaskID):

   pstart = datetime.date(2000, 1, 1)
   pend   = datetime.date(2000, 1, 1)
   mapFileName = "/tmp/" + 'map-' + str(uuid.uuid4()) + ".tif"

   pstart, pend = processPeriodAggregateMap.processPeriodAggregateMap(
                     mapFileName,
                     aBaseFileName,
                     aLtaFileName,
                     aMaskFileName,
                     aIndicatorIsLTA,
                     aPeriodStart,
                     aPeriodEnd,
                     aAggregateType,
                     aReferenceType,
                     aReferenceStart, 
                     aIntegrate,
                     aWindowLength,
                     aLtaStartDate,
                     aLtaEndDate,
                     aSource,
                     aIndicator, 
                     aMaskID, 
                     aLabel)

   ###################
   # Retrieve the bounding box indices for the datacube to start with
   ###################
   # Get Cube GeoTransform 
   ds  = gdal.Open(mapFileName)
   d   = ds.ReadAsArray()
   noDataValue = ds.GetRasterBand(1).GetNoDataValue()

   # Create masked array because we do not want to aggregate over NaN and NoData values
   d = np.ma.masked_array(d)
   d = np.ma.masked_invalid(d, copy=False)
   if noDataValue <> None:
      d = np.ma.masked_where(d == noDataValue, d, copy=False)

   l = d.compressed()
   bs = np.percentile(l, [0.0, 25.0, 50.0, 75.0, 100.0])
   bs = [float(np.average(l)), float(np.sum(l))] + bs.tolist()

   # Compute area
   gt   = ds.GetGeoTransform()
   area = round(_computeArea(gt, d), 2)

   ###################
   # Create JSON return object
   ###################

   result = OrderedDict()
   result['type']   = 'PeriodAggregateROI'
   result['label']  = aLabel
   if aIndicatorIsLTA:
      result['pstart'] = pstart.timetuple().tm_yday
      result['pend']   = pend.timetuple().tm_yday
   else:
      result['pstart'] = pstart.isoformat()
      result['pend']   = pend.isoformat()
   result['mean']   = bs[0]
   result['sum']    = bs[1]
   result['P00']    = bs[2]
   result['P25']    = bs[3]
   result['P50']    = bs[4]
   result['P75']    = bs[5]
   result['P100']   = bs[6]
   result['area']   = area
   result['N']      = np.ma.count(d)

   # Echo request 
   rq = OrderedDict()
   rq['source']         = aSource
   rq['maskID']         = aMaskID
   rq['indicator']      = aIndicator
   rq['periodStart']    = aPeriodStart.isoformat()
   rq['periodEnd']      = aPeriodEnd.isoformat()
   rq['integrateType']  = aIntegrate
   rq['windowLength']   = aWindowLength
   rq['aggregateType']  = aAggregateType
   rq['referenceType']  = aReferenceType
   rq['referenceDate']  = aReferenceStart.isoformat()
   rq['ltaStartDate']   = aLtaStartDate.isoformat()
   rq['ltaEndDate']     = aLtaEndDate.isoformat()

   result['request']    = rq


   ########################
   # Generate output string
   ########################
   returnString = json.dumps(result)
   print returnString

   # Remove map file
   os.remove(mapFileName)

   return

def usage():
   global gAGGREGATE_TYPE_SET, gREFERENCE_TYPE_SET
   print "Usage:"
   print "python %s [flags] <base-file-name> <lta-file-name> <mask-file-name> <period-begin> <period-end>" % sys.argv[0]
   print ""
   print "The <base-file-name> must be the file path name of a netCDF cube type file."
   print "The <lta-file-name> must be the file path name of a netCDF cube type file."
   print "The <mask-file-name> must be the file path name of a TIFF type file which will be used as mask."
   print "Allowed flag examples:"
   print "--aggregate=AVG      Temporal aggragation, possible values: %s" % gAGGREGATE_TYPE_SET
   print "--reftype=LTA        The reference type. Allowed string values: %s" % gREFERENCE_TYPE_SET
   print "--label='string'     The content of this flag is returned in the output."
   print "--source=<string>    Name of source to be echoed in request field."
   print "--indicator=<string> Name of indicator to be echoed in request field."
   print "--maskID=<string>    Name of mask to be echoed in request field."
   print ""
   print "Example(s):"
   print "python %s /net/satarch/CommonSense/DataCubes/Released/MODIS/cube_EthiopiaNdvi500m.hdf5 /net/satarch/CommonSense/DataCubes/Released/MODIS/multi_cube_EthiopiaNdvi500mStats.hdf5 /tmp/mask.tiff 2010-01-01 2010-04-01" % sys.argv[0]
   print "python %s --label=\"this is the label\" --aggregate=AVG /net/satarch/CommonSense/DataCubes/Released/CHIRPS/cube_EthiopiaRain.hdf5 /net/satarch/CommonSense/DataCubes/Released/MODIS/multi_cube_EthiopiaNdvi500mStats.hdf5 /tmp/mask.tif 2010-01-01 2010-04-01" % sys.argv[0]

def main(argv):

   global gINTEGRATE_TYPE_SET, gAGGREGATE_TYPE_SET, gREFERENCE_TYPE_SET 

   ######################################################################################################
   # Check and process the input parameters
   ######################################################################################################
   if len(argv) < 4: 
      usage()
      sys.exit("ERROR : not enough arguments (argc=%d)" % len(argv))

   flags_dict = {}

   # Extract all arguments
   mArgCount = 0 # Number of mandatory arguments
   for arg in argv[1:]:
      # Optional arguments
      if arg.startswith('--'):
         l = arg.split('=')
         if len(l) == 2:
            key, value = l
         else:
            key, value = l[0], None
         flags_dict[key] = value
      else:
         # Mandatory arguments
         if   mArgCount == 0:
            baseFileName = arg
            mArgCount += 1
         elif mArgCount == 1:
            ltaFileName = arg
            mArgCount += 1
         elif mArgCount == 2:
            maskFileName = arg
            mArgCount += 1
         elif mArgCount == 3:
            periodStart = datetime.datetime.strptime(arg, "%Y-%m-%d").date()
            mArgCount += 1
         elif mArgCount == 4:
            periodEnd = datetime.datetime.strptime(arg, "%Y-%m-%d").date()
            mArgCount += 1
         else:
            usage("ERROR : unkown argument '" + arg + "'")

   # Check number of mandatory arguments
   if mArgCount <> 5: usage(); print "ERROR : unexpected mandatory arguments (argc=%d)" % mArgCount; sys.exit(-1)

   if periodStart > periodEnd:
      sys.exit("ERROR : periodStart(%s) must be <= periodEnd (%s)." % (periodStart.isoformat(), periodEnd.isoformat()))

   # Initialise optional arguments
   aggregateType      = gAGGREGATE_TYPE_SET[0]
   referenceStart     = periodStart
   referenceType      = 'NONE'
   indicatorIsLTA     = False
   integrate          = 'NONE'
   windowLength       = 0
   ltaStartDate       = gLTA_START_DATE
   ltaStartDate       = gLTA_END_DATE
   label              = ''
   source             = ''
   indicator          = ''
   maskID             = ''

   if not os.path.isfile(baseFileName):
      sys.exit("Error: Input base file " + baseFileName + " does not exist.")

   if ltaFileName <> '' and not os.path.isfile(ltaFileName):
      sys.exit("Error: Input lta  file " + ltaFileName + " does not exist.")

   if not os.path.isfile(maskFileName):
      sys.exit("Error: Input mask file " + maskFileName + " does not exist.")

   ##########################################
   # Check the flags and extract their values
   ##########################################

   for key, value in flags_dict.items():
      if   key == '--refdate':
         referenceStart = datetime.datetime.strptime(value, "%Y-%m-%d").date()
      elif key == '--reftype':
         if value in gREFERENCE_TYPE_SET:
            referenceType = value
         else:
            sys.exit("Error: referenceType ('" + value + "') not valid ('" + str(gREFERENCE_TYPE_SET) + ").")
      elif key == '--aggregate':
         if value in gAGGREGATE_TYPE_SET:
            aggregateType = value
         else:
            sys.exit("Error: aggregate ('" + value + "') not valid ('" + str(gAGGREGATE_TYPE_SET) + ").")
      elif key == '--integrate':
         if value in gINTEGRATE_TYPE_SET:
            integrate = value
         else:
            sys.exit("Error: deltatime ('" + value + "') not valid ('" + str(gINTEGRATE_TYPE_SET) + ").")
      elif key == '--lta':
         indicatorIsLTA = True
      elif key == '--window':
         windowLength = int(value)
      elif key == '--label':
         label = value
      elif key == '--source':
         source = value
      elif key == '--indicator':
         indicator = value
      elif key == '--maskID':
         maskID = value
      elif key == '--lta-start':
         ltaStartDate = datetime.datetime.strptime(value, "%Y-%m-%d").date()
      elif key == '--lta-end':
         ltaEndDate = datetime.datetime.strptime(value, "%Y-%m-%d").date()
      else:
         print "Warning: Key \'" + key + "\' is unknown and ignored."
   
   processPeriodAggregateROI(
      baseFileName,
      ltaFileName,
      maskFileName,
      indicatorIsLTA,
      aggregateType,
      periodStart,
      periodEnd,
      referenceType, 
      referenceStart, 
      integrate,
      windowLength,
      ltaStartDate,
      ltaEndDate,
      label,
      source,
      indicator, 
      maskID)

if __name__ == "__main__":
   main(sys.argv[:])
