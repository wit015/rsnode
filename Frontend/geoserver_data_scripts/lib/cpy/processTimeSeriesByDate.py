#!/usr/bin/env python
###############################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
###############################################################################

import math
import re
import os
import sys
import datetime
import csv, io

# Global script identifiers
svn_revision = str('$Rev: 7861 $').replace('$', '').strip(' \t\n\r')
svn_date = re.sub('\) \$', ' ' ,re.sub('^.*\(', 'Date: ', '$Date: 2017-01-27 12:50:14 +0100 (Fri, 27 Jan 2017) $')).strip(' \t\n\r')
svn_author = str('$Author: jb76278 $').replace('$', '').strip(' \t\n\r')
script_file_name = os.path.basename(__file__)
now_string = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

local_path = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, local_path + '/code')
sys.path.insert(0, local_path + '/pyLibs')

from netCDF4 import Dataset
from netCdfGeoMetaData import NetCdfGeoMetaData
import numpy as np

# Handling of nan will result in run-time warnings to be printed.
# Prevent these messages as it will fail the process.
import warnings
logWarnings = open('/tmp/' + os.path.basename(__file__) + '.log', 'a')
def customwarn(message, category, filename, lineno, file=None, line=None):
    now = str(datetime.datetime.now())
    logWarnings.write(now + ':' + warnings.formatwarning(message, category, filename, lineno))
warnings.showwarning = customwarn

import json
from collections import OrderedDict
import calendar
import psutil

# The GDAL imports for geoTiff (a.o.)
from osgeo import gdal
from osgeo import osr
from osgeo.gdalconst import *

import baseStats
import sourceInfo


gINTEGRATE_TYPE_SET = [ 'NONE', 'DIF', 'SUM' ]
gREFERENCE_TYPE_SET = [ 'NONE', 'ABS', 'LTA' ]
gMIME_TYPE_SET      = [ 'application/json', 'application/csv' ]


def print_memory_usage(line):
    rss = psutil.Process(os.getpid()).memory_info().rss
    print('At line {line}: current memory used: {rss}'.format(rss=rss, line=line))


def world2Pixel(geoMatrix, x, y):
  ulX = geoMatrix[0]
  ulY = geoMatrix[3]
  xDist = geoMatrix[1]
  yDist = geoMatrix[5]
  rtnX = geoMatrix[2]
  rtnY = geoMatrix[4]
  pixel = int((x - ulX) / xDist)
  line  = int((y - ulY) / yDist)
  return (pixel, line)

def formatResult(ts, rq, mimeType):

   returnString = ''
   if mimeType == 'application/json':
      returnString = json.dumps({ 'timeseries' : ts, 'request' : rq })
   elif mimeType == 'application/csv':
      # function to output CSV string (wrapper for csv writer)
      def csv2string(data):
          si = io.BytesIO()
          cw = csv.writer(si)
          cw.writerow(data)
          return si.getvalue()
      header = [ 'type', 'label', 'N', 'min', 'max']
      for r in header:
         returnString = returnString + csv2string([r] + [ts[r]])
      header = [ 'pstart', 'pend', 'value', 'P00', 'P25', 'P50', 'P75', 'P100', 'Avg' ]
      for r in header:
         returnString = returnString + csv2string([r] + ts[r])
      for r in rq.keys():
         returnString = returnString + csv2string([r] + [rq[r]])

   return returnString


def jsonTimeSeries(aLabel, timeSeriesData, bsdata, pstart, pend):
   ts = OrderedDict()
   ts['type']   = 'TimeSeriesByDate'
   ts['label']  = aLabel
   ts['N']      = timeSeriesData.shape[1]
   ts['pstart'] = pstart.tolist()
   ts['pend']   = pend.tolist()
   if timeSeriesData.shape[0]>0:
      r = np.array(range(timeSeriesData.shape[1]))
      r = r % bsdata.shape[1]
      # Take data from masked array otherwise Nan ends up as Null in output
      ts['value']  = timeSeriesData[2].data.tolist()
      ts['min']    = float(np.nanmin(timeSeriesData[2]))
      ts['max']    = float(np.nanmax(timeSeriesData[2]))
      # The LTA and percentile data
      ts['P00']  = bsdata[0,r].tolist()
      ts['P25']  = bsdata[1,r].tolist()
      ts['P50']  = bsdata[2,r].tolist()
      ts['P75']  = bsdata[3,r].tolist()
      ts['P100'] = bsdata[4,r].tolist()
      ts['Avg']  = bsdata[5,r].tolist()
   else:
      ts['value']  = []
      ts['min']    = []
      ts['max']    = []
      # The LTA and percentile data
      ts['P00']  = []
      ts['P25']  = []
      ts['P50']  = []
      ts['P75']  = []
      ts['P100'] = []
      ts['Avg']  = []
   return ts

def jsonRequest(aSource, aMaskID, aIndicator, aPeriodStart, aPeriodEnd, aReferenceType, aRefStartDate, aIntegrateType, aWindowLength, aLtaStartDate, aLtaEndDate):
   rq = OrderedDict()
   rq['source']         = aSource
   rq['maskID']         = aMaskID
   rq['indicator']      = aIndicator
   rq['periodStart']    = aPeriodStart.isoformat()
   rq['periodEnd']      = aPeriodEnd.isoformat()
   rq['referenceType']  = aReferenceType
   rq['referenceStart'] = aRefStartDate.isoformat()
   rq['integrateType']  = aIntegrateType
   rq['windowLength']   = aWindowLength
   rq['ltaStartDate']   = aLtaStartDate.isoformat()
   rq['ltaEndDate']     = aLtaEndDate.isoformat()
   return rq

#############################
# Return index in days array of largest element less or equal to the selected day
# If no element exists return 0 (index of first element)
#############################
def _lookup(dataArray, value):
   return max(np.where(dataArray <= value, range(dataArray.shape[0]), 0))

######################################################################################################
# Method will extract the requested map data and put it in a geoTiff file
######################################################################################################
def processTimeSeriesDate(aBaseFileName, aLtaFileName, aRoiFileName, aPeriodStart, aPeriodEnd, aRefStartDate, aReferenceType, aIntegrateType, aWindowLength, aLtaStartDate, aLtaEndDate, aLabel, aSource, aIndicator, aMaskID, aMimeType):

   global svn_revision, svn_date, svn_author, now_string, script_file_name

   # Objects for the meta data of the input (will be similar to that of the output)
   metaData = NetCdfGeoMetaData()
   roiMetaData = NetCdfGeoMetaData()

   ###################
   # Open the base file and the long-time-average file
   ###################
   baseDataset = Dataset(os.path.join(aBaseFileName), 'r')
   metaData.copyFromNetCdfObject(baseDataset)
   latSize = len(baseDataset.dimensions['latitude'])
   longSize = len(baseDataset.dimensions['longitude'])
   noDataValueCube = float(metaData.getNoDataValue())


   ###################
   # Retrieve the ROI mask from the geotif file
   ###################
   roiFile = gdal.Open(aRoiFileName)
   roiMetaData.copyFromGdalData(roiFile)
   upperLat, leftLong = roiMetaData.getLatLongFromPixelYx(0,0)
   xOrg, xRes = metaData.getLongitudeTransform()
   yOrg, yRes = metaData.getLatitudeTransform()
   # Take mid of upper left mask pixel to prevent precision errors.
   upperIndex,leftIndex = metaData.getPixelYxFromLatLong(upperLat+0.5*yRes, leftLong+0.5*xRes)
   lowerIndex = upperIndex + roiFile.RasterYSize
   rightIndex = leftIndex + roiFile.RasterXSize
   upperIndex = min(max(0, upperIndex), latSize-1)
   leftIndex = min(max(0, leftIndex), longSize-1)
   lowerIndex = min(max(0, lowerIndex), latSize-1)
   rightIndex = min(max(0, rightIndex), longSize-1)
   # Create true/false array from roi-tif-file to use as mask later.
   noDataValueMask = roiFile.GetRasterBand(1).GetNoDataValue()
   roiMaskDataArray = np.where(np.isnan(np.array(roiFile.GetRasterBand(1).ReadAsArray())), 1, 0).astype(bool)
   roiMaskDataArray = np.logical_or(roiMaskDataArray, np.equal(roiFile.GetRasterBand(1).ReadAsArray(), np.full(roiMaskDataArray.shape, noDataValueMask)))
   del roiFile
   
   ###################
   # Retrieve the time indices for the base datacube to start with (if one day of an interval is included the whole interval is included)
   ###################
   bDaysArray = baseDataset.variables['time'][:]
   startDay   = max(0, (aPeriodStart - metaData.getDate()).days)
   endDay     = min(bDaysArray[-1], (aPeriodEnd - metaData.getDate()).days)
   startIndex = _lookup(bDaysArray, startDay)
   endIndex   = _lookup(bDaysArray, endDay) + 1
   # Create function to add offset to a date array
   def addRefDate(x):
      return (metaData.getDate() + datetime.timedelta(days=int(x)))
   # Get all measurement interval start dates
   periodArray = np.vectorize(addRefDate)(bDaysArray)

   # If the selected period does not contain measurements return empty time series
   if aPeriodStart > periodArray[-1] or aPeriodEnd < periodArray[0]:
      ts = jsonTimeSeries(aLabel, np.array([]), np.array([]), np.array([]), np.array([]))
      rq = jsonRequest(aSource, aMaskID, aIndicator, aPeriodStart, aPeriodEnd, aReferenceType, aRefStartDate, aIntegrateType, aWindowLength, aLtaStartDate, aLtaEndDate)
      print formatResult(ts, rq, aMimeType)
      sys.exit()

   ###################
   # Retrieve the time indices for the reference datacube if needed
   ###################
   # Situation: ABSolute reference type and Reference Start date is set
   refStartDay = (aRefStartDate - metaData.getDate()).days
   if refStartDay < 0:
      sys.exit("Error: Reference start date " + str(aRefStartDate) + " not in time axis range.")
   refStartIndex = _lookup(bDaysArray, refStartDay)
   refEndIndex   = refStartIndex + (endIndex - startIndex)
   if refEndIndex > len(bDaysArray):
      sys.exit("Error: Reference end date beyond last measurement date. Index is " + str(refEndIndex) + " while max index is: " + str(len(bDaysArray) - 1))

   ###################
   # Make the bouding box selection from the data and fill the time series
   ###################
   ySlice = slice(upperIndex, lowerIndex)
   xSlice = slice(leftIndex, rightIndex)

   key,  value = baseDataset.variables.items()[3]  # Index 3 is the indicator data in a cube
   bdata = baseDataset.variables[key][startIndex:endIndex, ySlice, xSlice]
   rdata = baseDataset.variables[key][refStartIndex:refEndIndex, ySlice, xSlice]

   if aIntegrateType == 'DIF':
      bdata = np.pad(bdata, ((1,0), (0,0), (0,0)), mode='edge')
      bdata = np.diff(bdata, axis=0)
      rdata = np.pad(rdata, ((1,0), (0,0), (0,0)), mode='edge')
      rdata = np.diff(rdata, axis=0)

   bdata = np.ma.array(bdata)
   rdata = np.ma.array(rdata)

   ###################
   # Determine size of the integration window
   ###################
   windowLength = int(math.ceil(float(aWindowLength)/sourceInfo.measurementInterval(aSource)))
   if windowLength == 0 :
      if aIntegrateType == 'DIF': windowLength = 1
      if aIntegrateType == 'SUM': windowLength = endIndex - startIndex
   windowLength = max(1, windowLength)
      
   if aIntegrateType <> 'NONE' and windowLength > 1:
      window = np.ones(windowLength)
      bdata = np.apply_along_axis(np.convolve, 0, bdata, window, mode='full')
      rdata = np.apply_along_axis(np.convolve, 0, rdata, window, mode='full')
      bdata = bdata[0:-windowLength+1]
      rdata = rdata[0:-windowLength+1]

   ###################
   # Make multi dimensional array to hold the data
   ###################
   timeSeriesData = np.ma.empty(((4,)+bdata.shape), np.float32)
   timeSeriesData[0] = bdata
   timeSeriesData[1] = rdata

   #########################
   # Get Base statistics data
   ##########################

   ltaStart, ltaEnd, ltaLength = sourceInfo.periodLTA(aSource, (aPeriodStart, aPeriodEnd), aRefStartDate)
   ltaNInt = (ltaEnd-ltaStart) % ltaLength + 1

   if aLtaFileName <> '':

      ltaDataset = Dataset(os.path.join(aLtaFileName), 'r')

      if ltaStart <= ltaEnd:
         ltaIndices = range(ltaStart, ltaEnd+1)
      else:
         ltaIndices = range(ltaStart, ltaLength) + range(0, ltaEnd+1)
   
      bsdata = np.empty((6,) + (ltaNInt, bdata.shape[1], bdata.shape[2]))

      for i in range(6):
         key, value = ltaDataset.variables.items()[i+3]
         ltaData = ltaDataset.variables[key]
         bsdata[i] = np.array(ltaData[ltaIndices, ySlice, xSlice])

      ltaDataset.close()

   else:

      # There is no precomputed base statistics cube, so compute the base statistics from the base data
      key,  value = baseDataset.variables.items()[3]  # Index 3 is the indicator data in a cube
      baseData = baseDataset.variables[key]

      # Determine upper and lower bounds for base statistics dates
      ltaStartIndex = _lookup(periodArray, aLtaStartDate)
      ltaEndIndex   = _lookup(periodArray, aLtaEndDate)

      start = _lookup(periodArray, aRefStartDate) % ltaLength

      interval = (ltaStartIndex, start, ltaEndIndex, ltaNInt, ltaLength)
      bsdata = baseStats.computeBaseStatsInterval(
                  baseData, interval, ySlice, xSlice, roiMaskDataArray, aIntegrateType, windowLength, noDataValueCube)

   baseDataset.close()

   # The base statistics cover at most one year. 
   # The time series may be more than one year.
   r = np.array(range(timeSeriesData.shape[1]))
   r = r % ltaLength
   timeSeriesData[3] = bsdata[5,r]

   bsdata = np.ma.array(bsdata)
   bsdata.mask = roiMaskDataArray
   bsdata = bsdata.reshape(bsdata.shape[0], bsdata.shape[1], bsdata.shape[2] * bsdata.shape[3])
   bsdata = np.nanmean(bsdata, axis=2)

   ###################
   # Remove values outside mask, no-data values and not-a-number values
   ###################
   roiMaskDataArray    = np.ma.mask_or(roiMaskDataArray, np.equal(timeSeriesData, np.full(timeSeriesData.shape, noDataValueCube)))
   roiMaskDataArray    = np.ma.mask_or(roiMaskDataArray, np.isnan(timeSeriesData))
   timeSeriesData.mask = np.ma.make_mask(roiMaskDataArray)

   ###################
   # Create the reference (difference) array
   ###################
   if aReferenceType == 'NONE':
      timeSeriesData[2] = timeSeriesData[0]
   elif aReferenceType == 'ABS':
      timeSeriesData[2] = timeSeriesData[0] - timeSeriesData[1]
   elif aReferenceType == 'LTA':
      timeSeriesData[2] = timeSeriesData[0] - timeSeriesData[3]

   ###################
   # Calculate the spacial aggregation
   ###################
   # Get latitude and longitude on the same axis first (so rows with more masked values do not get same weight as rows with just a few masked values)
   timeSeriesData = timeSeriesData.reshape(timeSeriesData.shape[0], timeSeriesData.shape[1], timeSeriesData.shape[2] * timeSeriesData.shape[3])
   timeSeriesData = np.nanmean(timeSeriesData, axis=2)
   
   ###################
   # Create period start and end dates
   ###################
   pstart          = periodArray[startIndex:endIndex]
   pend            = np.roll(pstart, -1) - datetime.timedelta(1)
   isLastInterval  = endIndex == periodArray.shape[0]
   pend[-1]        = sourceInfo.endDate(aSource, pstart[-1], isLastInterval)
 
   # Convert period start/end dates to string
   pstart = np.vectorize(datetime.date.isoformat)(pstart)
   pend   = np.vectorize(datetime.date.isoformat)(pend)

   ###################
   # Create JSON return object
   ###################
   ts = jsonTimeSeries(aLabel, timeSeriesData, bsdata, pstart, pend)
   rq = jsonRequest(aSource, aMaskID, aIndicator, aPeriodStart, aPeriodEnd, aReferenceType, aRefStartDate, aIntegrateType, aWindowLength, aLtaStartDate, aLtaEndDate)

   print formatResult(ts, rq, aMimeType)

   return

def usage(n):
   print "Usage:"
   print "python %s [flags] <base-file-name> <lta-file-name> <roi-file-name> <period-begin> <period-end>" % sys.argv[0]
   print ""
   print "The base file must be a netCDF cube type file."
   print "Allowed flag examples:"
   print "--refdate=2012-01-01   The reference period begin date. If this date is not explicitly set then the <period-begin> date is used. (The reference period end date is derived from the period length.)"
   print "--reftype=LTA          The reference type. Allowed values: NONE, ABS and LTA. ABS takes reference within the (smoothed) data, LTA takes reference from long time average values. NONE (default) returns the smoothed data."
   print "--difference=True      If this flag is set the precipitation is accumulated for the time series (on a spacial basis)"
   print "--label                The content of this flag is returned in the output."
   print "--source=<string>      Name of source to be echoed in request field."
   print "--indicator=<string>   Name of indicator to be echoed in request field."
   print "--maskID=<string>      Name of mask to be echoed in request field."
   print ""
   print "Example(s):"
   print "python processTimeSeriesDate.py MODIS_500 NDVI NONE /tmp/roi.tif 2010-01-01 2010-04-01"
   print "python processTimeSeriesDate.py --reftype=ABS --refdate=2009-01-01 --difference=True --label=\"this is the label\" MODIS_500 NDVI NONE /tmp/roi.tif 2010-01-01 2010-04-01"
   print ""
   sys.exit(n)

######################################################################################################
# Check and process the input parameters
######################################################################################################
def main(argv):

   global gINTEGRATE_TYPE_SET, gREFERENCE_TYPE_SET, gMIME_TYPE_SET

   if len(argv) < 6: usage("ERROR : not enough arguments (argc=%d)" % len(argv))

   flags_dict = {}

   # Extract all arguments
   mArgCount = 0 # Number of mandatory arguments
   for arg in argv[1:]:
      # Optional arguments
      if arg.startswith('--'):
         l = arg.split('=')
         if len(l) == 2:
            key, value = l
         else:
            key, value = l[0], None
         flags_dict[key] = value
      else:
         # Mandatory arguments
         if   mArgCount == 0:
            baseFileName = arg
            mArgCount += 1
         elif mArgCount == 1:
            ltaFileName = arg
            mArgCount += 1
         elif mArgCount == 2:
            roiFileName = arg
            mArgCount += 1
         elif mArgCount == 3:
            periodStart = datetime.datetime.strptime(arg, "%Y-%m-%d").date()
            mArgCount += 1
         elif mArgCount == 4:
            periodEnd = datetime.datetime.strptime(arg, "%Y-%m-%d").date()
            mArgCount += 1
         else:
            usage("ERROR : unkown argument '" + arg + "'")
   
   # Check number of mandatory arguments
   if mArgCount <> 5: usage("ERROR : unexpected mandatory arguments (argc=%d)" % mArgCount)

   if periodStart > periodEnd:
      sys.exit("ERROR : periodStart(%s) must be <= periodEnd (%s)." % (periodStart.isoformat(), periodEnd.isoformat()))

   # Initialise optional arguments
   refStartDate       = periodStart
   referenceType      = gREFERENCE_TYPE_SET[0]
   integrate          = 'NONE'
   windowLength       = 0
   label              = ''
   source             = ''
   indicator          = ''
   maskID             = ''
   ltaStartDate       = datetime.date(1900, 1, 1)
   ltaEndDate         = datetime.date(2100, 1, 1)
   mimeType           = 'application/json'

   # Evaluate the optional arguments and extract their values
   for key, value in flags_dict.items():
      if   key == '--refdate':
         refStartDate = datetime.datetime.strptime(value, "%Y-%m-%d").date()
      elif key == '--reftype':
         if value in gREFERENCE_TYPE_SET:
            referenceType = value
         else:
            sys.exit("Error: referenceType ('" + value + "') not valid ('" + str(gREFERENCE_TYPE_SET) + ").")
      elif key == '--integrate':
         if value in gINTEGRATE_TYPE_SET:
            integrate = value
         else:
            sys.exit("Error: integrate ('" + value + "') not valid ('" + str(gINTEGRATE_TYPE_SET) + ").")
      elif key == '--window':
         windowLength = int(value)
      elif key == '--label':
         label = str(value)
      elif key == '--source':
         source = value
      elif key == '--indicator':
         indicator = value
      elif key == '--maskID':
         maskID = value
      elif key == '--lta-start':
         ltaStartDate = datetime.datetime.strptime(value, "%Y-%m-%d").date()
      elif key == '--lta-end':
         ltaEndDate = datetime.datetime.strptime(value, "%Y-%m-%d").date()
      elif key == '--mimetype':
         if value in gMIME_TYPE_SET:
            mimeType = value
         else:
            sys.exit("Error: mimeType ('" + value + "') not valid ('" + str(gMIME_TYPE_SET) + ").")
      else:
         print "Warning: Key \'" + key + "\' is unknown and ignored."

   # Make sure the input files exist
   if not os.path.isfile(baseFileName):
      sys.exit("Error: Input (base) file " + baseFileName + " does not exist.")
   if (referenceType == 'LTA') and ltaFileName <> '' and (not os.path.isfile(ltaFileName)):
      sys.exit("Error: Input (LTA reference) file " + ltaFileName + " does not exist.")
   if not os.path.isfile(roiFileName):
      sys.exit("Error: Input (ROI-mask) file " + roiFileName + " does not exist.")

   processTimeSeriesDate(
      baseFileName,
      ltaFileName,
      roiFileName,
      periodStart,
      periodEnd,
      refStartDate,
      referenceType, 
      integrate, 
      windowLength,
      ltaStartDate,
      ltaEndDate,
      label,
      source, 
      indicator, 
      maskID,
      mimeType)

######################################################################################################
# Only execute when run as a script
######################################################################################################
if __name__ == "__main__":
   main(sys.argv[:])

