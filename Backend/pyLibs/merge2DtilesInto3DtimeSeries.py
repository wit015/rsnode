#!/usr/bin/env python 

################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

SVN_REVISION = '$Rev: 6685 $'
SVN_DATE = '$Date: 2016-05-20 12:44:22 +0200 (Fri, 20 May 2016) $'
SVN_AUTHOR = '$Author: jb76278 $'

import sys
import os

# The following import(s) need correct setting of the PYTHONPATH environment variable. For example in .bashrc:
# export PYTHONPATH="${PYTHONPATH}:`pwd`/SvnCheckout/CommonSense/RSDS/DataProcessing/Python/code:`pwd`/SvnCheckout/CommonSense/RSDS/DataProcessing/Python/pyLibs"
import merge2DtilesInto3DtimeSeriesAlgorithm
import tiles
import fileSystemOps

######################################################################################################
# Check and process the input parameters
######################################################################################################
def main(argv):
    if len(sys.argv) < 3 or len(sys.argv) > 5:
        print ""
        print "Usage:"
        print "python merge2DtilesInto3DtimeSeries.py [flags] <output-dir-name> <input-dir-name>"
        print ""
        print "The input files must be netCDF type files!"
        print "Allowed flag examples:"
        print "--toi=0,0,10,2  The tiles of interest is defined with four comma separated pixel-index values upper, left, lower, right. All tiles are processed by default."
        print "--add_new=true Script will only add new data and leave old data as it was."
        print ""
        print "Example:"
        print "python merge2DtilesInto3DtimeSeries.py --toi=10,10,12,14 --add_new=true /net/satarch/MODIS/jelle/NdviTimeSeries500m/ /net/satarch/MODIS/jelle/NdviTileData500m/"
        print "python merge2DtilesInto3DtimeSeries.py /net/satarch/CHIRPS/jelle/rainTimeSeries/ /net/satarch/CHIRPS/jelle/rainTileData/"
        print ""
    else:
        flags_dict = {}
        outputDirName = ''
        inputDirName = ''
        toiSet = False
        upperIndex = 0
        leftIndex = 0
        lowerIndex = 0
        rightIndex = 0
        addNewData = False

        # Extract the flags
        argIndex = 1
        while outputDirName == '' and argIndex < len(sys.argv):
            # Check for flags
            if sys.argv[argIndex].count('=') == 1 and sys.argv[argIndex].startswith('--'):
                key, value = sys.argv[argIndex].split('=')
                flags_dict[key] = value
            else:
                outputDirName = sys.argv[argIndex]
            argIndex += 1

        # Check validity of the outputdirectory
        fileSystemOps.MakeSureDirectoryExists(outputDirName)

        if argIndex == (len(sys.argv) - 1):
            inputDirName = sys.argv[argIndex]
        else:
            sys.exit("Error: Number of arguments is wrong.")

        # Check the flags and extract their values
        for key, value in flags_dict.items():
            if key == '--toi':
                if value.count(',') == 3:
                    one,two,three,four = value.split(',')
                    upperIndex = int(one)
                    leftIndex = int(two)
                    lowerIndex = int(three)
                    rightIndex = int(four)
                    toiSet = True
                else:
                    sys.exit("Error: Format of Value \'" + value + "\' for Key \'" + key + "\' is wrong.")
            elif key == '--add_new':
                if value == "true" or value == "True":
                    addNewData = True
            else:
                print "Warning: Key \'" + key + "\' is unknown and ignored."

        # Get the directories and assume the first one is representative
        allDirs = sorted(os.listdir(inputDirName), reverse=False)
        tilesObject = tiles.Tiles()
        tilesObject.setTilesOfInterest(
                os.path.join(inputDirName, allDirs[0]), "tile_", ".hdf5", toiSet, upperIndex, leftIndex, lowerIndex, rightIndex)

        if addNewData:
            merge2DtilesInto3DtimeSeriesAlgorithm.Add2DtilesTo3DtimeSeries(inputDirName, outputDirName, tilesObject)
        else:
            merge2DtilesInto3DtimeSeriesAlgorithm.Merge2DtilesInto3DtimeSeries(inputDirName, outputDirName, tilesObject)

######################################################################################################
# Only execute when run as a script
######################################################################################################
if __name__ == "__main__":
    main(sys.argv[:])

