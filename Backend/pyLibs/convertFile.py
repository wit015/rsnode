##################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
##################################################################################

SVN_REVISION = '$Rev: 7715 $'
SVN_DATE = '$Date: 2016-12-16 11:40:50 +0100 (Fri, 16 Dec 2016) $'
SVN_AUTHOR = '$Author: jb76278 $'

import os
SCRIPT_FILE_NAME = os.path.basename(__file__)
NOW_STRING = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

import numpy as np
import sys
import re
import resource
import Image
import ImageDraw

# The following import(s) need correct setting of the PYTHONPATH environment variable. For example in .bashrc:
# export PYTHONPATH="${PYTHONPATH}:`pwd`/SvnCheckout/CommonSense/RSDS/DataProcessing/Python/code:`pwd`/SvnCheckout/CommonSense/RSDS/DataProcessing/Python/pyLibs"
import arrayToPng as arr2png
import tiles

from datetime import time, datetime
from netCDF4 import Dataset
from osgeo import gdal

# Global variable that are entered as of derived from the arguments
flags_dict = {}
inputFileName = ''
inputFileType = 'dat'
outputFileName = ''
outputFileType = 'png'
regionOfInterest = 'full'
leftIndex = 0
rightIndex = 0
upperIndex = 0
lowerIndex = 0
timeIndex = 0
hdfKeyOrIndex = 'index'
hdfVarIndex = 0
hdfVarKey = 'data'

######################################################################################################
######################################################################################################
def convertFile(aInputFile, aOutputFile, aIndex = None):
    now = datetime.time(datetime.now())
    print "Now: " + str(now)
    if aInputFile.endswith('.dat'):
        enviDataSet = gdal.Open(aInputFile)
        index = 1
        if not aIndex == None:
            index = int(aIndex)
            # TODO: Check for an index that does not return a raster-band
        dataArray = np.array(enviDataSet.GetRasterBand(index).ReadAsArray())
        arr2png.arrayToRgbPng(dataArray,aOutputFile)

    elif aInputFile.endswith('.nc') or aInputFile.endswith('.hdf5'):
        nc4file = Dataset(aInputFile, 'r')
        # Get the key (name) of the first variable stored
        key, value = nc4file.variables.items()[0]
#        key, value = nc4file.variables.items()[3]
        dataArray = nc4file.variables[key][:]
        print "Shape: " + str(dataArray.shape)

        if len(dataArray.shape) == 3:
            index = 0
            if not aIndex == None:
                index = int(aIndex)
#            arr2png.arrayToRgbPng(dataArray[:,:,index],aOutputFile,500,9500)
            arr2png.arrayToRgbPng(dataArray[:,:,index],aOutputFile)
        elif len(dataArray.shape) == 2:
            arr2png.arrayToRgbPng(dataArray,aOutputFile)

    else:
        print "Error: Input file does not have entension .dat or .nc"

    now = datetime.time(datetime.now())
    print "Now: " + str(now)

######################################################################################################
# Check and process the input parameters
######################################################################################################
def main(argv):
    if len(sys.argv) < 3:
        print ""
        print "Usage:"
        print "python convertFile.py <input-file-name> <output-file-name> [full [index]|roi <left> <top> <right> <bottom> [index]]"
    else:
        print "Revision string: " + SVN_REVISION
        print "File name: " + SCRIPT_FILE_NAME
        inputFileName = sys.argv[len(sys.argv) - 2]
        if inputFileName.endswith('.nc') or inputFileName.endswith('.hdf5'):
            inputFileType = 'netCDF'
        elif inputFileName.endswith('.dat'):
            inputFileType = 'ENVI_GDAL'
        elif inputFileName.endswith('.tif'):
            inputFileType = 'geoTiff'
        else:
            sys.exit("Error: Input file name \'" + str(inputFileName) + "\' is of unknown type.")

        outputFileName = sys.argv[len(sys.argv) - 1]
        if outputFileName.endswith('.jpg'):
            outputFileType = 'jpeg'
        elif outputFileName.endswith('.png'):
            outputFileType = 'png'
        elif outputFileName.endswith('.tif'):
            outputFileType = 'geoTiff'
        else:
            sys.exit("Error: Output file name \'" + str(outputFileName) + "\' is of unknown type.")

        for argIndex in range(1,len(sys.argv) - 2):
            # Check the formatting
            if sys.argv[argIndex].count('=') == 1:
                key, value = sys.argv[argIndex].split('=')
                flags_dict[key] = value
            else:
                sys.exit("Error: Flag \'" + sys.argv[argIndex] + "\' will be ignored. It has the wrong format.")

         for key, value in flags_dict.items():
             if key == '--roi':
                 if value.count(',') == 3:
                     one,two,three,four = value.split(',')
                     leftIndex = min(int(one),int(three))
                     upperIndex = min(int(two),int(four))
                     rightIndex = max(int(one),int(three))
                     lowerIndex = max(int(two),int(four))
                     roiSet = True
                 else:
                     sys.exit("Error: Format of Value \'" + value + "\' for Key \'" + key + "\' is wrong.")
             elif key == '--time_index':
                 timeIndex = int(value)
             elif key == '--item_key':
                 hdfVarKey = value
             elif key == '--item_index':
                 if '--item_key' in flags_dict:
                     print "Warning: Both \'" + key + "\' and \'--item_key\' are given. Ignoring \'" + key + "\' value \'" + value + "\'."
                 else:
                     hdfVarIndex = value
             else:
                 print "Warning: Key \'" + key + "\' is unknown and ignored."
    quit()


    if len(sys.argv) != 3 and len(sys.argv) != 4 and len(sys.argv) != 5 and len(sys.argv) != 8 and len(sys.argv) != 9:
        print ""
        print "Usage:"
        print "python convertFile.py [flags] <input-file-name> <output-file-name>"
        print ""
        print "Examples:"
        print "python convertFile.py /net/satarch/MODIS/jelle/tileData500m/EVI_20130101_500m.dat/arrayTile_011_007.hdf5 temp1.png"
        print "python convertFile.py --roi=0,0,30,25 --time_index=10 --item_key=data /net/satarch/MODIS/jelle/smoothTimeSeries500m/smooth011_007.hdf5 temp1.png"
        print ""
    elif len(sys.argv) == 3:
        viewFile(sys.argv[1], sys.argv[2], None, None, None, None, None, None)
    elif len(sys.argv) == 4:
        viewFile(sys.argv[1], sys.argv[2], sys.argv[3], None, None, None, None, None)
    elif len(sys.argv) == 5:
        viewFile(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], None, None, None, None)
    elif len(sys.argv) == 8:
        convertFile(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5], sys.argv[6], sys.argv[7], None)
    else:
        convertFile(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5], sys.argv[6], sys.argv[7], sys.argv[8])

######################################################################################################
# Only execute when run as a script
######################################################################################################
if __name__ == "__main__":
    main(sys.argv[:])

