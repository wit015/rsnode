#!/usr/bin/env python
#
# Process sentinel2 data
################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2018, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################


# Process field
import os
import uuid
import geojson
from osgeo import gdal
import shutil
import datetime

import geopandas as gpd

import csutils.os
import csutils.raster
import csutils.geojson

import fieldDB
import polygonSelection
import S2DB

def processField(field, dt_stamp, granule_B04_fn, granule_B08_fn, granule_SCL_fn):

    # Extract field from B04 and B08
    tmp_field_B04_fn = csutils.os.makeTempFileName('field_B04', '.tif')
    tmp_field_B08_fn = csutils.os.makeTempFileName('field_B08', '.tif')
    tmp_field_SCL_fn = csutils.os.makeTempFileName('field_SCL', '.tif')
    tmp_field_POL_fn = csutils.os.makeTempFileName('field_POL', '.geojson')

    # Get field polygon
    polygon = field.getGeoJSON()
 
    # Save field polygon (actually a feature collection)
    f = open(tmp_field_POL_fn,'w')
    geojson.dump(polygon, f)
    f.close()

    epsg_code  = csutils.raster.getEPSGCode(granule_B04_fn)
    raster_srs = "EPSG:%0d" % epsg_code

    polygonSelection.clipPolygonFromRaster(granule_B04_fn, tmp_field_POL_fn, tmp_field_B04_fn, t_srs=raster_srs)
    polygonSelection.clipPolygonFromRaster(granule_B08_fn, tmp_field_POL_fn, tmp_field_B08_fn, t_srs=raster_srs)
    polygonSelection.clipPolygonFromRaster(granule_SCL_fn, tmp_field_POL_fn, tmp_field_SCL_fn, t_srs=raster_srs, dstnodata=0)
 
    B04_f = gdal.Open(tmp_field_B04_fn)
    B04_b = B04_f.GetRasterBand(1)
    B04   = B04_b.ReadAsArray()
    B08_f = gdal.Open(tmp_field_B08_fn)
    B08_b = B08_f.GetRasterBand(1)
    B08   = B08_b.ReadAsArray()
    SCL_f = gdal.Open(tmp_field_SCL_fn)
    SCL_b = SCL_f.GetRasterBand(1)
    SCL   = SCL_b.ReadAsArray()

    # Update field DB
    field.addAcquisition(dt_stamp, B04, B08, SCL)

    # Cleanup tmp files
    os.remove(tmp_field_B04_fn)
    os.remove(tmp_field_B08_fn)
    os.remove(tmp_field_SCL_fn)
    os.remove(tmp_field_POL_fn)

def defineNewField(field):

    field_gj = field.getGeoJSON()
    field_df = gpd.GeoDataFrame.from_features(field_gj)

    # Add CRS since geopandas doesn't do that
    epsg = csutils.geojson.EPSGCode(field_gj)
    field_df.crs = { 'init' : 'EPSG:%0d' % epsg }

    # get granule containing the field
    granule_id = S2DB.getContainingGranuleId(field_df)
    if granule_id is None: 
        raise RuntimeError("ERROR : addNewFields : no matching granule")
 
    template_pn = S2DB.getGranuleTemplateFilePath(granule_id)
    epsg = csutils.raster.getEPSGCode(template_pn)
    srs = 'EPSG:%0d' % epsg
 
    polygon_fn = csutils.os.makeTempFileName('polygon', '.geojson')
    geojson.dump(field_gj, open(polygon_fn, 'w'))
 
    mask_pn = csutils.os.makeTempFileName('mask', '.tif')
 
    polygonSelection.clipPolygonFromRaster(template_pn, polygon_fn, mask_pn, dstnodata=1, fill=[0], t_srs=srs)
 
    field.define(mask_pn, { "granule_id" : granule_id } )
 
    os.remove(mask_pn)

    print "defined %s %s %s" % (field.user_id, field.group_id, field.field_id)

def defineNewFields():

    newFields = 0
    for user_id in fieldDB.getUserIDs():
        for field in fieldDB.Field.getDeclaredFields(user_id):
            print field
            defineNewField(field)
            newFields = newFields + 1

    return newFields

def updateSCL(gs):
    
    tmp_pn = csutils.os.makeTempFileName('granule')
    tmp_granule_pn =S2DB.getGranule(gs, tmp_pn)
    SCL_fn = csutils.os.makeTempFileName('CSL', '.tif')

    cmd = [ "scl.bash", tmp_granule_pn, SCL_fn ]

    try:
        csutils.subprocess.callcmd(cmd)
    except:
        # Touch output file to signal we have tried
        open(SCL_fn, 'w')
        print "ERROR : SCL generation failed for %s %s" % \
            (gs.MGRS_TILE, str(gs.SENSING_TIME))

    # If generation failed, we store this as empty file.
    S2DB.putSCL(gs, SCL_fn)

    if os.path.exists(tmp_pn): shutil.rmtree(tmp_pn)
    if os.path.exists(SCL_fn): os.remove(SCL_fn)

def updateSCLs(granule_id_list, start_date, always=False):

    print "Loading sentinel2 index"
    s2i_df = S2DB.getGranuleIndex()

    # Select only rows with granules we are interested in
    if granule_id_list is not None: s2i_df = s2i_df[ s2i_df['MGRS_TILE'].isin(granule_id_list) ]

    print "Loading SCL index"
    scl_df = S2DB.getSCLIndex()

    # Select granules with sensing datetime after last processed
    s2i_df = s2i_df[ s2i_df['SENSING_TIME'] > start_date ]

    for index, gs in s2i_df.iterrows(): 
        print "updateSCLs : checking %s %s" % (gs.MGRS_TILE, gs.SENSING_TIME)
        if always or S2DB.getSCLStatus(gs.MGRS_TILE, gs.SENSING_TIME, scl_df) is None: 
            print "updateSCLs: generating SCL for %s %s" % (gs.MGRS_TILE, str(gs.SENSING_TIME))
            updateSCL(gs)



def updateFieldsHistoryByGranule(field_list=None, always=False):

    if field_list is None:
        field_list = []
        for user_id in fieldDB.Field.getUserIDs():
            field_list.extend(fieldDB.Field.getDefinedFields(user_id))

    # First get the granule ids covering the fields
    # as most fields will be in same granule
    gid_dict = {}
    for field in field_list:
        p = field.getProperties()
        gid = p['properties']['granule_id']
        if gid not in gid_dict.keys(): gid_dict[gid] = []
        gfl = gid_dict[gid]
        gfl.append(field)

    for gid in gid_dict.keys():
        scl_dates = S2DB.getSCLDates(gid)

        for scl_date, passed in scl_dates:

            # Skip files for which the SCL generation failed
            if not passed:
                print "createFieldsHistory : skipping history for %s %s" % (gid, scl_date)
                continue

            needToProcessGranule = False
            for field in gid_dict[gid]:
                field_dates = field.getAcquisitionDateTimes(all_acq=True)
                if (not always) and scl_date in field_dates: continue
                needToProcessGranule = True
                break

            if not needToProcessGranule: continue

            print "createFieldsHistory : processing fields history for %s %s" % (gid, scl_date)
            tmp_dp = csutils.os.makeTempFileName('fh')
            tmp_granule_dp = S2DB.getSCLGranule(gid, scl_date, tmp_dp)
            B04_fp = S2DB.getGranuleImageFilePath(tmp_granule_dp, 'B04')
            B08_fp = S2DB.getGranuleImageFilePath(tmp_granule_dp, 'B08')
            SCL_fp = csutils.os.makeTempFileName('scl', base=tmp_dp)
            S2DB.getSCL(gid, scl_date, SCL_fp)

            for field in gid_dict[gid]:
                field_dates = field.getAcquisitionDateTimes(all_acq=True)
                if (not always) and scl_date in field_dates: continue
                print "process field %s %s %s" \
                    % (field.user_id, field.group_id, field.field_id)
                processField(field, scl_date, B04_fp, B08_fp, SCL_fp)

            # Cleanup 
            shutil.rmtree(tmp_dp)


def updateFieldsHistory(field_list, always=False):

    gid_dict = {}
    for user_id, group_id, field_id in field_list:
        field = fieldDB.Field(user_id, group_id, field_id)
        field_dates = field.getAcquisitionDateTimes(all_acq=True)

        p = field.getProperties()
        gid = p['properties']['granule_id']
        scl_dates = S2DB.getSCLDates(gid)

        for scl_date, passed in scl_dates: 

            # Skip files for which the SCL generation failed
            if not passed:
                print "updateFieldsHistory : skipping %s-%s-%s history for %s %s" \
                    % (user_id, group_id, field_id, gid, scl_date)
                continue

            if (not always) and scl_date in field_dates: continue

            print "updateFieldsHistory : processing field %s-%s-%s history for %s %s" \
                % (user_id, group_id, field_id, gid, scl_date)

            tmp_dp = csutils.os.makeTempFileName('fh')
            tmp_granule_dp = S2DB.getSCLGranule(gid, scl_date, tmp_dp)
            B04_fp = S2DB.getGranuleImageFilePath(tmp_granule_dp, 'B04')
            B08_fp = S2DB.getGranuleImageFilePath(tmp_granule_dp, 'B08')
            SCL_fp = csutils.os.makeTempFileName('scl', base=tmp_dp)
            S2DB.getSCL(gid, scl_date, SCL_fp)
            processField(field, scl_date, B04_fp, B08_fp, SCL_fp)

            # Cleanup
            shutil.rmtree(tmp_dp)

def generateGranuleTemplates():

    print "Loading sentinel2 index"
    s2idx_df = S2DB.getGranuleIndex()

    granule_id_list = S2DB.getGranuleIdList()

    for gid in granule_id_list:

        # Get last granule index from bucket
        gdf = s2idx_df[ s2idx_df.MGRS_TILE == gid ]
        if len(gdf.index) == 0:
            print "WARNING :  no granule data for granule_id %s" % gid
            continue

        # Use last one as index
        gs = gdf.iloc[-1]
        tmp_pn = csutils.os.makeTempFileName('granule')
        tmp_granule_pn =S2DB.getGranule(gs, tmp_pn)

        TCI_fp      = S2DB.getGranuleImageFilePath(tmp_granule_pn, 'TCI')
        template_fp = S2DB.getGranuleTemplateFilePath(gid)

        cmd = [ "gdal_translate",
                TCI_fp, 
                "-of", "vrt",
                template_fp ]

        TCI_fn      = os.path.basename(TCI_fp)
        print "Generating granule template %s for %s" % (template_fp, TCI_fn)

        csutils.subprocess.callcmd(cmd)

        # Check if generic granule is present
        # Generate if not present
        tile_fp = S2DB.getGranuleTemplateFilePath("TILE")
        if not os.path.exists(tile_fp):
            print "Generating generic tile %s" % tile_fp
            cmd = [ "gdal_calc.py",
                    "-A", TCI_fp,
                    "--allBands=A",
                    "--calc=0",
                    "--outfile",  tile_fp,
                    "--format", "GTIFF",
                    "--co", "COMPRESS=LZW"
                   ]
            csutils.subprocess.callcmd(cmd)

        # Replace source granule with generic granule
        vrt = open(template_fp)
        vrt_contents = vrt.read()
        vrt_contents = vrt_contents.replace('relativeToVRT="0"', 'relativeToVRT="1"')
        vrt_contents = vrt_contents.replace(TCI_fp, os.path.basename(tile_fp))
        f = open(template_fp, 'w')
        f.write(vrt_contents)
        f.close()

        # Cleanup 
        shutil.rmtree(tmp_pn)
