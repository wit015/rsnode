################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

SVN_REVISION = '$Rev: 7715 $'
SVN_DATE = '$Date: 2016-12-16 11:40:50 +0100 (Fri, 16 Dec 2016) $'
SVN_AUTHOR = '$Author: jb76278 $'

# Shortened and readable SVN description
import re
SVN_DESCR = (SVN_REVISION + re.sub('\) \$', ' ' ,re.sub('^.*\(', 'Date: ', SVN_DATE)) + SVN_AUTHOR).replace('$', '')

import os
import numpy as np
from osgeo import gdal
import datetime
import sys

# The following import(s) need correct setting of the PYTHONPATH environment variable. For example in .bashrc:
# export PYTHONPATH="${PYTHONPATH}:`pwd`/SvnCheckout/CommonSense/RSDS/DataProcessing/Python/code:`pwd`/SvnCheckout/CommonSense/RSDS/DataProcessing/Python/pyLibs"
import tiles
from netCdfGeoMetaData import NetCdfGeoMetaData

# Global variables
SCRIPT_FILE_NAME = os.path.basename(__file__)
NOW_STRING = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

######################################################################################################
# Method will:
# - take the input file
# - select the region of interest
# - create tiles with the given size
# - write those tiles to the given output directory
######################################################################################################
def SplitGdalIntoHdf5Tiles(aOutputDirName, aRoiSet, aUpperIndex, aLeftIndex, aLowerIndex, aRightIndex, aTileSet, aTileHeight, aTileWidth, aAddNewData, aFullInputFileName):
    global SVN_DESCR, NOW_STRING, SCRIPT_FILE_NAME

    # Check if input file exists (including the '.hdr' description record for .dat files)
    if (not os.path.isfile(aFullInputFileName)) or (aFullInputFileName.endswith('.dat') and not os.path.isfile(aFullInputFileName + '.hdr')):
        sys.exit("Error: File does not exist! " + aFullInputFileName)

    # Check if the output directory for this specific inputfile exists    
    inputFileName = os.path.basename(aFullInputFileName)
    tilesOutputDirName = os.path.join(aOutputDirName, inputFileName)
    if aAddNewData and os.path.exists(tilesOutputDirName):
        # Only new data should be added, so if the output already exists we can stop this function here.
        return
    elif not os.path.exists(tilesOutputDirName):
        # Try to create the directory if it does not exist yet.
        try:
            os.makedirs(tilesOutputDirName)
        except OSError as exception:
            sys.exit("Error: Output directory " + tilesOutputDirName + " did not exist and could not be created.")

    # Copy the (1st raster-)data into a numpy 2D array
    gdalDataSet = gdal.Open( aFullInputFileName )
    if aRoiSet:
        aUpperIndex = min(aUpperIndex, gdalDataSet.RasterYSize)
        aLeftIndex = min(aLeftIndex, gdalDataSet.RasterXSize)
        aLowerIndex = min(aLowerIndex, gdalDataSet.RasterYSize)
        aRightIndex = min(aRightIndex, gdalDataSet.RasterXSize)
    else:
        aUpperIndex = 0
        aLeftIndex = 0
        aLowerIndex = gdalDataSet.RasterYSize
        aRightIndex = gdalDataSet.RasterXSize

    inputMetaData = NetCdfGeoMetaData()
    inputMetaData.copyFromGdalData(gdalDataSet)
    # If the region of interest is set we need to recalculate the GEO Transformation values.
    if aRoiSet:
        inputMetaData.adjustPixelOriginGeoTransform(aUpperIndex, aLeftIndex)
    historyString = NOW_STRING + " " + SCRIPT_FILE_NAME + " " + SVN_DESCR + " Arguments"
    if aRoiSet: historyString += " --roi=" + str(aUpperIndex) + "," + str(aLeftIndex) + "," + str(aLowerIndex) + "," + str(aRightIndex)
    if aTileSet: historyString += " --tile_size=" + str(aTileHeight) + "," + str(aTileWidth)
    historyString += " " + str(aOutputDirName)
    historyString += " " + str(aFullInputFileName)
    inputMetaData.addToHistory(historyString)
#    print str(inputMetaData)

    dataArray = np.array(gdalDataSet.GetRasterBand(1).ReadAsArray())[aUpperIndex:aLowerIndex,aLeftIndex:aRightIndex]

    tiles.CreateTilesFromArray(dataArray, aTileHeight, aTileWidth, tilesOutputDirName, inputMetaData)
    del gdalDataSet

#    arr2png.arrayToRgbPng(dataArray, 'temp.png',0.0)
    print aFullInputFileName

