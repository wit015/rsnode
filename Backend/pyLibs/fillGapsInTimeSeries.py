#!/usr/bin/env python 

##################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
##################################################################################

SVN_REVISION = '$Rev: 6705 $'
SVN_DATE = '$Date: 2016-05-23 08:58:21 +0200 (Mon, 23 May 2016) $'
SVN_AUTHOR = '$Author: jb76278 $'

# Shortened and readable SVN description
import re
svn_descr = (SVN_REVISION + re.sub('\) \$', ' ' ,re.sub('^.*\(', 'Date: ', SVN_DATE)) + SVN_AUTHOR).replace('$', '')

#import matplotlib.pyplot as plt
import numpy as np
import sys
import os
import re
import resource
import datetime
from time import gmtime, strftime
from netCDF4 import Dataset

local_path = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, local_path + '/code')
sys.path.insert(0, local_path + '/pyLibs')

# The following import(s) need correct setting of the PYTHONPATH environment variable. For example in .bashrc:
# export PYTHONPATH="${PYTHONPATH}:`pwd`/SvnCheckout/CommonSense/RSDS/DataProcessing/Python/code:`pwd`/SvnCheckout/CommonSense/RSDS/DataProcessing/Python/pyLibs"
import tiles
from netCdfGeoMetaData import NetCdfGeoMetaData

script_file_name = os.path.basename(__file__)
now_string = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

######################################################################################################
# Method will set the tiles-of-interest values to include all available tiles.
######################################################################################################
def GetLastIndexToFill(aInputDirName, aTilesObject):
    returnValue = 1

    # Get the time array from an input file    
    nc4FileName = aTilesObject.getUpperLeftFileName("cube_", ".hdf5")
    nc4file = Dataset(os.path.join(aInputDirName, nc4FileName), 'r')
    timeArray = nc4file.variables['time'][:]
    nc4file.close()

    # Look for the last gap of 16 days
    while not timeArray[returnValue] - timeArray[returnValue-1] == 8:
        returnValue += 1

    print "Index found: " + str(returnValue)
    return returnValue    

######################################################################################################
# Method will check the existance of the output directory and try to create it if needed.
######################################################################################################
def MakeSureOutputDirectoryExists(aOutputDirName):

    # Make sure the output directory exists (if not, try to create it)
    if not os.path.exists(aOutputDirName):
        try:
            os.makedirs(aOutputDirName)
        except OSError as exception:
            sys.exit("Error: Output directory " + aOutputDirName + " did not exist and could not be created.")

######################################################################################################
# ExpandCube function will take an input cube and expand it in the time dimension. It will
# input the average value between every two original values.
######################################################################################################
def ExpandCube(aInputCube):
#    print "Shape: " + str(aInputCube.shape)

    # This works for the third dimension so we switch the time axis to the third dimension.
    tempCube = np.swapaxes(aInputCube,0,2)
#    print "Shape: " + str(tempCube.shape)

    # Get the average values inbetween each two consecutive value in the time dimension
    averageCube = tempCube[:,:,:-1] + tempCube[:,:,1:]
    averageCube = np.multiply(averageCube, 0.5)

    # Expand the original with the averages
    positions = np.delete(np.arange(tempCube.size), np.arange(0, tempCube.size, tempCube.shape[2]))
    tempCube = np.resize(np.insert(tempCube.flatten(), positions, averageCube.flatten()), (tempCube.shape[0], tempCube.shape[1], 2*tempCube.shape[2] - 1))
#    print "Filled: " + str(tempCube[0,0,:])

    # Swap the time-axis back to original position before returning
    return np.swapaxes(tempCube, 0, 2)

######################################################################################################
# Method will smooth the timeseries for the cubes defined by the input flags.
######################################################################################################
def FillGapsInTimeSeries(aCallingTilesObject, aInputDirName, aOutputDirName, aLastFillIndex):
    global svn_descr, now_string, script_file_name

    # String for self documenting script
    historyString = now_string + " " + script_file_name + " " + svn_descr + " Arguments"
    historyString += " --toi=" + aCallingTilesObject.getIndicesString()
    historyString += " " + str(aOutputDirName)
    historyString += " " + str(aInputDirName)

    # Object for the meta data of the input (will be similar to that of the output)
    metaData = NetCdfGeoMetaData()

    # Create filename for both the input and output cube 3D-array
    nc4InAndOutputFileName = aCallingTilesObject.getCurrentFileName("cube_", ".hdf5")

    # Get the input data
    nc4file = Dataset(os.path.join(aInputDirName, nc4InAndOutputFileName), 'r')
    key, value = nc4file.variables.items()[3]
    dataArray = nc4file.variables[key][:]

    # Copy the meta data and alter the needed fields for the output later.
    metaData.copyFromNetCdfObject(nc4file)
    metaData.setTitle("Filled " + metaData.getTitle())
    metaData.addToHistory(historyString)
    metaData.setSource("Input dir: " + aInputDirName + " files: " + nc4InAndOutputFileName)
#    print str(metaData)

    # Make a copy of the time variable and its attributes
    timeArray = nc4file.variables['time'][:]
    timeVarAttr = {}
    for key in nc4file.variables['time'].ncattrs():
        timeVarAttr[key] = nc4file.variables['time'].getncattr(key)
    nc4file.close()

    # Create a copy of the part of the cube to put the filled data in
    print "Filling: " + str(nc4InAndOutputFileName)
#    print "Time array: " + str(timeArray[0:aLastFillIndex])
    sparceCube = dataArray[0:aLastFillIndex,:,:]
            
    # Fill the sparce cube with the average values
#    print "Shape: " + str(sparceCube.shape)
    filledCube = ExpandCube(sparceCube)
#    print "Filled: " + str(filledCube[:,0,0])
#    print "Shape: " + str(filledCube.shape)
#    print "Shape: " + str(dataArray[aLastFillIndex:,:,:].shape)
    fullCube = np.append(filledCube, dataArray[aLastFillIndex:,:,:], axis=0)
#    print "Full: " + str(fullCube[:,0,0])
#    print "Shape: " + str(fullCube.shape)
#    print "Shape: " + str(dataArray.shape)

    # Fill in the time values in the time array
    sparceTimeArray = timeArray[0:aLastFillIndex]
    addedTimeArray = np.add(sparceTimeArray[:-1], 8)
    positions = np.arange(1, aLastFillIndex)
    filledTimeArray = np.insert(sparceTimeArray, positions, addedTimeArray)
    fullTimeArray = np.append(filledTimeArray, timeArray[aLastFillIndex:])
#    print "Time:   " + str(sparceTimeArray)
#    print "Added:  " + str(addedTimeArray)
#    print "Filled: " + str(filledTimeArray)
#    print "Full:   " + str(fullTimeArray)
#    print "Shape: " + str(fullTimeArray.shape)
            
#    # Plot the original and smoothed timeseries in a plot
#    t = np.arange(0, aLastFillIndex, 1)
#    plt.figure(1)
#    plt.subplot(211)
#    plt.plot(t, fullCube[:aLastFillIndex,0,0], "rx-");
#    s = np.arange(0, aLastFillIndex, 2)
#    plt.plot(s, dataArray.astype(np.float32)[:(1 + aLastFillIndex/2),0,0], "bx-");
#    plt.show()

    # Create output file
    ncfileOut = Dataset(os.path.join(aOutputDirName, nc4InAndOutputFileName), 'w', format='NETCDF4')
    metaData.copyToNetCdfObject(ncfileOut)
    ncfileOut.createDimension(timeVarAttr['standard_name'], None)
    variable = ncfileOut.createVariable(timeVarAttr['standard_name'], np.dtype('int32').char, (timeVarAttr['standard_name']))
    for key, value in timeVarAttr.iteritems():
        variable.setncattr(key, value)
    variable[:] = fullTimeArray
    metaData.createDimVarsLatLong(ncfileOut, fullCube.shape[1], fullCube.shape[2])

    # Create variable for smoothed data
    data = ncfileOut.createVariable(metaData.getTitle(),np.dtype('float32').char, ('time','latitude','longitude'))
    data.setncattr('standard_name',metaData.getTitle())
    data.setncattr('units',metaData.getUnits())
    data.setncattr('long_name',metaData.getTitle())
    data[:] = fullCube
    ncfileOut.close()
    return

######################################################################################################
# Check and process the input parameters
######################################################################################################
def main(argv):
    if len(sys.argv) < 3 or len(sys.argv) > 4:
        print ""
        print "Usage:"
        print "python fillGapsInTimeSeries.py [flags] <output-dir-name> <input-dir-name>"
        print ""
        print "The input files must be netCDF cube type files!"
        print "Allowed flag examples:"
        print "--toi=0,0,10,2  The tiles of interest are defined with four comma separated pixel-index values upper, left, lower, right. All tiles are processed by default."
        print "--logfile=/tmp/fillGapsInTimeSeries20160314.log  The name (and directory) for the log file."
        print "Example:"
        print "python fillGapsInTimeSeries.py --toi=10,10,12,14 --logfile=/tmp/fillGapsInTimeSeries20160314.log /net/satarch/MODIS/jelle/NdviSmoothTimeSeriesFull500m /net/satarch/MODIS/jelle/NdviSmoothTimeSeries500m/"
        print "python fillGapsInTimeSeries.py /net/satarch/MODIS/jelle/NdviSmoothTimeSeriesFull500m /net/satarch/MODIS/jelle/NdviSmoothTimeSeries500m/"
        print ""
    else:
        flags_dict = {}
        outputDirName = ''
        inputDirName = ''
        toiSet = False
        upperIndex = 0
        leftIndex = 0
        lowerIndex = 0
        rightIndex = 0
        logFileName = None

        # Extract the flags
        argIndex = 1
        while outputDirName == '' and argIndex < len(sys.argv):
            # Check for flags
            if sys.argv[argIndex].count('=') == 1 and sys.argv[argIndex].startswith('--'):
                key, value = sys.argv[argIndex].split('=')
                flags_dict[key] = value
            else:
                outputDirName = sys.argv[argIndex]
            argIndex += 1

        MakeSureOutputDirectoryExists(outputDirName)

        if argIndex == (len(sys.argv) - 1):
            inputDirName = sys.argv[argIndex]
        else:
            sys.exit("Error: Number of arguments is wrong.")

        # Check the flags and extract their values
        for key, value in flags_dict.items():
            if key == '--toi':
                if value.count(',') == 3:
                    one,two,three,four = value.split(',')
                    upperIndex = int(one)
                    leftIndex = int(two)
                    lowerIndex = int(three)
                    rightIndex = int(four)
                    toiSet = True
                else:
                    sys.exit("Error: Format of Value \'" + value + "\' for Key \'" + key + "\' is wrong.")
            else:
                print "Warning: Key \'" + key + "\' is unknown and ignored."

        # Set the tiles indices and loop through them calling the FillGapsInTimeSeries function
        tilesObject = tiles.Tiles()
        tilesObject.setTilesOfInterest(inputDirName, "cube_", ".hdf5", toiSet, upperIndex, leftIndex, lowerIndex, rightIndex)
        lastFillIndex = GetLastIndexToFill(inputDirName, tilesObject)
        tilesObject.loopThroughTiles(FillGapsInTimeSeries,(inputDirName, outputDirName, lastFillIndex))

######################################################################################################
# Only execute when run as a script
######################################################################################################
if __name__ == "__main__":
    main(sys.argv[:])

