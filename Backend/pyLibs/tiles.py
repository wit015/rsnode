################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

SVN_REVISION = '$Rev: 7715 $'
SVN_DATE = '$Date: 2016-12-16 11:40:50 +0100 (Fri, 16 Dec 2016) $'
SVN_AUTHOR = '$Author: jb76278 $'

# Shortened and readable SVN description
import re
SVN_DESCR = (SVN_REVISION + re.sub('\) \$', ' ' ,re.sub('^.*\(', 'Date: ', SVN_DATE)) + SVN_AUTHOR).replace('$', '')

import os
import sys
import copy as copyer
import numpy as np
from netCDF4 import Dataset
import datetime
SCRIPT_FILE_NAME = os.path.basename(__file__)
NOW_STRING = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

######################################################################################################
# Method converts a 2D (numpy) array into tiles with the specified size
# 
# aArray            : the 2D numpy array that is to be split into tiles
# aHeight           : the height in pixels the tiles should have
# aWidth            : the width in pixels the tiles should have
# aDestDir          : the name of the output (destination) where the directory with tile-files will be stored
# aDataDescription  : the description of the data in the 2D array (to make it CF compliant)
######################################################################################################
def CreateTilesFromArray(aArray, aHeight, aWidth, aDestDir, aDataDescription):
    global SVN_DESCR, NOW_STRING, SCRIPT_FILE_NAME

    returnArray = []
    if not os.path.exists(aDestDir):
        os.makedirs(aDestDir)
        print "Created destination directory: " + aDestDir

    # How many tiles do we need?
    heightNum = 1+(aArray.shape[0]-1)/aHeight  # height
    widthNum = 1+(aArray.shape[1]-1)/aWidth  # width

    # Add processing to the history
    historyString = NOW_STRING + " " + SCRIPT_FILE_NAME + " " + SVN_DESCR + " Arguments"
    historyString += " height=" + str(aHeight) + ", width=" + str(aWidth) + ", numY_tiles=" + str(heightNum) + ", numX_tiles=" + str(widthNum)
    aDataDescription.addToHistory(historyString)

    # Create array with enough room for one tile.
    tileArray = np.zeros((aHeight, aWidth), aArray.dtype)

    # Loop through the tiles
    for y in range(0, heightNum):
        for x in range(0, widthNum):
            ySize = min(aHeight,aArray.shape[0]-(y*aHeight))
            xSize = min(aWidth,aArray.shape[1]-(x*aWidth))
            # Copy the data in a tile
            tileArray = aArray[y*aHeight:y*aHeight+ySize, x*aWidth:x*aWidth+xSize]

            # Write the tile-data to files in NetNCF-4 format
            arrayFileName = "tile_" + str(y).zfill(3) + "_" + str(x).zfill(3)
            nc4file = Dataset(aDestDir + "/" + arrayFileName + '.hdf5','w', format='NETCDF4')

            tileDescription = copyer.copy(aDataDescription)
            tileDescription.adjustPixelOriginGeoTransform(y*aHeight, x*aWidth)
            tileDescription.copyToNetCdfObject(nc4file)

            tileDescription.createDimVarsLatLong(nc4file, ySize, xSize)
            data = nc4file.createVariable(tileDescription.getTitle(), tileArray.dtype, ('latitude','longitude'))
            data.setncattr('standard_name',tileDescription.getTitle())
            data.setncattr('units',tileDescription.getUnits())
            data.setncattr('long_name',tileDescription.getTitle())
            data[:] = tileArray[0:ySize, 0:xSize]
            nc4file.close()
         
#    print os.path.basename(aDestDir)

##################################################################################
class Tiles(object):

    def __init__(self):
        self.upperIndex = 0
        self.leftIndex = 0
        self.lowerIndex = 0
        self.rightIndex = 0
        self.toiSet = False
        self.currentUpDownIndex = 0
        self.currentLeftRightIndex = 0
        self.currentlyInLoop = False

    def getUpperIndex(self):
        return self.upperIndex

    def getLeftIndex(self):
        return self.leftIndex

    def getLowerIndex(self):
        return self.lowerIndex

    def getRightIndex(self):
        return self.rightIndex

    def getIndicesString(self):
        return str(self.upperIndex) + "," + str(self.leftIndex) + "," + str(self.lowerIndex) + "," + str(self.rightIndex)

    def getCurrentUpDownIndex(self):
        return self.currentUpDownIndex

    def getCurrentLeftRightIndex(self):
        return self.currentLeftRightIndex

    def getCurrentFileName(self, aPrefix="cube_", aExtension=".hdf5"):
        if self.currentlyInLoop:
            return str(aPrefix) + str(self.currentUpDownIndex).zfill(3) + "_" + str(self.currentLeftRightIndex).zfill(3) + str(aExtension)
        else:
            return "Not in loop. So current file has no meaning."

    def getUpperLeftFileName(self, aPrefix="cube_", aExtension=".hdf5"):
        return str(aPrefix) + str(self.upperIndex).zfill(3) + "_" + str(self.leftIndex).zfill(3) + str(aExtension)

    def getLowerRightFileName(self, aPrefix="cube_", aExtension=".hdf5"):
        return str(aPrefix) + str(self.lowerIndex).zfill(3) + "_" + str(self.rightIndex).zfill(3) + str(aExtension)

    ######################################################################################################
    # Method will loop through the tile-of-intrest, and call the given function with the tile-id-string
    # and the given additional arguments
    ######################################################################################################
    def loopThroughTiles(self, aFunction, aFunctionArguments):
        if self.toiSet and not self.currentlyInLoop:
            self.currentlyInLoop = True
            for self.currentUpDownIndex in range(self.upperIndex, self.lowerIndex + 1):
                for self.currentLeftRightIndex in range(self.leftIndex, self.rightIndex + 1):
                    # Create tile identifying string
                    aFunction(self, *aFunctionArguments)
            self.currentUpDownIndex = 0
            self.currentLeftRightIndex = 0
            self.currentlyInLoop = False
        elif self.currentlyInLoop:
            sys.exit("Error: Trying to start a loop (through the tiles) while already in a loop: ")

    ######################################################################################################
    # Method will set the tiles-of-interest index values. If index values were pre-set it checks if
    # these values are valid (and corrects, and returns, them if not). If no index value were entered then it will
    # set (and return) the index values that include all available tiles.
    #
    # aDirName     : The directory in which to look for the tiles
    # aPrefix      : The prefix of the tile file-names
    # aExtension   : The extension of the tile file-names
    # aToiSet      : (bool) Set to true if the following index values should be considered
    # aUpperIndex  : (int) The upper index value of the upper-left tile
    # aLeftIndex   : (int) The left index value of the upper-left tile
    # aLowerIndex  : (int) The lower index value of the lower-right tile
    # aRightIndex  : (int) The right index value of the lower-right tile
    ######################################################################################################
    def setTilesOfInterest(self, aDirName, aPrefix="cube_", aExtension=".hdf5", aToiSet=False, aUpperIndex=0, aLeftIndex=0, aLowerIndex=0, aRightIndex=0):
        # If directory name exists we can proceed.
        if os.path.exists(aDirName):
            # Make sure the indices are positive and left index is lower in value then right index and so on
            upperIndex = max(min(int(aUpperIndex),int(aLowerIndex)), 0)
            leftIndex = max(min(int(aLeftIndex),int(aRightIndex)), 0)
            lowerIndex = max(max(int(aUpperIndex),int(aLowerIndex)), 0)
            rightIndex = max(max(int(aLeftIndex),int(aRightIndex)), 0)

            # If the tiles-of-interst are not set all indices are zero
            if not aToiSet:
                upperIndex = 0
                leftIndex = 0
                lowerIndex = 0
                rightIndex = 0

            # Set the indices member variables (from here on we start checking the values)
            self.upperIndex = upperIndex
            self.leftIndex = leftIndex
            self.lowerIndex = lowerIndex
            self.rightIndex = rightIndex

            # Upper left tile must exist
            nc4FileName = self.getUpperLeftFileName(aPrefix, aExtension)
            if not os.path.isfile(os.path.join(aDirName, nc4FileName)):
                sys.exit("Error: Upper left tile- or cube-file \'" + str(os.path.join(aDirName, nc4FileName)) + "\' does not exist.")

            # Start looking for the lower right tile
            y = upperIndex + 1
            x = leftIndex + 1

            # Look for the tile files adjust the lower right indices if needed
            nc4FileName = str(aPrefix) + str(y).zfill(3) + "_" + str(leftIndex).zfill(3) + str(aExtension)
            while os.path.isfile(os.path.join(aDirName, nc4FileName)) and (y <= lowerIndex or not aToiSet):
                y += 1
                nc4FileName = str(aPrefix) + str(y).zfill(3) + "_" + str(leftIndex).zfill(3) + str(aExtension)
            lowerIndex = y - 1

            nc4FileName = str(aPrefix) + str(upperIndex).zfill(3) + "_" + str(x).zfill(3) + str(aExtension)
            while os.path.isfile(os.path.join(aDirName, nc4FileName)) and (x <= rightIndex or not aToiSet):
                x += 1
                nc4FileName = str(aPrefix) + str(upperIndex).zfill(3) + "_" + str(x).zfill(3) + str(aExtension)
            rightIndex = x - 1

            # Set the indices member variables
            self.upperIndex = upperIndex
            self.leftIndex = leftIndex
            self.lowerIndex = lowerIndex
            self.rightIndex = rightIndex
            self.toiSet = True
        else:
            sys.exit("Error: No tile directory present at: " + str(aDirName))

    # Convert object to string representation for printing/debugging
    def __str__(self):
        returnString = "Tiles Of Interest set: "
        returnString += "True" if self.toiSet else "False"
        returnString += "\n"
        returnString += "TOI: "
        returnString += self.getIndicesString()
        returnString += "\n"
        return returnString

