#!/usr/bin/env python
#
# Replace all values unequal to NoDataValue in a TIFF file
#
################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2018, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

import sys
import numpy as np

# The GDAL imports for geoTiff (a.o.)
from osgeo import gdal
from osgeo import osr
from osgeo.gdalconst import *


MASKSET = [0, 140, 150, 152, 153, 160, 170, 180, 190, 200, 201, 202, 210, 220]

def _genEsaCciMask(aTemplateFileName, aOutFileName, aMaskSet):
   mds         = gdal.Open(aTemplateFileName)
   driver      = mds.GetDriver()
   lc          = mds.ReadAsArray()
   nds         = driver.CreateCopy(aOutFileName, mds, 0)
   band        = nds.GetRasterBand(1)
   mask        = np.copy(lc)
   # Mask pixels in set
   mask[:,:] = 0
   # Unmask pixels not in set
   ix = np.in1d(lc.ravel(), aMaskSet, invert=True).reshape(lc.shape)
   mask[ix]  = 1
   # Write data
   band.WriteArray(mask)
   band.SetNoDataValue(0.0)
   # Recompute statistics
   band.ComputeStatistics(0)
   # Close files
   mds      = None
   nds      = None

def genEsaCciMask(argv):
   if len(argv)<2:
      print('Usage: %s <infile> <outfile> [--mask=<value>]*' % (argv[0]))
      sys.exit(1)
   num    = 1.0
   maskset = []
   for i in range(3,len(argv)):
      key, value = argv[argIndex].split('=')
      if   key == '--mask':
         mask.append(eval(argv[i]))
      else:
         print('%s : illegal flag "%s"' % (argv[0], argv[i]))
         sys.exit(1)
   if len(maskset) == 0:
      maskset = MASKSET
   _genEsaCciMask(argv[1], argv[2], maskset)

if __name__ == "__main__":
   genEsaCciMask(sys.argv)
