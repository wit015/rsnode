################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2018, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

SVN_REVISION = '$Rev: 6835 $'
SVN_DATE = '$Date: 2016-06-01 13:52:19 +0200 (Wed, 01 Jun 2016) $'
SVN_AUTHOR = '$Author: jb76278 $'

import os
import sys
import datetime
import numpy as np
from netCDF4 import Dataset

from netCdfGeoMetaData import NetCdfGeoMetaData

# Shortened and readable SVN description
import re
SVN_DESCR = (SVN_REVISION + re.sub('\) \$', ' ' ,re.sub('^.*\(', 'Date: ', SVN_DATE)) + SVN_AUTHOR).replace('$', '')
SCRIPT_FILE_NAME = os.path.basename(__file__)
NOW_STRING = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

def cumsumwindow(aInputFileName, aDataVarName, aWindowSize, aOutputFileName) :

    metaData = NetCdfGeoMetaData()

    ########################################################
    # Open input file
    ########################################################

    ncfilein = Dataset(os.path.join(aInputFileName), 'r')
    ts       = ncfilein.variables[aDataVarName]
    metaData.copyFromNetCdfObject(ncfilein)

    ########################################################
    # Create output file
    ########################################################

    print aWindowSize
    metaData.setTitle("Cumulative sum with windows=%d of %s" % (aWindowSize, metaData.getTitle()))
    historyString = NOW_STRING + " " + \
                    SCRIPT_FILE_NAME + " " + \
                    SVN_DESCR + " Arguments"
    metaData.addToHistory(historyString)
    metaData.setSource("Input file: " + aInputFileName)

    ncfileOut = Dataset(os.path.join(aOutputFileName),'w', format='NETCDF4')
    metaData.copyToNetCdfObject(ncfileOut)

    #Copy dimensions
    for dname, ddata in ncfilein.dimensions.iteritems():
        print dname, len(ddata)
        ncfileOut.createDimension(dname, len(ddata))

    # Copy variables
    for vname, vdata in ncfilein.variables.iteritems():

        print vname, vdata.datatype, vdata.dimensions
        nvar = ncfileOut.createVariable(vname, vdata.datatype, vdata.dimensions)

        # Copy variable attributes
        nvar.setncatts({k: vdata.getncattr(k) for k in vdata.ncattrs()})

        if vname == aDataVarName:
            nvar[:] = vdata[:]
            for i in range(1, aWindowSize):
               print i
               nvar[i:] = nvar[i:] + vdata[:-i]
        else:
            nvar[:] = vdata[:]

def usage():
    usageString = "usage: %s <inputFileName> <dataVarName> <windowSize> <outputFilename>\n" % sys.argv[0]
    sys.exit(usageString)

if __name__ == "__main__":

    inputFileName  = ''
    dataVarName    = ''
    outputFilename = ''
    windowSize     = 1

    fixedParamaterCntr = 0
    for i in range(1, len(sys.argv)):
        if sys.argv[i].startswith('--'):
            key, value = sys.argv[i].split('=')
            usage()
        else:
            if fixedParamaterCntr == 0:
                inputFileName = sys.argv[i]
                fixedParamaterCntr += 1
            elif fixedParamaterCntr == 1:
                dataVarName   = sys.argv[i]
                fixedParamaterCntr += 1
            elif fixedParamaterCntr == 2:
                windowSize    = int(sys.argv[i])
                fixedParamaterCntr += 1
            elif fixedParamaterCntr == 3:
                outputFilename = sys.argv[i]
                fixedParamaterCntr += 1
            else:
                usage()

    if fixedParamaterCntr < 4:
        usage()

    cumsumwindow(inputFileName, dataVarName, windowSize, outputFilename)
