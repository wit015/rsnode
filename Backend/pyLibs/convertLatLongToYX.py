##################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
##################################################################################

SVN_REVISION = '$Rev: 7715 $'
SVN_DATE = '$Date: 2016-12-16 11:40:50 +0100 (Fri, 16 Dec 2016) $'
SVN_AUTHOR = '$Author: jb76278 $'

# Shortened and readable SVN description
import re
SVN_DESCR = (SVN_REVISION + re.sub('\) \$', ' ' ,re.sub('^.*\(', 'Date: ', SVN_DATE)) + SVN_AUTHOR).replace('$', '')

import sys
import os
import datetime

# The following import(s) need correct setting of the PYTHONPATH environment variable. For example in .bashrc:
# export PYTHONPATH="${PYTHONPATH}:`pwd`/SvnCheckout/CommonSense/RSDS/DataProcessing/Python/code:`pwd`/SvnCheckout/CommonSense/RSDS/DataProcessing/Python/pyLibs"
from netCdfGeoMetaData import NetCdfGeoMetaData
import fileSystemOps

from netCDF4 import Dataset
import datetime
import json

SCRIPT_FILE_NAME = os.path.basename(__file__)
NOW_STRING = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

######################################################################################################
# Method will convert the given lat/long upper-left and lower-right corners to indexes
######################################################################################################
def convertLatLongToYX(aInputFileName, aRoiSet, aUpperLat, aLeftLong, aLowerLat, aRightLong):

    # Object for the meta data of the input (will be similar to that of the output)
    metaData = NetCdfGeoMetaData()

    # Open the input file and get the needed (meta-)data
    nc4file = Dataset(os.path.join(aInputFileName), 'r')
    metaData.copyFromNetCdfObject(nc4file)
    latSize = len(nc4file.dimensions['latitude'])
    longSize = len(nc4file.dimensions['longitude'])
    nc4file.close()

    upperIndex = 0;
    lowerIndex = latSize - 1;
    leftIndex = 0;
    rightIndex = longSize - 1;

    if aRoiSet:
        upperIndex,leftIndex = metaData.getPixelYxFromLatLong(aUpperLat, aLeftLong)
        lowerIndex,rightIndex = metaData.getPixelYxFromLatLong(aLowerLat, aRightLong)
        if upperIndex < 0 or upperIndex >= latSize:
            upperIndex = min(max(0, upperIndex), latSize-1)
            sys.stderr.write("Warning: Upper latitude outside input file data. Setting it to extreme: " + str(upperIndex) + "\n")
        if leftIndex < 0 or leftIndex >= longSize:
            leftIndex = min(max(0, leftIndex), longSize-1)
            sys.stderr.write("Warning: Left longitude outside input file data. Setting it to extreme: " + str(leftIndex) + "\n")
        if lowerIndex < 0 or lowerIndex >= latSize:
            lowerIndex = min(max(0, lowerIndex), latSize-1)
            sys.stderr.write("Warning: Lower latitude outside input file data. Setting it to extreme: " + str(lowerIndex) + "\n")
        if rightIndex < 0 or rightIndex >= longSize:
            rightIndex = min(max(0, rightIndex), longSize-1)
            sys.stderr.write("Warning: Right longitude outside input file data. Setting it to extreme: " + str(rightIndex) + "\n")

    print str(json.dumps([upperIndex, leftIndex, lowerIndex, rightIndex]))

######################################################################################################
# Check and process the input parameters
######################################################################################################
if len(sys.argv) < 2 or len(sys.argv) > 3:
    print ""
    print "Usage:"
    print "python convertLatLongToYX.py [flags] <input-file-name>"
    print ""
    print "The script returns the region of interest (entered in WGS-84 coordinates) as pixel-value coordinates specific to the input file. The input file must be netCDF type file!"
    print "Flag format:"
    print "--roi=18.5,34.5,17.1,36.5  The region of interest is defined with four comma separated lat/long values upper-lat, left-long, lower-lat, right-long."
    print "By default the whole input-file region is returned."
    print ""
    print "Example:"
    print "python convertLatLongToYX.py --roi=18.5,34.5,17.1,36.5 /net/satarch/CommonSense/DataCubes/Released/CHIRPS/cube_EthiopiaRain.hdf5"
    print "python convertLatLongToYX.py /net/satarch/CommonSense/DataCubes/Released/CHIRPS/multi_cube_EthiopioRainStats.hdf5"
    print ""
else:
    flags_dict = {}
    inputFileName = ''
    roiSet = False
    upperLat = 0
    leftLong = 0
    lowerLat = 0
    rightLong = 0

    # Extract the flags
    argIndex = 1
    while inputFileName == '' and argIndex < len(sys.argv):
        # Check for flags
        if sys.argv[argIndex].count('=') == 1 and sys.argv[argIndex].startswith('--'):
            key, value = sys.argv[argIndex].split('=')
            flags_dict[key] = value
        else:
            inputFileName = sys.argv[argIndex]
        argIndex += 1

    # Check validity of the outputdirectory
    fileSystemOps.CheckIfFileExists(inputFileName)

    # Check the flags and extract their values
    for key, value in flags_dict.items():
        if key == '--roi':
            if value.count(',') == 3:
                one,two,three,four = value.split(',')
                upperLat = max(float(one),float(three))
                leftLong = min(float(two),float(four))
                lowerLat = min(float(one),float(three))
                rightLong = max(float(two),float(four))
                roiSet = True
            else:
                sys.exit("Error: Format of Value \'" + value + "\' for Key \'" + key + "\' is wrong.")
        else:
            print "Warning: Key \'" + key + "\' is unknown and ignored."

    convertLatLongToYX(inputFileName, roiSet, upperLat, leftLong, lowerLat, rightLong)

