#!/usr/bin/env python 

################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

SVN_REVISION = '$Rev: 6685 $'
SVN_DATE = '$Date: 2016-05-20 12:44:22 +0200 (Fri, 20 May 2016) $'
SVN_AUTHOR = '$Author: jb76278 $'

import sys

# The following import(s) need correct setting of the PYTHONPATH environment variable. For example in .bashrc:
# export PYTHONPATH="${PYTHONPATH}:`pwd`/SvnCheckout/CommonSense/RSDS/DataProcessing/Python/code:`pwd`/SvnCheckout/CommonSense/RSDS/DataProcessing/Python/pyLibs"
import fileSystemOps
import splitGdalIntoHdf5TilesAlgorithm

######################################################################################################
# Check and process the input parameters
######################################################################################################
def Main(argv):
    if len(sys.argv) < 3:
        print ""
        print "Usage:"
        print "python splitGdalIntoHdf5Tiles.py [flags] <output-dir-name> <input-file-names>"
        print ""
        print "The input files must be GeoTiff or ENVI type files!"
        print "Allowed flag examples:"
        print "--roi=0,0,1000,2000  The region of interest is defined with four comma separated pixel-index values upper, left, lower, right. The whole dataset is processed by default."
        print "--tile_size=200,300 The tile size is defined with two comma separated pixel values height, width. By default height 150 and width 200 is used."
        print "--add_new=true Script will only add new data and leave old data as it was."
        print ""
        print "Example(s):"
        print "python splitGdalIntoHdf5Tiles.py --tile_size=200,300 --roi=0,0,1000,2000 /net/satarch/MODIS/jelle/tileData/ /net/satarch/MODIS/Processed/MYD13A1/NDVI_2*.dat"
        print "python splitGdalIntoHdf5Tiles.py /net/satarch/MODIS/jelle/tileData/ /net/satarch/MODIS/Processed/MYD13A1/NDVI_2*.dat"
        print "python splitGdalIntoHdf5Tiles.py --tile_size=300,400 /net/satarch/CHIRPS/jelle/rainTileData/ /net/satarch/CHIRPS/africa_dekad/chirps-v2.0.1992.*.tif"
        print "python splitGdalIntoHdf5Tiles.py --roi=400,1040,800,1400 /net/satarch/CHIRPS/jelle/rainTileData/ /net/satarch/CHIRPS/africa_dekad/chirps-v2.0.*.tif"
        print ""
    else:
        flags_dict = {}
        outputDirName = ''
        roiSet = False
        upperIndex = 0
        leftIndex = 0
        lowerIndex = 0
        rightIndex = 0
        tileSet = False
        addNewData = False
        tileHeight = 150  # latitude-axis, default value
        tileWidth = 200   # longitude-axis, default value

        # Extract the flags
        argIndex = 1
        while outputDirName == '' and argIndex < len(sys.argv):
            # Check for flags
            if sys.argv[argIndex].count('=') == 1 and sys.argv[argIndex].startswith('--'):
                key, value = sys.argv[argIndex].split('=')
                flags_dict[key] = value
            else:
                outputDirName = sys.argv[argIndex]
            argIndex += 1

        # Check validity of the (top level) outputdirectory
        fileSystemOps.MakeSureDirectoryExists(outputDirName)

        # Check the flags and extract their values
        for key, value in flags_dict.items():
            if key == '--roi':
                if value.count(',') == 3:
                    one,two,three,four = value.split(',')
                    upperIndex = min(int(one),int(three))
                    leftIndex = min(int(two),int(four))
                    lowerIndex = max(int(one),int(three))
                    rightIndex = max(int(two),int(four))
                    roiSet = True
                else:
                    sys.exit("Error: Format of Value \'" + value + "\' for Key \'" + key + "\' is wrong.")
            elif key == '--tile_size':
                if value.count(',') == 1:
                    one,two = value.split(',')
                    tileHeight = int(one)
                    tileWidth = int(two)
                    tileSet = True
                else:
                    sys.exit("Error: Format of Value \'" + value + "\' for Key \'" + key + "\' is wrong.")
            elif key == '--add_new':
                if value == "true" or value == "True":
                    addNewData = True
            else:
                print "Warning: Key \'" + key + "\' is unknown and ignored."

        # Loop through all the input files and split them into tiles
        while argIndex < len(sys.argv):
            splitGdalIntoHdf5TilesAlgorithm.SplitGdalIntoHdf5Tiles(
                outputDirName, roiSet, 
                upperIndex, leftIndex, lowerIndex, rightIndex,
                tileSet, tileHeight, tileWidth, addNewData, sys.argv[argIndex])
            argIndex += 1

######################################################################################################
# Only execute when run as a script
######################################################################################################
if __name__ == "__main__":
    Main(sys.argv[:])

