#!/usr/bin/python
#
################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2018, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################


from os import listdir
from os.path import isfile, join, isdir
from os.path import basename
from dateutil.parser import parse as dateparse
import subprocess as sp
import glob
import argparse
from shutil import copyfile
import uuid
import s2utils as s2


if __name__ == "__main__":
    # execute only if run as a script
    parser = argparse.ArgumentParser()
    parser.add_argument("s2datadir",   help="Sentinel-2 data directory for granule")
    parser.add_argument("ref",   help="Reference image")
    parser.add_argument("--output", help="Output file")
    parser.add_argument("--tmp", help="Use temporary folder")
    args = parser.parse_args()

    DATADIR 	= args.s2datadir
    REFIMG 	= args.ref
    OUTIMG 	= args.output
    TMPDIR      = args.tmp
    
    lastprod = s2.get_last_s21cproduct(DATADIR)
    granfile = s2.get_granule('TCI', lastprod)
    if TMPDIR is None:
        #ndfile = s2.set_no_data(granfile, '/tmp/nofile.tif')   
        #print('Produced transparent granule of latest: ' + str(granfile))
        s2.merge_granules(REFIMG, granfile, ('out.tif' if OUTIMG is None else OUTIMG))
    else:        
        tmpfile = join(TMPDIR,str(uuid.uuid4()) + '.tif')
        copyfile(granfile, tmpfile)
        s2.merge_granules(REFIMG, tmpfile, ('out.tif' if OUTIMG is None else OUTIMG))

    print('Done.')

