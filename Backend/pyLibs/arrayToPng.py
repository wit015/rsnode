##################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
##################################################################################

SVN_REVISION = '$Rev: 7715 $'
SVN_DATE = '$Date: 2016-12-16 11:40:50 +0100 (Fri, 16 Dec 2016) $'
SVN_AUTHOR = '$Author: jb76278 $'

import sys
import numpy as np
import png

##################################################################################
# Convert a 2D array into a greyscale png image where
# 0 is black and 255 is white
##################################################################################
def arrayToGreyPng(aArray, aFileName='test.png'):
    myPngArray = convertTo8bitArray(aArray)
    f = open(aFileName, 'wb')
    w = png.Writer(myPngArray.shape[1], myPngArray.shape[0], greyscale=True)
    w.write(f, myPngArray)
    f.close()

##################################################################################
# Convert three 2D arrays (of the same size) into a RGB png image
##################################################################################
def arraysToRgbPng(aArrayRed, aArrayGreen=None, aArrayBlue=None, aFileName='test.png', aArrayMin=None, aArrayMax=None, aBrightNess=None, aBrightnessMin=None, aBrightnessMax=None):
    if aArrayRed is None or aArrayRed.shape is (0,0):
        sys.stderr.write("Error: Cannot have None or empty 2D array as first argument")
        return None
    else:
        myRedArray = convertTo8bitArray(aArrayRed, aArrayMin, aArrayMax, aBrightNess, aBrightnessMin, aBrightnessMax)

    if aArrayGreen is None:
        myGreenArray=np.zeros(aArrayRed.shape)
    else:
        if aArrayGreen.shape == aArrayRed.shape:
            myGreenArray = convertTo8bitArray(aArrayGreen, aArrayMin, aArrayMax, aBrightNess, aBrightnessMin, aBrightnessMax)
        else:
            sys.stderr.write("Error: Second array argument has to have same shape as first array argument")
            return None

    if aArrayBlue is None:
        myBlueArray=np.zeros(aArrayRed.shape)
    else:
        if aArrayBlue.shape == aArrayRed.shape:
            myBlueArray = convertTo8bitArray(aArrayBlue, aArrayMin, aArrayMax, aBrightNess, aBrightnessMin, aBrightnessMax)
        else:
            sys.stderr.write("Error: Third array argument has to have same shape as first array argument")
            return None
    rgbArray = np.dstack((myRedArray,myGreenArray,myBlueArray))
    rgbFlatArray = rgbArray.reshape(myRedArray.shape[0],myRedArray.shape[1]*3)

    f = open(aFileName, 'wb')
    w = png.Writer(myRedArray.shape[1], myRedArray.shape[0])
    w.write(f, rgbFlatArray)
    f.close()

##################################################################################
# Convert one 2D array into a 'rainbow' RGB png image
##################################################################################
def arrayToRgbPng(aArray, aFileName='test.png', aArrayMin=None, aArrayMax=None, aBrightNess=None, aBrightnessMin=None, aBrightnessMax=None):
    arrayMin = aArray.min()
    arrayMax = aArray.max()
    if not aArrayMin == None:
        arrayMin = float(aArrayMin)
        aArray = np.maximum(aArray, arrayMin)
    if not aArrayMax == None:
        arrayMax = float(aArrayMax)
        aArray = np.minimum(aArray, arrayMax)
    arrayRange = float(arrayMax - arrayMin)

    sixThousandArray = np.zeros(aArray.shape, dtype=float)
    if arrayRange != 0.0:
        sixThousandArray = np.subtract(aArray.astype(float), arrayMin)
        sixThousandArray = np.multiply(sixThousandArray, (6000.0/arrayRange))
    else:
        sixThousandArray = np.where(np.isnan(aArray), np.nan, 0)

#    amplitude = 75.0
    myRedArrayFloat = np.zeros(aArray.shape, dtype=float)
    myRedArrayFloat = np.subtract(sixThousandArray, 4000.0)
    myRedArrayFloat = np.absolute(myRedArrayFloat)
    myRedArrayFloat = np.multiply(myRedArrayFloat, -1)
    myRedArrayFloat = np.clip(myRedArrayFloat, -2000.0, -1000.0)
#    myRedArrayFloat = np.clip(myRedArrayFloat, -2000.0, 0.0)
#    myRedArrayFloat = np.where(myRedArrayFloat >= -1000, myRedArrayFloat / (1000/(2*np.pi)), myRedArrayFloat)
#    myRedArrayFloat = np.where(myRedArrayFloat >= -1000, np.cos(myRedArrayFloat), myRedArrayFloat)
#    myRedArrayFloat = np.where(myRedArrayFloat >= -1000, (amplitude * myRedArrayFloat) - (1000 + 2 * amplitude), myRedArrayFloat)
   
    myGreenArrayFloat = np.zeros(aArray.shape, dtype=float)
    myGreenArrayFloat = np.subtract(sixThousandArray, 2000.0)
    myGreenArrayFloat = np.absolute(myGreenArrayFloat)
    myGreenArrayFloat = np.multiply(myGreenArrayFloat, -1)
    myGreenArrayFloat = np.clip(myGreenArrayFloat, -2000.0, -1000.0)
#    myGreenArrayFloat = np.clip(myGreenArrayFloat, -2000.0, 0.0)
#    myGreenArrayFloat = np.where(myGreenArrayFloat >= -1000, myGreenArrayFloat / (1000/(2*np.pi)), myGreenArrayFloat)
#    myGreenArrayFloat = np.where(myGreenArrayFloat >= -1000, np.cos(myGreenArrayFloat), myGreenArrayFloat)
#    myGreenArrayFloat = np.where(myGreenArrayFloat >= -1000, (amplitude * myGreenArrayFloat) - (1000 + 2 * amplitude), myGreenArrayFloat)

    myBlueArrayFloat = np.zeros(aArray.shape, dtype=float)
    myBlueArrayFloat = np.subtract(sixThousandArray, 3000.0)
    myBlueArrayFloat = np.absolute(myBlueArrayFloat)
    myBlueArrayFloat = np.subtract(myBlueArrayFloat, 3000.0)
    myBlueArrayFloat = np.clip(myBlueArrayFloat, -2000.0, -1000.0)
#    myBlueArrayFloat = np.clip(myBlueArrayFloat, -2000.0, 0.0)
#    myBlueArrayFloat = np.where(myBlueArrayFloat >= -1000, myBlueArrayFloat / (1000/(2*np.pi)), myBlueArrayFloat)
#    myBlueArrayFloat = np.where(myBlueArrayFloat >= -1000, np.cos(myBlueArrayFloat), myBlueArrayFloat)
#    myBlueArrayFloat = np.where(myBlueArrayFloat >= -1000, (amplitude * myBlueArrayFloat) - (1000 + 2 * amplitude), myBlueArrayFloat)

    arraysToRgbPng(myRedArrayFloat, myGreenArrayFloat, myBlueArrayFloat, aFileName, -2000.0, -1000.0, aBrightNess, aBrightnessMin, aBrightnessMax)

##################################################################################
# Will scale the argument array to an 8-bit unsigned int array
# aArray       : The input array that is to be scaled into an 8-bit unsigned integer [0..255] array
# aArrayMin    : The forced value that is to be taken as the minimum of the input array (optional)
# aArrayMin    : The forced value that is to be taken as the maximum of the input array (optional)
# aBrightness  : The input array that is to be taken as the brightness of the output array (optional)
#                So if for a pixel the aArray would result in value 240 but the brightness for that
#                pixel is 50% then the output for that pixel becomes 120.
# aArrayMin    : The forced value that is to be taken as the minimum of the brightness array (optional)
# aArrayMin    : The forced value that is to be taken as the minimum of the brightness array (optional)
##################################################################################
def convertTo8bitArray(aArray, aArrayMin=None, aArrayMax=None, aBrightness=None, aBrightnessMin=None, aBrightnessMax=None):
    # Casting to float
    myArray = np.copy(aArray.astype(float))

    # Set the Min and Max is they are not set already
    # and clip the input array if the min and/or max were given
    if aArrayMin == None:
        aArrayMin = float(myArray.min())
    else:
        myArray = np.maximum(myArray, float(aArrayMin))

    if aArrayMax == None:
        aArrayMax = float(myArray.max())
    else:
        myArray = np.minimum(myArray, float(aArrayMax))

    # Forcing the whole array  into the [0.0 .. 255.0] range
    arrayRange = float(aArrayMax - aArrayMin)
    myArray = np.subtract(myArray, float(aArrayMin))
    if arrayRange != 0.0:
        myArray = np.multiply(myArray, (255.0/arrayRange))

    # Multiplying to brightness (range [0.0 .. 1.0] if brightness array was given
    if not aBrightness == None:
        # Casting to float
        zeroToOneArray = np.copy(aBrightness.astype(float))

        # Set the Min and Max is they are not set already
        # and clip the input array if the min and/or max were given
        if aBrightnessMin == None:
            aBrightnessMin = float(zeroToOneArray.min())
        else:
            zeroToOneArray = np.maximum(zeroToOneArray,float(aBrightnessMin))
   
        if aBrightnessMax == None:
            aBrightnessMax = float(zeroToOneArray.max())
        else:
            zeroToOneArray = np.minimum(zeroToOneArray,float(aBrightnessMax))

        # Forcing the whole brightness array  into the [0.0 .. 1.0] range
        arrayRange = float(aBrightnessMax - aBrightnessMin)
        zeroToOneArray = np.subtract(zeroToOneArray, float(aBrightnessMin))
        if arrayRange != 0.0:
            zeroToOneArray = np.multiply(zeroToOneArray, (1.0/arrayRange))

        # Combining the brightness with the converted input array
        myArray = np.multiply(myArray, zeroToOneArray)

    # Casting to 8-bit unsigned int
    myUInt8Array = np.copy(myArray.astype(np.uint8))
    return myUInt8Array

