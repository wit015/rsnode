#!/usr/bin/env python 

################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

import re
SVN_REVISION = '$Rev: 6960 $'
SVN_DATE = '$Date: 2016-06-15 10:13:10 +0200 (Wed, 15 Jun 2016) $'
SVN_AUTHOR = '$Author: jb76278 $'

#import numpy as np
import sys
import os
#import gc
#import resource

# The following import(s) need correct setting of the PYTHONPATH environment variable. For example in .bashrc:
# export PYTHONPATH="${PYTHONPATH}:`pwd`/SvnCheckout/CommonSense/RSDS/DataProcessing/Python/code:`pwd`/SvnCheckout/CommonSense/RSDS/DataProcessing/Python/pyLibs"
import merge3DnetCDFsAlgorithm
#import arrayToPng as arr2png
import tiles
import fileSystemOps

#from osgeo import osr
#from osgeo import gdal
from netCdfGeoMetaData import NetCdfGeoMetaData
from netCDF4 import Dataset
#import datetime

######################################################################################################
# Check and process the input parameters
######################################################################################################
def Main(aArgValues):
    if len(aArgValues) < 3 or len(aArgValues) > 5:
        print ""
        print "Usage:"
        print "python merge3DnetCDFs.py [flags] <output-file-name> <input-dir-name>"
        print ""
        print "The input files must be netCDF cube type files!"
        print "Allowed flag examples:"
        print "--toi=0,0,10,2  The tiles of interest is defined with four comma separated pixel-index values upper, left, lower, right. All tiles are processed by default."
        print "Example:"
        print "python merge3DnetCDFs.py --toi=0,0,2,1 /net/satarch/CHIRPS/jelle/cube_new.hdf5 /net/satarch/CHIRPS/jelle/rainTimeSeries/"
        print "python merge3DnetCDFs.py /net/satarch/CHIRPS/jelle/cube_new.hdf5 /net/satarch/CHIRPS/jelle/rainTimeSeries/"
        print "python merge3DnetCDFs.py --toi=7,4,10,8 /net/satarch/MODIS/jelle/cube_new.hdf5 /net/satarch/MODIS/jelle/NdviSmoothTimeSeries500m/"
        print "python merge3DnetCDFs.py /net/satarch/MODIS/jelle/cube_new.hdf5 /net/satarch/MODIS/jelle/NdviSmoothTimeSeries500m/"
        print ""
    else:
        flagsDict      = {}
        outputFileName = ''
        inputDirName   = ''
        toiSet         = False
        upperIndex     = 0
        leftIndex      = 0
        lowerIndex     = 0
        rightIndex     = 0
        addNewData     = False
        noDataValue    = None
        replaceNoData  = False

        # Extract the flags
        argIndex = 1
        while outputFileName == '' and argIndex < len(aArgValues):
            # Check for flags
            if aArgValues[argIndex].count('=') == 1 and aArgValues[argIndex].startswith('--'):
                key, value = aArgValues[argIndex].split('=')
                flagsDict[key] = value
            else:
                outputFileName = aArgValues[argIndex]
            argIndex += 1

        fileSystemOps.MakeSureDirectoryExists(os.path.dirname(outputFileName))

        if argIndex == (len(aArgValues) - 1):
            inputDirName = aArgValues[argIndex]
        else:
            sys.exit("Error: Number of arguments is wrong.")

        # Check the flags and extract their values
        for key, value in flagsDict.items():
            if key == '--toi':
                if value.count(',') == 3:
                    one,two,three,four = value.split(',')
                    upperIndex = int(one)
                    leftIndex = int(two)
                    lowerIndex = int(three)
                    rightIndex = int(four)
                    toiSet = True
                else:
                    sys.exit("Error: Format of Value \'" + value + "\' for Key \'" + key + "\' is wrong.")
            elif key == '--add_new':
                if value == "true" or value == "True":
                    addNewData = True
            elif key == '--replace_nodata':
                replaceNoData = True
            else:
                print "Warning: Key \'" + key + "\' is unknown and ignored."

        # Set the tiles indices and check output directory and the first year with full data.
        tilesObject = tiles.Tiles()
        tilesObject.setTilesOfInterest(inputDirName, "cube_", ".hdf5", toiSet, upperIndex, leftIndex, lowerIndex, rightIndex)

        height, width = merge3DnetCDFsAlgorithm.GetHeightAndWidth(inputDirName, tilesObject.getUpperLeftFileName("cube_", ".hdf5"))
        if addNewData:
            print "Enlarging output file: " + str(outputFileName)
            outputCube = Dataset(outputFileName, 'a')
            print "Adding new data."
            addedLayers = merge3DnetCDFsAlgorithm.EnlargeOutputCube(inputDirName, outputCube, tilesObject)
            print "Layers to add: " + str(addedLayers)
            key, value = outputCube.variables.items()[3]
            outputData = outputCube.variables[key]

            # set noDataValue If NoDataValue is to be replaced
            metaData = NetCdfGeoMetaData()
            metaData.copyFromNetCdfObject(outputCube)
            if replaceNoData and metaData.getNoDataValue() <> '<None>':
               noDataValue = eval(metaData.getNoDataValue())

            tilesObject.loopThroughTiles(
               merge3DnetCDFsAlgorithm.AppendOutputWithInput,
               (inputDirName, outputData, height, width, addedLayers, noDataValue))

            outputCube.close()
        else:
            outputCube = merge3DnetCDFsAlgorithm.CreateOutputCube(inputDirName, outputFileName, tilesObject)
            key, value = outputCube.variables.items()[3]
            outputData = outputCube.variables[key]

            # set noDataValue If NoDataValue is to be replaced
            metaData = NetCdfGeoMetaData()
            metaData.copyFromNetCdfObject(outputCube)
            if replaceNoData and metaData.getNoDataValue() <> '<None>':
               noDataValue = eval(metaData.getNoDataValue())

            tilesObject.loopThroughTiles(
               merge3DnetCDFsAlgorithm.CopyCubesIntoOutput,
               (inputDirName, outputData, height, width, noDataValue))

            outputCube.close()
            
######################################################################################################
# Only execute when run as a script
######################################################################################################
if __name__ == "__main__":
    Main(sys.argv[:])

