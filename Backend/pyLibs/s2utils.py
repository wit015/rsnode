################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2018, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

from os import listdir
from os.path import isfile, join, isdir
from os.path import basename
from dateutil.parser import parse as dateparse
import subprocess as sp
import glob
import sys
import os

def parse_s21cfilename(pfile):
    bname = basename(pfile)
    bparts = bname.split('_')
    return bparts[0], bparts[1], bparts[2], bparts[3], bparts[4], bparts[5]


def get_last_s21cproduct(dpath):    
    pdates = []
    pnames = []
    for f in listdir(dpath):
        if isdir(join(dpath, f)):
            sat, prodtype, sensingt, procnumber, rorbitn, tile = parse_s21cfilename(f)
            #YYYYMMDDHHMMSS
            sensingdt = dateparse(sensingt)
            pdates.append(sensingdt)
            pnames.append(f)

    psorted = zip( *sorted( zip(pdates, pnames) ) )
    #print(psorted[1])

    return join(dpath,(psorted[1][-1]))
            

def get_granule(gtype, pdir):
    grandf = join(pdir, 'GRANULE')
    for f in listdir(grandf):
       # there should only be one dirctory
      if f.startswith('L1C_'):
          imgglob = join(grandf, f, 'IMG_DATA', '*_' + gtype + '.jp2')
          for gf in glob.glob(imgglob):
              if isfile(gf):
                  return join(grandf, f, 'IMG_DATA', gf)
          
    return None



def set_no_data(infile, outfile):
    argv = [ 'gdalwarp',
             '-of',
             'GTiff',
             '-srcnodata',
             '0',
             '-dstalpha',
             infile,
             outfile
           ]

    print(argv)

    p = sp.Popen(' '.join(argv), shell=True, stdout=sp.PIPE, stderr=sp.STDOUT)
    for line in p.stdout.readlines():
        print line,
    retval = p.wait()

    return outfile

def merge_granules(reference, latest, outfile):  
    '''
    argv = [ 'gdal_merge.py',
             '-n','0',
             '-o',
             outfile,
             latest,
             reference             
           ]
    '''
    argv = [ 'gdalwarp',
             '-overwrite',
             '-of','GTiff',
             '-srcnodata','0', 
             '-dstalpha',            
             latest,
             reference,
             outfile
           ]
    print(argv)

    p = sp.Popen(' '.join(argv), shell=True, stdout=sp.PIPE, stderr=sp.STDOUT)
    for line in p.stdout.readlines():
        print line,
    retval = p.wait()

    return outfile

