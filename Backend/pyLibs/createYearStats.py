#!/usr/bin/env python

##################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
##################################################################################

SVN_REVISION = '$Rev: 6835 $'
SVN_DATE = '$Date: 2016-06-01 13:52:19 +0200 (Wed, 01 Jun 2016) $'
SVN_AUTHOR = '$Author: jb76278 $'

# Shortened and readable SVN description
import re
SVN_DESCR = (SVN_REVISION + re.sub('\) \$', ' ' ,re.sub('^.*\(', 'Date: ', SVN_DATE)) + SVN_AUTHOR).replace('$', '')

import re
import os
import sys
import datetime
import calendar
import numpy as np

from netCDF4 import Dataset

# The following import(s) need correct setting of the PYTHONPATH environment variable. For example in .bashrc:
# export PYTHONPATH="${PYTHONPATH}:`pwd`/SvnCheckout/CommonSense/RSDS/DataProcessing/Python/code:`pwd`/SvnCheckout/CommonSense/RSDS/DataProcessing/Python/pyLibs"
from netCdfGeoMetaData import NetCdfGeoMetaData

SCRIPT_FILE_NAME = os.path.basename(__file__)
NOW_STRING = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

def createBaseStatistics(aInputFileName, aDataVarName, aTimeVarName, aOutputFileName, aFirstYear, aLastYear) :

    # get modified day of year; 1 - 365, with 1 substracted for days after feb 28 in a leap year 
    def getmdoy(d):
        doy      = d.timetuple().tm_yday
        doyFeb28 = datetime.date(1990, 2, 28).timetuple().tm_yday
        if calendar.isleap(d.year) and doy > doyFeb28:
            doy = doy - 1
        return doy

    metaData = NetCdfGeoMetaData()
   
    ########################################################
    # Open input file
    ########################################################

    nc4file     = Dataset(os.path.join(aInputFileName), 'r')
    ts          = nc4file.variables[aDataVarName]
    ta          = nc4file.variables[aTimeVarName][:]
    units       = ts.getncattr('units')

    metaData.copyFromNetCdfObject(nc4file)
    noDataValueCube = None
    if metaData.getNoDataValue() <> '<None>':
       noDataValueCube = float(metaData.getNoDataValue())

   
    ####################################################
    # Determine the intervals for the base statistics
    ####################################################

    # Generate array with measurement interval start dates
    def addRefDate(x):
        return metaData.getDate() + datetime.timedelta(days=int(x))
    mStartDates = np.vectorize(addRefDate)(ta)
   
    # We have no end dates so create them by taking the start day of the next interval
    mEndDates     = np.roll(mStartDates, -1)
    mEndDates     = mEndDates - datetime.timedelta(days=1)
    mEndDates[-1] = mStartDates[-1]
   
    # Find first measurement interval(MI) of the first year
    for ii in range(len(mStartDates)) :
        if mStartDates[ii].year < aFirstYear: continue
        if mStartDates[ii].month == 1 and mStartDates[ii].day == 1: break
   
    # Reduce start dates upto and including last year
    i = 0
    for i in range(0, len(mStartDates)):
        if mStartDates[i].year > aLastYear: break
    mStartDates = mStartDates[:i]
 
   # Compute number of intervals in year
    for ni in range(ii,len(mStartDates)) :
        if mStartDates[ni].year <> mStartDates[ii].year:
            break
   
    oi        = 0               # output interval counter 
    ni        = ni - ii         # number of intervals 
    startDate = mStartDates[ii]  # date of first interval in a year
   
    doyArray  = np.array(range(0, ni))
    for i in range(len(doyArray)) :
        doyArray[i] = getmdoy(mStartDates[ii+i])

    ########################################################
    # Create output file
    ########################################################

    metaData.setTitle("Statistics of " + metaData.getTitle())
   
    historyString = NOW_STRING + " " + \
                    SCRIPT_FILE_NAME + " " + \
                    SVN_DESCR + " Arguments"
    metaData.addToHistory(historyString)
   
    metaData.setSource("Input file: " + aInputFileName)
    metaData.setComment(metaData.getComment() + " Yearly Statistics are Percentiles 0,25,50,75,100 and Average.")
    metaData.setDate(None)
   
    ncfileOut = Dataset(os.path.join(aOutputFileName),'w', format='NETCDF4')
    metaData.copyToNetCdfObject(ncfileOut)
   
    ncfileOut.createDimension('time', ni)
    variable = ncfileOut.createVariable('time', np.dtype('int32').char, ('time'))
    variable.setncattr('standard_name','time')
    variable.setncattr('units','day of year')
    variable.setncattr('long_name','time in d.o.y. (January 1st = 1)')
    variable.setncattr('axis','Z')
    variable[:] = np.subtract(doyArray+1,doyArray[0])
    metaData.createDimVarsLatLong(ncfileOut, ts.shape[1], ts.shape[2])

    p00Data = ncfileOut.createVariable('Percentile 0', ts.dtype, ('time','latitude','longitude'))
    p00Data.setncattr('standard_name', 'Percentile 0')
    p00Data.setncattr('units', units)
    p00Data.setncattr('long_name', 'Percentile 0 or Minimum')
   
    p25Data = ncfileOut.createVariable('Percentile 25', ts.dtype, ('time','latitude','longitude'))
    p25Data.setncattr('standard_name', 'Percentile 25')
    p25Data.setncattr('units', units)
    p25Data.setncattr('long_name', 'Percentile 25')
   
    p50Data = ncfileOut.createVariable('Percentile 50', ts.dtype, ('time','latitude','longitude'))
    p50Data.setncattr('standard_name', 'Percentile 50')
    p50Data.setncattr('units', units)
    p50Data.setncattr('long_name', 'Percentile 50 or Median')

    p75Data = ncfileOut.createVariable('Percentile 75', ts.dtype, ('time','latitude','longitude'))
    p75Data.setncattr('standard_name', 'Percentile 75')
    p75Data.setncattr('units', units)
    p75Data.setncattr('long_name', 'Percentile 75')

    p100Data = ncfileOut.createVariable('Percentile 100', ts.dtype, ('time','latitude','longitude'))
    p100Data.setncattr('standard_name', 'Percentile 100')
    p100Data.setncattr('units', units)
    p100Data.setncattr('long_name', 'Percentile 100 or Maximum')

    avgData = ncfileOut.createVariable('average data', ts.dtype, ('time','latitude','longitude'))
    avgData.setncattr('standard_name', 'Average')
    avgData.setncattr('units', units)
    avgData.setncattr('long_name', 'Average')

    ####################################################
    # Process input data 
    ####################################################

    # Find each MI in that year
    while mStartDates[ii].year == startDate.year:
   
        sdoy = 0
        edoy = 0
        il   = []              # Index list
        d    = mStartDates[ii] # start date for selected interval
        # Find index for all corresponding MI in all years
        for j in range(ii, len(mStartDates)) :
            sdoy = getmdoy(mStartDates[j])
            edoy = getmdoy(mEndDates[j])
            if getmdoy(d) >= sdoy and getmdoy(d) <= edoy :
                il.append(j)
   
        print('period %d - %d' % (getmdoy(mStartDates[ii]), getmdoy(mEndDates[ii])))
        print il

        # Collect data
        idata = np.ma.array(ts[il])
   
        idate = np.ma.masked_invalid(idata, copy=False)
        idate = np.ma.masked_where(idata == noDataValueCube, idata, copy=False)

        # Generate percentiles
        # If nan appears all percentiles for the point and time will be nan.
        pdata = np.percentile(idata, [0.0, 25.0, 50.0, 75.0, 100.0], axis=0)
      
        # Write percentiles + average
        p00Data[oi]  = pdata[0]
        p25Data[oi]  = pdata[1]
        p50Data[oi]  = pdata[2]
        p75Data[oi]  = pdata[3]
        p100Data[oi] = pdata[4]
        avgData[oi]  = np.nanmean(idata, axis=0)
   
        ii = ii + 1  # Next input interval
        oi = oi + 1  # Next output interval

def usage():
    usageString = "usage: %s <inputFileName> <dataVarName> <timeVarName> <outputFilename> [<flags>]\n" % sys.argv[0] + \
                  "         <flags>: --last-year=<YYYY>"
    sys.exit(usageString)

if __name__ == "__main__":

    inputFileName  = ''
    dataVarName    = ''
    timeVarName    = ''
    outputFilename = ''
    firstYear      = 0
    lastYear       = 9999

    fixedParamaterCntr = 0
    for i in range(1, len(sys.argv)):
        if sys.argv[i].startswith('--'):
            key, value = sys.argv[i].split('=')
            if   key == '--last-year':
                lastYear = eval(value)
            elif key == '--first-year':
                firstYear = eval(value)
            else:
                usage()
        else:
            if fixedParamaterCntr == 0:
                inputFileName  = sys.argv[i]
                fixedParamaterCntr += 1
            elif fixedParamaterCntr == 1:
                dataVarName    = sys.argv[i]
                fixedParamaterCntr += 1
            elif fixedParamaterCntr == 2:
                timeVarName    = sys.argv[i]
                fixedParamaterCntr += 1
            elif fixedParamaterCntr == 3:
                outputFilename = sys.argv[i]
                fixedParamaterCntr += 1
            else:
                usage()
 
    if fixedParamaterCntr < 4:
        usage()

    createBaseStatistics(inputFileName, dataVarName, timeVarName, outputFilename, firstYear, lastYear)
