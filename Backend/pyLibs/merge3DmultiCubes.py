#!/usr/bin/env python 

###############################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

SVN_REVISION = '$Rev: 6687 $'
SVN_DATE = '$Date: 2016-05-20 13:32:10 +0200 (Fri, 20 May 2016) $'
SVN_AUTHOR = '$Author: jb76278 $'

# Shortened and readable SVN description
import re
SVN_DESCR = (SVN_REVISION + re.sub('\) \$', ' ' ,re.sub('^.*\(', 'Date: ', SVN_DATE)) + SVN_AUTHOR).replace('$', '')

import numpy as np
import sys
import os
import re
import gc
import resource

# The following import(s) need correct setting of the PYTHONPATH environment variable. For example in .bashrc:
# export PYTHONPATH="${PYTHONPATH}:`pwd`/SvnCheckout/CommonSense/RSDS/DataProcessing/Python/code:`pwd`/SvnCheckout/CommonSense/RSDS/DataProcessing/Python/pyLibs"
import fileSystemOps
import arrayToPng as arr2png
import tiles
from netCdfGeoMetaData import NetCdfGeoMetaData

from osgeo import osr
from osgeo import gdal
from netCDF4 import Dataset
import datetime

SCRIPT_FILE_NAME = os.path.basename(__file__)
NOW_STRING = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

######################################################################################################
# This method will create the output multi-cube that can fit the input multi-cubes
######################################################################################################
def CreateOutputMultiCube(aInputDirName, aOutputFileName, aTilesObject):
    global SVN_DESCR, NOW_STRING, SCRIPT_FILE_NAME

    # String for self documenting script
    historyString = NOW_STRING + " " + SCRIPT_FILE_NAME + " " + SVN_DESCR + " Arguments"
    historyString += " --toi=" + aTilesObject.getIndicesString()
    historyString += " " + str(aOutputFileName)
    historyString += " " + str(aInputDirName)

    # Object for the meta data of the input (will be similar to that of the output)
    metaData = NetCdfGeoMetaData()

    # Get the upper left cube and get the (meta) data from it that we need
    fileName = aTilesObject.getUpperLeftFileName("multi_cube_", ".hdf5")
    nc4file = Dataset(os.path.join(aInputDirName, fileName), 'r')
    key, value = nc4file.variables.items()[3]
    variableDType = value.dtype
    dataArray = nc4file.variables[key][:]
    cubeLatSize = dataArray.shape[1]
    cubeLongSize = dataArray.shape[2]
    # Make a copy of the time variable
    timeArray = nc4file.variables['time'][:]
    timeVarAttr = {}
    for key in nc4file.variables['time'].ncattrs():
        timeVarAttr[key] = nc4file.variables['time'].getncattr(key)

    # Copy the meta data and alter the needed fields for the output later.
    metaData.copyFromNetCdfObject(nc4file)
    metaData.addToHistory(historyString)
    metaData.setSource("Input dir: " + aInputDirName)
    nc4file.close()

    # Get the lower right cube and derive the sizes of the output cube
    fileName = aTilesObject.getLowerRightFileName("multi_cube_", ".hdf5")
    nc4file = Dataset(os.path.join(aInputDirName, fileName), 'r')
    key, value = nc4file.variables.items()[3]
    dataArray = nc4file.variables[key][:]
    outputTimeSize = dataArray.shape[0]
    outputLatSize = cubeLatSize * (aTilesObject.getLowerIndex() - aTilesObject.getUpperIndex()) + dataArray.shape[1]
    outputLongSize = cubeLongSize * (aTilesObject.getRightIndex() - aTilesObject.getLeftIndex()) + dataArray.shape[2]
    nc4file.close()

    # Create the output netCDF cube
    print "Creating output file: " + str(aOutputFileName)
    ncfileOut = Dataset(aOutputFileName,'w', format='NETCDF4')

    # Copy the meta data and the dimensions
    metaData.copyToNetCdfObject(ncfileOut)
    ncfileOut.createDimension(timeVarAttr['standard_name'], outputTimeSize)
    variable = ncfileOut.createVariable(timeVarAttr['standard_name'], np.dtype('int32').char, (timeVarAttr['standard_name']))
    for key, value in timeVarAttr.iteritems():
        variable.setncattr(key, value)
    variable[:] = timeArray
    metaData.createDimVarsLatLong(ncfileOut, outputLatSize, outputLongSize)

    # Copy the variables
    p0Data = ncfileOut.createVariable('Percentile 0', variableDType, ('time','latitude','longitude'))
    p0Data[:] = np.zeros(shape=(outputTimeSize, outputLatSize, outputLongSize), dtype=variableDType)
    p0Data.setncattr('standard_name', 'Percentile 0')
    p0Data.setncattr('units', metaData.getUnits())
    p0Data.setncattr('long_name', 'Percentile 0 or Minimum')

    p25Data = ncfileOut.createVariable('Percentile 25', variableDType, ('time','latitude','longitude'))
    p25Data[:] = np.zeros(shape=(outputTimeSize, outputLatSize, outputLongSize), dtype=variableDType)
    p25Data.setncattr('standard_name', 'Percentile 25')
    p25Data.setncattr('units', metaData.getUnits())
    p25Data.setncattr('long_name', 'Percentile 25')

    p50Data = ncfileOut.createVariable('Percentile 50', variableDType, ('time','latitude','longitude'))
    p50Data[:] = np.zeros(shape=(outputTimeSize, outputLatSize, outputLongSize), dtype=variableDType)
    p50Data.setncattr('standard_name', 'Percentile 50')
    p50Data.setncattr('units', metaData.getUnits())
    p50Data.setncattr('long_name', 'Percentile 50 or Median')

    p75Data = ncfileOut.createVariable('Percentile 75', variableDType, ('time','latitude','longitude'))
    p75Data[:] = np.zeros(shape=(outputTimeSize, outputLatSize, outputLongSize), dtype=variableDType)
    p75Data.setncattr('standard_name', 'Percentile 75')
    p75Data.setncattr('units', metaData.getUnits())
    p75Data.setncattr('long_name', 'Percentile 75')

    p100Data = ncfileOut.createVariable('Percentile 100', variableDType, ('time','latitude','longitude'))
    p100Data[:] = np.zeros(shape=(outputTimeSize, outputLatSize, outputLongSize), dtype=variableDType)
    p100Data.setncattr('standard_name', 'Percentile 100')
    p100Data.setncattr('units', metaData.getUnits())
    p100Data.setncattr('long_name', 'Percentile 100 or Maximum')

    avgData = ncfileOut.createVariable('average data', variableDType, ('time','latitude','longitude'))
    avgData[:] = np.zeros(shape=(outputTimeSize, outputLatSize, outputLongSize), dtype=variableDType)
    avgData.setncattr('standard_name', 'Average')
    avgData.setncattr('units', metaData.getUnits())
    avgData.setncattr('long_name', 'Average')
    ncfileOut.close()

#   print str(metaData)
#   print "Size: " + str(outputLatSize) + ", " + str(outputLongSize)

######################################################################################################
# Method will get the height and width of given multi-cube
######################################################################################################
def GetHeightAndWidth(aInputDirName, aInputFileName):
    nc4file = Dataset(os.path.join(aInputDirName, aInputFileName), 'r')

    key, value = nc4file.variables.items()[3]
    inputArray = nc4file.variables[key][:]
    nc4file.close()

    height = inputArray.shape[1]
    width = inputArray.shape[2]

    return height, width

######################################################################################################
# This method will copy the input multi-cubes into the output (merged) multi-cube
######################################################################################################
def Copy3DmultiCubes(aCallingTilesObject, aInputDirName, aOutputFileName, aHeight, aWidth):

    # Determine the y and x positions in the output multi-cube for the current input multi-cube
    yPosit = aHeight * (aCallingTilesObject.getCurrentUpDownIndex() - aCallingTilesObject.getUpperIndex())
    xPosit = aWidth * (aCallingTilesObject.getCurrentLeftRightIndex() - aCallingTilesObject.getLeftIndex())

    fileName = aCallingTilesObject.getCurrentFileName("multi_cube_", ".hdf5")
    print "Copying file: " + fileName
    nc4file = Dataset(os.path.join(aInputDirName, fileName), 'r')
    for cube_index in range(6):
        key, value = nc4file.variables.items()[3 + cube_index]
        inputArray = nc4file.variables[key][:]

        # open netCDF file for writing.
        ncfileOut = Dataset(aOutputFileName, 'a')
        key, value = ncfileOut.variables.items()[3 + cube_index]

        ncfileOut.variables[key][:, yPosit:yPosit+inputArray.shape[1], xPosit:xPosit+inputArray.shape[2]] = inputArray
        ncfileOut.close()
    nc4file.close()

    del inputArray
    gc.collect()

    return

######################################################################################################
# Check and process the input parameters
######################################################################################################
def Main(aArgValues):
    if len(aArgValues) < 3 or len(aArgValues) > 4:
        print ""
        print "Usage:"
        print "python merge3DmultiCubes.py [flags] <output-file-name> <input-dir-name>"
        print ""
        print "The input files must be netCDF cube type files!"
        print "Allowed flag examples:"
        print "--toi=0,0,10,2  The tiles of interest is defined with four comma separated pixel-index values upper, left, lower, right. All tiles are processed by default."
        print "Example:"
        print "python merge3DmultiCubes.py --toi=0,0,0,1 /net/satarch/CHIRPS/jelle/multi_cube_new.hdf5 /net/satarch/CHIRPS/jelle/rainYearStats/"
        print "python merge3DmultiCubes.py /net/satarch/CHIRPS/jelle/multi_cube_new.hdf5 /net/satarch/CHIRPS/jelle/rainYearStats/"
        print "python merge3DmultiCubes.py --toi=7,4,10,8 /net/satarch/MODIS/jelle/multi_cube_new.hdf5 /net/satarch/MODIS/jelle/NdviYearStats500m/"
        print "python merge3DmultiCubes.py /net/satarch/MODIS/jelle/multi_cube_new.hdf5 /net/satarch/MODIS/jelle/NdviYearStats500m/"
        print ""
    else:
        flagsDict = {}
        outputFileName = ''
        inputDirName = ''
        toiSet = False
        upperIndex = 0
        leftIndex = 0
        lowerIndex = 0
        rightIndex = 0

        # Extract the flags
        argIndex = 1
        while outputFileName == '' and argIndex < len(aArgValues):
            # Check for flags
            if aArgValues[argIndex].count('=') == 1 and aArgValues[argIndex].startswith('--'):
                key, value = aArgValues[argIndex].split('=')
                flagsDict[key] = value
            else:
                outputFileName = aArgValues[argIndex]
            argIndex += 1

        fileSystemOps.MakeSureDirectoryExists(os.path.dirname(outputFileName))

        if argIndex == (len(aArgValues) - 1):
            inputDirName = aArgValues[argIndex]
        else:
            sys.exit("Error: Number of arguments is wrong.")

        # Check the flags and extract their values
        for key, value in flagsDict.items():
            if key == '--toi':
                if value.count(',') == 3:
                    one,two,three,four = value.split(',')
                    upperIndex = int(one)
                    leftIndex = int(two)
                    lowerIndex = int(three)
                    rightIndex = int(four)
                    toiSet = True
                else:
                    sys.exit("Error: Format of Value \'" + value + "\' for Key \'" + key + "\' is wrong.")
            else:
                print "Warning: Key \'" + key + "\' is unknown and ignored."

        # Set the tiles indices and check output directory and the first year with full data.
        tilesObject = tiles.Tiles()
        tilesObject.setTilesOfInterest(inputDirName, "multi_cube_", ".hdf5", toiSet, upperIndex, leftIndex, lowerIndex, rightIndex)
        
        CreateOutputMultiCube(inputDirName, outputFileName, tilesObject)
        height, width = GetHeightAndWidth(inputDirName, tilesObject.getUpperLeftFileName("multi_cube_", ".hdf5"))

        tilesObject.loopThroughTiles(Copy3DmultiCubes,(inputDirName, outputFileName, height, width))

######################################################################################################
# Only execute when run as a script
######################################################################################################
if __name__ == "__main__":
    Main(sys.argv[:])

