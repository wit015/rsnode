#!/usr/bin/python
#
################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2018, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################


from os import listdir
from os.path import isfile, join, isdir
from os.path import basename
from dateutil.parser import parse as dateparse
import subprocess as sp
import glob
import argparse
import sys
import os
import s2utils as s2

def ndvi_create(b04file, b08file, outfile):  
    argv = [ 'gdal_calc.py',
             '-A', b04file,
             '-B', b08file,
             '--outfile', outfile,
             '--overwrite',
             '--type=Float32',
             '--NoDataValue=0',
             '--calc="((B*1.0)-(A*1.0))/((B*1.0)+(A*1.0))"'
           ]

    print(argv)

    p = sp.Popen(' '.join(argv), shell=True, stdout=sp.PIPE, stderr=sp.STDOUT)
    for line in p.stdout.readlines():
        print line,
    retval = p.wait()

    return outfile


if __name__ == "__main__":
    # execute only if run as a script
    parser = argparse.ArgumentParser()
    parser.add_argument("S2TileDir",   help="Sentinel-2 data directory for tile")
    parser.add_argument("ref",   help="Reference image")
    parser.add_argument("--output", help="Output file")
    parser.add_argument("--tmp", help="Temporary folder")

    args = parser.parse_args()

    DATADIR 	= args.S2TileDir
    OUTIMG 	= args.output
    REFIMG 	= args.ref 
    TMPDIR      = args.tmp

    if not isdir(DATADIR):
        print('Tile directory not a directory or does not exist.')
        sys.stdout.flush()
        sys.exit(-1)

    lastprod = s2.get_last_s21cproduct(DATADIR)
    
    tileB4 = s2.get_granule('B04', lastprod)
    tileB8 = s2.get_granule('B08', lastprod)

    if (not isfile(tileB4)) or (not isfile(tileB8)):
        print('Band files not found in the directory.')
        sys.stdout.flush()
        sys.exit(-2)

    if TMPDIR is None:
        tmpFile='/tmp/tmpNDVIgen001.tif'
        ndvi_create(tileB4, tileB8, tmpFile)
        s2.merge_granules(REFIMG, tmpFile, ('out.tif' if OUTIMG is None else OUTIMG))
        os.remove(tmpFile)
    else:
        tmpuuid = str(uuid.uuid4())
        tmpB04 = join(TMPDIR, 'B04-' + tmpuuid + '.tif')
        tmpB08 = join(TMPDIR, 'B08-' + tmpuuid + '.tif')
        copyfile(tileB4, tmpB04)
        copyfile(tileB8, tmpB08)
        tmpout = join(TMPDIR,'NDVI-' + tmpuuid + '.tif')
        ndvi_create(tmpB04, tmpB08, tmpout)
        s2.merge_granules(REFIMG, tmpout, ('out.tif' if OUTIMG is None else OUTIMG))


    print('Done.')

