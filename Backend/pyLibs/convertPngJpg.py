##################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
##################################################################################

SVN_REVISION = '$Rev: 7715 $'
SVN_DATE = '$Date: 2016-12-16 11:40:50 +0100 (Fri, 16 Dec 2016) $'
SVN_AUTHOR = '$Author: jb76278 $'

import Image
import numpy as np
import sys

######################################################################################################
# Convert the format of a single image file
######################################################################################################
def convertPngJpg(aImageFile):
    if aImageFile.endswith('.png'):
        im = Image.open(aImageFile)
#        im.save(aImageFile[::-1].replace(".png"[::-1], ".tif"[::-1], 1)[::-1], "JPG")
#        print "File in: " + str(aImageFile) + "  File out: " + aImageFile[::-1].replace(".png"[::-1], ".jpg"[::-1], 1)[::-1]
        im.save(aImageFile[::-1].replace(".png"[::-1], ".tif"[::-1], 1)[::-1], "TIFF")
        print "File in: " + str(aImageFile) + "  File out: " + aImageFile[::-1].replace(".png"[::-1], ".tif"[::-1], 1)[::-1]
    elif aImageFile.endswith('.jpg'):
        im = Image.open(aImageFile)
        im.save(aImageFile[::-1].replace(".jpg"[::-1], ".png"[::-1], 1)[::-1], "PNG")
        print "File in: " + str(aImageFile) + "  File out: " + aImageFile[::-1].replace(".jpg"[::-1], ".png"[::-1], 1)[::-1]
    else:
        print "Error: Unknown file-type"
   
######################################################################################################
# Go through all the arguments (which should be a list of image files)
######################################################################################################
def main(argv):
    for argumentNumber in np.arange(1, len(sys.argv)):
        convertPngJpg(sys.argv[argumentNumber])

######################################################################################################
# Only execute when run as a script
######################################################################################################
if __name__ == "__main__":
    main(sys.argv[:])

