#!/usr/bin/env python 
#
################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2018, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################


SVN_REVISION = '$Rev: 6912 $'
SVN_DATE = '$Date: 2016-06-10 17:25:28 +0200 (Fri, 10 Jun 2016) $'
SVN_AUTHOR = '$Author: jb76278 $'

import os
import sys
import config
import pandas
import modis_processing

#config.init("MOD13Q1")
#db = pandas.read_pickle(config.MODIS_HDF_DB) 
#db = db[~db.modis_fname_fp.str.contains("/2015/2")]
#db.to_pickle(config.MODIS_HDF_DB) 
#modis_processing.main()

#config.init("MYD13Q1")
#db = pandas.read_pickle(config.MODIS_HDF_DB) 
#db = db[~db.modis_fname_fp.str.contains("/2015/2")]
#db.to_pickle(config.MODIS_HDF_DB) 
#modis_processing.main()

#config.init("MOD13A1")
#db = pandas.read_pickle(config.MODIS_HDF_DB) 
#db = db[~db.modis_fname_fp.str.contains("/2015/")]
#db.to_pickle(config.MODIS_HDF_DB) 
#modis_processing.main()

#config.init("MYD13A1")
#db = pandas.read_pickle(config.MODIS_HDF_DB) 
#db = db[~db.modis_fname_fp.str.contains("/2015/")]
#db.to_pickle(config.MODIS_HDF_DB) 
#modis_processing.main()

######################################################################################################
# Check and process the input parameters
######################################################################################################
def Main(argv):
    if len(sys.argv) < 3 or len(sys.argv) > 5:
        print ""
        print "Usage:"
        print "do.py [flags] <product-name> <input-dir-name> <tools-dir-name> "
        print ""
        print "Allowed flag examples:"
        print "--constraint=/2015/3  The constaint tell the script that files in the constaint specified directory/directories have to be processed (even if the database says they were processed already)."
        print ""
        print "Example:"
        print "do.py MYD13A1 / /"
        print "do.py --constaint=/2015/3 MYD13A1 /net/satarch/MODIS500/ /home/jane/SvnCheckout/CommonSense/RSDS/DataProcessing/Backend/Tools/"
        print ""
    else:
        flags_dict = {}
        productName = ''
        inputDirName = ''
        toolsDirName = ''

        # Extract the flags
        argIndex = 1
        while productName == '' and argIndex < len(sys.argv):
            # Check for flags
            if sys.argv[argIndex].count('=') == 1 and sys.argv[argIndex].startswith('--'):
                key, value = sys.argv[argIndex].split('=')
                flags_dict[key] = value
            else:
                productName = sys.argv[argIndex]
            argIndex += 1

        if argIndex == (len(sys.argv) - 2):
            inputDirName = sys.argv[argIndex]
            argIndex += 1
            toolsDirName = sys.argv[argIndex]
        else:
            sys.exit("Error: Number of arguments is wrong.")

	# Set the configuration with the given values
        config.init(productName, inputDirName, toolsDirName)

        # Check the flags and extract their values
        for key, value in flags_dict.items():
            if key == '--constraint':
                if os.path.exists(config.MODIS_HDF_DB):
                    db = pandas.read_pickle(config.MODIS_HDF_DB) 
                    db = db[~db.modis_fname_fp.str.contains(value)]
                    db.to_pickle(config.MODIS_HDF_DB) 
            else:
                print "Warning: Key \'" + key + "\' is unknown and ignored."

        # Merge the input tiles into one and re-project them
        modis_processing.main(toolsDirName)


######################################################################################################
# Only execute when run as a script
######################################################################################################
if __name__ == "__main__":
    Main(sys.argv[:])

