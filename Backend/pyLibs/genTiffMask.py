#!/usr/bin/env python
#
# Replace all values unequal to NoDataValue in a TIFF file
#
################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2018, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################


import sys
import numpy as np

# The GDAL imports for geoTiff (a.o.)
from osgeo import gdal
from osgeo import osr
from osgeo.gdalconst import *

def _pixel2World(gt, y, x):
   returnLat = gt[3] + float(y) * gt[5]
   returnLong = gt[0] + float(x) * gt[1]
   return (returnLat, returnLong)

def _genTiffMask(aTemplateFileName, aOutFileName, aNumber, aNoDataValue, aIgnoreNoData, aDeltaX=None, aDeltaY=None):
   mds         = gdal.Open(aTemplateFileName)
   driver      = mds.GetDriver()
   maskdata    = mds.ReadAsArray()
   nds         = driver.CreateCopy(aOutFileName, mds, 0)
   band        = nds.GetRasterBand(1)
   noDataValue = band.GetNoDataValue()

   if noDataValue == None:
      noDataValue = aNoDataValue

   if aIgnoreNoData: 
      # Replace all values 
      maskdata[:,:] = aNumber
   else:
      # Replace all values unequal to noDataValue with a number
      maskdata[maskdata <> noDataValue] = aNumber

   # Generate NoDataValue pattern
   if aDeltaX <> None or aDeltaY <> None:
      gt = mds.GetGeoTransform()
      dwX = aDeltaX
      if dwX == None: dwX = mlon - olon
      dwY = aDeltaY
      if dwY == None: dwY = mlat - olat
      for y in range(0, maskdata.shape[0]):
         for x in range(0, maskdata.shape[1]):
            lat, lon = _pixel2World(gt, y, x)
            if np.mod(lat, 2*dwY) >= dwY and np.mod(lon, 2*dwX) >= dwX:
               maskdata[y,x] = noDataValue

   # Set value for which no data is present
   band.SetNoDataValue(noDataValue)
   # Write data
   band.WriteArray(maskdata)
   # Recompute statistics
   band.ComputeStatistics(0)
   # Close files
   mds      = None
   nds      = None

def genTiffMask(argv):
   if len(argv)<2:
      print('Usage: %s <infile> <outfile> [--value=<value> --nodata=<value> --ignore_nodata=[yes|no]]' % (argv[0]))
      sys.exit(1)

   # Initialise default argument values
   num          = 1.0
   nodata       = -9999.0
   ignoreNoData = False
   deltaX       = None
   deltaY       = None

   # Loop over all arguments
   for i in range(3,len(argv)):
      key, value = argv[i].split('=')
      if   key == '--data':
         num = eval(value)
      elif key == '--nodata':
         nodata = eval(value)
      elif key == '--ignore_nodata':
         ignoreNoData = value == 'yes'
      elif key == '--data-width-X':
         deltaX = eval(value)
      elif key == '--data-width-Y':
         deltaY = eval(value)
      else:
         print('%s : illegal flag "%s"' % (argv[0], argv[i]))
         sys.exit(1)
   _genTiffMask(argv[1], argv[2], num, nodata, ignoreNoData, deltaX, deltaY)

if __name__ == "__main__":
   genTiffMask(sys.argv)
