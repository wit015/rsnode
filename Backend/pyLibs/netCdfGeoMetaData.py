################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

SVN_REVISION = '$Rev: 7715 $'
SVN_DATE = '$Date: 2016-12-16 11:40:50 +0100 (Fri, 16 Dec 2016) $'
SVN_AUTHOR = '$Author: jb76278 $'

# Shortened and readable SVN description
import re
SVN_DESCR = (SVN_REVISION + re.sub('\) \$', ' ' ,re.sub('^.*\(', 'Date: ', SVN_DATE)) + SVN_AUTHOR).replace('$', '')

from osgeo import gdal
import datetime
import ast
import numpy as np

##################################################################################
class NetCdfGeoMetaData(object):

    def __init__(self):
        self.title = None
        self.convention = None
        self.history = None
        self.institution = None
        self.source = None
        self.comment = None
        self.reference = None
        self.noDataValue = None
        self.geoProjection = None
        self.geoTransform = None
        self.date = None
        self.units = None

    # Title
    def setTitle(self, aTitle):
        self.title = aTitle      
    def getTitle(self):
        returnString = "<None>" if self.title == None else self.title
        return returnString

    # Convention
    def setConvention(self, aConvention):
        self.convention = aConvention      

    # History
    def setHistory(self, aHistory):
        self.history = aHistory
    def addToHistory(self, aHistoryLine):
        if not self.history == None:
            self.history += '\n'
            self.history += aHistoryLine
        else:
            self.setHistory(aHistoryLine)

    #Institution
    def setInstitution(self, aInstitution):
        self.institution =  aInstitution
    def getInstitution(self):
        returnString = "<None>" if self.institution == None else self.institution
        return returnString

    # Source
    def setSource(self, aSource):
        self.source = aSource

    # Comment
    def setComment(self, aComment):
        self.comment = aComment
    def getComment(self):
        returnString = "<None>" if self.comment == None else self.comment
        return returnString

    # Reference
    def setReference(self, aReference):
        self.reference = aReference

    # No Data Value
    def setNoDataValue(self, aValue):
        self.noDataValue = None if aValue == None else str(aValue)
    def getNoDataValue(self):
        returnString = "<None>" if self.noDataValue == None else self.noDataValue
        return returnString

    # GEO Projection
    def setGeoProjection(self, aProjection):
        self.geoProjection = aProjection

    # GEO Transform(ation)
    def setGeoTransform(self, aTransformation):
        self.geoTransform = None if aTransformation == None else str(aTransformation)
    def getGeoTransform(self):
        if self.geoTransform == None:
            return 0,1
        else:
            returnTuple = ast.literal_eval(self.geoTransform)
            return returnTuple[0],returnTuple[1],returnTuple[2],returnTuple[3],returnTuple[4],returnTuple[5]
    def getLatitudeTransform(self):
        if self.geoTransform == None:
            return 0,1
        else:
            returnTuple = ast.literal_eval(self.geoTransform)
            return returnTuple[3],returnTuple[5]
    def getLongitudeTransform(self):
        if self.geoTransform == None:
            return 0,1
        else:
            returnTuple = ast.literal_eval(self.geoTransform)
            return returnTuple[0],returnTuple[1]
    def adjustPixelOriginGeoTransform(self, aYOrigin, aXOrigin):
        geoTransform = list(ast.literal_eval(self.geoTransform))
        geoTransform[3] = geoTransform[3] + aYOrigin * geoTransform[5]
        geoTransform[0] = geoTransform[0] + aXOrigin * geoTransform[1]
        self.setGeoTransform(tuple(geoTransform))
    def getPixelYxFromLatLong(self,aLat,aLong):
        geoTransform = list(ast.literal_eval(self.geoTransform))
        returnY = (aLat - geoTransform[3]) / geoTransform[5]
        returnX = (aLong - geoTransform[0]) / geoTransform[1]
        return int(round(returnY)), int(round(returnX))
    def getLatLongFromPixelYx(self,aY,aX):
        geoTransform = list(ast.literal_eval(self.geoTransform))
        returnLat = geoTransform[3] + float(aY) * geoTransform[5]
        returnLong = geoTransform[0] + float(aX) * geoTransform[1]
        return returnLat, returnLong

    # Date
    def setDate(self, aDate):
        self.date = None if aDate == None else str(aDate)
    def getDate(self):
        if self.date == None:
            return datetime.datetime.strptime("1900-01-01", "%Y-%m-%d").date()
        else:
            return datetime.datetime.strptime(self.date, "%Y-%m-%d").date()

    # Units
    def setUnits(self, aUnits):
        self.units = None if aUnits == None else str(aUnits)
    def getUnits(self):
        returnString = "<unknown>" if self.units == None else self.units
        return returnString

    # Copy -from- GDAL data
    def copyFromGdalData(self, aGdalDataSet):
        self.setConvention("CF-1.6")
        print  aGdalDataSet.GetDescription() 
        if "MODIS" in aGdalDataSet.GetDescription() or "modis" in aGdalDataSet.GetDescription():
            self.setTitle(str(aGdalDataSet.GetMetadata()[aGdalDataSet.GetMetadata().keys()[0]]) + " " + aGdalDataSet.GetMetadata().keys()[0])
            self.setInstitution("MODIS")
            self.setNoDataValue(aGdalDataSet.GetRasterBand(1).GetNoDataValue())
            dateString = re.sub('_.*$', '', re.sub('.*VI_', '', aGdalDataSet.GetDescription()))
            self.setDate(datetime.datetime.strptime(dateString, "%Y%m%d").date())
            self.setUnits("VI")
        elif "CHIRPS" in aGdalDataSet.GetDescription() or "chirps" in aGdalDataSet.GetDescription():
            self.setTitle("Precipitation")
            self.setInstitution("CHIRPS")
            srcband = aGdalDataSet.GetRasterBand(1)
            stats = srcband.GetStatistics( True, True )
            self.setNoDataValue(stats[0])
            dateString = re.sub('\.tif$', '', re.sub('.*v2\.0\.', '', aGdalDataSet.GetDescription()))
            date = datetime.datetime.strptime(dateString, "%Y.%m.%d").date()
            day = (date.day - 1 ) * 5 + 1
            date = datetime.date(date.year, date.month, day)
            self.setDate(date)
            self.setUnits("mm")
        else:
            self.setInstitution("unknown")
            self.setUnits("unknown")
        self.setSource(aGdalDataSet.GetDescription())
        self.setComment(str(aGdalDataSet.GetDriver().LongName))
        self.setReference(aGdalDataSet.GetDescription())
        self.setGeoProjection(aGdalDataSet.GetProjection())
        self.setGeoTransform(aGdalDataSet.GetGeoTransform())

    # Copy -from- netCDF data
    def copyFromNetCdfObject(self, aNetCdfObject):
        self.setTitle(aNetCdfObject.description)
        self.setConvention(getattr(aNetCdfObject, 'Conventions', None))
        self.setNoDataValue(getattr(aNetCdfObject, '_FillValue', None))
        self.setHistory(getattr(aNetCdfObject, 'history', None))
        self.setInstitution(getattr(aNetCdfObject, 'institution', None))
        self.setSource(getattr(aNetCdfObject, 'source', None))
        self.setComment(getattr(aNetCdfObject, 'comment', None))
        self.setReference(getattr(aNetCdfObject, 'reference', None))
        self.setGeoProjection(getattr(aNetCdfObject, 'GEO_Projection', None))
        self.setGeoTransform(getattr(aNetCdfObject, 'GEO_Transform', None))
        self.setDate(getattr(aNetCdfObject, 'Recording_Date', None))
        self.setUnits(getattr(aNetCdfObject, 'Units', None))

    # Copy -to- netCDF data
    def copyToNetCdfObject(self, aNetCdfObject):
        if not self.title == None: aNetCdfObject.description = str(self.title)
        if not self.convention == None: setattr(aNetCdfObject, 'Conventions', str(self.convention))
#        if not self.noDataValue == None: setattr(aNetCdfObject, 'missing_value', self.noDataValue) Deprecated notation
        if not self.noDataValue == None: setattr(aNetCdfObject, '_FillValue', str(self.noDataValue))
        if not self.history == None: setattr(aNetCdfObject, 'history', str(self.history))
        if not self.institution == None: setattr(aNetCdfObject, 'institution', str(self.institution))
        if not self.source == None: setattr(aNetCdfObject, 'source', str(self.source))
        if not self.comment == None: setattr(aNetCdfObject, 'comment', str(self.comment))
        if not self.reference == None: setattr(aNetCdfObject, 'reference', str(self.reference))
        if not self.geoProjection == None: setattr(aNetCdfObject, 'GEO_Projection', str(self.geoProjection))
        if not self.geoTransform == None: setattr(aNetCdfObject, 'GEO_Transform', str(self.geoTransform))
        if not self.date == None: setattr(aNetCdfObject, 'Recording_Date', str(self.date))
        if not self.units == None: setattr(aNetCdfObject, 'Units', str(self.units))

    # Add latitude and longitude dimensions and variables to a netCDF object
    def createDimVarsLatLong(self, aNetCdfObject, aLatSize, aLongSize):
        aNetCdfObject.createDimension('latitude', aLatSize)
        variable = aNetCdfObject.createVariable('latitude', np.dtype('float32').char, ('latitude'))
        variable.setncattr('standard_name','Latitude')
        variable.setncattr('units','degrees_north')
        variable.setncattr('long_name','Latitude')
        variable.setncattr('axis','Y')
        lat, latDelta = self.getLatitudeTransform()
        variable[:] = np.arange(lat, lat + (latDelta * (aLatSize - 0.5)), latDelta, dtype=float)

        aNetCdfObject.createDimension('longitude', aLongSize)
        variable = aNetCdfObject.createVariable('longitude', np.dtype('float32').char, ('longitude'))
        variable.setncattr('standard_name','Longitude')
        variable.setncattr('units','degrees_east')
        variable.setncattr('long_name','Longitude')
        variable.setncattr('axis','X')
        lon, lonDelta = self.getLongitudeTransform()
        variable[:] = np.arange(lon, lon + (lonDelta * (aLongSize - 0.5)), lonDelta, dtype=float)

    # Convert object to string representation for printing/debugging
    def __str__(self):
        returnString = "Title: "
        returnString += "<None>" if self.title == None else self.title
        returnString += "\n"
        returnString += "Convention: "
        returnString += "<None>" if self.convention == None else self.convention
        returnString += "\n"
        returnString += "History: "
        returnString += "<None>" if self.history == None else self.history
        returnString += "\n"
        returnString += "Institution: "
        returnString += "<None>" if self.institution == None else self.institution
        returnString += "\n"
        returnString += "Source: "
        returnString += "<None>" if self.source == None else self.source
        returnString += "\n"
        returnString += "Comment: "
        returnString += "<None>" if self.comment == None else self.comment
        returnString += "\n"
        returnString += "Reference: "
        returnString += "<None>" if self.reference == None else self.reference
        returnString += "\n"
        returnString += "No Data Value: "
        returnString += "<None>" if self.noDataValue == None else self.noDataValue
        returnString += "\n"
        returnString += "GEO Projection: "
        returnString += "<None>" if self.geoProjection == None else self.geoProjection
        returnString += "\n"
        returnString += "GEO Transform: "
        returnString += "<None>" if self.geoTransform == None else self.geoTransform
        returnString += "\n"
        returnString += "Date: "
        returnString += "<None>" if self.date == None else self.date
        returnString += "Units: "
        returnString += "<None>" if self.units == None else self.units
        return returnString

