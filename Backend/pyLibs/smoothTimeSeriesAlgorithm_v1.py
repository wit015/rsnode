################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

SVN_REVISION = '$Rev: 7715 $'
SVN_DATE = '$Date: 2016-12-16 11:40:50 +0100 (Fri, 16 Dec 2016) $'
SVN_AUTHOR = '$Author: jb76278 $'

# One-quarter of the stepsize for the smoothing algorithm
SMOOTHING_QUARTER_STEPSIZE = 10

# Shortened and readable SVN description
import re
SVN_DESCR = (SVN_REVISION + re.sub('\) \$', ' ' ,re.sub('^.*\(', 'Date: ', SVN_DATE)) + SVN_AUTHOR).replace('$', '')

import numpy as np
import os
import datetime
from netCDF4 import Dataset

# The following import(s) need correct setting of the PYTHONPATH environment variable. For example in .bashrc:
# export PYTHONPATH="${PYTHONPATH}:`pwd`/SvnCheckout/CommonSense/RSDS/DataProcessing/Python/code:`pwd`/SvnCheckout/CommonSense/RSDS/DataProcessing/Python/pyLibs"
import tiles
from netCdfGeoMetaData import NetCdfGeoMetaData

SCRIPT_FILE_NAME = os.path.basename(__file__)
NOW_STRING = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

######################################################################################################
# Smooth the given timeseries with Whittaker smoothing
# as specified in article by Clement Atzberger and Paul H.C. Eilers:
# "A time series for monitoring vegetation activity and phenology at
# 10-daily time steps covering large parts of South America"
#
# This implementation does it for approximately 1 year (40 datapoints)
# of a timeseries and uses the middle 20 datapoints. Then it repeats
# this for 40 datapoint that are 20 datapoints further along the
# series.
# Reason: The computation time of the Whittaker smoothing algorithm
# increases (exponentially?) with longer time series. This makes the
# implementation below much faster for the lengthy timeseries we are using.
# If an test output file is specified input, intermediate result and end result
# are written to that file.
######################################################################################################
def Smooth(aInputArray1D, aNoDataValue, aStartIndex=0, aTestOutputFile=None):
    if aTestOutputFile != None:
        outputStr = ','.join(['%.5f' % num for num in aInputArray1D])
        aTestOutputFile.write('#input,' + outputStr + '\n')
    returnTimeSeries = np.zeros(aInputArray1D.shape, float)
    whittakerLambda = 2.0

    # Get the first part (snip) of the timeseries to smooth   
    stopCondition = False
    startIndex = min(max(aStartIndex, 0), aInputArray1D.size)
    endIndex = min(startIndex + (4 * SMOOTHING_QUARTER_STEPSIZE), aInputArray1D.size)
    snip = np.zeros([endIndex-startIndex], aInputArray1D.dtype)

    # Repeat until the end is reached
    while not stopCondition:
        # Copy the selected part of the timeseries
        snip = aInputArray1D[startIndex:endIndex]

        # Prepare for the Whittaker smoothing
        m  = snip.size
        w = np.ones(snip.size, float)
        w[snip==aNoDataValue] = 0.0
        p  = np.diff(np.eye(m),3);
        diagonal = np.diag(w)
        firstArg = diagonal + whittakerLambda * np.dot(p, p.transpose())

        if np.sum(w)==0.0:
            # All values are 'no Data Value'
            vv = np.copy(snip)
        else:
            # Whittaker no.1 with clipping
            vv = np.linalg.solve(firstArg, np.dot(diagonal, snip));
            cv = np.maximum(snip, vv);
            if aTestOutputFile != None:
                outputStr = ','.join(['%.5f' % num for num in snip])
                aTestOutputFile.write('#snip,' + outputStr + '\n')
                outputStr = ','.join(['%.5f' % num for num in vv])
                aTestOutputFile.write('#vv 1,' + outputStr + '\n')
                outputStr = ','.join(['%.5f' % num for num in cv])
                aTestOutputFile.write('#cv 1,' + outputStr + '\n')

            # Whittaker no.2 with clipping
            vv = np.linalg.solve(firstArg, np.dot(diagonal, cv));
            cv = np.maximum(cv, vv);
            if aTestOutputFile != None:
                outputStr = ','.join(['%.5f' % num for num in vv])
                aTestOutputFile.write('#vv 2,' + outputStr + '\n')
                outputStr = ','.join(['%.5f' % num for num in cv])
                aTestOutputFile.write('#cv 2,' + outputStr + '\n')

            # Whittaker no.3
            vv = np.linalg.solve(firstArg, np.dot(diagonal, cv));
            if aTestOutputFile != None:
                outputStr = ','.join(['%.5f' % num for num in vv])
                aTestOutputFile.write('#vv 3,' + outputStr + '\n')

        # Copy the result into the output (return) variable
        if startIndex == 0:
            returnTimeSeries[startIndex:endIndex] = vv
        else:
            returnTimeSeries[startIndex+SMOOTHING_QUARTER_STEPSIZE:endIndex] = vv[SMOOTHING_QUARTER_STEPSIZE:]

        if aTestOutputFile != None:
            outputStr = ','.join(['%.5f' % num for num in returnTimeSeries])
            aTestOutputFile.write('#Output intermediate,' + outputStr + '\n')

        # Set the end-condition if needed
        if endIndex == aInputArray1D.size:
            stopCondition = True

        # Prepare for the next part (snip) of the timeseries
        startIndex = startIndex + (2 * SMOOTHING_QUARTER_STEPSIZE)
        endIndex = min(startIndex + (4 * SMOOTHING_QUARTER_STEPSIZE), aInputArray1D.size)

    if aTestOutputFile != None:
        outputStr = ','.join(['%.5f' % num for num in returnTimeSeries])
        aTestOutputFile.write('#Output final,' + outputStr + '\n' + '\n')
    return returnTimeSeries

######################################################################################################
# Method will smooth the timeseries for one cube.
######################################################################################################
def SmoothTimeSeries(aCallingTilesObject, aInputDirName, aOutputDirName, aAddNewData):
    global SVN_DESCR, NOW_STRING, SCRIPT_FILE_NAME

    # String for self documenting script
    historyString = NOW_STRING + " " + SCRIPT_FILE_NAME + " " + SVN_DESCR + " Arguments"
    historyString += " --toi=" + aCallingTilesObject.getIndicesString()
    if aAddNewData:
        historyString += " --add_new=true" 
    historyString += " " + str(aOutputDirName)
    historyString += " " + str(aInputDirName)

    # Create filename for both the input and output cube 3D-array
    nc4InAndOutputFileName = aCallingTilesObject.getCurrentFileName("cube_", ".hdf5")

    # Copy the 'raw' (not smoothed) input data
    rawInputFile = Dataset(os.path.join(aInputDirName, nc4InAndOutputFileName), 'r')
    key, value = rawInputFile.variables.items()[3]
    if aAddNewData:
        rawDataArray = rawInputFile.variables[key][(-4 * SMOOTHING_QUARTER_STEPSIZE):,:,:].astype(np.float32)
    else:
        rawDataArray = rawInputFile.variables[key][:].astype(np.float32)

    # Copy the time variable and its attributes
    rawTimeArray = rawInputFile.variables['time'][:]
    rawTimeVarAttr = {}
    for key in rawInputFile.variables['time'].ncattrs():
        rawTimeVarAttr[key] = rawInputFile.variables['time'].getncattr(key)

    # Copy the meta data and alter the needed fields for the output later.
    metaData = NetCdfGeoMetaData()
    metaData.copyFromNetCdfObject(rawInputFile)
    metaData.setTitle("Smoothed " + metaData.getTitle())
    metaData.addToHistory(historyString)
    metaData.setSource("Input dir: " + aInputDirName + " files: " + nc4InAndOutputFileName)
    rawInputFile.close()

    # When adding new data only the last bit is smoothed, otherwise the whole is smoothed
    rawTimeLength = len(rawTimeArray)
    if aAddNewData:
        # When adding new data we need a copy of the previously smoothed data original
        smoothedInputFile = Dataset(os.path.join(aOutputDirName, nc4InAndOutputFileName), 'r', format='NETCDF4')
        key, value = smoothedInputFile.variables.items()[3]
        previouslySmoothedDataArray = smoothedInputFile.variables[key][:]
        previouslySmoothedTimeArray = smoothedInputFile.variables['time'][:]
        smoothedInputFile.close()
        lastSmoothedDate = previouslySmoothedTimeArray[-1]
        addedDatesNumber = 0

        # How many new dates are added in the raw data
        while lastSmoothedDate < rawTimeArray[rawTimeLength - addedDatesNumber - 1]:
            addedDatesNumber += 1

        # Stop processing if there is no new data
        if addedDatesNumber == 0:
            print "No added data to smooth for: " + str(nc4InAndOutputFileName)
            return
        else:
            outputTimeLength = len(previouslySmoothedTimeArray) + addedDatesNumber
    else:
        outputTimeLength = rawTimeLength

    # Create a copy of the cube to put the smoothed data in
    smoothCube = np.zeros(rawDataArray.shape, np.float32)
    smoothTimeArray = np.zeros((outputTimeLength), np.int32)
    noDataValue = float(metaData.getNoDataValue())

    # Smooth the data in the cube (replaces original data)
    print "Smoothing: " + str(nc4InAndOutputFileName)
    for latitude in range(smoothCube.shape[1]):
        for longitude in range(smoothCube.shape[2]):
            # If 50% of values is negative, use not a number value
            if outputTimeLength < (2 * (smoothCube[:,latitude,longitude] < 0).sum()):
                smoothCube[:,latitude,longitude] = noDataValue
            else:
                smoothCube[:,latitude,longitude] = Smooth(rawDataArray[:,latitude,longitude], noDataValue)


    if aAddNewData:
        # Create output file
        ncfileOut = Dataset(os.path.join(aOutputDirName, nc4InAndOutputFileName), 'r+', format='NETCDF4')
#        metaData.copyToNetCdfObject(ncfileOut) TODO Figure out how to rewrite/overwrite the metadata of an existing file
        key, value = ncfileOut.variables.items()[3]
        dataArray = ncfileOut.variables[key]
        timeArray = ncfileOut.variables['time']
        dataArray[(outputTimeLength - 3 * SMOOTHING_QUARTER_STEPSIZE):outputTimeLength,:,:] = smoothCube[(-3 * SMOOTHING_QUARTER_STEPSIZE):,:,:]
        timeArray[(outputTimeLength - 3 * SMOOTHING_QUARTER_STEPSIZE):outputTimeLength] = rawTimeArray[(-3 * SMOOTHING_QUARTER_STEPSIZE):]
        ncfileOut.close()
    else:
        # Create output file
        ncfileOut = Dataset(os.path.join(aOutputDirName, nc4InAndOutputFileName), 'w', format='NETCDF4')
        metaData.copyToNetCdfObject(ncfileOut)
        ncfileOut.createDimension(rawTimeVarAttr['standard_name'], None)
        variable = ncfileOut.createVariable(rawTimeVarAttr['standard_name'], np.dtype('int32').char, (rawTimeVarAttr['standard_name']))
        for key, value in rawTimeVarAttr.iteritems():
            variable.setncattr(key, value)
        variable[:] = rawTimeArray
        metaData.createDimVarsLatLong(ncfileOut, smoothCube.shape[1], smoothCube.shape[2])

        # Create variable for smoothed data
        data = ncfileOut.createVariable(metaData.getTitle(),np.dtype('float32').char, ('time','latitude','longitude'))
        data.setncattr('standard_name',metaData.getTitle())
        data.setncattr('units',metaData.getUnits())
        data.setncattr('long_name',metaData.getTitle())
        data[:] = smoothCube
        ncfileOut.close()

#   # Plot the original and smoothed timeseries in a plot
#   t = np.arange(0, smoothCube.shape[2], 1)
#   plt.figure(1)
#   plt.subplot(211)
#   plt.plot(t, smoothCube[0,0], "rx-");
#   plt.plot(t, rawDataArray.astype(np.float32)[0,0], "bx-");
#   plt.show()

    return

