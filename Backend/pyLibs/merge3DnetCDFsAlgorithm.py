###############################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
###############################################################################

import re
SVN_REVISION = '$Rev: 7715 $'
SVN_DATE = '$Date: 2016-12-16 11:40:50 +0100 (Fri, 16 Dec 2016) $'
SVN_AUTHOR = '$Author: jb76278 $'

# Shortened and readable SVN description
import re
SVN_DESCR = (SVN_REVISION + re.sub('\) \$', ' ' ,re.sub('^.*\(', 'Date: ', SVN_DATE)) + SVN_AUTHOR).replace('$', '')

import numpy as np
import os
import gc

# The following import(s) need correct setting of the PYTHONPATH environment variable. For example in .bashrc:
# export PYTHONPATH="${PYTHONPATH}:`pwd`/SvnCheckout/CommonSense/RSDS/DataProcessing/Python/code:`pwd`/SvnCheckout/CommonSense/RSDS/DataProcessing/Python/pyLibs"
from netCdfGeoMetaData import NetCdfGeoMetaData

from netCDF4 import Dataset
import datetime

SCRIPT_FILE_NAME = os.path.basename(__file__)
NOW_STRING = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

######################################################################################################
# This method will enlarge the output cube so that it can fit the input cubes
######################################################################################################
def EnlargeOutputCube(aInputDirName, aOutputCube, aTilesObject):
    global SVN_DESCR, NOW_STRING, SCRIPT_FILE_NAME

    # Get the upper left cube and copy the time axis data from it
    fileName = aTilesObject.getUpperLeftFileName("cube_", ".hdf5")
    ncFileIn = Dataset(os.path.join(aInputDirName, fileName), 'r')
    inputTimeArray = ncFileIn.variables['time'][:]
    ncFileIn.close()

    # Get the output netCDF cube and enlarge it (if necessary)
    outputTimeArray = aOutputCube.variables['time']
    origTimeAxisLen = len(outputTimeArray)
    newTimeAxisLen = len(inputTimeArray)
    layersToAdd = newTimeAxisLen - origTimeAxisLen

    if newTimeAxisLen > origTimeAxisLen:
        # Get reference to the main cube of data
        key, value   = aOutputCube.variables.items()[3]
        dataArrayOut = aOutputCube.variables[key]

        print((layersToAdd, dataArrayOut.shape[1], dataArrayOut.shape[2]))
        dataToAdd = np.zeros(shape=(layersToAdd, dataArrayOut.shape[1], dataArrayOut.shape[2]), dtype=dataArrayOut.dtype)
        dataArrayOut[origTimeAxisLen:newTimeAxisLen,:,:] = dataToAdd
        outputTimeArray[origTimeAxisLen:newTimeAxisLen] = inputTimeArray[origTimeAxisLen:newTimeAxisLen]

    return layersToAdd

######################################################################################################
# This method will merge the cubes specified into a new cube.
######################################################################################################
def AppendOutputWithInput(aCallingTilesObject, aInputDirName, aOutputData, aHeight, aWidth, aAddedLayers, aNoDataValue=None):

    # Quit if no layers need to be added.
    if aAddedLayers <= 0:
        return

    # Determine the y and x positions in the output multi-cube for the current input multi-cube
    yPosit = aHeight * (aCallingTilesObject.getCurrentUpDownIndex() - aCallingTilesObject.getUpperIndex())
    xPosit = aWidth * (aCallingTilesObject.getCurrentLeftRightIndex() - aCallingTilesObject.getLeftIndex())

    fileName = aCallingTilesObject.getCurrentFileName("cube_", ".hdf5")
    print "Adding with file: " + fileName
    nc4file = Dataset(os.path.join(aInputDirName, fileName), 'r')
    key, value = nc4file.variables.items()[3]
    inputArray = np.array(nc4file.variables[key][:])
    nc4file.close()

    if aNoDataValue <> None:
       index = np.where(inputArray == aNoDataValue)
       print "#noDataValue (%s) = %d" % (aNoDataValue, len(index[0]))
       inputArray[index] = float('nan')

    # Copy the data with 30 extra added layers to account for the fact that smoothing affects the last 30 layers.
    aAddedLayers += 30
    if aAddedLayers > inputArray.shape[0]:
        aAddedLayers = inputArray.shape[0]
    aOutputData[(inputArray.shape[0]-aAddedLayers):,yPosit:yPosit+inputArray.shape[1],xPosit:xPosit+inputArray.shape[2]] = inputArray[(inputArray.shape[0]-aAddedLayers):,:,:]

    # Cleanup
    del inputArray
    gc.collect()

    return

######################################################################################################
# This method will create the output cube that can fit the input multi-cubes
######################################################################################################
def CreateOutputCube(aInputDirName, aOutputFileName, aTilesObject):
    global SVN_DESCR, NOW_STRING, SCRIPT_FILE_NAME

    # String for self documenting script
    historyString = NOW_STRING + " " + SCRIPT_FILE_NAME + " " + SVN_DESCR + " Arguments"
    historyString += " --toi=" + aTilesObject.getIndicesString()
    historyString += " " + str(aOutputFileName)
    historyString += " " + str(aInputDirName)

    # Object for the meta data of the input (will be similar to that of the output)
    metaData = NetCdfGeoMetaData()

    # Get the upper left cube and get the (meta) data from it that we need
    fileName = aTilesObject.getUpperLeftFileName("cube_", ".hdf5")
    nc4file = Dataset(os.path.join(aInputDirName, fileName), 'r')
    key, value = nc4file.variables.items()[3]
    variableDType = value.dtype
    dataArray = nc4file.variables[key][:]
    cubeLatSize = dataArray.shape[1]
    cubeLongSize = dataArray.shape[2]
    # Make a copy of the time variable and its attributes
    timeArray = nc4file.variables['time'][:]
    timeVarAttr = {}
    for key in nc4file.variables['time'].ncattrs():
        timeVarAttr[key] = nc4file.variables['time'].getncattr(key)

    # Copy the meta data and alter the needed fields for the output later.
    metaData.copyFromNetCdfObject(nc4file)
    metaData.addToHistory(historyString)
    metaData.setSource("Input dir: " + aInputDirName)
    nc4file.close()

    # Get the lower right cube and derive the sizes of the output cube
    fileName = aTilesObject.getLowerRightFileName("cube_", ".hdf5")
    nc4file = Dataset(os.path.join(aInputDirName, fileName), 'r')
    key, value = nc4file.variables.items()[3]
    dataArray = nc4file.variables[key][:]
    outputTimeSize = dataArray.shape[0]
    outputTimeSize = dataArray.shape[0]
    outputLatSize = cubeLatSize * (aTilesObject.getLowerIndex() - aTilesObject.getUpperIndex()) + dataArray.shape[1]
    outputLongSize = cubeLongSize * (aTilesObject.getRightIndex() - aTilesObject.getLeftIndex()) + dataArray.shape[2]
    nc4file.close()

    # Create the output netCDF cube
    print "Creating output file: " + str(aOutputFileName)
    ncfileOut = Dataset(aOutputFileName,'w', format='NETCDF4')
    metaData.copyToNetCdfObject(ncfileOut)
    ncfileOut.createDimension(timeVarAttr['standard_name'], None)
    variable = ncfileOut.createVariable(timeVarAttr['standard_name'], np.dtype('int32').char, (timeVarAttr['standard_name']))
    for key, value in timeVarAttr.iteritems():
        variable.setncattr(key, value)
    variable[:] = timeArray
    metaData.createDimVarsLatLong(ncfileOut, outputLatSize, outputLongSize)
    data = ncfileOut.createVariable(metaData.getTitle(),np.dtype('float32').char, ('time','latitude','longitude'))
    data.setncattr('standard_name',metaData.getTitle())
    data.setncattr('units',metaData.getUnits())
    data.setncattr('long_name',metaData.getTitle())
    return ncfileOut

######################################################################################################
# Method will get the height and width of given multi-cube
######################################################################################################
def GetHeightAndWidth(aInputDirName, aInputFileName):
    nc4file = Dataset(os.path.join(aInputDirName, aInputFileName), 'r')

    key, value = nc4file.variables.items()[3]
    inputArray = nc4file.variables[key][:]
    nc4file.close()

    height = inputArray.shape[1]
    width = inputArray.shape[2]

    return height, width

######################################################################################################
# This method will merge the cubes specified into a new cube.
######################################################################################################
def CopyCubesIntoOutput(aCallingTilesObject, aInputDirName, aOutput, aHeight, aWidth, aNoDataValue=None):

    # Determine the y and x positions in the output multi-cube for the current input multi-cube
    yPosit = aHeight * (aCallingTilesObject.getCurrentUpDownIndex() - aCallingTilesObject.getUpperIndex())
    xPosit = aWidth * (aCallingTilesObject.getCurrentLeftRightIndex() - aCallingTilesObject.getLeftIndex())

    fileName = aCallingTilesObject.getCurrentFileName("cube_", ".hdf5")
    print "Copying file: " + fileName
    ncFileIn = Dataset(os.path.join(aInputDirName, fileName), 'r')
    key, value = ncFileIn.variables.items()[3]
    inputArray = np.array(ncFileIn.variables[key])

    if aNoDataValue <> None:
       index = np.where(inputArray == aNoDataValue)
       print "#noDataValue (%s) = %d" % (aNoDataValue, len(index[0]))
       inputArray[index] = float('nan')

    # Copy the data
    aOutput[:, yPosit:yPosit+inputArray.shape[1], xPosit:xPosit+inputArray.shape[2]] = inputArray

    # Cleanup
    ncFileIn.close()
    del inputArray
    gc.collect()

    return
