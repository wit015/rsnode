################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2018, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

SVN_REVISION = '$Rev: 7715 $'
SVN_DATE = '$Date: 2016-12-16 11:40:50 +0100 (Fri, 16 Dec 2016) $'
SVN_AUTHOR = '$Author: jb76278 $'

# Settings for the MODIS toolchain
import os
import re

def check_dir(d):
    """Check existence of directory `d` otherwise create it.
    """
    d = os.path.abspath(d)
    if not os.path.exists(d):
        os.mkdir(d)
    return d

res = None 
product = None

MODIS_MAIN_DIR = None
MODIS_HDF_DIR = None
MODIS_PROJECTED_DIR = None
MODIS_META_DIR = None
MODIS_PRODUCTS = None
MODIS_HDF_DB = None
MODIS_NSAMPLES = None
MODIS_NROWS = None
MODIS_NBANDS = None
MODIS_FINAL_FILE_FMT = None

HDFPATTERN = None
EXPECTED_MODIS_TILES = None

MRT_TMP = None
MRTMOSAIC = None
MRTRESAMPLE = None
MRT_PRMFILE = None
MRT_NEEDED_SDS = None
MRT_TMP_OUT_FILE_FMT = None
MRT_DATA_DIR = None

#settings for MODIS tiles that should be processed product "MO|YD13A1" = None
def init(p, aMainDir, aToolsDir):

   global res
   global product

   global MODIS_MAIN_DIR
   global MODIS_HDF_DIR
   global MODIS_PROJECTED_DIR
   global MODIS_META_DIR
   global MODIS_PRODUCTS
   global MODIS_HDF_DB
   global MODIS_NSAMPLES
   global MODIS_NROWS
   global MODIS_NBANDS
   global MODIS_FINAL_FILE_FMT
   
   global HDFPATTERN
   global EXPECTED_MODIS_TILES
   
   global MRT_TMP
   global MRTMOSAIC
   global MRTRESAMPLE
   global MRT_PRMFILE
   global MRT_NEEDED_SDS
   global MRT_TMP_OUT_FILE_FMT
   global MRT_DATA_DIR
   
   product = p

   # Number of samples, rows, bands of the final mosaic
   if re.match('M[O|Y]D13Q1', product) :
      MODIS_NSAMPLES = 8000
      MODIS_NROWS    = 8889
      res            = "250m"
   elif re.match('M[O|Y]D13A1', product) :
      MODIS_NSAMPLES = 4000
      MODIS_NROWS    = 4450
      res            = "500m"
   else:
      print("Illegal product %s" % product)
      return
   
   #settings for MODIS tiles that should be processed product "MO|YD13A1"
#   MODIS_MAIN_DIR = check_dir("/net/satarch/MODIS")
   MODIS_MAIN_DIR = check_dir(aMainDir)
   MODIS_HDF_DIR = check_dir(os.path.join(MODIS_MAIN_DIR, "hdf/"+product))
   MODIS_PROJECTED_DIR = check_dir(os.path.join(MODIS_MAIN_DIR, "projected_"+product))
   MODIS_META_DIR = check_dir(os.path.join(MODIS_MAIN_DIR, "meta_projected_"+product))
   MODIS_PRODUCTS = [(res+"_16_days_EVI", "EVI", "EVI_"+res+".hdr"),
                     (res+"_16_days_NDVI", "NDVI", "NDVI_"+res+".hdr"),
                     (res+"_16_days_pixel_reliability", "PIXREL", "PIXREL_"+res+".hdr"),
                     (res+"_16_days_VI_Quality", "VIQUAL", "VIQUAL_"+res+".hdr")]
   MODIS_HDF_DB = os.path.join(MODIS_HDF_DIR, "MODISHDFFileCollection.pkl")
   
   MODIS_NBANDS = 1
   MODIS_FINAL_FILE_FMT = "{prefix}_{date}_{postfix}.{ext}"
   HDFPATTERN = re.compile("%s\.A\d{7,7}\.h\d\dv\d\d\."
                           "\d\d\d\.\d{13,13}.hdf" % product)
   EXPECTED_MODIS_TILES = [(21,7), (21,8), (21,8), (21,8)]
   
   # Settings for ModisTool
   MRT_TMP = check_dir("/tmp/MODIS_processing")
#   MRTMOSAIC = "/net/satarch/MODIS/tools/MRT/bin/mrtmosaic"
#   MRTMOSAIC = "/home/jane/jelle/workspace/CommonSense/MODIS/tools/MRT/bin/mrtmosaic"
   MRTMOSAIC = os.path.join(aToolsDir, "bin/mrtmosaic")
#   MRTRESAMPLE = "/net/satarch/MODIS/tools/MRT/bin/resample"
#   MRTRESAMPLE = "/home/jane/jelle/workspace/CommonSense/MODIS/tools/MRT/bin/resample"
   MRTRESAMPLE = os.path.join(aToolsDir, "bin/resample")
#   MRT_PRMFILE = os.path.join(os.path.dirname(os.path.abspath(__file__)), "modis_mosaic_ethiopia_"+res+".prm")
   MRT_PRMFILE = os.path.join(aToolsDir, "prm", "modis_mosaic_ethiopia_"+res+".prm")
   # list of scientific data sets (SDS) that you want to include (marked with 1)
   MRT_NEEDED_SDS = '"1 1 1 0 0 0 0 0 0 0 0 1"'
   MRT_TMP_OUT_FILE_FMT = "MODIS13_{obs_date}.{ext}"
#   MRT_DATA_DIR="/net/satarch/MODIS/tools/MRT/data"
#   MRT_DATA_DIR="/home/jane/jelle/workspace/CommonSense/MODIS/tools/MRT/data"
   MRT_DATA_DIR = os.path.join(aToolsDir, "data")

#init("MOD13A1")
