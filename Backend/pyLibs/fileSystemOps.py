##################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
##################################################################################

SVN_REVISION = '$Rev: 7715 $'
SVN_DATE = '$Date: 2016-12-16 11:40:50 +0100 (Fri, 16 Dec 2016) $'
SVN_AUTHOR = '$Author: jb76278 $'

# Shortened and readable SVN description
import re
SVN_DESCR = (SVN_REVISION + re.sub('\) \$', ' ' ,re.sub('^.*\(', 'Date: ', SVN_DATE)) + SVN_AUTHOR).replace('$', '')

import sys
import os

######################################################################################################
# Method will check the existance of the given directory and try to create it if needed.
######################################################################################################
def MakeSureDirectoryExists(aDirName):
    # Make sure the directory exists (if not, try to create it)
    if not os.path.exists(aDirName):
        try:
            os.makedirs(aDirName)
        except OSError as exception:
            sys.exit("Error: Directory " + aDirName + " did not exist and could not be created.")

######################################################################################################
# Method will check the existance of the output directory and try to create it if needed.
######################################################################################################
def CheckIfFileExists(aFileName):
    # Make sure the directory exists (if not, try to create it)
    if not os.path.isfile(aFileName):
        sys.exit("Error: file " + aFileName + " did not exist.")

