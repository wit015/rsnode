################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2018, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

SVN_REVISION = '$Rev: 7715 $'
SVN_DATE = '$Date: 2016-12-16 11:40:50 +0100 (Fri, 16 Dec 2016) $'
SVN_AUTHOR = '$Author: jb76278 $'

__author__ = 'wit015'
import os, sys
from datetime import datetime, date
import subprocess
import tempfile
import shutil
from collections import OrderedDict
import glob
from textwrap import dedent

import pandas

import config

class MODISHDFFileObject(OrderedDict):
    """Represents a MODIS HDF file on disk
    """

    def __init__(self, modis_fname):
        OrderedDict.__init__(self)

        modis_fname_fp = os.path.abspath(modis_fname)
        if not os.path.exists(modis_fname_fp):
            msg = "Cannot find MODIS file on disk: %s" % modis_fname_fp
            raise RuntimeError(msg)
        self.parse_filename(modis_fname_fp)

    def parse_filename(self, modis_fname_fp):
        """Parses a MODIS filename in (product, doy, htile, vtile, collection)
        """

        modis_fpath, modis_fname = os.path.split(modis_fname_fp)
        if not config.HDFPATTERN.match(modis_fname):
            msg = "Invalid MODIS file name pattern provided: %s" % modis_fname
            # raise RuntimeError(msg)
            return

        product, obsdate, tile, collection, prod_date, ext = modis_fname\
            .split(".")

        self['product'] = product
        self['obs_date'] = datetime.strptime(obsdate, "A%Y%j").date()
        self['collection'] = int(collection)
        self['tile'] = tile
        self['htile_no'] = int(tile[1:3])
        self['vtile_no'] = int(tile[4:6])
        self['prod_date'] = datetime.strptime(prod_date, "%Y%j%H%M%S")
        self['modis_fname_fp'] = modis_fname_fp

    def __str__(self):
        return [self.product, self.obs_date, self.collection, self.tile,
                self.htile_no, self.vtile_no, self.prod_date,
                self.modis_fname_fp]

class MODISHDFFileCollection(object):
    """Represents a collection of MODIS files on disk obtained by
    recursively searching a root folder.
    """
    def __init__(self):
        # Find all HDF files that should be part of the collection
        hdf_files = self._find_HDF_files()
        if not hdf_files:
            msg = "No HDF files found at location: %s" % config.MODIS_HDF_DIR
            raise RuntimeError(msg)

        # Walk trough files and generate a list objects
        MODISHDFlist = []
        for fname in hdf_files:
            MODISHDFlist.append(MODISHDFFileObject(fname))

        # Generate a data frame with all curent MODIS HDF files
        cols = list(MODISHDFlist[0].keys())
        self.CurrentMODISHDF_df = pandas.DataFrame(MODISHDFlist, columns=cols)

        # Read database with previously available MODIS HDF files
        self.PreviousMODISHDF_df = None
        if os.path.exists(config.MODIS_HDF_DB):
            self.PreviousMODISHDF_df = pandas.read_pickle(config.MODIS_HDF_DB)

    def get_new_files_by_date(self):
        """Returns the dates and hdf_files for which new MODIS tiles are
        available and which should be processed.
        """
        # Create and empty dataframe copying from the existing one
        if self.PreviousMODISHDF_df is None:
            self.PreviousMODISHDF_df = pandas.DataFrame(
                columns=self.CurrentMODISHDF_df.columns)

        # run a left join between CurrentMODISHDF_df and PreviousMODISHDF_df
        r = pandas.merge(self.CurrentMODISHDF_df, self.PreviousMODISHDF_df,
                         how='left', on=['product','obs_date', 'collection',
                                         'tile'])
        # find entries where the right hand side for the production date
        # 'prod_date_y' is NULL
        records = r[pandas.isnull(r['prod_date_y'])]

        # Get unique dates for records
        new_dates = list(pandas.unique(records.obs_date))

        # Get all rows for the obs_dates with new files
        results = []
        for obs_date in new_dates:
            index = self.CurrentMODISHDF_df.obs_date == obs_date
            files_for_processing = \
                list(self.CurrentMODISHDF_df[index].modis_fname_fp)
            results.append((obs_date, files_for_processing))

        return results

    def _find_HDF_files(self):
        """Find all hdf files in the path given by config.MODIS_HDF_DIR
        """
        hdf_files = []
        for root, dirs, files in os.walk(config.MODIS_HDF_DIR):
            for file in files:
                if file.endswith(".hdf") or file.endswith(".HDF"):
                     hdf_files.append(os.path.join(root, file))
        return sorted(hdf_files)

    def store_MODISHDF_db(self):
        """Dumps the current HDF db to file
        """
        self.CurrentMODISHDF_df.to_pickle(config.MODIS_HDF_DB)

    def  update_MODISHDF_db(self, obs_date):
        """Updates the current HDF db to file
        """
        index = self.CurrentMODISHDF_df.obs_date == obs_date
        self.PreviousMODISHDF_df = pandas.concat([self.PreviousMODISHDF_df, self.CurrentMODISHDF_df[index]])
        self.PreviousMODISHDF_df.to_pickle(config.MODIS_HDF_DB)

def run_mrtmosaic(obs_date, modis_files):
    """Uses the MODIS Reproject Toolkit to mosaic the given
    modis_files for the specific obs_date.


    Note that the mrtmosaic tool expects its input file (-i flag) to
    be in the !!current directory!!. So running mrtmosaic with:

        mrtmosaic -i /home/dir1/dir2/my_inputs -o my_output.hdf

    does not work. This should be formulated as:

        cd /home/dir1/dir2/
        mrtmosaic -i my_inputs  -o my_output.hdf
    """

    # Create temporary file containing the modis HDF files
    with tempfile.NamedTemporaryFile(delete=False, mode='w',
                                     dir=config.MRT_TMP) as fp:
        for fname in modis_files:
            fp.write("%s\n" % fname)


    # First we run the MRTMOSAIC tool to stitch the tiles together.
    input_hdf_files = os.path.basename(fp.name)
    tmp_hdf_file = config.MRT_TMP_OUT_FILE_FMT.format(obs_date=obs_date, ext="hdf")

    tmp_log = fp.name + ".log"
    call_signature = "{mrt_bin} -i {infile_list} -s {SDS} " \
                     "-o {out_hdf} -g {log}"
    shell_call = call_signature.format(mrt_bin=config.MRTMOSAIC,
                                       infile_list=input_hdf_files,
                                       out_hdf=tmp_hdf_file, log=tmp_log,
                                       SDS=config.MRT_NEEDED_SDS)

    os.environ["MRT_DATA_DIR"] = config.MRT_DATA_DIR
    shell_call = "(cd %s; %s; rm -rf %s %s)" % (config.MRT_TMP, shell_call, fp.name, tmp_log)
    print("call %s\n" % shell_call)

    try:
        subprocess.check_call([shell_call], shell=True)
        # remove file listing the hdf files and the log.
        #os.remove(fp.name)
        #os.remove(tmp_log)
        return tmp_hdf_file
    except subprocess.CalledProcessError as exc:
        msg = "Mosaicing failed for date {date} see {logfile}s"
        msg = msg.format(date=obs_date, logfile=tmp_log)
        print(msg)
        return None

def run_mrtresample(obs_date, tmp_hdf_file):
    """Runs the MRT resample program to resample to the geographic
    projection.

    Reads the projection settings from the PRM file provided by
    config.MRT_PRMFILE

    Also renames output files file naming convention and creates a
    .hdr file for SPIRITS/ENVI.
    """

    call_signature = "{mrt_resample} -i {hdf_file} -o {out_file} " \
                     "-p {prm_file} -g {log}"
    out_file = config.MRT_TMP_OUT_FILE_FMT.format(obs_date=obs_date, ext="hdr")
    log_file = out_file + ".log"
    shell_call = call_signature.format(mrt_resample=config.MRTRESAMPLE,
                                       hdf_file=tmp_hdf_file,
                                       out_file=out_file,
                                       prm_file=config.MRT_PRMFILE,
                                       log=log_file)

    os.environ["MRT_DATA_DIR"] = config.MRT_DATA_DIR
    shell_call = "(cd %s; %s; rm -rf %s)" % (config.MRT_TMP, shell_call, tmp_hdf_file)
    print("call %s\n" % shell_call)

    try:
        subprocess.check_call([shell_call], shell=True)
        #os.remove(tmp_hdf_file)
        return True
    except subprocess.CalledProcessError as exc:
        msg = "Resampling failed for date {date} see {log_file}s"
        msg = msg.format(date=obs_date, logfile=log_file)
        print(msg)
        return False

def create_headers(obs_date, aHdrDir):
    """Creates the ENVI/SPIRITS headers for the modis products for given
    obs_date
    """
    tmp_date = obs_date.strftime("%Y%m%d")
    for product_name, final_name, hdr_file in config.MODIS_PRODUCTS:
        # Construct the final name and path for ENVI/SPIRITS header
        final_product_name = config.MODIS_FINAL_FILE_FMT.format(
            prefix=final_name, date=tmp_date, postfix=config.res, ext="dat")
        final_product_name = os.path.join(config.MODIS_PROJECTED_DIR,
                                          final_product_name)

        # Create the associated header (.hdr) for this file
        # template_hdr = os.path.join(os.path.dirname('__file__'), hdr_file)
#        template_hdr = os.path.join("/net/satarch/MODIS/MainProcess/",hdr_file)
        template_hdr = os.path.join(aHdrDir, hdr_file)
        final_product_hdr = final_product_name + ".hdr"
        print(template_hdr, final_product_hdr)
        shutil.copyfile(template_hdr, final_product_hdr)

def move_modis_mosaics(obs_date):
    """Moves the resampled MODIS mosaics to the right folder and naming
    for the SPIRITS tools. Moreover, it sets up the header for the indivual
    files based on templates.
    """
    basename = config.MRT_TMP_OUT_FILE_FMT.format(obs_date=obs_date,
                                                  ext="dat")
    basename = os.path.join(config.MRT_TMP, basename)
    base, ext = os.path.splitext(basename)
    tmp_date = obs_date.strftime("%Y%m%d")
    for product_name, final_name, hdr_file in config.MODIS_PRODUCTS:
        # First construct the temporary name and path of each product
        full_product_name = "{base}.{product}.{ext}".format(base=base,
            product=product_name, ext="dat")
        # Now construct the final name and path for SPIRITS
        final_product_name = config.MODIS_FINAL_FILE_FMT.format(
            prefix=final_name, date=tmp_date, postfix=config.res, ext="dat")
        final_product_name = os.path.join(config.MODIS_PROJECTED_DIR,
                                          final_product_name)

        # First try to delete target file, shutil.move fails if target exists.
        try:
            os.remove(final_product_name)
        except OSError as e:
            pass

        # Try to move the file to its final position
        try:
            shutil.move(full_product_name, final_product_name)
        except IOError:
            msg = "Failed moving file %s to %s" % (full_product_name, final_product_name)
            print(msg)

    # Remove the <basename>.hdr and <basename>.hdr.log file
    os.remove(basename.replace(".dat", ".hdr"))
    os.remove(basename.replace(".dat", ".hdr.log"))

def run_MODISTool(obs_date, modis_files, aToolsDir):
    """Runs the MODIS processing chain.

    This is done in four steps:
    1. Stitching of individual tiles
    2. Resampling to a geographic projection
    3. Moving the files to the output folder and final product name
    4. Generating headers for the final product name
    """
    tmp_hdf_file = run_mrtmosaic(obs_date, modis_files)
    if tmp_hdf_file is None: # Mosaic failed!
        return
    ok = run_mrtresample(obs_date, tmp_hdf_file)
    move_modis_mosaics(obs_date)
    create_headers(obs_date, os.path.join(aToolsDir, "hdr"))

def generate_ENVI_META_files():
    """Creates an ENVI meta file for the entire time-series and also per year
    """
    meta_record = """\
                    File : {fname}
                    Bands: 1
                    Dims: 1-{nsamples},1-{nrows}
                     \n"""
    meta_record = dedent(meta_record)

    for product, final_name, template_hdr in config.MODIS_PRODUCTS:
        # First generate META file for entire time series
        mask = "%s*.dat" % final_name
        fnames = sorted(glob.glob(os.path.join(config.MODIS_PROJECTED_DIR, mask)))
        meta_file_fp = os.path.join(config.MODIS_META_DIR, "%s_META_%s"
                                                             ".dat" % (final_name, config.res))
        if not fnames:
            msg = "No files found for product %s" % final_name
            print(msg)
            continue

        recs = []
        for fname in fnames:
            rec = meta_record.format(fname=fname,
                                     nsamples=config.MODIS_NSAMPLES,
                                     nrows=config.MODIS_NROWS)
            recs.append(rec)
        lines = "".join(recs)

        with open(meta_file_fp, 'w') as fp:
            fp.write("ENVI META FILE\n")
            fp.write(lines)

        # Generate META files for each year
        for year in range(2000, date.today().year+1):
            mask = "%s_%4i*.dat" % (final_name, year)
            fnames = sorted(glob.glob(os.path.join(config.MODIS_PROJECTED_DIR,
                                                   mask)))
            meta_file_fp = os.path.join(config.MODIS_META_DIR,
                                "%s_%i_META_%s.dat" % (final_name, year, config.res))

            if not fnames:
                msg = "No files found for product %s, year %i" % (final_name,
                                                                  year)
                print(msg)
                continue

            recs = []
            for fname in fnames:
                rec = meta_record.format(fname=fname,
                                         nsamples=config.MODIS_NSAMPLES,
                                         nrows=config.MODIS_NROWS)
                recs.append(rec)
            lines = "".join(recs)

            with open(meta_file_fp, 'w') as fp:
                fp.write("ENVI META FILE\n")
                fp.write(lines)

def main(aToolsDir):
    modis_collection = MODISHDFFileCollection()
    #modis_collection.store_MODISHDF_db()
    for obs_date, modis_files in modis_collection.get_new_files_by_date():
        msg = "files for date %s: %s\n"
        print(msg % (obs_date, modis_files))
        run_MODISTool(obs_date, modis_files, aToolsDir)
        modis_collection.update_MODISHDF_db(obs_date)
    generate_ENVI_META_files()



if __name__ == "__main__":
    main()
