################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

SVN_REVISION = '$Rev: 7715 $'
SVN_DATE = '$Date: 2016-12-16 11:40:50 +0100 (Fri, 16 Dec 2016) $'
SVN_AUTHOR = '$Author: jb76278 $'

# Shortened and readable SVN description
import re
SVN_DESCR = (SVN_REVISION + re.sub('\) \$', ' ' ,re.sub('^.*\(', 'Date: ', SVN_DATE)) + SVN_AUTHOR).replace('$', '')

import numpy as np
import os
import datetime
from netCDF4 import Dataset

# The following import(s) need correct setting of the PYTHONPATH environment variable. For example in .bashrc:
# export PYTHONPATH="${PYTHONPATH}:`pwd`/SvnCheckout/CommonSense/RSDS/DataProcessing/Python/code:`pwd`/SvnCheckout/CommonSense/RSDS/DataProcessing/Python/pyLibs"
import tiles
from netCdfGeoMetaData import NetCdfGeoMetaData

SCRIPT_FILE_NAME = os.path.basename(__file__)
NOW_STRING = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

######################################################################################################
# Method parse the input string and return the date that lies within.
######################################################################################################
def getDateFromString(aInputString):
    returnDate = datetime.datetime.strptime("1900-01-01", "%Y-%m-%d").date()

    if aInputString.startswith('chirps'):
        dateStr = re.sub('\.tif$', '', re.sub('.*v2\.0\.', '', aInputString))
        returnDate = datetime.datetime.strptime(dateStr, "%Y.%m.%d").date()
        day = (returnDate.day - 1 ) * 5 + 1
        returnDate = datetime.date(returnDate.year, returnDate.month, day)
    else:
        headerStr,dateStr,restStr = aInputString.split('_')
        returnDate = datetime.datetime.strptime(dateStr, "%Y%m%d").date()

    return returnDate

######################################################################################################
# Method will merge the tiles for the same area into time series cubes for those areas with
# the time as the 3rd dimension.
######################################################################################################
def Merge2DtilesInto3DtimeSeries(aInputDirName, aOutputDirName, aTilesObject):
    # First we examine how much room we need for the output cubes
    ndirs = 0
    height = 0
    width = 0
    dimensionsSet = False

    # Create filename of the tile array
    nc4FileName = aTilesObject.getUpperLeftFileName("tile_")

    # Loop through all the directories
    allDirs = sorted(os.listdir(aInputDirName), reverse=False)
    for d in allDirs:
        # Count the directories
        ndirs += 1
        # Set the dimensions to the size of the tiles (if not done already)
        if not dimensionsSet:
            dimensionsSet = True
#            print "File: " + str(os.path.join(aInputDirName, d, nc4FileName))
            nc4file = Dataset(os.path.join(aInputDirName, d, nc4FileName), 'r')
            # Get the key (name) of the first variable stored
            key, value = nc4file.variables.items()[2]
            dataArray = nc4file.variables[key][:]
            nc4file.close()
            height = dataArray.shape[0]
            width = dataArray.shape[1]
            arrayType = dataArray.dtype

    # With the number of files and the tile size now known, initialize the array sizes
    placeholderCube = np.empty([ndirs, height, width], dtype=arrayType)

    # Loop through all the tiles (as specified in the input arguments)
    aTilesObject.loopThroughTiles(Merge2DtileInto3DtimeSerie,
            (aInputDirName, aOutputDirName, placeholderCube, allDirs))

######################################################################################################
# Method will merge one set of tiles for one (and the same) area into a time series cube with the
# time as the 3rd dimension.
######################################################################################################
def Merge2DtileInto3DtimeSerie(aCallingTilesObject, aInputDirName, aOutputDirName, aPlaceholderCube, aAllDirs):
    global SVN_DESCR, NOW_STRING, SCRIPT_FILE_NAME
    timeArray = np.empty([aPlaceholderCube.shape[0]], np.dtype('int32'))

    # String for self documenting script
    historyString = NOW_STRING + " " + SCRIPT_FILE_NAME + " " + SVN_DESCR + " Arguments"
    historyString += " --toi=" + aCallingTilesObject.getIndicesString()
    historyString += " " + str(aOutputDirName)
    historyString += " " + str(aInputDirName)

    # Create filenames of the tile 2D-array and the cube 3D-array
    nc4InputFileName = aCallingTilesObject.getCurrentFileName("tile_", ".hdf5")
    nc4OutputFileName = aCallingTilesObject.getCurrentFileName("cube_", ".hdf5")

    # Process (read) the files and collect the data in the 3D arrays
    n = 0
    dimensionsSet = False
    inputMetaData = NetCdfGeoMetaData()
    for d in aAllDirs:
        nc4file = Dataset(os.path.join(aInputDirName, d, nc4InputFileName), 'r')
        key, value = nc4file.variables.items()[2]
        dataArray = nc4file.variables[key][:]
        if not dimensionsSet:
            dimensionsSet = True
            height = dataArray.shape[0]
            width = dataArray.shape[1]

            # Copy the meta data and alter the needed fields for the output later.
            inputMetaData.copyFromNetCdfObject(nc4file)
            inputMetaData.setTitle("Time Series for " + inputMetaData.getTitle())
            inputMetaData.addToHistory(historyString)
            inputMetaData.setSource("Input dir: " + aInputDirName + " files: " + nc4InputFileName)
            inputMetaData.setComment("Recording_Date " + str(inputMetaData.getDate()) + " is date of first measurement.")
            inputMetaData.setReference("netCDF files")
            noDataValue = float(inputMetaData.getNoDataValue())

        currentDateStr = getattr(nc4file, 'Recording_Date', None)
        nc4file.close()
        aPlaceholderCube[n,:height,:width]=dataArray
        timeArray[n] = 0 if currentDateStr == None else (datetime.datetime.strptime(currentDateStr, "%Y-%m-%d").date() - inputMetaData.getDate()).days
        n += 1

    # If more than 50% of the time series is of no-data-value then make them all no-data-value
    for latitude in range(aPlaceholderCube.shape[1]):
        for longitude in range(aPlaceholderCube.shape[2]):
            # If 50% of values is negative, use not a number value
            if aPlaceholderCube.shape[0] < (2 * (aPlaceholderCube[:,latitude,longitude] == noDataValue).sum()):
                aPlaceholderCube[:,latitude,longitude] = noDataValue

    # Write the time-series data to the given destination directory
    print "Creating time-series file: " + str(nc4OutputFileName)
    nc4file = Dataset(os.path.join(aOutputDirName, nc4OutputFileName), 'w', format='NETCDF4')
    inputMetaData.copyToNetCdfObject(nc4file)

    nc4file.createDimension('time', None)
    variable = nc4file.createVariable('time', np.dtype('int32').char, ('time'))
    variable.setncattr('standard_name', 'time')
    variable.setncattr('start_date', str(inputMetaData.getDate()))
    variable.setncattr('start_date_format_string', '%Y-%m-%d')
    variable.setncattr('start_date_format_standard', 'ISO-8601 Calendar extended format')
    variable.setncattr('units','days since ' + str(inputMetaData.getDate()))
    variable.setncattr('long_name','time in days since start_date')
    variable.setncattr('axis','Z')
    variable[:] = timeArray
    inputMetaData.createDimVarsLatLong(nc4file, height, width)

    data = nc4file.createVariable(inputMetaData.getTitle(), aPlaceholderCube.dtype, ('time','latitude','longitude'))
    data.setncattr('standard_name',inputMetaData.getTitle())
    data.setncattr('units',inputMetaData.getUnits())
    data.setncattr('long_name',inputMetaData.getTitle())
    data[:] = aPlaceholderCube[:,:height,:width]

    nc4file.close()
    return

######################################################################################################
# Method will add the new tiles for the same area into time series cubes for those areas with
# the time as the 3rd dimension.
######################################################################################################
def Add2DtilesTo3DtimeSeries(aInputDirName, aOutputDirName, aTilesObject):
    # First we examine how much room we need for the output cubes (taking the upper left tile as representative sample)

    # Loop through all the directories (sorting them is essential when adding new data)
    allDirs = sorted(os.listdir(aInputDirName), reverse=False)

    # Loop through all the tiles (as specified in the input arguments)
    aTilesObject.loopThroughTiles(Add2DtileTo3DtimeSerie,
            (aInputDirName, aOutputDirName, allDirs))

######################################################################################################
# Method will merge one set of tiles for one (and the same) area into a time series cube with the
# time as the 3rd dimension.
######################################################################################################
def Add2DtileTo3DtimeSerie(aCallingTilesObject, aInputDirName, aOutputDirName, aAllDirs):
    global SVN_DESCR, NOW_STRING, SCRIPT_FILE_NAME

    # String for self documenting script
    historyString = NOW_STRING + " " + SCRIPT_FILE_NAME + " " + SVN_DESCR + " Arguments"
    historyString += " --toi=" + aCallingTilesObject.getIndicesString()
    historyString += " --add_new=true"
    historyString += " " + str(aOutputDirName)
    historyString += " " + str(aInputDirName)

    # Create filenames of the tile 2D-array and the cube 3D-array
    nc4InputFileName = aCallingTilesObject.getCurrentFileName("tile_", ".hdf5")
    nc4OutputFileName = aCallingTilesObject.getCurrentFileName("cube_", ".hdf5")

    # Get the time axis data from the TimeSeries data cube
    outFile = Dataset(os.path.join(aOutputDirName, nc4OutputFileName), 'r+', format='NETCDF4')
    outputMetaData = NetCdfGeoMetaData()
    outputMetaData.copyFromNetCdfObject(outFile)
    key, value = outFile.variables.items()[3]
    dataArray = outFile.variables[key]
    # Make a copy of the time variable and its attributes
    timeArray = outFile.variables['time']
    noDataValue = float(outputMetaData.getNoDataValue())
    firstDate = datetime.date(1980, 1, 1)
    lastDate = datetime.date(1980, 1, 1)
    firstDate = outputMetaData.getDate()
    lastDate = firstDate + datetime.timedelta(days=int(timeArray[-1]))
#    outFile.close()

    # Process (read) the files and collect the data in the 3D arrays
    newDataAdded = False
    for d in aAllDirs:
        currentDate = getDateFromString(d)

        # Add the tile to the cube if it is from a later date then the last in the cube
        if currentDate > lastDate:
            nc4file = Dataset(os.path.join(aInputDirName, d, nc4InputFileName), 'r')
            key, value = nc4file.variables.items()[2]
            newDataArray = np.expand_dims(nc4file.variables[key][:], axis=0)
            timeDimSize = dataArray.shape[0]
            dataArray[timeDimSize,:,:] = newDataArray
            dateDelta = (currentDate - firstDate).days
            timeArray[timeDimSize] = dateDelta
            outputMetaData.addToHistory(historyString)
            newDataAdded = True
            nc4file.close()

    # Write the new output if new data was added
    if newDataAdded:
        print "Added to time-series file: " + str(nc4OutputFileName)
    else:
        print "No new data for time-series file: " + str(nc4OutputFileName)

    outFile.close()

    return

