# _______________[General Settings]_____________________________________________________________________________________
# verbose level for LFTP

# Maximum concurrent threads
MaxThreads = 1

# logging level for file, email and stream logging
# DEBUG or INFO or WARNING or ERROR
log_lvl = 'debug'
mail_log_lvl = 'debug'
stream_log_lvl = 'debug'

# _______________[Location Settings]____________________________________________________________________________________
# Base destination for downloaded files
BaseDestination = '/data/RSNODEDATA/INGESTDIR/logs'

# Job File Location
# Default:
#JobFileLocation = 'xml/OMI_OMIG.xml'
JobFileLocation = 'xml/jobfile.xml'

# DTD File Location
DTDFileLocation = 'xml/jobfile.dtd'

''' Log file location
    NOTE: If the location does not exist the system will try to create the new destination folder.
        A new file will be created in this folder. The system will not remove the old file or copy its content
    Default:
'''
LogFileLocation = BaseDestination + '/logs/'

# Download Completion Log Location
# Default:
dclLoc = BaseDestination + '/logs/Completed_Downloads'


# _______________[SMTP Logging Settings]________________________________________________________________________________
# Adress Outgoing Server
smtpHost = 'localhost'

# Email adres of the sender of the email
sender = 'DSL@airbusds.nl'

# List of recipients of the email
receiver = ['DLS@airbusDS.nl']

# Default Email Subject
mailSubject = 'ERROR message ASDAS'

# _______________[Rotating File Handler]________________________________________________________________________________
# Max file size of the log file in bytes
maxFileSize = 20000000

# Max amount of rotating log files
maxLogFiles = 5

# _______________[System Disk Space Check]______________________________________________________________________________
# Percentage to issue memory warning
warningPercentage = 80

# Maximum percentage to issue memory full error
maxPercentage = 95
