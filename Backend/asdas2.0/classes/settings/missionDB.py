# ---------------[EXAMPLE]-----------------------
# *_URL = ftp:// or http://         (string)    |
# *_BASE_FILE_PATH = path           (string)    |
# *_user = username                 (string)    |
# *_pass = password                 (string)    |
# ______________________________________________|

# ---------------[modis500]-------------------------

#modis500_URL = "ftp://ladsweb.nascom.nasa.gov"
modis500_URL = "https://ladsweb.modaps.eosdis.nasa.gov/archive"
modis500_BASE_FILE_PATH = "/allData/6"
modis500_user = ""
modis500_pass = ""
modis500_cmd_options = [ '--header', '"Authorization: Bearer 396B67BA-020B-11E9-A336-CDB570C49BBF"' ]

# ---------------[CHRIPS]------------------------

chirps_URL = 'ftp://chg-ftpout.geog.ucsb.edu'
chirps_BASE_FILE_PATH = '/pub/org/chg/products/CHIRPS-2.0'
chirps_user = 'anonymous'
chirps_pass = 'allard.dewit@wur.nl'
