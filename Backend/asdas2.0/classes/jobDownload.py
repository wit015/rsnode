# This class defines all the functions to be used by the mission classes.

from abc import ABCMeta, abstractmethod
from bcolors import bcolors as bc
import sys                      # sys.exit()


class downloadData:
    __metaclass__ = ABCMeta


    def jobHandler(self):
        pass

    @abstractmethod
    def parseJob(self):
        pass

    @abstractmethod
    def createLocation(self):
        pass

    @abstractmethod
    def createDestination(self):
        pass

    @abstractmethod
    def createSpecialisation(self):
        pass

    @abstractmethod
    def createDownloadArgument(self):
        pass

    @abstractmethod
    def startDownload(self):
        pass

