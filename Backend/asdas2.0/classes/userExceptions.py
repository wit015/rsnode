# This file holds custom exceptions used in the mission classes

class EmptyVariable(Exception):
    pass

class LftpError(Exception):
    pass

class WgetError(Exception):
    pass

class JobExit(Exception):
    pass
