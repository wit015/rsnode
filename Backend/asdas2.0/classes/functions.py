# This class holds functions shared by all the mission classes. Most important functions are the parseJobDescription
# and the logCompleteDownload

from collections import namedtuple  # return value for parseJobDescription()
import datetime
import urlparse
import os
import sys
import fcntl
import time
import logging
import re
import gzip
import tarfile
import fnmatch

log = logging.getLogger(__name__)

def checkPath(_filePath):  # check if path ends with a slash '/'
    if _filePath is None:
        _filePath = ''
    elif _filePath is '':
        _filePath = '/'
    else:
        if _filePath.endswith('//'):
            _filePath = _filePath.rstrip('/')
            _filePath = ''.join([_filePath, '/'])
        elif _filePath.endswith('/') == 0:  # == false, does not end with slash '/'
            _filePath = ''.join([_filePath, '/'])  # add slash to end of file path
    return _filePath
# -----------------------------------------------------------------


def checkEndSpace(_argument):
    if _argument.endswith(' ') == False:  # checks if there is no trailing space (False == 0)
        _argument = ''.join([_argument, ' '])  # adds one space at end of string
    else:  # if there is one or more spaces at end of file
        _argument = _argument.strip()  # remove all training spaces
        checkEndSpace(_argument)  # recurse function (to add the one trailing space)
    return _argument  # return new string
# -----------------------------------------------------------------


def parseJobDescription(_job, _jobLabel):
    """
    Parses the job description and finds each attribute. each attribute is added to a local variable
    a list is made of all the patterns.
    a namedtuple is created that can hold all the attributes

    :rtype : namedtuple
    :param _job: job description
    :param _jobLabel: Job label used for logging
    :return:
    """
    _location          = None
    _path              = None
    _product           = None
    _start_date        = None
    _end_date          = None
    _extract           = None
    _remove            = None
    _window            = None
    _storage_dir       = None

    try:
        jobLabel          = _job.get('number')
        _product          = _job.get('product')
        _location         = _job.find('location')
        _path             = _location.find('path').text if _location.find('path') is not None else None
        _start_date       = _location.find('start_date').text if _location.find('start_date') is not None else None
        _end_date         = _location.find('end_date').text if _location.find('end_date') is not None else None
        _window           = _location.find('window').text if _location.find('window') is not None else None
        _extract          = _job.find('extract')
        _storage_dir      = _job.find('storage_dir').text if _job.find('storage_dir') is not None else None

    except AttributeError as e:
        log.error('job {0} : parseJobDescription exception with: failed {1}. '
                     'Check job description if all tags or attributes are present'.format(jobLabel, str(e)))
        raise
        sys.exit('ParsXml Exception : Check job {0} description if all tags or attributes are present'
                 .format(jobLabel))

    _remove = []
    for _aremove in _job.findall('remove'):
        _remove.append(_aremove.text)

    _pattern = []
    for _apattern in _job.findall('pattern'):
        _pattern.append(_apattern.text)

    _urlpattern = []
    for _apattern in _job.findall('url-pattern'):
        _urlpattern.append(_apattern.text)

    # create namedtuple of all parsed information from the job description
    jobReturn = namedtuple("jobReturn", ["product", "location", "path", "start_date", "end_date", "window",
                                         "pattern", "urlpattern", "extract", "remove", "storage_dir"])
    # print 'returning the parsed job'
    log.info('job {0} : Returning the Parsed Job'.format(jobLabel))
    log.debug('job {0} : Returning the Parsed Job {1}'.format(jobLabel, jobReturn))
    return jobReturn(_product, _location, _path, _start_date, _end_date, _window, _pattern, _urlpattern, _extract, _remove, _storage_dir)
# -----------------------------------------------------------------


def calcDateOfYear(_date):
    """
    function calculates julian date from a date

    :param _date:
    :return _day:
    """
    valDate(_date)
    _sdate = _date.split('-')
    _year = int(_sdate[0])
    _month = int(_sdate[1])
    _day = int(_sdate[2])
    _doy = datetime.datetime(_year, _month, _day).timetuple().tm_yday  # calculate day of year

    return _doy  # Day Of Year
# -----------------------------------------------------------------


def normDate(_date):
    """
    Function normalizes the date to ensure the seperators are correct
    :param _date: date input from period date
    :rtype : string
    :return: _date
    """
    if _date is not None:
        if _date.count('.') > 0:
            _date = _date.replace('.', '-')  # replace - with .
        elif _date.count(',') > 0:  # check for , in date string
            _date = _date.replace(',', '-')  # replace , with .
        elif _date.count('/') > 0:
            _date = _date.replace('/', '-')  # replace / with .
    else:
        _date = ''  # if date is None, make string empty ''
    if _date:  # if _date =! empty
        valDate(_date)  # verify if date has proper YYYY-MM-DD format
    return _date

    # d - datetime.datetime.strptime(_date, '%Y-%m-%d)
    # newdate = d.strftime('%Y.%m.%d)
# -----------------------------------------------------------------


def valDate(_date):
    """
    This function valitades the date string to see if it has the format YYYY-MM-DD.
    If not the function will raise an error.
    :param string _date: date string from start or end date
    :raise ValueError: Incorrect data format, should be YYYY-MM-DD
    """
    try:
        datetime.datetime.strptime(_date, '%Y-%m-%d')
    except ValueError:
        sys.exit(('Incorrect data format, should be YYYY-MM-DD, got {0}'.format(_date)))
# -----------------------------------------------------------------


def logCompleteDownload(_class, _stdvar):
    """
    Parses the _stdvar and adds all downloaded files to the completeDownload log.

    :type _class: mission class object
    :type _stdvar: string
    """
    _dllCom = []
    _dllComUnitTest = []  # added for unit testing
    for i in _stdvar.splitlines():
        if 'Finished transfer' in i:
            _date = str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
            _fileSource = urlparse.urljoin(_class.dbUrl, _class.location)

            _a = i.split("`")  # split the string to remove first apostrophe
            _b = _a[1].split("'")  # split the string to remove from second apostrophe
            if '/' in _b[0]:  # find if there is a path in the string
                _c = _b[0].split('/')  # split the paths
                _fileName = _c[-1]  # add last element of the list to _fileName variable
                for j in range(len(_c) - 1):  # add path elements to destination
                    _fileSource = urlparse.urljoin(_fileSource, _c[j])  # add path element i to the _fileSource
            else:
                _fileName = _b[0]

            for root, dirs, files in os.walk(BaseDestination):  # walk through all folders
                for name in files:  # check for each file
                    if name == _fileName:  # check if name == _file name
                        _filePath = os.path.abspath(os.path.join(root, name))  # retrieve filepath of that file
                        _dllCom.append('\t| '.join([_date, _fileSource, _filePath]))  # create dll complete line
                        _dllComUnitTest.append('\t| '.join([_fileSource, _filePath]))  # added for unit testing

    for _ in range(2):
        try:
            # _dclLoc = checkPath(dclLoc)
            _logFileName = ''.join(['downloadComplete-job', _class.jobLabel, '.log'])  # create log filename
            _logFileLoc = os.path.join(dclLoc, _logFileName)  # add log file name to file destination
            f = open(_logFileLoc, 'a')  # open log file
            for j in _dllCom:
                f.write(j + '\n')  # write each line
            f.close()  # close file
            # break  # break from loop if no exceptions
            return _dllComUnitTest  # added for unit testing
        except IOError as e:
            if e.errno is 2:  # location does not exist
                _class.logger.warning('job {0}-{1} : The location for Completion Logging does not exist, '
                                      'The system will try to create "{2}"'
                                      .format(_class.jobLabel, _class.mission, dclLoc))
                if not os.path.exists(dclLoc):  # check if destination exists
                    try:
                        os.makedirs(dclLoc)  # create new destination
                    except OSError as e:
                        if e.errno is 13:  # cannot create location
                            _class.logger.error('job {0}-{1} : The location given for Completiomn Logging '
                                                'is not allowed'
                                                .format(_class.jobLabel, _class.mission))
                        sys.exit(e)
# -----------------------------------------------------------------

#
# Function to poll stdout/stderr of Popen call
def handlePopenOutput(proc, stdoutLogger, stderrLogger):

   def unblock(f):
      file_flags = fcntl.fcntl(f, fcntl.F_GETFL)
      fcntl.fcntl(f, fcntl.F_SETFL, file_flags | os.O_NONBLOCK)

   def read(f, of):
      while of is not None:
         try:
            line = f.readline()
         except:
            break
         if line <> '' : 
            of(line)
         else:
             break

   unblock(proc.stdout)
   unblock(proc.stderr)

   while proc.returncode == None:
      read(proc.stdout, stdoutLogger)
      read(proc.stderr, stderrLogger)
      time.sleep(0.1)
      proc.poll()

   # There may still be some data in the pipeline
   read(proc.stdout, stdoutLogger)
   read(proc.stderr, stderrLogger)

def handleDownloadedFile(product, fp, fd, extract):
    log.debug("handleDownloadedFile %s %s %s %s" % (product, fp, fd, extract))
    # product not used
    if extract is not None:
        try:
            if re.match('.*\.tar', fp):
                if not os.path.isdir(fd): 
                   os.makedirs(fd)
                else:
                   # Remove old stuff
                   for root, directories, filenames in os.walk(fd):
                       for fn in filenames:
                           dfn = root + '/' + fn
                           os.remove(dfn)
                           log.debug("Removed %s" % dfn)
                tb = tarfile.open(fp)
                for f in tb:
                    try:
                        tb.extract(f.name, path=fd)
                        log.debug("extracted %s from %s" % (f.name, fp))
                    except IOError as e:
                        os.remove(fd + '/' + f.name)
                        tb.extract(f.name, path=fd)
                        log.debug("extracted %s from %s" % (f.name, fp))
                    finally:
                        os.chmod(fd + '/' + f.name, f.mode)
                return None
            m = re.match('(.*)\.gz', fp)
            if m:
                if not os.path.isdir(fd): os.makedirs(fd)
                gz = gzip.open(fp, 'rb')
                f = open(fd + '/' + os.path.basename(m.group(1)), 'w')
                f.write(gz.read())
                f.close()
                log.debug("extracted gz %s" % (fp))
                return None
        except IOError as e:
            return "I/O error({0}): {1} : '{2}'".format(e.errno, e.strerror, fp)
    return None

def handleCleanUp(product, pathname, pattern):
    log.debug("handleCleanUp %s %s %s" % (product, pathname, pattern))
    # product not used
    if pattern is not None and pattern <> '':
        if os.path.isdir(pathname):
            for root, directories, filenames in os.walk(pathname):
                for fn in filenames:
                    fp = root+'/'+fn
                    if fnmatch.fnmatch(fp, pattern):
                        os.remove(fp)
                        log.debug("Removed %s" % fp)
            if not os.listdir(pathname):
                os.rmdir(pathname)
                log.debug("Removed %s" % pathname)
        elif os.path.isfile(pathname):
            if fnmatch.fnmatch(pathname, pattern) : 
                log.debug("Removed %s" % pathname)
                os.remove(pathname)
    return None
