# This class is used to add color the stdoutput

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ERROR = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

    RED = '\033[91m'        # FAIL / ERROR
    GREEN = '\033[92m'      # OKGREEN
    YELLOW = '\033[93m'     # WARNING
    BLUE = '\033[94m'       # OKBLUE
    PURPLE = '\033[95m'     # HEADER
    CYAN = '\033[96m'
    DARKCYAN = '\033[36m'
    # BOLD = '\033[1m'
    # UNDERLINE = '\033[4m'
    END = '\033[0m'


    def colorTest(self):
        print self.HEADER+'HEADER'
        print self.OKBLUE+'OKBLUE'
        print self.OKGREEN+'OKGREEN'
        print self.WARNING+'WARNING'
        print self.FAIL+'FAIL'
        print self.BOLD+'BOLD'
        print self.UNDERLINE+'UNDERLINE'
        print self.ENDC+'ENDC'

        print ''

        print self.RED+'RED'
        print self.YELLOW+'YELLOW'
        print self.BLUE+'BLUE'
        print self.PURPLE+'PURPLE'
        print self.CYAN+'CYAN'
        print self.DARKCYAN+'DARKCYAN'


# a = bcolors()
# a.colorTest()
# test add some text