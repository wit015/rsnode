# this class initializes the logger and its handlers

import string
import logging
import logging.handlers
import sys  # for sys.exit
import os  # for os.makedirs and path.exists
from bcolors import bcolors as bc  # for colors in terminal
from settings.config import *  # for log file location

gLogFileLocation = '/tmp'

def setLogLocation(p):
   global gLogFileLocation 
   gLogFileLocation = p

class logger:
    def __init__(self):
        global gLogFileLocation
        print "LOGGER %s" % gLogFileLocation
        self.lfl = os.path.join(gLogFileLocation, 'ASDASlog.log')
        self.dateFormat = '%Y-%m-%d %H:%M:%S'  # YYYY-MM-DD HH:MM:SS
        self.messageFormat = '%(asctime)s %(levelname)-7s <%(name)-7s| %(threadName)-10s> %(message)s'
        self.formatter = logging.Formatter(fmt=self.messageFormat, datefmt=self.dateFormat)

    # BufferingSMTPHandler imported from https://gist.github.com/anonymous/1379446
    # credit Vinay Sajip
    class BufferingSMTPHandler(logging.handlers.BufferingHandler):
        def __init__(self, mailhost, fromaddr, toaddrs, subject, capacity, formatter):
            logging.handlers.BufferingHandler.__init__(self, capacity)
            self.mailhost = mailhost
            self.mailport = None
            self.fromaddr = fromaddr
            self.toaddrs = toaddrs
            self.subject = subject
            self.setFormatter(formatter)

        def flush(self):
            if len(self.buffer) > 0:
                try:
                    import smtplib
                    port = self.mailport
                    if not port:
                        port = smtplib.SMTP_PORT
                    smtp = smtplib.SMTP(self.mailhost, port)
                    msg = 'From: {0}\nTo: {1}\nSubject: {2}\n\n' \
                          '*** This is an automatically generated email, please do not reply ***\n\n' \
                        .format(self.fromaddr, string.join(self.toaddrs, ','), self.subject)
                    for record in self.buffer:
                        s = self.format(record)
                        # print s
                        msg = msg + s + "\n"
                    # print msg
                    smtp.sendmail(self.fromaddr, self.toaddrs, msg)
                    smtp.quit()
                except:
                    self.handleError(None)  # no particular record
                self.buffer = []
    # End of imported function

    def createRotatingFileHandler(self):
        rotHandler = logging.handlers.RotatingFileHandler(filename=self.lfl,
                                                          mode='a',  # append
                                                          maxBytes=maxFileSize,  # 200kb = 200000b
                                                          backupCount=maxLogFiles)  # amount of rotating files
        rotHandler.setFormatter(self.formatter)
        # rotHandler.setLevel(logging.DEBUG)        # doesn't override logger.setlevel()
        if log_lvl.upper() == 'DEBUG':
            logging.getLogger().setLevel(logging.DEBUG)
        elif log_lvl.upper() == 'INFO':
            logging.getLogger().setLevel(logging.INFO)
        elif log_lvl.upper() == 'WARNING':
            logging.getLogger().setLevel(logging.WARNING)
        elif log_lvl.upper() == 'ERROR':
            logging.getLogger().setLevel(logging.ERROR)
        else:
            print "WARNING, wrong logging severity input for rot Handler, got: {0}".format(log_lvl)
            logging.warning('wrong logging severity input for rot Handler, got: {0}'.format(log_lvl))
        return rotHandler

    def createSMTPHandler(self):
        mailHandler = self.BufferingSMTPHandler(mailhost=smtpHost, fromaddr=sender, toaddrs=receiver,
                                                subject=mailSubject, capacity=10, formatter=self.formatter)
        if mail_log_lvl.upper() == 'DEBUG':
            mailHandler.setLevel(logging.DEBUG)
        elif mail_log_lvl.upper() == 'INFO':
            mailHandler.setLevel(logging.INFO)
        elif mail_log_lvl.upper() == 'WARNING':
            mailHandler.setLevel(logging.WARNING)
        elif mail_log_lvl.upper() == 'ERROR':
            mailHandler.setLevel(logging.ERROR)
        else:
            print "WARNING, wrong logging severity input for SMTP Handler, got: {0}".format(mail_log_lvl)
            logging.warning('wrong logging severity input for SMTP Handler, got: {0}'.format(mail_log_lvl))
        return mailHandler

    def createStreamHandler(self):
        strHandler = logging.StreamHandler()
        strHandler.setFormatter(self.formatter)

        if stream_log_lvl.upper() == 'DEBUG':
            strHandler.setLevel(logging.DEBUG)
        elif stream_log_lvl.upper() == 'INFO':
            strHandler.setLevel(logging.INFO)
        elif stream_log_lvl.upper() == 'WARNING':
            strHandler.setLevel(logging.WARNING)
        elif stream_log_lvl.upper() == 'ERROR':
            strHandler.setLevel(logging.ERROR)
        else:
            print "WARNING, wrong logging severity input for Stream Handler, got: {0}".format(stream_log_lvl)
            logging.warning('wrong logging severity input for Stream Handler, got: {0}'.format(stream_log_lvl))
        return strHandler

    def addHandlers(self):
        global gLogFileLocation
        #logging.getLogger().addHandler(self.createSMTPHandler())
        logging.getLogger().addHandler(self.createStreamHandler())
        for _ in range(2):
            try:
                logging.getLogger().addHandler(self.createRotatingFileHandler())
                break

            except IOError as e:
                if e.errno is 2:
                    logging.warning('Logging location does not exist, The system will try to create \'{0}\''
                                    .format(gLogFileLocation))
                    print "{0}WARNING: Logging location does not exist, The system will try to create \'{1}\'{2}" \
                        .format(bc.WARNING, gLogFileLocation, bc.ENDC)
                    if not os.path.exists(gLogFileLocation):
                        try:
                            os.makedirs(gLogFileLocation)
                        except OSError as e:
                            if e.errno is 13:
                                print '{0}{1}ERROR{2}{3}: The given logging location is not allowed!{4}' \
                                    .format(bc.ERROR, bc.BOLD, bc.ENDC, bc.ERROR, bc.ENDC)
                                sys.exit(e)

def init():
   log = logger()
   log.addHandlers()
