import re
import os
import sys
import wget
import datetime
from functions import *

def genFilePaths(product, startDate, endDate, window):

    def filterDate(d):
        # Return true means do not filter
        if product == 'domino_monthly':
           # Only once per month
           return d.day == 1
        if product == 'domino_daily':
           return True
        if product == 'qa4ecv_no2_monthly':
           # Only once per month
           return d.day == 1
        if product == 'qa4ecv_no2_daily':
           return True
        if product == 'OMDOAO3':
           return True
        return False

    #: Convert date to location path
    def formatDate(d):
        if product == 'OMDOAO3':
           return d.strftime("%Y/o3col_omi%Y%02m%02d.tar.gz")
        if product == 'domino_monthly':
           return d.strftime("%Y/%02m/no2_%Y%02m.grd.gz")
        if product == 'domino_daily':
           return d.strftime("%Y/omi_no2_he5_%Y%02m%02d.tar")
        if product == 'qa4ecv_no2_monthly':
           return d.strftime("%Y/%02m/no2_%Y%02m.grd.gz")
        if product == 'qa4ecv_no2_daily':
           return d.strftime("%Y/%02m/omi_no2_qa4ecv_%Y%02m%02d.tar")
        return ''

    # If no start date is defined, assume empty file_path
    # Note this is different from an empty file_path_list
    if startDate is None:
        return ([''], [''])

    td = datetime.date.today()

    sd = datetime.datetime.strptime(startDate, '%Y-%m-%d').date()
    if endDate <> '':
        ed = datetime.datetime.strptime(endDate, '%Y-%m-%d').date()
    else:
        ed = td
     
    if ed > td : ed = td

    dr = [ sd + datetime.timedelta(days=x) for x in range((ed-sd).days+1) ]

    # Generate in-window file paths
    iwr = dr if window is None else dr[-window:]
    iwr = filter(filterDate, iwr)
    iwr = map(formatDate, iwr)

    # Generate out-window file paths
    owr = [] if window is None else dr[0:-window]
    owr = filter(filterDate, owr)
    owr = map(formatDate, owr)

    return (owr, iwr)

def handleTemisDownloadedFile(product, fp, extract):
    if product == 'OMDOAO3':
       m = re.match('.*o3col_omi([0-9]{8})\..*', fp)
       if m:
           d = datetime.datetime.strptime(m.group(1), "%Y%m%d").date()
           fd = os.path.dirname(fp)+'/daily/'+d.strftime("%03j")
           return handleDownloadedFile(product, fp, fd, extract)
    if product == 'domino_daily':
       m = re.match('.*omi_no2_he5_([0-9]{8})\..*', fp)
       if m:
           d = datetime.datetime.strptime(m.group(1), "%Y%m%d").date()
           fd = os.path.dirname(fp)+'/daily/'+d.strftime("%03j")
           return handleDownloadedFile(product, fp, fd, extract)
    if product == 'domino_monthly':
        fd = os.path.dirname(os.path.dirname(fp))+'/monthly'
        return handleDownloadedFile(product, fp, fd, extract)
    if product == 'qa4ecv_no2_daily':
       m = re.match('.*omi_no2_qa4ecv_([0-9]{8})\..*', fp)
       if m:
           d = datetime.datetime.strptime(m.group(1), "%Y%m%d").date()
           fd = os.path.dirname(os.path.dirname(fp))+'/daily/'+d.strftime("%03j")
           return handleDownloadedFile(product, fp, fd, extract)
    if product == 'qa4ecv_no2_monthly':
        fd = os.path.dirname(os.path.dirname(fp))+'/monthly'
        return handleDownloadedFile(product, fp, fd, extract)
    return None

def handleTemisCleanUp(product, pathname, pattern):
    # Do not remove any files so return immediately.
    return None

class TEMIS(wget.Wget):

    def __init__(self, jobDescr, jobLabel, base_destination, url, user, password, base_file_path):
       wget.Wget.__init__(self, jobDescr, jobLabel, base_destination, "TEMIS", url, user, password, base_file_path, 
                          genFilePaths, handleTemisDownloadedFile, handleTemisCleanUp)
