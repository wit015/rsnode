import re
import os
import sys
import wget
import datetime
from functions import *

def genFilePaths(product, startDate, endDate, window):

    # Whether date is a proper measurement interval date
    def filterDate(d):
        # Return true means do not filter
        if re.match('.*MOD.*', product):
            return (d.timetuple().tm_yday - 1) % 16 == 0
        if re.match('.*MYD.*', product):
            return (d.timetuple().tm_yday - 9) % 16 == 0
        return False

    #: Convert date to location path
    def formatDate(d):
        return d.strftime("%Y/%03j")

    # If no start date is defined, assume empty file_path
    # Note this is different from an empty file_path_list
    if startDate is None:
        return ([''], [''])

    td = datetime.date.today()

    sd = datetime.datetime.strptime(startDate, '%Y-%m-%d').date()
    if endDate <> '':
        ed = datetime.datetime.strptime(endDate, '%Y-%m-%d').date()
    else:
        ed = td
     
    if ed > td : ed = td

    dr = [ sd + datetime.timedelta(days=x) for x in range((ed-sd).days+1) ]

    # Generate in-window path names
    iwr = dr if window is None else dr[-window:]
    iwr = filter(filterDate, iwr)
    iwr = map(formatDate, iwr)

    # Generate out-window path names
    owr = [] if window is None else dr[0:-window]
    owr = filter(filterDate, owr)
    owr = map(formatDate, owr)

    return (owr, iwr)

def handleModis500DownloadedFile(product, fp, extract):
    fd = os.path.dirname(fp)
    return handleDownloadedFile(product, fp, fd, extract)

class modis500(wget.Wget):

    def __init__(self, jobDescr, jobLabel, base_destination, url, user, password, base_file_path, cmd_options=[]):
       wget.Wget.__init__(self, jobDescr, jobLabel, base_destination, "modis500", url, user, password, base_file_path, 
                          genFilePaths, handleModis500DownloadedFile, handleCleanUp, cmdOptions=cmd_options)
