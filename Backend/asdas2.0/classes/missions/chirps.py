import re
import os
import sys
import wget
import datetime
from functions import *

def genFilePaths(product, startDate, endDate, window):

    # Whether date is a proper measurement interval date
    def filterDate(d):
        return d.day in [1, 6, 11, 16, 21, 26]

    #: Convert date to location path
    def formatDate(d):
        # For FTP the location has to end with wildcard otherwise wget won't download any files
        # From 2017 prelimenary files are not compressed
        return "chirps-v2.0.%04d.%02d.%d.tif*" % (d.year, d.month, ((d.day-1)/5)+1)

    # If no start date is defined, assume empty file_path
    # Note this is different from an empty file_path_list
    if startDate is None:
        return ([''], [''])

    td = datetime.date.today()

    sd = datetime.datetime.strptime(startDate, '%Y-%m-%d').date()
    if endDate <> '':
        ed = datetime.datetime.strptime(endDate, '%Y-%m-%d').date()
    else:
        ed = td
     
    if ed > td : ed = td

    dr = [ sd + datetime.timedelta(days=x) for x in range((ed-sd).days+1) ]

    # Generate in-window path names
    iwr = dr if window is None else dr[-window:]
    iwr = filter(filterDate, iwr)
    iwr = map(formatDate, iwr)

    # Generate out-window path names
    owr = [] if window is None else dr[0:-window]
    owr = filter(filterDate, owr)
    owr = map(formatDate, owr)

    return (owr, iwr)

def handleChirpsDownloadedFile(product, fp, extract):
    fd = os.path.dirname(fp)
    return handleDownloadedFile(product, fp, fd, extract)

class chirps(wget.Wget):

    def __init__(self, jobDescr, jobLabel, base_destination, url, user, password, base_file_path):
       wget.Wget.__init__(self, jobDescr, jobLabel, base_destination, "chirps", url, user, password, base_file_path, 
                          genFilePaths, handleChirpsDownloadedFile, handleCleanUp)
