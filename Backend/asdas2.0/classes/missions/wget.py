# this file handles the downloading through wget

from classes.jobDownload import *
import sys
import subprocess
from classes.userExceptions import *
import imp
import re
import os
import time
import fcntl
import logging
import uuid
from functions import *

class Wget(downloadData):

    def __init__(self, job, jobLabel, base_destination, mission, url, user, password, base_file_path, 
                 genFilePaths, handleFile, handleCleanUp, cmdOptions=[]):

        # general
        assert isinstance(job, object)  # makes sure that job is of type object.
        self.jobfile = job
        self.jobLabel = jobLabel
        self.mission = mission
        self.logger = logging.getLogger(self.mission)
        self.base_destination = base_destination
        self.genFilePaths = genFilePaths
        self.handleFile = handleFile
        self.handleCleanUp = handleCleanUp
        self.urltype = ''
        self.cookies = '/tmp/.wget' + str(uuid.uuid4()) + '.cookies'
        self.cmdOptions = cmdOptions

        # Job Description Variables
        self.location = ''
        self.path = ''
        self.product = ''
        self.pattern = ''
        self.period = ''
        self.start_date = ''
        self.end_date = ''
        self.window = None
        self.attempt = 1
        self.extract = None
        self.remove = []
        self.storage_dir = None

        # Download Arguments
        self.dllArg = ''
        self.login = ''
        self.dllType = ''
        self.extraArgument = ''
        self.url = ''
        self.destination = ''
        self.urltype = ''

        # MissionDB Variables
        try:
            self.dbUrl = url
            self.dbUser = user
            self.dbPass = password
            self.dbBase = base_file_path
        except NameError as e:
            sys.exit('Problems in MissionDB - {0}'
                     .format(e))

    def jobHandler(self):
        self.parseJob()  # start parsing job
        self.createSpecialisation()  # create specialisation variable
        (owr, iwr) = self.genFilePaths(self.product, self.start_date, self.end_date, self.window)
        for urlpath in iwr:
            filepath = None
            outpath  = urlpath
            if type(outpath) is tuple: 
               urlpath, outpath = outpath
               filepath         = outpath
            self.createLocation(urlpath)     # create download location
            self.createDestination(outpath)  # create destination variable
            self.createDownloadArgument(filepath)  # create download argument from other arguments
            self.startDownload()
        for urlpath in owr:
            filepath = urlpath
            if type(filepath) is tuple: (urlpath, filepath) = filepath
            pn = os.path.join(self.base_destination, self.mission, self.path, filepath)
            for pattern in self.remove: self.handleCleanUp(self.product, pn, pattern)
        os.remove(self.cookies)

    def parseJob(self):
        self.logger.info('job {0}-{1} : Parsing started'.format(self.jobLabel, self.mission))

        _px = parseJobDescription(self.jobfile, self.jobLabel)

        try:  # try for problems from parsing
            self.product = _px.product
            if not self.product:  # Check if product is empty
                raise EmptyVariable  # if empty
            self.path = _px.path
            self.window = int(_px.window) if _px.window is not None else None
            self.pattern = _px.pattern
            self.urlpattern = _px.urlpattern
            self.start_date = _px.start_date
            self.end_date = _px.end_date
            self.extract = _px.extract
            self.remove = _px.remove
            self.storage_dir = _px.storage_dir
            # check if date is correct
            self.start_date = normDate(self.start_date)  # Normalise Start Date
            self.end_date = normDate(self.end_date)  # Normalise End Date
        except AttributeError as e:
            sys.exit(e)
        except EmptyVariable:
            sys.exit('No product has been filled in')

    def createLocation(self, file_path):
        """
        Create the location argument for the download argument

        """
        self.logger.info('job {0}-{1} : Creating Location {2}'.format(self.jobLabel, self.mission, file_path))
        self.location = os.path.join(self.dbBase, self.path, file_path)

    def createDestination(self, file_path):
        self.logger.info('job {0}-{1} : Creating Destination {2} {3} {4}'.format(self.jobLabel, self.mission, self.base_destination, file_path, self.path))

        try:
            if self.storage_dir is not None:
                self.destination = os.path.join(self.storage_dir, file_path)
            else:
                self.destination = os.path.join(self.base_destination, self.mission, self.path, file_path)
            if self.destination[-1] <> '/': self.destination = os.path.dirname(self.destination)
            if not os.path.isdir(self.destination): os.makedirs(self.destination)
            open(self.cookies, "a").close()

        except EmptyVariable:
            sys.exit('ERROR : failed to create destination %s' % self.destination)  # shut down system with warning


    def createSpecialisation(self):
        self.extraArgument = []
        for _pat in self.urlpattern:
            self.extraArgument = self.extraArgument + ["--accept-regex", "%s" % _pat]
        for _pat in self.pattern:
            self.extraArgument = self.extraArgument + ["--accept", "%s" % _pat]

        self.logger.debug('job {0}-{1} : Extra Argument: {2}'.format(self.jobLabel, self.mission, self.extraArgument))

    def createDownloadArgument(self, filepath):
        self.logger.info('job {0}-{1} : Creating the Download Argument'.format(self.jobLabel, self.mission))

        self.dllArg = ['/usr/bin/wget']

        # Add server URL
        try:
            self.url = self.dbUrl   # import url from settings
            if self.url is '':      # if empty
                raise ValueError
        except NameError:
            sys.exit('Server URL not present in MissionDB')
        except ValueError:
            sys.exit('Server URL is empty')

        # create Download Argument
        try:
            # -r                  = recursive
            # -N                  = use timestamping
            # -nH                 = Disable generation of host-prefixed directories
            # -np                 = Do not ever ascend to the parent directory when retrieving recursively.
            # -nv                 = No verbose
            # --force-directories = create destination dir if it does not exist
            # --*cookeies*        = keep cookies across session
            # -e robots=off       = ignore robots convetion (https://www.gnu.org/software/wget/manual/html_node/Robot-Exclusion.html)
            self.dllArg.extend("-nH -np -l 10 -nv --force-directories".split(" "))
            self.dllArg.extend(["-e",  "robots=off"])
            if filepath is not None: 
               self.dllArg.append("-O")
               self.dllArg.append("%s" % self.destination + '/' + os.path.basename(filepath))
            else:
               self.dllArg.append("-r")
               self.dllArg.append("-N")
            self.dllArg.append("--directory-prefix=%s" % self.destination)
            self.dllArg.append("--user=%s" % self.dbUser)
            self.dllArg.append("--password=%s" % self.dbPass)
            self.dllArg.append("--keep-session-cookies")
            self.dllArg.extend(["--load-cookies", "%s" % self.cookies])
            self.dllArg.extend(["--save-cookies", "%s" % self.cookies])

            self.dllArg.extend(self.cmdOptions)
          
            # Set cut-dirs to prevent copying dir structure of location
            depth = self.location.count('/')
            if self.location[-1] == '/': depth = depth - 1
            self.dllArg.append("--cut-dirs=%d" % depth)
            self.dllArg.append("%s%s" % (self.url, self.location))
        except AttributeError as e:
            print bc.ERROR + '!! Attribute Error' + bc.ENDC
            self.logger.error('job {0}-{1} : attribute error {3}'
                              .format(self.jobLabel, self.mission, e))
            sys.exit(e)
        self.logger.debug('job {0}-{1} : Download Argument:\n{2}'
                          .format(self.jobLabel, self.mission, self.dllArg))


    def startDownload(self):

        def handleLine(line):
            hdr = 'job {0}-{1} : '.format(self.jobLabel, self.mission)
            self.logger.debug(hdr + line.strip('\n'))
            m = re.match('.* -> "(.*)".*', line)
            if m:
               fp = m.group(1)
               e = self.handleFile(self.product, fp, self.extract)
               if e: self.logger.error('job {0}-{1} : attribute error {2}'.format(self.jobLabel, self.mission, e))

        self.logger.info('job {0}-{1} : Starting Download | attemtp {2}'.format(self.jobLabel, self.mission, self.attempt))
        cmd = self.dllArg + self.extraArgument
        self.logger.debug('job {0}-{1} : cmd\n{2}'.format(self.jobLabel, self.mission, cmd))
        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=False)
        handlePopenOutput(proc, handleLine, handleLine)
        errcode = proc.returncode           # reads return code from proc

        # errcode
        if errcode:
            self.logger.error('job {0}-{1} : Attempt failed, closing job | {2}'
                              .format(self.jobLabel, self.mission, errcode))
        else:
            self.logger.info('job {0}-{1} : Download completed successfully'
                             .format(self.jobLabel, self.mission))
