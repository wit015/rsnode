import re
import os
import sys
import wget
import datetime
from functions import *

def genFilePaths(product, startDate, endDate, window):

    # product not used

    # Whether date is a proper measurement interval date
    def filterDate(d):
        # Return true means do not filter
        return True

    #: Convert date to location path
    def formatDate(dts):
        d, ts = dts
        ds = d.strftime("%Y%m%d")
        if product <> 'manifest':
           return ds + ts + "/" + "z_cams_c_ecmf_" + ds + ts + "0000_" + product + '*'
        else:
           return ds + ts + "/" + "z_cams_c_ecmf_*.manifest"

    # If no start date is defined, assume empty file_path
    # Note this is different from an empty file_path_list
    if startDate is None:
        return ([''], [''])

    td = datetime.date.today()

    sd = datetime.datetime.strptime(startDate, '%Y-%m-%d').date()
    if endDate <> '':
        ed = datetime.datetime.strptime(endDate, '%Y-%m-%d').date()
    else:
        ed = td
     
    if ed > td : ed = td

    dr = [ (sd + datetime.timedelta(days=x), y) for x in range((ed-sd).days+1) for y in [ "00", "12"] ]

    # Generate in-window file paths
    iwr = dr if window is None else dr[-window:]
    iwr = filter(filterDate, iwr)
    iwr = map(formatDate, iwr)

    # Generate out-window file paths
    owr = [] if window is None else dr[0:-window]
    owr = filter(filterDate, owr)
    owr = map(formatDate, owr)

    return (owr, iwr)

def handleCAMSDownloadedFile(product, fp, extract):
    fd = os.path.dirname(fp)
    return handleDownloadedFile(product, fp, fd, extract)

class CAMS_GLOBAL(wget.Wget):

    def __init__(self, jobDescr, jobLabel, base_destination, url, user, password, base_file_path):
       wget.Wget.__init__(self, jobDescr, jobLabel, base_destination, "CAMS_GLOBAL", url, user, password, base_file_path, 
                          genFilePaths, handleCAMSDownloadedFile, handleCleanUp)
