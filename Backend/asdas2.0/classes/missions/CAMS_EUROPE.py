import re
import os
import sys
import wget
import datetime
from functions import *

def genFilePaths(product, startDate, endDate, window):

    basename = 'CAMS50?&token=__M0bChV6QsoOFqHz31VRqnpr4GhWPtcpaRy3oeZjBNSg__&grid=0.1&licence=yes'

    # product format : <model>/<package>/<time>/<format>
    m = re.match('(.*)/(.*)/(.*)/(.*)', product)
    if m.lastindex <> 4: return ([''], [''])

    model, package, time, fileformat = m.groups()
    restname = '&model=%s&package=%s&time=%s&format=%s' % (model, package, time, fileformat)

    def filterDate(d):
        # Return true means do not filter
        return True

    #: Convert date to location path
    def formatDate(d):
        ds = d.strftime("&referencetime=%Y-%m-%dT00:00:00Z")
        rn = basename + restname + ds
        fn = '%s/%s/%s/%s.%s' % (package, model, d.strftime("%Y-%m-%d"), time, fileformat)
        return (rn, fn)

    # If no start date is defined, assume empty file_path
    # Note this is different from an empty file_path_list
    if startDate is None:
        return ([''], [''])

    td = datetime.date.today()

    sd = datetime.datetime.strptime(startDate, '%Y-%m-%d').date()
    if endDate <> '':
        ed = datetime.datetime.strptime(endDate, '%Y-%m-%d').date()
    else:
        ed = td
     
    if ed > td : ed = td

    dr = [ sd + datetime.timedelta(days=x) for x in range((ed-sd).days+1) ]

    # Generate in-window file paths
    iwr = dr if window is None else dr[-window:]
    iwr = filter(filterDate, iwr)
    iwr = map(formatDate, iwr)

    # Generate out-window file paths
    owr = [] if window is None else dr[0:-window]
    owr = filter(filterDate, owr)
    owr = map(formatDate, owr)

    return (owr, iwr)

def handleCamsDownloadedFile(product, fp, extract):
    fd = os.path.dirname(fp)
    return handleDownloadedFile(product, fp, fd, extract)

class CAMS_EUROPE(wget.Wget):

    def __init__(self, jobDescr, jobLabel, base_destination, url, user, password, base_file_path):
       wget.Wget.__init__(self, jobDescr, jobLabel, base_destination, "CAMS_EUROPE", url, user, password, base_file_path, 
                          genFilePaths, handleCamsDownloadedFile, handleCleanUp)
