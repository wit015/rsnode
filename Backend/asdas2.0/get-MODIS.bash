#!/bin/bash
cd /opt/rsnode/Backend/asdas2.0
export PYTHONPATH=/opt/rsnode/Backend/asdas2.0/classes:/opt/rsnode/Backend/asdas2.0/classes/missions:/opt/rsnode/Backend/asdas2.0/classes/settings:$PYTHONPATH
python main.py --setloc /opt/rsnode/Backend/asdas2.0/classes/settings --jobfile /opt/rsnode/Backend/asdas2.0/xml/modis500.xml --logdir /data/RSNODEDATA//modis500-work/ingest/logs
