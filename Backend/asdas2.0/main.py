# This is the main class that runs the system. This class parses the job file for the first time and creates
# separate job descriptions. Next the max amounts of threads are created and the job descriptions from the
# queue are then run one by one creating jobs.

# Datetime is not threadsafe. Forcing import here before thread are kicked off.
import datetime
datetime.datetime.strptime('1970-01-01 12:00:00', "%Y-%m-%d %H:%M:%S")
import sys
from os import path     # path.join()
from sys import argv
from argparse import ArgumentParser, SUPPRESS
import imp              # imp.load_source
import logging
import logger
from Queue import Queue, Empty
import subprocess
import threading
# from classes.settings.config import *
import time
from lxml import etree
import traceback

parser = ArgumentParser()               # create arg parse
parser.add_argument('--setloc', help='add location of personal settings folder')  # create setloc option
parser.add_argument('--jobfile', help='job file')
parser.add_argument('--logdir', help='log directory')
args = parser.parse_args()              # parse arguments
settingsSource = args.setloc            # set setloc argyment value
jobFile        = args.jobfile
logDir         = args.logdir

# script, settingsSource = argv
configSource = path.join(settingsSource, 'config.py')       # create config location
missionDBSource = path.join(settingsSource, 'missionDB.py') # create missionDB location
con = imp.load_source('conf', configSource)                 # import configuration file
mDB = imp.load_source('mDB', missionDBSource)               # import missionDB file
if jobFile is not None:
   con.JobFileLocation = jobFile
if logDir is not None:
   print "LOGDIR %s" % logDir
   con.LogFileLocation = logDir

logger.setLogLocation(con.LogFileLocation)
logger.init()

log = logging.getLogger(__name__)      # set logger name

class jobExecutor:
    def __init__(self):
        pass

    def jobWorker(self, q):
        jobsDone = False  # initialise jobsDone as False for while loop
        threadNum = threading.current_thread().getName()  # get thread name
        log.debug('starting worker in {0}'.format(threadNum))

        while not jobsDone:  # while jobsDone is False, queue not empty
            try:  # to to catch empty queue
                jobDescr = q.get(0)  # taking job description from queue
                mission = jobDescr.get('mission') # get mission from job description
                jobLabel = jobDescr.get('label')  # get job number from job description

                url             = getattr(mDB, mission + '_URL')
                user            = getattr(mDB, mission + '_user')
                password        = getattr(mDB, mission + '_pass')
                base_file_path  = getattr(mDB, mission + '_BASE_FILE_PATH')
                cmd_options     = getattr(mDB, mission + '_cmd_options', [])

                base_destination = getattr(con, 'BaseDestination')

                exec("import %s" % mission)

                if len(cmd_options)>0:
                    jobObjectStr = "%s.%s(jobDescr, jobLabel, base_destination, url, user, password, base_file_path, cmd_options)"
                else:
                    jobObjectStr = "%s.%s(jobDescr, jobLabel, base_destination, url, user, password, base_file_path)"

                jobObject = eval(jobObjectStr % (mission, mission))

                log.info('job {0}-{1} : starting in {2} '  # log start of new job in thread
                            .format(jobLabel, mission, threadNum))

                jobObject.jobHandler()  # start new job

                log.info('job {0}-{1} : Finished Successfully in {2} '  # log thead exited properly
                            .format(jobLabel, mission, threadNum))
                q.task_done()  # mark task done in queue

            except UnboundLocalError:  # catch exception with wrong mission
                sys.stderr.write('The mission "{0}" in job description {1} has not been recognized'
                                 .format(mission, jobLabel))
                log.error('The mission "{0}" in job description {1} has not been recognized'
                             .format(mission, jobLabel))
                q.task_done()  # needed to empty the queue
                # remove job from queue
            except SystemExit as e:  # Catch sys.exit()
                time.sleep(0.1)  # sleep needed or task_done will fail in certain cases
                q.task_done()
                log.error('job {0}-{1} : Failed with message: {2} '  # log thread exited properly
                             .format(jobLabel, mission, e))
            except Empty:  # catch when queue is empty
                jobsDone = True  # set jobsDone to True for while loop
                log.debug('Queue is empty')

            except:
                print "Unexpected error:", traceback.print_exc()
                q.task_done()

        log.info('has exited properly')

    # -----------------------------------------------------------------
    def createDownloadJob(self):
        self.checkDiskSpace()


        parser = etree.XMLParser(remove_blank_text=True)

        try:  # try if file can be accessed (or exists)
            if not con.JobFileLocation:  # if empty
                raise ValueError
            else:
                tree = etree.parse(con.JobFileLocation, parser)  # get job file location from config
                self.validateXML(_tree=tree)
        except IOError as e:
            log.error('failed to open file: %s' % (str(e)))
            sys.exit('failed to open file: %s' % (str(e)))  # shut down system with warning
        except ValueError:
            log.error('Job File Location is missing, please check the configuration file')
            sys.exit('ERROR: Job File Location is missing, please check the configuration file')
        else:
            root = tree.getroot()  # get root from job file xml

        q = Queue()  # Initialise queue
        for jobDescr in root.findall('job'):  # for each job description
            q.put(jobDescr)  # put all jobs in queue
        mt = con.MaxThreads  # get MaxThreads from Config
        if mt > q.qsize():  # see if queue size is smaller than maxThreads
            mt = q.qsize()  # set maxThreads to queue size
            log.info('maxThreads changed from {0} to {1} due to queue size'.format(con.MaxThreads,mt))
        for i in range(mt):  # create 'MaxThreads' threads
            log.debug('Creating Thread-{0}'.format(i + 1))
            t = threading.Thread(target=self.jobWorker, args=(q,))  # Create thread
            t.start()  # Start Thread

        q.join()  # waits for all the threads to complete
        separatorLineLength = 120
        log.info('End Of Job Run\n' + '_' * separatorLineLength) # separator string for in the log file

    # -----------------------------------------------------------------
    def validateXML(self, _tree):
        xmlValidity = False
        dtd = etree.DTD(open(con.DTDFileLocation, 'rb'))
        if dtd.validate(_tree):
            xmlValidity = True
        else:
            log.error('Jobfile has NOT been validated. Please check file for mistakes\n{0}'
                         .format(dtd.error_log.filter_from_errors()))
            sys.exit('Jobfile has NOT been validated. Please check file for mistakes\n{0}'
                     .format(dtd.error_log.filter_from_errors()))
        return xmlValidity

    # -----------------------------------------------------------------
    def checkDiskSpace(self):
	return
        df = subprocess.Popen(["df", con.BaseDestination, '-h'], stdout=subprocess.PIPE)
        output = df.communicate()[0]
        device = output.split("\n")[1].split()
        size, used, available, percent, mountpoint = output.split("\n")[2].split()

        intPercent = int(percent.rstrip('%'))

        if intPercent > con.warningPercentage:
            log.warning('Disk space is running low!\n'
                           'Disk is at {0} of its capacity\n'
                           'There is {1} Available of the {2}'.format(percent, available, size))
        elif intPercent > con.maxPercentage:
            log.error('Disk space is full! The system will exit. \n'
                         'Disk is at {0} of its capacity\n'
                         'There is {1} Available of the {2}'.format(percent, available, size))
            sys.exit('Disk space is full!\n'
                         'Disk is at {0} of its capacity\n'
                         'There is {1} Available of the {2}'.format(percent, available, size))
        else:
            log.info('Sufficient disk space available on {0} : {1} used of {2}'.format(mountpoint, percent, size))

# -----------------------------------------------------------------

jobEx = jobExecutor()

jobEx.createDownloadJob()
