#!/bin/bash
ALIVE=`basename $0`-$$.txt
keepalive set ${ALIVE}
cd /home/adsnl2/asdas1.0
export PYTHONPATH=/home/adsnl2/asdas1.0/classes:/home/adsnl2/asdas1.0/classes/missions:/home/adsnl2/asdas1.0/classes/settings:$PYTHONPATH
python main.py --setloc ~/asdas1.0/classes/settings --jobfile ~/asdas1.0/xml/TEMIS.xml --logdir /net/satarch/OMI/TEMIS/log
keepalive rm ${ALIVE}
