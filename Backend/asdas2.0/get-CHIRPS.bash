#!/bin/bash
cd /opt/rsnode/Backend/asdas2.0
export PYTHONPATH=/opt/rsnode/Backend/asdas2.0/classes:/opt/rsnode/Backend/asdas2.0/classes/missions:/opt/rsnode/Backend/asdas2.0/classes/settings:$PYTHONPATH
python main.py --setloc /opt/rsnode/Backend/asdas2.0/classes/settings --jobfile /opt/rsnode/Backend/asdas2.0/xml/chirps.xml --logdir /data/RSNODEDATA//chirps-work/ingest2/logs
