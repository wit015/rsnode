#!/bin/bash
################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2018, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################


BASE_DIR='/mnt/sentinel2/tiles/37'
REF_FILE='/net/satarch/DataServer/cs/S2_ETH_NDVI_REF.tif'
TMP_DIR="/net/satarch/DataServer/cs/tmp"

GRAN_FILES="\
$BASE_DIR/P/BR/S2B_MSIL1C_20171226T080329_N0206_R035_T37PBR_20171226T101254.SAFE/GRANULE/L1C_T37PBR_A004208_20171226T080925/IMG_DATA \
$BASE_DIR/P/BR/S2B_MSIL1C_20171223T075319_N0206_R135_T37PBR_20171223T100946.SAFE/GRANULE/L1C_T37PBR_A004165_20171223T075743/IMG_DATA \
$BASE_DIR/P/BQ/S2B_MSIL1C_20171226T080329_N0206_R035_T37PBQ_20171226T101254.SAFE/GRANULE/L1C_T37PBQ_A004208_20171226T080925/IMG_DATA \
$BASE_DIR/P/BQ/S2B_MSIL1C_20171223T075319_N0206_R135_T37PBQ_20171223T100946.SAFE/GRANULE/L1C_T37PBQ_A004165_20171223T075743/IMG_DATA \
$BASE_DIR/P/EK/S2B_MSIL1C_20171230T074319_N0206_R092_T37PEK_20171230T095744.SAFE/GRANULE/L1C_T37PEK_A004265_20171230T075429/IMG_DATA \
$BASE_DIR/P/DK/S2B_MSIL1C_20171230T074319_N0206_R092_T37PDK_20171230T095744.SAFE/GRANULE/L1C_T37PDK_A004265_20171230T075429/IMG_DATA \
$BASE_DIR/N/DJ/S2B_MSIL1C_20171230T074319_N0206_R092_T37NDJ_20171230T095744.SAFE/GRANULE/L1C_T37NDJ_A004265_20171230T075429/IMG_DATA \
$BASE_DIR/N/EJ/S2B_MSIL1C_20171230T074319_N0206_R092_T37NEJ_20171230T095744.SAFE/GRANULE/L1C_T37NEJ_A004265_20171230T075429/IMG_DATA"

TMP_NDVI_FILES=()
counter=0
for pd in $GRAN_FILES;
do

   B04_FILE=`ls $pd/*_B04.jp2`
   B08_FILE=`ls $pd/*_B08.jp2`
   cp $B04_FILE $TMP_DIR/b04-tmp.jp2
   cp $B08_FILE	$TMP_DIR/b08-tmp.jp2
   O_FILE="$TMP_DIR/ndvi_tmp_$counter.tif"
   echo "NDVI for [$pd] -> [$O_FILE] ... " 
   gdal_calc.py -A $TMP_DIR/b04-tmp.jp2 -B $TMP_DIR/b08-tmp.jp2 --outfile $O_FILE --overwrite --type=Float32 --NoDataValue=0 --calc="((B*1.0)-(A*1.0))/((B*1.0)+(A*1.0))"
   counter=$((counter+1))
   TMP_NDVI_FILES+=($O_FILE)
done

rm $TMP_DIR/b08-tmp.jp2 $TMP_DIR/b04-tmp.jp2
echo "Processing ${TMP_NDVI_FILES[*]} ..."

gdalbuildvrt $TMP_DIR/NDVI_REF_000.virt ${TMP_NDVI_FILES[*]}
gdal_translate -of GTiff $TMP_DIR/NDVI_REF_000.virt $TMP_DIR/NDVI_REF_000.tif



#rm $REF_FILE
#echo 'Adding granules to REF NDVI image...'
#gdalwarp -overwrite -srcnodata 0 -dstalpha ${TMP_NDVI_FILES[*]} $REF_FILE
#rm ${TMP_NDVI_FILES[*]}
