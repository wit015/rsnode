################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

SVN_REVISION = '$Rev: 7715 $'
SVN_DATE = '$Date: 2016-12-16 11:40:50 +0100 (Fri, 16 Dec 2016) $'
SVN_AUTHOR = '$Author: jb76278 $'

# Shortened and readable SVN description
import re
SVN_DESCR = (SVN_REVISION + re.sub('\) \$', ' ' ,re.sub('^.*\(', 'Date: ', SVN_DATE)) + SVN_AUTHOR).replace('$', '')

import numpy as np
import sys
import os
import resource
import datetime
from time import gmtime, strftime

# The following import(s) need correct setting of the PYTHONPATH environment variable. For example in .bashrc:
# export PYTHONPATH="${PYTHONPATH}:`pwd`/SvnCheckout/CommonSense/RSDS/DataProcessing/Python/code:`pwd`/SvnCheckout/CommonSense/RSDS/DataProcessing/Python/pyLibs"
from netCdfGeoMetaData import NetCdfGeoMetaData

from netCDF4 import Dataset

SCRIPT_FILE_NAME = os.path.basename(__file__)
NOW_STRING = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

######################################################################################################
# Method will set the tiles-of-interest values to include all available tiles.
######################################################################################################
def checkTilesOfInterest(aInputDirName, aToiSet, aUpperIndex, aLeftIndex, aLowerIndex, aRightIndex):
    y = aUpperIndex + 1
    x = aLeftIndex + 1

    # Look for the tile files
    nc4FileName = "cube_" + str(y).zfill(3) + "_" + str(aLeftIndex).zfill(3) + ".hdf5"
    while os.path.isfile(os.path.join(aInputDirName, nc4FileName)) and (y <= aLowerIndex or not aToiSet):
        y += 1
        nc4FileName = "cube_" + str(y).zfill(3) + "_" + str(aLeftIndex).zfill(3) + ".hdf5"
    aLowerIndex = y - 1

    nc4FileName = "cube_" + str(aUpperIndex).zfill(3) + "_" + str(x).zfill(3) + ".hdf5"
    while os.path.isfile(os.path.join(aInputDirName, nc4FileName)) and (x <= aRightIndex or not aToiSet):
        x += 1
        nc4FileName = "cube_" + str(aUpperIndex).zfill(3) + "_" + str(x).zfill(3) + ".hdf5"
    aRightIndex = x - 1

    aToiSet = True
    return aToiSet, aUpperIndex, aLeftIndex, aLowerIndex, aRightIndex

######################################################################################################
# Method will check the existance of the output directory and try to create it if needed.
######################################################################################################
def makeSureOutputDirectoryExists(aOutputDirName):
    # Make sure the output directory exists (if not, try to create it)
    if not os.path.exists(aOutputDirName):
        try:
            os.makedirs(aOutputDirName)
        except OSError as exception:
            sys.exit("Error: Output directory " + aOutputDirName + " did not exist and could not be created.")

######################################################################################################
# Method create a test-data cube with the same size as the input cube, applying the following
# algorithm:
# (DayEpoch2000(date) + round(Lattitude * 1000) + round(Longitude * 1000)) mod 250
#
# DayEpoch2000(date) : days since Jan 1st 2000 for date, with Jan 1st 2000 being day 1.
#round(x)                    : round x to nearest integer
######################################################################################################
def createTestDataCube(aData, aDateArray, aMetaData, aShape):
    referenceDate = datetime.date(2000, 1, 1)
    startDateDelta = (aMetaData.getDate() - referenceDate).days

    # 1D Array for the time axis
    dateArray = np.add(aDateArray, startDateDelta)

    # 2D Array for time and latitude axes
    latBase, latDelta = aMetaData.getLatitudeTransform()
    lonBase, lonDelta = aMetaData.getLongitudeTransform()

    b = np.empty(aShape)
 
    latArray = np.array(range(aShape[0])).reshape(-1,1)
    latarray = 10000 * (latBase + latArray * latDelta)
    latArray = np.vectorize(np.round)(latarray)

    lonArray = np.array(range(aShape[1]))
    lonArray = 10000 * (lonBase + lonArray * lonDelta)
    lonArray = np.vectorize(np.round)(lonArray)
 
    b[:,:] = latArray

    b[:,:] = b + lonArray

    for di in range(len(dateArray)):

       print di, dateArray[di]

       c = b + dateArray[di]

       aData[di] = c % 2500

    return

######################################################################################################
# Method will replace the cube data with test data
######################################################################################################
def createTestCubes(aInputDirName, aOutputDirName, aToiSet = False, aUpperIndex = 0, aLeftIndex = 0, aLowerIndex = 0, aRightIndex = 0):
    global SVN_DESCR, NOW_STRING, SCRIPT_FILE_NAME

    # String for self documenting script
    historyString = NOW_STRING + " " + SCRIPT_FILE_NAME + " " + SVN_DESCR + " Arguments"
    if aToiSet: historyString += " --toi=" + str(aUpperIndex) + "," + str(aLeftIndex) + "," + str(aLowerIndex) + "," + str(aRightIndex)
    historyString += " " + str(aOutputDirName)
    historyString += " " + str(aInputDirName)

    # Loop through all the tiles (as specified in the input arguments)
    for x in range(aUpperIndex, aLowerIndex + 1):
        for y in range(aLeftIndex, aRightIndex + 1):
            # Object for the meta data of the input (will be similar to that of the output)
            metaData = NetCdfGeoMetaData()

            # Create filename for both the input and output cube 3D-array
            nc4InAndOutputFileName = "cube_" + str(x).zfill(3) + "_" + str(y).zfill(3) + ".hdf5"

            # Get the input data
            print aInputDirName
            nc4file = Dataset(os.path.join(aInputDirName, nc4InAndOutputFileName), 'r')
            key, value = nc4file.variables.items()[3]
            dataArray  = nc4file.variables[key]
            s = dataArray.shape
            print s

            # Copy the meta data and alter the needed fields for the output later.
            metaData.copyFromNetCdfObject(nc4file)
            metaData.setTitle("Test Data " + metaData.getTitle())
            print "NODATA = ", metaData.getNoDataValue()
            metaData.addToHistory(historyString)
            metaData.setSource("Input dir: " + aInputDirName + " files: " + nc4InAndOutputFileName)

            # Make a copy of the time variable and its attributes
            timeArray = nc4file.variables['time'][:]
            timeVarAttr = {}
            for key in nc4file.variables['time'].ncattrs():
                timeVarAttr[key] = nc4file.variables['time'].getncattr(key)
            nc4file.close()

            # Create output file
            print "Creating: " + str(nc4InAndOutputFileName)
            ncfileOut = Dataset(os.path.join(aOutputDirName, nc4InAndOutputFileName), 'w', format='NETCDF4')
            metaData.copyToNetCdfObject(ncfileOut)
            ncfileOut.createDimension(timeVarAttr['standard_name'], None)
            variable = ncfileOut.createVariable(timeVarAttr['standard_name'], np.dtype('int32').char, (timeVarAttr['standard_name']))
            for key, value in timeVarAttr.iteritems():
                variable.setncattr(key, value)
            variable[:] = timeArray
            metaData.createDimVarsLatLong(ncfileOut, s[1], s[2])

            # Create variable for the test data
            data = ncfileOut.createVariable(metaData.getTitle(),np.dtype('float32').char, ('time','latitude','longitude'))
            data.setncattr('standard_name',metaData.getTitle())
            if metaData.getInstitution() == 'CHIRPS':
                data.setncattr('units','mm')
            elif metaData.getInstitution() == 'MODIS':
                data.setncattr('units','VI')
            else:
                data.setncattr('units','<unknown>')
            data.setncattr('long_name',metaData.getTitle())

            # Create the test data-cube
            testDataCube = createTestDataCube(data, timeArray, metaData, (s[1], s[2]))

            ncfileOut.close()

    return

######################################################################################################
# Check and process the input parameters
######################################################################################################
def main(argv):
    if len(sys.argv) < 3 or len(sys.argv) > 4:
        print ""
        print "Usage:"
        print "python createTestCubes.py [flags] <output-dir-name> <input-dir-name>"
        print ""
        print "The input files must be netCDF cube type files!"
        print "Allowed flag examples:"
        print "--toi=0,0,10,2  The tiles of interest is defined with four comma separated pixel-index values upper, left, lower, right. All tiles are processed by default."
        print "--logfile=/tmp/createTestCubes20160314.log  The name (and directory) for the log file."
        print "Example:"
        print "python createTestCubes.py --toi=10,10,12,14 --logfile=/tmp/createTestCubes20160314.log /net/satarch/MODIS/jelle/NdviTestTimeSeries500m /net/satarch/MODIS/jelle/NdviTimeSeries500m/"
        print "python createTestCubes.py /net/satarch/MODIS/jelle/NdviTestTimeSeries500m /net/satarch/MODIS/jelle/NdviTimeSeries500m/"
        print ""
    else:
        flags_dict = {}
        outputDirName = ''
        inputDirName = ''
        toiSet = False
        upperIndex = 0
        leftIndex = 0
        lowerIndex = 0
        rightIndex = 0
        logFileName = None

        # Extract the flags
        argIndex = 1
        while outputDirName == '' and argIndex < len(sys.argv):
            # Check for flags
            if sys.argv[argIndex].count('=') == 1 and sys.argv[argIndex].startswith('--'):
                key, value = sys.argv[argIndex].split('=')
                flags_dict[key] = value
            else:
                outputDirName = sys.argv[argIndex]
            argIndex += 1

        makeSureOutputDirectoryExists(outputDirName)

        if argIndex == (len(sys.argv) - 1):
            inputDirName = sys.argv[argIndex]
        else:
            sys.exit("Error: Number of arguments is wrong.")

        # Check the flags and extract their values
        for key, value in flags_dict.items():
            if key == '--toi':
                if value.count(',') == 3:
                    one,two,three,four = value.split(',')
                    upperIndex = max(min(int(one),int(three)), 0)
                    leftIndex = max(min(int(two),int(four)), 0)
                    lowerIndex = max(int(one),int(three))
                    rightIndex = max(int(two),int(four))
                    toiSet = True
                else:
                    sys.exit("Error: Format of Value \'" + value + "\' for Key \'" + key + "\' is wrong.")
            elif key == '--logfile':
                logFileName = value
            else:
                print "Warning: Key \'" + key + "\' is unknown and ignored."

        toiSet, upperIndex, leftIndex, lowerIndex, rightIndex = checkTilesOfInterest(inputDirName, toiSet, upperIndex, leftIndex, lowerIndex, rightIndex)

        createTestCubes(inputDirName, outputDirName, toiSet, upperIndex, leftIndex, lowerIndex, rightIndex)

######################################################################################################
# Only execute when run as a script
######################################################################################################
if __name__ == "__main__":
    main(sys.argv[:])

