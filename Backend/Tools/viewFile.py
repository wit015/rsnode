################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

import re
SVN_REVISION = '$Rev: 7715 $'
SVN_DATE = '$Date: 2016-12-16 11:40:50 +0100 (Fri, 16 Dec 2016) $'
SVN_AUTHOR = '$Author: jb76278 $'

import numpy as np
import sys
import os
import re
import resource
from PIL import Image

# The following import(s) need correct setting of the PYTHONPATH environment variable. For example in .bashrc:
# export PYTHONPATH="${PYTHONPATH}:`pwd`/SvnCheckout/CommonSense/RSDS/DataProcessing/Python/code:`pwd`/SvnCheckout/CommonSense/RSDS/DataProcessing/Python/pyLibs"
import arrayToPng as arr2png
import tiles
from netCdfGeoMetaData import NetCdfGeoMetaData

from datetime import time, datetime
from netCDF4 import Dataset
from osgeo import gdal

SCRIPT_FILE_NAME = os.path.basename(__file__)
NOW_STRING = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

######################################################################################################
# Method will check the existance of the output directory
######################################################################################################
def makeSureInputFileExists(aInputFileName):

    # Make sure the output directory exists
    if not os.path.isfile(aInputFileName):
        sys.exit("Error: Input file " + aInputFileName + " does not exist.")

######################################################################################################
# 
######################################################################################################
def viewFile(aOutputFileName, aInputFileName, aRangeSet, aLowerRange, aUpperRange, aTimeIndex, aStatIndex):

    # Object for the meta data of the input (will be similar to that of the output)
    metaData = NetCdfGeoMetaData()

    now = datetime.time(datetime.now())
    dataArray = []
    print "Now: " + str(now)
    if aInputFileName.endswith('.dat') or aInputFileName.endswith('.tif'):
        enviDataSet = gdal.Open(aInputFileName)
        aTimeIndex = 1
        # TODO: Check for an timeIndex that does not return a raster-band
        dataArray = np.array(enviDataSet.GetRasterBand(aTimeIndex).ReadAsArray())

    elif aInputFileName.endswith('.nc') or aInputFileName.endswith('.hdf5'):
        nc4file = Dataset(aInputFileName, 'r')

        # Get the key (name) of the first variable stored
        key = ''
        value = ''
        if os.path.basename(aInputFileName).startswith('tile_'):
            key, value = nc4file.variables.items()[2]
            dataArray = nc4file.variables[key][:]
        elif os.path.basename(aInputFileName).startswith('cube_'):
            key, value = nc4file.variables.items()[3]
            dataArray = nc4file.variables[key][aTimeIndex,:,:]
#            dataArray[dataArray <= 0] = -40
#            dataArray[dataArray > 0] = 40
        elif os.path.basename(aInputFileName).startswith('multi_cube_'):
            key, value = nc4file.variables.items()[3 + aStatIndex]
            dataArray = nc4file.variables[key][aTimeIndex,:,:]
        else:
            sys.exit("Error: Input file name " + aInputFileName + " does not start with tile_ cube_ or multi_cube .")

        # Copy the meta data and alter the needed fields for the output later.
        metaData.copyFromNetCdfObject(nc4file)
        noDataValue = float(metaData.getNoDataValue())
        if not dataArray.dtype == np.int16:
            dataArray[dataArray == noDataValue] = np.nan
        dataArray[dataArray <= 0 ] = 0
#        dataArray = np.power(dataArray,0.75) # python viewFile.py --range=0,1400 --time=152 /net/satarch/CommonSense/DataCubes/Released/MODIS/cube_EthiopiaNdvi500m.hdf5 temp1.png
        nc4file.close()
    else:
        sys.exit("Error: Input file name " + aInputFileName + " does not have entension .dat .gif .hdf5 or .nc .")

    minVal = np.nanmin(dataArray)
    maxVal = np.nanmax(dataArray)
    print "Min: " + str(minVal) + "  Max: " + str(maxVal)
    if not aRangeSet:
        aLowerRange = minVal
        aUpperRange = maxVal
    arr2png.arrayToRgbPng(dataArray, aOutputFileName, aLowerRange, aUpperRange)
    now = datetime.time(datetime.now())
    print "Now: " + str(now)


######################################################################################################
# Check and process the input parameters
######################################################################################################
def Main(aArgValues):
    if len(aArgValues) < 3 or len(aArgValues) > 6:
        print ""
        print "Usage:"
        print "python viewFile.py [flags] <input-file-name> <output-file-name>"
        print ""
        print "The input files must be netCDF cube type files!"
        print "Allowed flag examples:"
        print "--range=-100,9000  The range in which the rainbow colors are fitted. Min and max are used by default."
        print "--time=531  The index for the time axis. Only useful for 3D cubes and multi-cubes. Zero is used by default."
        print "--stats=3  The index for the statistics indicator. 0 .. 4 for percentiles 0, 25, 50, 75 and 100. 5 for average."
        print "           Only useful for 3D multi-cubes. Five is used by default."
        print "Example:"
        print "python viewFile.py --range=1500,10000 /net/satarch/MODIS/jelle/NdviTileData500m/NDVI_20111008_500m.dat/tile_011_005.hdf5 temp1.png"
        print "python viewFile.py --range=1500,10000 --time=531 /net/satarch/MODIS/jelle/NdviSmoothTimeSeries500m/cube_012_005.hdf5 temp2.png"
        print "python viewFile.py --range=-1,27 --time=31 --stats=3 /net/satarch/CHIRPS/jelle/rainYearStats/multi_cube_001_000.hdf5 temp3.png"
        print ""
    else:
        flagsDict = {}
        outputFileName = ''
        inputFileName = ''
        rangeSet = False
        lowerRange = 0
        upperRange = 0
        timeIndex = 0
        statIndex = 5

        # Extract the flags
        argIndex = 1
        while inputFileName == '' and argIndex < len(aArgValues):
            # Check for flags
            if aArgValues[argIndex].count('=') == 1 and aArgValues[argIndex].startswith('--'):
                key, value = aArgValues[argIndex].split('=')
                flagsDict[key] = value
            else:
                inputFileName = aArgValues[argIndex]
            argIndex += 1

        makeSureInputFileExists(inputFileName)

        if argIndex == (len(aArgValues) - 1):
            outputFileName = aArgValues[argIndex]
        else:
            sys.exit("Error: Number of arguments is wrong.")

        # Check the flags and extract their values
        for key, value in flagsDict.items():
            if key == '--range':
                if value.count(',') == 1:
                    one,two = value.split(',')
                    lowerRange = min(int(one),int(two))
                    upperRange = max(int(two),int(one))
                    rangeSet = True
                else:
                 sys.exit("Error: Format of Value \'" + value + "\' for Key \'" + key + "\' is wrong.")
            elif key == '--time':
                timeIndex = int(value)
            elif key == '--stats':
                statIndex = int(value)
            else:
                print "Warning: Key \'" + key + "\' is unknown and ignored."

        # Call function that does actual work
        viewFile(outputFileName, inputFileName, rangeSet, lowerRange, upperRange, timeIndex, statIndex)

######################################################################################################
# Only execute when run as a script
######################################################################################################
if __name__ == "__main__":
    Main(sys.argv[:])

