#!/usr/bin/env python
from netCDF4 import Dataset
import argparse
import datetime
import numpy as np

parser = argparse.ArgumentParser(description='Concat two cubes')
parser.add_argument('dst_cube_fn', metavar='<output file>', type=str, help='Output NetCDF')
parser.add_argument('src_cube_fn', metavar='<input file>', type=str, help='Input NetCDF')
parser.add_argument('insert_date', metavar='<insert date>', type=str, help='Date string to start')

args = parser.parse_args()

dst_cube = Dataset(args.dst_cube_fn, 'a')
src_cube = Dataset(args.src_cube_fn, 'r')

rdate_str      = getattr(dst_cube, 'Recording_Date', None)
dst_cube_rdate = datetime.datetime.strptime(rdate_str, "%Y-%m-%d").date()
dst_cube_delta = dst_cube.variables['time'][:]
dst_cube_dates = np.vectorize(lambda x: dst_cube_rdate + datetime.timedelta(days=int(x)))(dst_cube_delta)

rdate_str      = getattr(src_cube, 'Recording_Date', None)
src_cube_rdate = datetime.datetime.strptime(rdate_str, "%Y-%m-%d").date()
src_cube_delta = src_cube.variables['time'][:]
src_cube_dates = np.vectorize(lambda x: src_cube_rdate + datetime.timedelta(days=int(x)))(src_cube_delta)

insert_date = datetime.datetime.strptime(args.insert_date, "%Y-%m-%d").date()

# Find index for insert date in destination cube
for dst_cube_idx in range(len(dst_cube_dates)):
    if dst_cube_dates[dst_cube_idx] >= insert_date: break

# Find index for insert date in source cube
for src_cube_idx in range(len(src_cube_dates)):
    if src_cube_dates[src_cube_idx] >= insert_date: break

key, value     = dst_cube.variables.items()[3]
dst_cube_value = dst_cube.variables[key]
key, value     = src_cube.variables.items()[3]
src_cube_value = src_cube.variables[key]

while src_cube_idx < len(src_cube_dates):

    if dst_cube_idx >= len(dst_cube_dates) or \
       src_cube_dates[src_cube_idx] == dst_cube_dates[dst_cube_idx]:

        None
        
    elif src_cube_dates[src_cube_idx] < dst_cube_dates[dst_cube_idx]:

        for j in range(len(dst_cube_dates), dst_cube_idx, -1):
            dst_cube.variables['time'][j] = dst_cube_delta[j-1]
            dst_cube.variables[key][j]    = dst_cube_value[j-1]

    elif src_cube_dates[src_cube_idx] > dst_cube_dates[dst_cube_idx]:

        src_cube_idx = src_cube_idx + 1
        continue

    print "Adding layer %d for date %s" % (dst_cube_idx, src_cube_dates[src_cube_idx])
    dst_cube.variables[key][dst_cube_idx]    = src_cube_value[src_cube_idx]
    dst_cube.variables['time'][dst_cube_idx] = (src_cube_dates[src_cube_idx] - dst_cube_rdate).days

    src_cube_idx = src_cube_idx + 1
    dst_cube_idx = dst_cube_idx + 1

src_cube.close()
dst_cube.close()
