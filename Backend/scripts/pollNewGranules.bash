#!/usr/bin/env bash
################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2018, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################


set -x 

pid=$$
JOBTMPL="/tmp/pollNewGranules"
JOBFILE="${JOBTMPL}.$pid.txt"
LOGFILE="${JOBTMPL}.log"

shopt -s nullglob
f=$JOBTMPL.*.txt
a=( $f )
n=${#a[@]}
if [[ "$n" -gt 0 ]]; then
    echo "`date` : too many processes already running" >> $LOGFILE
    exit
fi
echo $$ > $JOBFILE
echo "`date` : RUNNING $0" >> $LOGFILE
updateSCLs.py '' >> $LOGFILE 2>&1
updateDefinedFields.py  '[]' >> $LOGFILE 2>&1
rm $JOBFILE
