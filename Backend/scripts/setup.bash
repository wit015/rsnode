################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################
# $Rev: 7742 $
# $Date: 2016-12-22 08:19:29 +0100 (Thu, 22 Dec 2016) $
# $Author: jb76278 $
################################################################################

################################################################################
# Bash script that needs to be sourced to set the environment variables for the dataprocessing scripts
################################################################################
#export PATH=/usr/local/miniconda/bin:`readlink -e .`:$PATH
export PATH=`readlink -e .`:$PATH
export CS_PYTHON_SCRIPTS=`readlink -e ../pyLibs`
export CS_TOOLS=`readlink -e ../Tools`
export PATH=$CS_PYTHON_SCRIPTS:$CS_TOOLS"/bin":$PATH
export MISSIONS=`readlink -e ../ASDAS/missions`
export PYTHONPATH=$MISSIONS:$CS_PYTHON_SCRIPTS:$PYTHONPATH
export IMAGE_TEMPLATES=`readlink -e ../ImageTemplates`
export MASKS=`readlink -e ../Masks`
# $1 (optional) contains the project-id
export SIGNALFILE=ingest-busy$1.txt

export ASDASDIR=/opt/rsnode/Backend/asdas2.0
DATEFORMAT=+%Y%m%d-%H%M%S

HOST=`hostname -s`
case $HOST in
    dsl-sandbox-*)
        export INGESTDIR="/net/satarch/ASDAS_DATA_INGEST"
        export BASEDIR="/net/satarch/BASEDIR"
        export RELEASEDIR="/net/satarch/RELEASEDIR"
        export MASKDIR="/net/satarch/MASK"
        export SIGNALDIR="/net/satarch/signals"
        ;;
    backend-*)
        export INGESTDIR="/home/jane/INGESTDIR"
        export BASEDIR="/home/jane/BASEDIR"
        export RELEASEDIR="/home/jane/RELEASEDIR"
        export MASKDIR="/home/jane/MASKDIR"
        export SIGNALDIR="/home/jane/signals"
        ;;
    frontend-s2)
        export SENTINEL2_DB_PATH=/mnt/disks/background/SENTINEL2_DB
        export FIELD_DB_BACKEND_PATH=/mnt/disks/field-db/FIELD_DB
        export PYTHONPATH=/opt/CommonSense/Common/pyLibs:/opt/CommonSense/Backend/pyLibs:$PYTHONPATH
        export PATH=/opt/CommonSense/Backend/scripts:$PATH
        ;;
    RSnode)
        export INGESTDIR="/data/RSNODEDATA/INGESTDIR"
        export BASEDIR="/data/RSNODEDATA/BASEDIR"
        export RELEASEDIR="/data/RSNODEDATA/RELEASEDIR"
        #export MASKDIR="/data/jane/MASKDIR"
        export SIGNALDIR="/tmp/signals"
        export SENTINEL2_DB_PATH=/data/S2/SENTINEL2_DB
        export FIELD_DB_BACKEND_PATH=/data/S2/FIELD_DB
	export SEN2COR_LOGDIR=/data/S2/cs-sentinel2-scl/log
        export PYTHONPATH=/opt/rsnode/Common/pyLibs:/opt/rsnode/Backend/pyLibs:$PYTHONPATH
        export PATH=/opt/rsnode/Backend/scripts:$PATH
        ;;

    *)
        echo "WARNING: Unknown host: $HOST"
        #exit
        ;;
esac

export ASDAS_CONFIGDIR=$ASDASDIR/classes/settings
export PYTHONPATH=$PYTHONPATH:$INGESTDIR


#####################################
## added by nc77376

GRANULE_ARRAY="37/P/DK 37/P/BR 37/P/BQ 37/N/DJ 37/P/EK 37/N/EJ 37/P/FK 37/N/FJ"

export TMP_DIR=/data/S2/WMS/S2/tmp
#export S2_DATA_DIR=/mnt/sentinel2/tiles

# the reference image for the TCI layer
export REF_TCI_IMAGE=/data/S2/WMS/S2/S2_ETH_TCI_REF.tif

# the last image for the LATEST TCI layer
export LAST_TCI_IMAGE=/data/S2/WMS/S2/S2_ETH_TCI_LAST.tif

# the reference image for the NDVI layer
export REF_NDVI_IMAGE=/data/S2/WMS/S2/S2_ETH_NDVI_REF.tif

# the last image for the LATEST NDVI layer
export LAST_NDVI_IMAGE=/data/S2/WMS/S2/S2_ETH_NDVI_LAST.tif


## the ssh url for the TCI reference image
#export FE_TCI_IMG_SURL=adsnl4@omi-data-server:/tmp
#export FE_NDVI_IMG_SURL=adsnl4@omi-data-server:/tmp

