#!/usr/bin/env python
#
################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2018, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

import processS2
import datetime
import sys

#   "37PEK",
#   "37NEJ",
#   "37NDJ",
#   "36PZV",
#   "37PDK",
#   "37PFK",
#   "37NFJ",
#   "37PBQ",
#   "37PBR",
#   "36PZA",
#   "37PCK",

granule_id_list = None
granules   = sys.argv[1]
if len(granules) > 0: granule_id_list = eval(granules)

datetime  = datetime.datetime(2018,1,1,0,0,0)
if len(sys.argv)>2:
    datetime = datetime.strptime(sys.argv[2], "%Y-%m-%d")

processS2.updateSCLs(granule_id_list, datetime)
