#!/usr/bin/env bash
#
# Script to generate scene classification (cloud mask) for S2 image.
#
# usage: <granule-dir> <filename> [<logdir>]
#
################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2018, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

set -x
export S2CHOME=/opt/Sen2Cor
export TARGET_FILE=${2}
export SEN2COR_LOGDIR=${SEN2COR_LOGDIR:-`dirname $TARGET_FILE`/log}
if [[ -n "$3" ]]; then
    SEN2COR_LOGDIR=$3
fi

date

export TMP=/tmp/`mktemp -u S2L2.XXXXXXXX`
mkdir -p $TMP

export SEN2COR_CFGFN=${TMP}/L2A_GIPP.xml
cfg_tmplate=$S2CHOME/cfg/L2A_GIPP.xml
sed 's@<Target_Directory>.*</Target_Directory>@<Target_Directory>'${TMP}'</Target_Directory>@' < $cfg_tmplate > $SEN2COR_CFGFN

export GRANULE_DIR=$1

date
if [[ ! -e "$GRANULE_DIR" ]]; then
   echo "ERROR : granule $GRANULE_DIR does not exist"
   rm -rf $TMP
   exit -1
fi

# Prep dir contents for L2A_Process
# Sometimes granules have no AUX_DATA
(cd $GRANULE_DIR/GRANULE/*L1C_*; if [[ ! -e AUX_DATA ]]; then mkdir AUX_DATA; fi)
(cd $GRANULE_DIR; ln -s GRANULE/*L1C_*/AUX_DATA .)
(cd $GRANULE_DIR; mkdir HTML)

# Generate scene classification
# Sometimes the L2A_Process simply hangs ... have a max of 20 minutes (typ is 10)
timeout 20m $S2CHOME/bin/L2A_Process --resolution 20 --sc_only $GRANULE_DIR

# Resample to 10m
l2bn=`basename $GRANULE_DIR`
l2bn=${l2bn/1C/2A}

l2pn_20m=$TMP/$l2bn/GRANULE/L2A*/IMG_DATA/R20m/*SCL_20m.jp2
if [[ "$oldformat" ]]; then
    l2pn_20m=$TMP/${l2bn/OPER/USER}/GRANULE/S2A*/IMG_DATA/*SCL*_20m.jp2
fi

if [[ ! -e `ls $l2pn_20m` ]]; then
   echo "ERROR : file $l2pn_20m does not exist"
   exit -1
fi

gdalwarp -overwrite -tr 10.0 10.0 -co compress=LZW -of GTIFF $l2pn_20m $TARGET_FILE

# Cleanup 
rm -rf $TMP

# Next argument
shift

date
