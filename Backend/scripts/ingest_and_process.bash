#!/bin/bash
################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2018, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

PROJECTID=$1
# Define environment variables from setup.bash
source setup.bash

#
# define constants. $1(optional) contains project-id.
#
MACHINE=`uname -n`
BUSY_FILE=/tmp/ingest_and_process-busy.txt
DONE_FILE=$SIGNALDIR/ingest_and_process-done.txt
NEW_MODIS500_FILE=$SIGNALDIR/new-modis500.txt
NEW_CHIRPS_FILE=$SIGNALDIR/new-chirps.txt

MODIS500_RELEASE_DIR=$RELEASEDIR/modis500
CHIRPS_RELEASE_DIR=$RELEASEDIR/chirps

MODIS500_OUTPUT_FILES=$MODIS500_RELEASE_DIR/{base,lta}*.hdf5
CHIRPS_OUTPUT_FILES=$CHIRPS_RELEASE_DIR/{base,lta}*.hdf5

MODIS500_TIMESTAMP_FILE=$MODIS500_RELEASE_DIR/timestamp.txt
CHIRPS_TIMESTAMP_FILE=$CHIRPS_RELEASE_DIR/timestamp.txt

mkdir -p $SIGNALDIR
#
# functions
#
notify()
{
        d=`date +%Y%m%d-%H%M%S`
        echo "$d : $1" >> $BUSY_FILE
        if [[ "$PUSHOVERHANS" ]]
        then
	   my_message="$d on $MACHINE: $1"
	   curl -s --form-string "user=$PUSHOVERHANS" --form-string "message=$my_message" \
               --form-string "token=$PUSHOVERTOKEN" https://api.pushover.net/1/messages.json
        fi
}

# Function to run bash line depending on whether we test or not
run()
{
   if [[ "$TEST" ]]
   then 
      echo "$1"
      sleep 5
   else
      eval $1
   fi
}

# Check if an older ingest & process is still running
#if [[ -f $BUSY_FILE || -f $DONE_FILE  ]]
if [-f $BUSY_FILE]
then
#	signal file exists, blocking further processing 
        test -f $BUSY_FILE && echo "$BUSY_FILE exists"
#        test -f $DONE_FILE && echo "$DONE_FILE exists"
	exit -1
else
#	no signal blocking, continue with ingest and processing, but first set signalfile
	notify "starting ingest"
        run runASDAS.script
	notify "ingest completed"

#	start processing
        notify "Starting delta processing for CHIRPS"
	run "bash -x deltaProcessChirps.script Ethiopia 2>&1 | tee $CHIRPS_RELEASE_DIR/deltaProcessChirps.log"

#	Check whether new data has been generated
        for file in `eval "ls $CHIRPS_OUTPUT_FILES"`; do
           if [[ "$file" -nt "$CHIRPS_TIMESTAMP_FILE" ]]
           then
              echo "new $file"
              echo $file >> $NEW_CHIRPS_FILE
           fi
        done
        date >> $CHIRPS_TIMESTAMP_FILE

	notify "Ended delta processing for CHIRPS"

	notify "Starting delta processing for MODIS NDVI 500m"
	run "bash -x processModis.script NDVI 500m delta 2>&1 | tee $MODIS500_RELEASE_DIR/processModis_NDVI_500m_delta.log"
	notify "Ended delta processing for MODIS NDVI 500m"

	notify "Starting delta processing for MODIS EVI 500m"
	run "bash -x processModis.script EVI 500m delta  2>&1 | tee $MODIS500_RELEASE_DIR/processModis_EVI_500m_delta.log"
	notify "Ended delta processing for MODIS EVI 500m"

#	Check whether new data has been generated
        for file in `eval "ls $MODIS500_OUTPUT_FILES"`; do
           if [[ "$file" -nt "$MODIS500_TIMESTAMP_FILE" ]]
           then
              echo "new $file"
              echo $file >> $NEW_MODIS500_FILE
           fi
        done
        date >> $MODIS500_TIMESTAMP_FILE

#
#	Processsing completed
	notify "Processing completed"
#
#	Remove busy flag
#
	mv $BUSY_FILE $DONE_FILE
fi  
