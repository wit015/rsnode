#!/usr/bin/env python
################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2018, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

import sys
import fieldDB
import processS2

if len(sys.argv)<2: 
    print "Usage : %s <id-list> [always]" % sys.argv[0]
    exit()

field_list = []
id_list = eval(sys.argv[1])
always=len(sys.argv)>2 and sys.argv[2]=='always'

for user_id in fieldDB.getUserIDs():
    if len(id_list)>0 and not user_id in id_list: continue
    fl = fieldDB.Field.getDefinedFields(user_id)
    field_list.extend(fl)

processS2.updateFieldsHistoryByGranule(field_list, always)
