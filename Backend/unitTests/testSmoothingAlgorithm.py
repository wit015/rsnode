#!/usr/bin/env python 

################################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2014-2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
################################################################################

SVN_REVISION = '$Rev: 6832 $'
SVN_DATE = '$Date: 2016-06-01 12:18:23 +0200 (Wed, 01 Jun 2016) $'
SVN_AUTHOR = '$Author: jb76278 $'

##################################################################################
# Unit test for the smoothing algirithm
# File takes a CSV file as input and will smooth the data therein.
# -3000 is the 'no data value'.
# Output is a CSV file with prefix 'output_'. It contains the input array,
# intermediate results and the endresult.
##################################################################################

import sys
import numpy as np
import os.path

# The following import(s) need correct setting of the PYTHONPATH environment variable. For example in .bashrc:
# export PYTHONPATH="${PYTHONPATH}:`pwd`/SvnCheckout/CommonSense/RSDS/DataProcessing/Python/code:`pwd`/SvnCheckout/CommonSense/RSDS/DataProcessing/Python/pyLibs"
import smoothTimeSeriesAlgorithm_v1

######################################################################################################
# Check and process the input parameters
######################################################################################################
def Main(argv):
    if len(sys.argv) < 2 or len(sys.argv) > 2:
        print ""
        print "Usage:"
        print "python testSmoothingAlgorithm.py <input-file-name>"
        print ""
        print "The input file must be a Comma Separated Values file!"
        print ""
        print "Example:"
        print "python testSmoothingAlgorithm.py  ./testInput/smoothingAlgUnitTest.csv"
        print ""
    else:
        inputFileName = ''
        argIndex = 1

        if argIndex == (len(sys.argv) - 1):
            inputFileName = sys.argv[argIndex]
        else:
            sys.exit("Error: Number of arguments is wrong.")

        # Set the tiles indices and loop through them calling the smoothTimeSeries function
        if (os.path.isfile(inputFileName)):
            inputFile = open(inputFileName, 'r')
            outputFile = open(os.path.join(os.path.dirname(inputFileName), "output_" + os.path.basename(inputFileName)), 'w')
            # Loop through all the lines
            for line in inputFile:
                cleanLine = line.strip().strip(',')
                if cleanLine.startswith('#') or len(cleanLine)==0:
                    outputFile.write(cleanLine + '\n')
                    print cleanLine
                else:
                    inputArray = None
                    outputArray = None

                    # Convert into an array of floats
                    inputArray = np.fromstring(cleanLine, dtype=float, sep=',')

                    # Call the smoothing algorithm (with flag to record outputs)
                    smoothTimeSeriesAlgorithm_v1.Smooth(inputArray, -3000., 0, outputFile)
            print "Output written to file: " + str(os.path.join(os.path.dirname(inputFileName), "output_" + os.path.basename(inputFileName)))
        else:
            print "ERROR: Input file (" + str(inputFileName) + ") does not exist!"
        

######################################################################################################
# Only execute when run as a script
######################################################################################################
if __name__ == "__main__":
    Main(sys.argv[:])

