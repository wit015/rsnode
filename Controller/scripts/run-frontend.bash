#!/bin/bash

###############################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
###############################################################################
# $Rev: 6800 $
# $Date: 2016-05-30 16:34:22 +0200 (Mon, 30 May 2016) $
# $Author: hw23316 $
###############################################################################
#
# This script starts a frontend machine, and attaches disks with latest results
# to it.
# The names of the new disks with released data are recorded in the releaseset
#
# Arguments: 
#
#   - id of the project. E.g. '-cs' is the id for the CommonSense
#     project. This argument ensures different projects don't mix their disks.
#
# Resources (local):
#
#   - in direcory CONFIGDIR:
#       -   frontend.txt contains name of the frontend host
#       -   chirps-disk.txt contains the name of the CHIRPS release disk 
#       -   modis500-disk.txt contains the name of the MODIS500 release disk 
#
SCRIPT=$( readlink -m $0 )
BASE_DIR=`dirname ${SCRIPT}`
source ${BASE_DIR}/gcloud_config.bash

### Constants ####################################################################
#
# Parameters
PROJECTID=$1

#
#
# Configurations
CONFIGDIR=${BASE_DIR}/../config${PROJECTID}
RELEASESUFFIX=${RELEASESUFFIX:-release}
echo "... Release suffix = $RELEASESUFFIX"
CHIRPSDEVICE=chirps-release
MODIS500DEVICE=modis500-release
#
# Cloud settings
CLOUDUSER=jane
MAIL_RECIPIENTS=${MAIL_RECIPIENTS:-rsdshelpdesk@airbusds.nl}

#
# Configuration files
#
#
#
### FUNCTIONS ####################################################################

detach-device()
{
    my_frontend=$1
    my_device=$2-$RELEASESUFFIX
    echo ".... detaching $my_device from $my_frontend"
    gcloud compute instances detach-disk $my_frontend --device-name=$my_device
}

get-disk-name()
{
    my_product=$1
    echo "... Get disk name for product = $my_product"
    current_name=`cat $CONFIGDIR/${my_product}-disk.txt`
    echo "... obtained name $current_name for $my_product in $PROJECTID"
}



### MAIN CODE #####################################################################
echo "Starting run-frontend ..."
#
# Check arguments
#
if  [ "$#" -ne "1" ] && [ "$#" -ne "2" ]
then
#   Argument missing
    echo "Error: Incorrect number of arguments"
    echo "Usage: run-frontend PROJECT_ID"
    exit    
else
    FRONTEND=`cat $CONFIGDIR/frontend.txt`
    FRONTENDS2=frontend-s2
    if [[ -z "$FRONTEND" ]]; then
       echo "ERROR no frontend for $CONFIGDIR"
       exit -1
    fi

    echo "... Frontend is $FRONTEND ..."
fi

    echo "... Shuting down frontend $FRONTEND ..."
    echo gcloud compute ssh $CLOUDUSER@$FRONTEND --command \"sudo shutdown -h now\"  
    gcloud compute ssh $CLOUDUSER@$FRONTEND --command "sudo shutdown -h now"  
    echo gcloud compute instances stop $FRONTEND
    gcloud compute instances stop $FRONTEND

#--- Detach devices --------------------------------------------------------------
    detach-device $FRONTEND chirps
    detach-device $FRONTEND modis500

#--- Obtain released disk names
    get-disk-name chirps
    chirps_disk=$current_name
    echo "... CHIRPS disk is $chirps_disk"
    get-disk-name modis500
    modis500_disk=$current_name
    echo "... MODIS500 disk is $modis500_disk"
     

#--- Attach disks
    echo "... attaching CHIRPS disk $chirps_disk to device $CHIRPSDEVICE on $FRONTEND"
    gcloud compute instances attach-disk $FRONTEND --disk $chirps_disk --device-name $CHIRPSDEVICE
    echo "... attaching MODIS500 disk $modis500_disk to device $MODIS500DEVICE on $FRONTEND"
    gcloud compute instances attach-disk $FRONTEND --disk $modis500_disk --device-name $MODIS500DEVICE

#--- Start frontend
    echo "... starting frontend $FRONTEND..."
    gcloud compute instances start $FRONTEND
    gcloud compute instances start $FRONTENDS2
    
    gcloud compute instances list > /tmp/instances$PROJECTID-status.txt
    echo "-----------------------------" >> /tmp/instances$PROJECTID-status.txt
    echo "$FRONTEND :" >> /tmp/instances$PROJECTID-status.txt
    gcloud compute instances describe $FRONTEND >> /tmp/instances$PROJECTID-status.txt

    #mail -s "Started frontend $FRONTEND" $MAIL_RECIPIENTS < /tmp/instances$PROJECTID-status.txt    
