#!/bin/bash
#
# stop-frontend.bash
#
# 2016-06-06 16:10:36 by Hans de Wolf
#
# This script stops the frontend machine in the google cloud, 
# and detaches all known dissk as identified by the files in the 
# /home/sk75362/CommonSense/frontend-config directory.
#
# Note: frontend must be running in order to execute correctly.
# This is typically run after release-product.bash, 
# and must run before config-frontend.bash
#
#
### Prologue ####################################################################
#
SCRIPT=$( readlink -m $0 )
BASE_DIR=`dirname ${SCRIPT}`
source ${BASE_DIR}/gcloud_config.bash

# Parameters
PROJECTID=$1

# DEFINE CONSTANTS
#
CLOUDUSER=${CLOUDUSER:-jane}
echo ... user is $CLOUDUSER
MACHINE=`uname -n`
echo ... executing on $MACHINE
MAIL_RECIPIENTS=${MAIL_RECIPIENTS:-rsdshelpdesk@airbusds.nl}
CONFIGDIR=${BASE_DIR}/../config${PROJECTID}

# Notification variables
#
PUSHOVERUSER=""
PUSHOVERTOKEN=""
#
#
# functions
#
notify()
{
#        echo ... notifiying ...
         my_message=`echo \`date +%Y%m%d-%H%M%S\` on $MACHINE: $1`
         echo ... message is: $my_message
         if [[ -n "$PUSHOVERUSER" ]]; then
            curl -s --form-string "user=$PUSHOVERUSER" \
                    --form-string "message=$my_message" \
                    --form-string "token=$PUSHOVERTOKEN" \
                    https://api.pushover.net/1/messages.json
         fi
}
#--- MAIN -------------------------------------------------------------------------
notify "$MACHINE is executing stop-frontend.bash"
#
# Check arguments
if  [ "$#" -ne "1" ] && [ "$#" -ne "2" ]
then
#   Argument missing
    echo "Error: Incorrect number of arguments"
    echo "Usage: stop-frontend [PROJECT_ID]"
    exit    
else
#
    FRONTEND=`cat $CONFIGDIR/frontend.txt`
    FRONTENDS2=frontend-s2
    if [[ -z "$FRONTEND" ]]; then
       echo "ERROR no frontend found for $PROJECTID"
       exit -1
    fi

    status_file=/tmp/instances${PROJECTID}-status.txt

#   Get client statistics
    gcloud compute ssh $CLOUDUSER@$FRONTEND --command "scripts/client_summary.bash -u /home/jane/logs/services.csv" \
        > $status_file

#   Stop frontend nicely
    gcloud compute ssh $CLOUDUSER@$FRONTEND --command "sudo shutdown -h now"  
#
    notify "Stopping frontend $FRONTEND"
    gcloud compute instances stop $FRONTEND
    gcloud compute instances stop $FRONTENDS2
    notify "Frontend $FRONTEND has been stopped."
#
    gcloud compute instances list >> $status_file
    
    mail -s "Stopped frontend $FRONTEND" $MAIL_RECIPIENTS < $status_file

    notify "ENDING stop-frontend.bash"
fi
