#!/bin/bash
#
# run-healthcheck.bash
#
# Arguments: 
#
#   - IP address and port (E.g 104.155.19.61:8080)

# Arguments
CSSERVER=$1

#
# Check arguments
if  [ "$#" -ne "1" ]
then
#   Argument missing
    echo "Error: Incorrect number of arguments"
    echo "Usage: run-healthcheck IP-ADDRESS:PORTNUM"
    echo "Example: run-healthcheck 104.155.19.61:8080"
    exit    
else
    # DEFINE CONSTANTS
    #
    CLOUDUSER=${CLOUDUSER:-jane}
    echo ... user is $CLOUDUSER
    MACHINE=`uname -n`
    echo ... executing on $MACHINE
    echo ... CSSERVER = $CSSERVER
    MAIL_RECIPIENTS=${MAIL_RECIPIENTS:-rsdshelpdesk@airbusds.nl}

    # Notification variables
    #
    PUSHOVERUSER=""
    PUSHOVERTOKEN=""
    #
    #
    # functions
    #
    notify()
    {
        #        echo ... notifiying ...
        my_message=`echo \`date +%Y%m%d-%H%M%S\` on $MACHINE: $1`
        echo ... message is: $my_message
        if [[ -n "$PUSHOVERUSER" ]]; then
            curl -s --form-string "user=$PUSHOVERUSER" \
                    --form-string "message=$my_message" \
                    --form-string "token=$PUSHOVERTOKEN" \
                    https://api.pushover.net/1/messages.json
        fi
    }
    RESULTDIR=/tmp/health-$CSSERVER
    mkdir -p $RESULTDIR
    #--- MAIN -------------------------------------------------------------------------
    # Precipitation prelim
    curl -g "http://$CSSERVER/geoserver/ows?service=WPS&version=1.0.0&RawDataOutput=result=@mimetype=application/json&request=Execute&identifier=Precipitation:GetDates&Datainputs=source=CHIRPS_5000m;indicator=PRECIP_PRELIM" >${RESULTDIR}/prec-prelim.txt
    # Precipitation final
    curl -g "http://$CSSERVER/geoserver/ows?service=WPS&version=1.0.0&RawDataOutput=result=@mimetype=application/json&request=Execute&identifier=Precipitation:GetDates&Datainputs=source=CHIRPS_5000m;indicator=PRECIP_FINAL">${RESULTDIR}/prec-final.txt
    # Precipitation lta
    curl -g "http://$CSSERVER/geoserver/ows?service=WPS&version=1.0.0&RawDataOutput=result=@mimetype=application/json&request=Execute&identifier=Precipitation:GetDates&Datainputs=source=CHIRPS_5000m;indicator=PRECIP_LTA" >${RESULTDIR}/prec-lta.txt
    # VegetationIndex NDVI
    curl -g "http://$CSSERVER/geoserver/ows?service=WPS&version=1.0.0&RawDataOutput=result=@mimetype=application/json&request=Execute&identifier=VegetationIndex:GetDates&Datainputs=source=MODIS_500m;indicator=NDVI" >${RESULTDIR}/ndvi.txt
    # VegetationIndex EVI
    curl -g "http://$CSSERVER/geoserver/ows?service=WPS&version=1.0.0&RawDataOutput=result=@mimetype=application/json&request=Execute&identifier=VegetationIndex:GetDates&Datainputs=source=MODIS_500m;indicator=EVI" >${RESULTDIR}/evi.txt
    # VegetationIndex NDVI LTA
    curl -g "http://$CSSERVER/geoserver/ows?service=WPS&version=1.0.0&RawDataOutput=result=@mimetype=application/json&request=Execute&identifier=VegetationIndex:GetDates&Datainputs=source=MODIS_500m;indicator=NDVI_LTA" >${RESULTDIR}/ndvi-lta.txt
    # VegetationIndex EVI LTA
    curl -g "http://$CSSERVER/geoserver/ows?service=WPS&version=1.0.0&RawDataOutput=result=@mimetype=application/json&request=Execute&identifier=VegetationIndex:GetDates&Datainputs=source=MODIS_500m;indicator=EVI_LTA" >${RESULTDIR}/evi-lta.txt

    ls -l ${RESULTDIR}/* > ${RESULTDIR}/status.txt

    mail -s "Healthcheck status $CSSERVER" \
       -a ${RESULTDIR}/evi-lta.txt \
       -a ${RESULTDIR}/ndvi-lta.txt \
       -a ${RESULTDIR}/prec-final.txt \
       -a ${RESULTDIR}/prec-prelim.txt \
       -a ${RESULTDIR}/evi.txt \
       -a ${RESULTDIR}/ndvi.txt \
       -a ${RESULTDIR}/prec-lta.txt \
       $MAIL_RECIPIENTS < ${RESULTDIR}/status.txt

    # clean-up 
    rm -rf ${RESULTDIR}

    #
    notify "ENDING run-healthcheck.bash"
fi
