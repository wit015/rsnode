#!/bin/bash

###############################################################################
# COPYRIGHT NOTICE (THIS NOTICE NOT TO BE REMOVED)
# This file, or parts of it, or modified versions of it, may not be copied,
# reproduced or transmitted in any form, including reprinting, translation,
# photocopying or microfilming, or by any means, electronic, mechanical or
# otherwise, or stored in a retrieval system, or used for any purpose, without
# the prior written permission of all Owners.
# (c) Copyright 2016, Airbus Defense and Space Netherlands, Leiden
# All rights, including copyrights, reserved. This file contains or may contain
# restricted information and is UNPUBLISHED PROPRIETARY SOURCE CODE OF THE
# Owners. This copyright Notice(s) above do not evidence any actual or intended
# publication of such source code.
###############################################################################
# $Rev: 6800 $
# $Date: 2016-05-30 16:34:22 +0200 (Mon, 30 May 2016) $
# $Author: hw23316 $
###############################################################################
#
# This script starts a backend machine, and releases disks with new results
# to the frontend (if applicable) and shuts it down. If processing is not done
# within the expected time, no shutdown is done. The caller is responsible for
# re-running this script later.
# The names of the new disks with released data are recorded in the release set
#
# The signals indicating that new data are have been processed, and that processing has
# completed are cleared
#
# Arguments: 
#
#   - name of the backend
#   - id of the project (optional). E.g. '-cs' is the id for the CommonSense
#     project. This argument ensures different projects don't mix their disks.
#
# Resources (local):
#
#   - in direcory CONFIGDIR:
#       -   chirps-disk.txt contains the name of the CHIRPS release disk 
#       -   modis000-disk.txt contains the name of the MODIS500 release disk 
#
### Prologue ####################################################################
#
SCRIPT=$( readlink -m $0 )
BASE_DIR=`dirname ${SCRIPT}`
source ${BASE_DIR}/gcloud_config.bash

### Constants ####################################################################
#
# Parameters
BACKEND=$1
PROJECTID=$2

#
# Time to allow ingest & process to complete
WAIT_TIME_MINUTES=120

#
# Configurations
CONFIGDIR=${BASE_DIR}/../config${PROJECTID}
RELEASESUFFIX=${RELEASESUFFIX:-release}
echo "... Release suffix = $RELEASESUFFIX"
MAIL_RECIPIENTS=${MAIL_RECIPIENTS:-rsdshelpdesk@airbusds.nl}

#
# Cloud settings
CLOUDUSER=jane

#
# Signals
#
SIGNALDIR=${BASE_DIR}/../signals
LOCALSIGNALDIR=${LOCALSIGNALDIR:-$SIGNALDIR}$PROJECTID
REMOTESIGNALDIR=${REMOTESIGNALDIR:-/home/jane/signals}
BACKENDDONESIGNAL=ingest_and_process-done.txt
NEWCHIRPSSIGNAL=new-chirps.txt
NEWMODIS500SIGNAL=new-modis500.txt

#
# Configuration files
#
#
# Ingest & process command (to be executed on the gcloud backend machine)
# Include TEST indication to skip long duration activies
SSHCOMMAND='(cd Backend/scripts; source setup.bash '$PROJECTID'; bash ./ingest_and_process.bash '$PROJECTID') > /tmp/t & '
# SSHCOMMAND='(cd Backend/scripts; source setup.bash; export TEST=true;  bash ./ingest_and_process.bash) > /tmp/t & '
#
#
### FUNCTIONS ####################################################################

function getsignal()
{
#   obtain the status from backend machine given in $1
#	for signal file given in $2 
#
#   It returns a value 1 if the signal is set,
#   or a 0 when the signal is not set
#   The returnvalue is returned in $?

    local my_backend=$1
    local my_signal_file=$2
    local my_return_value=0
    
#   Make sure LOCALSIGNALDIR exists
    mkdir -p $LOCALSIGNALDIR
#
#   First clean up locally
#    
    rm -f $LOCALSIGNALDIR/$my_signal_file
#   obtain the signal signal from the cloud

    gcloud compute scp $CLOUDUSER@$my_backend:$REMOTESIGNALDIR/$my_signal_file $LOCALSIGNALDIR/$my_signal_file

#   now check the existence of the file
    echo ... Checking $LOCALSIGNALDIR/$my_signal_file ...
    if [ -f $LOCALSIGNALDIR/$my_signal_file ] 
    then
#	we have a signal file
    	echo "... Found signal file for "$my_signal_file
    	my_return_value=1

    else
#	we did not get a signal file
    	echo "... Found no signal file for "$my_signal_file
    	my_return_value=0	
    fi
    echo "return value = $my_return_value"
    return $my_return_value
}

function clear-signal ()
{
    local my_backend=$1 
    local my_signalfile=$2

    echo "... clearing signal $my_signalfile on $my_backend ..."
    gcloud compute ssh $CLOUDUSER@$my_backend --command "rm $REMOTESIGNALDIR/$my_signalfile"
    echo "Signal $my_signalfile cleared."
}



function process-disk()
{
#   process a disk, release a new disk to the frontend if new data are available
#   This is done by creating a snapshot of the disk, and creating a persistent disk
#   from the snapshot.
#
#   The name of the current disk is obtained from the backend 
#   Arguments:  $1 is the name of the backend
#               $2 is the data type, chirps of modis500
#               $3 is the release set
#               $4 is the signal file name
    local my_backend=$1
    local my_product=$2
    local my_signalfile=$3

#   Check if any new data are available
    echo ... checking if new data are available on $my_backend for $my_signalfile
    test -f $LOCALSIGNALDIR/$my_signalfile
    return_value=$?
    echo "Process disk: $return_value"
    if [[ $return_value -eq 1 ]]
    then
#       No new data avaialble, do nothing
        echo "... no new data available from $my_backend from $my_signalfile"
    else
#       New data are available   
        echo "... new data are available from $my_backend from $my_signalfile"
#
#       Obtain the name of the disk, but first determine the configuration file
        echo "... obtaining disk from device $my_product at $my_backend"
        deviceName=new-${my_product}-${RELEASESUFFIX}
        my_sourcedisk=`gcloud compute instances describe $my_backend | \
                egrep -i 'deviceName|source' | \
                paste - - | \
                grep "deviceName: $deviceName" | \
                awk '{print $4}'`
        my_sourcedisk=`basename $my_sourcedisk`
        echo "... source disk for snapshot is $my_sourcedisk ..."
#
#       Generate timestamped container name
	local timestamp=`date +%Y%m%d-%H%M%S`
	echo "... timestamp is $timestamp ..."
	local my_container=$my_product-${RELEASESUFFIX}${PROJECTID}-$timestamp
        echo "... will use container name $my_container ...|"
#
#       Create snapshot
        echo "... Creating snapshot $my_container from disk $my_sourcedisk"
        gcloud compute disks snapshot $my_sourcedisk --snapshot-names $my_container
#
#       Create a persistent disk from this snapshot
        echo ... Creating persistent disk $my_disk from snapshot $my_disk
        gcloud compute disks create $my_container --source-snapshot $my_container --type=pd-standard

#       Store the name of the persistent disk in the releaseset
        local my_set_config=${my_product}-disk.txt
        echo "... Recording disk name $my_container in $CONFIGDIR/$my_set_config ..." 
        mkdir -p $CONFIGDIR
        echo "$my_container" > $CONFIGDIR/$my_set_config

        (gcloud compute disks describe $my_container; echo ""; \
         gcloud compute disks list; echo ""; \
         gcloud compute snapshots list) | \
           mail -s "New disk $my_container at $my_backend" $MAIL_RECIPIENTS
    fi
}

### MAIN CODE #####################################################################
echo "Starting"
#
# Check arguments
#
if  [ "$#" -ne "2" ] && [ "$#" -ne "3" ]
then
#   Argument missing
    echo "Error: Incorrect number of arguments"
    echo "Usage: run-backend BACKEND [PROJECT_ID]"
    exit    
else
    echo "... Backend is $BACKEND ..."

#--- START BACKEND ------------------------------------------------------------------

    echo ... Starting backend $BACKEND ...
    gcloud compute instances start $BACKEND
    
    gcloud compute instances list > /tmp/instances$3-status.txt
    #mail -s "Starting backend $BACKEND" $MAIL_RECIPIENTS </tmp/instances$3-status.txt

#   Wait some time to allow ssh daemon to start
    sleep 120

#--- Launch Ingest and process script on backend -----------------------------------
    gcloud compute ssh $CLOUDUSER@$BACKEND --command "$SSHCOMMAND" --ssh-flag="-n"

#--- Wait a while to see if something needs to be done ----------------------------------
    echo "... Polling for $WAIT_TIME_MINUTES minutes"
    waitTimeCounter=WAIT_TIME_MINUTES
    # getsignal returns 1 if signal set, which means return value <> 0, which evaluates to false!
    while getsignal $BACKEND $BACKENDDONESIGNAL && [[ $waitTimeCounter -ne 0 ]];
    do
       echo "waiting for $BACKEND $BACKENDDONESIGNAL ($waitTimeCounter)"
       sleep 60
       let waitTimeCounter-=1
    done

#--- Check if Ingest and process is done ... ---------------------------------------
    getsignal $BACKEND $BACKENDDONESIGNAL
    returned_value=$?
    echo $returned_value

    if [[ $returned_value -eq 0 ]]
    then
#---    Ingest and processing has not completed. Just exit, and keep backend running
        echo "... Ingest and processing has not completed - exiting ..."
    else
#---    Signal indicates that ingest and processing has completed
        echo ... Ingest and processing has completed - releasing disks

#---    Get signal files for new disks
        getsignal $BACKEND $NEWCHIRPSSIGNAL
        clear-signal $BACKEND $NEWCHIRPSSIGNAL
        getsignal $BACKEND $NEWMODIS500SIGNAL
        clear-signal $BACKEND $NEWMODIS500SIGNAL

#---    Clear processing done signal
        clear-signal $BACKEND $BACKENDDONESIGNAL

#---    Shutdown backend to make sure disk data is flushed
        gcloud compute instances stop $BACKEND

#---    Release disk for CHIRPS and (only if) new data are available, update release set
        process-disk $BACKEND chirps $NEWCHIRPSSIGNAL

#---    Release disk for MODIS500 if new data are available
        process-disk $BACKEND modis500 $NEWMODIS500SIGNAL

        gcloud compute instances list > /tmp/instances$3-status.txt
        #cat /tmp/instances$3-status.txt $LOCALSIGNALDIR/$BACKENDDONESIGNAL | \
        #   mail -s "Stopping backend $BACKEND" $MAIL_RECIPIENTS
        
    fi

fi
